import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProstheticRoutingModule } from './prosthetic-tools-routing.module';
import { ProstheticToolsComponent } from './prosthetic-tools.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { SharedModule } from '../../../shared/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxTranslateModule } from '../../../core/translate/translate.module';
import { NgxMaskModule } from 'ngx-mask';
import { InputMaskModule } from 'racoon-mask-raw';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule } from '../../../core/directive/directives.module';
import { PipesModule } from '../../../core/pipes/pipes.module';
import { FormServiceComponent } from './form-service/form-service.component';
import { CONSTANTS } from '../../../core/constants/constants';
import { CurrencyMaskConfig, NgxCurrencyModule } from 'ngx-currency';
import { CURRENCY_MASK_CONFIG } from 'ngx-currency/src/currency-mask.config';
import { ConfirmComponent } from './confirm/confirm.component';
import { AccountBillToPayModule } from '../../financial/bills-to-pay/account/account.module';

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: 'right',
  allowNegative: true,
  allowZero: true,
  decimal: CONSTANTS.COMMONS_VALUES.DECIMAL,
  precision: 2,
  prefix: CONSTANTS.COMMONS_VALUES.CURRENCY_SYMBOL + ' ',
  suffix: '',
  thousands: CONSTANTS.COMMONS_VALUES.THOUSANDS,
  nullable: false
};

@NgModule({
  declarations: [ProstheticToolsComponent, FormServiceComponent, ConfirmComponent],
  imports: [
    CommonModule,
    NgxDatatableModule,
    SimpleNotificationsModule,
    SharedModule,
    NgSelectModule,
    NgxTranslateModule,
    NgxCurrencyModule,
    NgxMaskModule.forRoot(),
    NgxDatatableModule,
    InputMaskModule,
    FormsModule,
    ReactiveFormsModule,
    DirectivesModule,
    PipesModule,
    AccountBillToPayModule,
    ProstheticRoutingModule
  ],
  providers: [
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig }
  ]
})
export class ProstheticToolsModule { }
