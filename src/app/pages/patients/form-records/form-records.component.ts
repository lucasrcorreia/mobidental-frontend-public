import { Component, Input, OnInit, ViewChildren } from '@angular/core';
import { UtilsService } from '../../../services/utils/utils.service';
import { ShortListItemDesc } from '../../../core/models/forms/common/common.model';
import { CONSTANTS } from '../../../core/constants/constants';
import { ContentPatientServicesTableModel } from '../../../core/models/patients/procedures/procedures.model';
import { LISTAS } from '../lists';
import * as d3 from 'd3-collection';
import * as moment from 'moment';
import { takeWhile } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { LOCAL_CONSTS } from '../constants';
import { classToPlain } from 'class-transformer';
import { D3CollectionServicesInterface } from '../../../core/models/table/columns.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DetachedTreatmentComponent } from './detached-treatment/detached-treatment.component';
import { DetachedTreatment } from './detached-treatment/model/detached-treatment';
import { GlobalSpinnerService } from "../../../lib/global-spinner.service";
import { NotificationsService } from "angular2-notifications";
import { PdfGeneratorService, PDFMake } from "../../../services/pdf-generator/pdf-generator.service";
import { CommonsReportBuildService, REPORT_STYLES } from "../../../services/report/commons-report-build.service";
import { PatientModel } from "../../../core/models/patients/patients.model";

const lastOfPage = (index: number): boolean => {
	return (index + 1) % 13 === 0;
}

@Component({
	selector: 'app-form-records',
	templateUrl: './form-records.component.html',
	styleUrls: ['./form-records.component.scss'],
})
export class FormRecordsComponent implements OnInit {
	@ViewChildren('tableRecords') tableRecords: any;

	@Input() patient: PatientModel;
	recordsDropdown: ShortListItemDesc[] = [];
	treatmentId = 0;
	proceduresTable: ContentPatientServicesTableModel[] = [];
	LISTAS_LOCAIS = new LISTAS(this.service.translate);
	locale = this.service.translate.instant('COMMON.LOCALE');
	showModalEditRecord = false;
	selectedProcedure: ContentPatientServicesTableModel;
	treatments: D3CollectionServicesInterface[] = [];
	activeDetail = {
		row: -1,
		table: -1,
	};

	tableProcedures = [
		{
			action: true,
			name: this.service.translate.instant(
					'PATIENTS.FORM_RECORDS.TABLE_PROCEDURES.COL1',
			),
			prop: ' id',
			sortable: false,
			width: 150,
		},
		{
			action: false,
			name: this.service.translate.instant(
					'PATIENTS.FORM_RECORDS.TABLE_PROCEDURES.COL2',
			),
			prop: 'convenioEspecialidadeProcedimentoProcedimentoNome',
			sortable: true,
			width: 300,
		},
		{
			action: false,
			name: this.service.translate.instant(
					'PATIENTS.FORM_RECORDS.TABLE_PROCEDURES.COL3',
			),
			prop: 'emDenteDescricao',
			sortable: true,
			width: 100,
		},
		{
			action: false,
			name: this.service.translate.instant(
					'PATIENTS.FORM_RECORDS.TABLE_PROCEDURES.COL4',
			),
			prop: 'profissionalNome',
			sortable: true,
			width: 150,
		},
		{
			action: false,
			name: this.service.translate.instant(
					'PATIENTS.FORM_RECORDS.TABLE_PROCEDURES.COL5',
			),
			prop: 'valor',
			sortable: true,
			width: 100,
			currency: true,
		},
		{
			action: false,
			name: this.service.translate.instant(
					'PATIENTS.FORM_RECORDS.TABLE_PROCEDURES.COL6',
			),
			prop: 'statusTratamentoServicoDescricao',
			sortable: true,
			width: 120,
		},
		{
			action: false,
			name: this.service.translate.instant(
					'PATIENTS.FORM_RECORDS.TABLE_PROCEDURES.COL7',
			),
			prop: 'dataFinalizacao',
			sortable: true,
			width: 120,
		},
	];

	readonly listFace = [
		{
			label: this.service.translate.instant(
					'PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceDistal',
			),
			porp: 'isFaceDistal',
		},
		{
			label: this.service.translate.instant(
					'PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceOclusalIncisal',
			),
			porp: 'isFaceOclusalIncisal',
		},
		{
			label: this.service.translate.instant(
					'PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceLingualPalatina',
			),
			porp: 'isFaceLingualPalatina',
		},
		{
			label: this.service.translate.instant(
					'PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceMesial',
			),
			porp: 'isFaceMesial',
		},
		{
			label: this.service.translate.instant(
					'PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceVestibular',
			),
			porp: 'isFaceVestibular',
		},
	];

	constructor(public service: UtilsService,
				private readonly _modalService: NgbModal,
				private readonly _globalSpinnerService: GlobalSpinnerService,
				private readonly _notificationsService: NotificationsService,
				private readonly _pdfGeneratorService: PdfGeneratorService,
				private readonly _commonsReportService: CommonsReportBuildService) {}

	ngOnInit() {

		this.getRecordsDropdown();
	}

	getRecordsDropdown() {
		this.service.loading(true);
		const ref = CONSTANTS.ENDPOINTS.patients.records.dropdown.replace(
				'#id',
				this.patient.id.toString(),
		);
		this.service.httpGET(ref).subscribe(
				data => {
					const all: ShortListItemDesc = {
						id: 0,
						descricao: this.service.translate.instant('COMMON.ALL'),
					};

					this.recordsDropdown = [all, ...(data.body as ShortListItemDesc[])];
					this.service.loading(false);

					this.getRecordsData();
				},
				err => {

					this.service.loading(false);
					this.service.notification.error(
							this.service.translate.instant('COMMON.ERROR.SEARCH'),
							err.error.error,
					);
				},
		);
	}

	getColorStatusTratamentoServicoDescricao(value: string) {
		return this.LISTAS_LOCAIS.listaStatusTratamentoServico.find(
				item => item.value === value,
		).color;
	}

	getRecordsData() {
		this.service.loading(true);
		const ref =
				CONSTANTS.ENDPOINTS.patients.records.changeRecord.replace(
						'#treatmentId',
						this.treatmentId.toString(),
				) + this.patient.id;

		this.service.httpGET(ref).subscribe(
				data => {
					this.proceduresTable = data.body as ContentPatientServicesTableModel[];
					this.service.loading(false);

					this.processRows();

				},
				err => {

					this.service.loading(false);
					this.service.notification.error(
							this.service.translate.instant('COMMON.ERROR.SEARCH'),
							err.error.error,
					);
				},
		);
	}

	processRows() {
		this.proceduresTable.sort((a, b) => {
			if (moment(a.data, 'DD/MM/YYYY').isBefore(moment(b.data, 'DD/MM/YYYY'))) {
				return 1;
			} else if (
					moment(a.data, 'DD/MM/YYYY').isAfter(moment(b.data, 'DD/MM/YYYY'))
			) {
				return -1;
			} else {
				return 0;
			}
		});

		this.treatments = d3
				.nest()
				.key((d: { tratamentoId: number }) => d.tratamentoId)
				.entries(this.proceduresTable);
	}

	editProcedure(row: ContentPatientServicesTableModel) {
		this.selectedProcedure = classToPlain(
				row,
		) as ContentPatientServicesTableModel;
		this.showModalEditRecord = true;
		this.service.modalResult.next(false);

		if (this.selectedProcedure.dataFinalizacao) {
			this.selectedProcedure.dataFinalizacao = this.service.parseDatePicker(
					this.selectedProcedure.dataFinalizacao,
					'DD/MM/YYYY',
			);
		}

		setTimeout(() => {
			this.service.modalResult
					.pipe(takeWhile(val => val !== null))
					.subscribe(data => {
						if (data) {
							const indexRow = this.proceduresTable.findIndex(
									item => item.id === data.id,
							);
							this.proceduresTable[indexRow] = data;
							this.getRecordsData();
							this.showModalEditRecord = false;
							this.service.modalResult.next(null);
						}
					});
			document.querySelector('#modal-edit-record').classList.add('md-show');
		}, 500);
	}

	finishProcedure(row: ContentPatientServicesTableModel) {
		const refSWAL = 'PATIENTS.FORM_RECORDS.SWAL';
		const translate = this.service.translate;

		Swal.fire({
			title: translate.instant(`${refSWAL}.TITLE`),
			text: translate.instant(`${refSWAL}.TEXT`),
			type: 'question',
			showCancelButton: true,
			confirmButtonText: translate.instant(`${refSWAL}.CONFIRM_BUTTON`),
			cancelButtonText: translate.instant(`${refSWAL}.CANCEL_BUTTON`),
		}).then(result => {
			if (result.value) {
				this.service.loading(true);

				row.dataFinalizacao = moment().format('DD/MM/YYYY');
				row.statusTratamentoServico = LOCAL_CONSTS.FINALIZADO;
				row.statusTratamentoServicoDescricao = this.LISTAS_LOCAIS.listaStatusTratamentoServico.find(
						item => item.value === LOCAL_CONSTS.FINALIZADO,
				).label;

				this.service
						.httpPOST(CONSTANTS.ENDPOINTS.patients.records.saveProcedure, row)
						.subscribe(
								data => {
									this.service.loading(false);
									this.service.notification.success('Tratamento finalizado com sucesso');
								},
								err => {

									this.service.loading(false);
									this.service.notification.error(
											this.service.translate.instant('COMMON.ERROR.SEARCH'),
											err.error.error,
									);
								},
						);
			}
		});
	}

	async deleteProcedure(id: number) {
		const { value } = await Swal.fire({
			title: 'Confirmação de exclusão',
			text: 'Tem certeza que quer apagar este Tratamento?',
			type: 'question',
			showCancelButton: true,
			confirmButtonText: 'Sim. Apague',
			cancelButtonText: 'Cancelar',
		});

		if (!value) {
			return;
		}

		this.service.loading(true);
		try {
			await this.service.httpDELETE(CONSTANTS.ENDPOINTS.patients.treatment.removeDetachedTreatment + id).toPromise();
			this.getRecordsDropdown();
			this.service.notification.success('Tratamento removido com sucesso');
		} catch (e) {
			this.service.notification.error('Ocorreu um erro ao remover o Tratamento');
		} finally {
			this.service.loading(false);
		}
	}

	async detachedTreatment() {
		const modalRef = this._modalService.open(DetachedTreatmentComponent, { size: 'lg', centered: true });
		modalRef.componentInstance.patientId = this.patient.id;
		const detachedTreatment: DetachedTreatment = await modalRef.result;

		if (detachedTreatment) {
			this.service.loading(true);
			try {
				await this.service.httpPOST(CONSTANTS.ENDPOINTS.patients.treatment.detachedTreatment, detachedTreatment).toPromise();
				this.getRecordsDropdown();
				this.service.notification.success('Tratamento criado com sucesso');
			} catch (e) {
				this.service.notification.error('Ocorreu um erro ao tentar criar o Tratamento');
			} finally {
				this.service.loading(false);
			}
		}
	}

	buildFacesDescription(procedure: ContentPatientServicesTableModel) {
		const { isFaceDistal, isFaceLingualPalatina, isFaceMesial, isFaceOclusalIncisal, isFaceVestibular } = procedure;
		if (isFaceDistal || isFaceLingualPalatina || isFaceMesial || isFaceOclusalIncisal || isFaceVestibular) {
			const faces = [
				{ label: 'isFaceDistal', value: isFaceDistal },
				{ label: 'isFaceLingualPalatina', value: isFaceLingualPalatina },
				{ label: 'isFaceMesial', value: isFaceMesial },
				{ label: 'isFaceOclusalIncisal', value: isFaceOclusalIncisal },
				{ label: 'isFaceVestibular', value: isFaceVestibular },
			];
			return ' (' + faces.filter(face => face.value)
					.map((face, index) => (index === 0 ? '' : ' ') +
							this.listFace.find(item => item.porp === face.label).label) + ')';
		}
		return '';
	}

	async print() {
		const loading = this._globalSpinnerService.loadingManager.start();
		try {
			const pdfMake = await this._buildReport();
			const pdfMakeDefinition = await this._pdfGeneratorService.exportPDFMakeAsPDF(pdfMake, '', this.patient.pessoaNome + ' - Relatório de Prontuário.pdf');
			pdfMakeDefinition.open();
		} catch (e) {
			this._notificationsService.error('Ocorreu um erro ao gerar o PDF');
		} finally {
			loading.finished();
		}
	}

	private async _buildReport(): Promise<PDFMake> {
		const pdfMake: PDFMake = { afterContent: [], pageMargins: [20, 20, 20, 20] };
		pdfMake.styles = REPORT_STYLES;
		pdfMake.styles.tableRow = { alignment: 'center', fontSize: 9 };
		pdfMake.afterContent.push(await this.buildReportHeader());
		pdfMake.afterContent.push(this._buildReportBody());

		return pdfMake;
	}

	private async buildReportHeader() {
		return [
			{ ...await this._commonsReportService.buildFinancialReportHeader('Prontuário do Paciente') },
			{
				text: [
					{ text: 'Paciente: ', bold: true },
					this.patient.pessoaNome,
				],
				fontSize: 10,
				margin: [0, 10, 0, 10],
			},
		]
	}

	private _buildReportBody() {
		return this.proceduresTable.map((service: ContentPatientServicesTableModel, index: number) => {
			return [
				{
					table: {
						widths: [70, 80, 70, 190, 100],
						body: [
							[
								{ text: 'Dente/Região', style: 'tableHeader' },
								{ text: 'Situação', style: 'tableHeader' },
								{ text: 'Finalizado em', style: 'tableHeader' },
								{ text: 'Procedimento', style: 'tableHeader' },
								{ text: 'Profissional', style: 'tableHeader' },
							],
							[
								{ text: service.emDenteDescricao || ' ', style: 'tableRow' },
								{ text: service.statusTratamentoServicoDescricao || ' ', style: 'tableRow' },
								{ text: (service.dataFinalizacao || ' ').split(' '), style: 'tableRow' },
								{ text: service.convenioEspecialidadeProcedimentoProcedimentoNome || ' ', style: 'tableRow' },
								{ text: service.profissionalNome || ' ', style: 'tableRow' }
							],
							[
								{
									colSpan: 5,
									text: [
										{ text: 'Observações: ', bold: true },
										service.observacao || ' ',
									], fontSize: 9
								},
							],
						],
					},
					margin: [0, 0, 0, 2],
					pageBreak: lastOfPage(index) ? 'after' : '',
				},
			]
		});
	}

}
