import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UtilsService } from '../../../services/utils/utils.service';
import { CONSTANTS } from '../../../core/constants/constants';
import { SelectItem, ShortListItem } from '../../../core/models/forms/common/common.model';
import { ColumnsTableModel } from '../../../core/models/table/columns.model';
import { ProcedureSpecialties } from '../../../core/models/patients/procedures/procedures.model';
import { ProceduresModel } from '../../../core/models/patients/patients.model';
import { toothsList } from '../tooths';
import { classToPlain } from 'class-transformer';
import Swal from 'sweetalert2';
import { isEmpty } from 'lodash-es';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
               selector: 'app-modal-procedure',
               templateUrl: './procedure-dialog.component.html',
               styleUrls: [
                   './procedure-dialog.component.scss',
                   './../../../../assets/icon/icofont/css/icofont.scss',
               ],
           })
export class ProcedureDialogComponent implements OnInit {

    @Input() convenioId: number;
    @Input() professionalId: number;
    @Input() tratamentoId: number;
    @Input() profissionais: ShortListItem[];
    @Output() emitSave = new EventEmitter();

    especialidadeId: number;
    specialties: ShortListItem[] = [];
    selectedSpecialties = [];
    procedures: ProcedureSpecialties[] = [];
    filteredProcedures: ProcedureSpecialties[] = [];
    selectedProcedures = [];
    tooths = toothsList;
    listaDentes = [];

    isFace = false;

    procedure: ProceduresModel = new ProceduresModel();

    listaStatusTratamentoServico: SelectItem[] = [
        {
            label: this.service.translate.instant(
                'PATIENTS.MODAL_PROCEDURE.STATUS_TREATMENT.REALIZAR',
            ),
            value: 'REALIZAR',
        },
        {
            label: this.service.translate.instant(
                'PATIENTS.MODAL_PROCEDURE.STATUS_TREATMENT.FINALIZADO',
            ),
            value: 'FINALIZADO',
        },
        {
            label: this.service.translate.instant(
                'PATIENTS.MODAL_PROCEDURE.STATUS_TREATMENT.INICIADO',
            ),
            value: 'INICIADO',
        },
        {
            label: this.service.translate.instant(
                'PATIENTS.MODAL_PROCEDURE.STATUS_TREATMENT.OBSERVADO',
            ),
            value: 'OBSERVADO',
        },
    ];

    listFace = [
        {
            label: this.service.translate.instant(
                'PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceDistal',
            ),
            porp: 'isFaceDistal',
        },
        {
            label: this.service.translate.instant(
                'PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceOclusalIncisal',
            ),
            porp: 'isFaceOclusalIncisal',
        },
        {
            label: this.service.translate.instant(
                'PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceLingualPalatina',
            ),
            porp: 'isFaceLingualPalatina',
        },
        {
            label: this.service.translate.instant(
                'PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceMesial',
            ),
            porp: 'isFaceMesial',
        },
        {
            label: this.service.translate.instant(
                'PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceVestibular',
            ),
            porp: 'isFaceVestibular',
        },
    ];

    columnsEspecialidades: ColumnsTableModel[] = [
        {
            action: true,
            name: this.service.translate.instant(
                'PATIENTS.MODAL_PROCEDURE.TABLE_SPECIALTIES.COL1',
            ),
            prop: 'nome',
            sortable: true,
            width: null,
        },
    ];

    columnsProcedures: ColumnsTableModel[] = [
        {
            action: true,
            name: this.service.translate.instant(
                'PATIENTS.MODAL_PROCEDURE.TABLE_PROCEDURES.COL1',
            ),
            prop: 'procedimentoNome',
            sortable: true,
            width: 250,
        },
    ];

    procedureFilter = '';

    constructor(
        public service: UtilsService,
        private readonly _activeModal: NgbActiveModal
    ) {
    }

    ngOnInit() {
        this.procedure.tratamentoId = this.tratamentoId;
        this.procedure.profissionalId = this.professionalId;
        this.procedure.statusTratamentoServico = 'REALIZAR';
        this.getEspecialidades();
    }

    get disableSaveButton() {
        return !this.procedure.profissionalId
            || !this.procedure.valor
            || this.procedure.valor <= 0
            || !this.procedure.statusTratamentoServico
            || this.listaDentes.length === 0;
    }

    getEspecialidades() {
        this.service.loading(true);
        this.service
            .httpGET(CONSTANTS.ENDPOINTS.specialty.listByConvenant + this.convenioId)
            .subscribe(
                data => {
                    this.specialties = data.body as ShortListItem[];
                    this.selectedSpecialties = [this.specialties[0]];
                    this.getProcedures(this.specialties[0].id);
                },
                err => {

                    this.service.loading(false);
                    this.service.notification.error(
                        this.service.translate.instant('COMMON.ERROR.SEARCH'),
                        err.error.error,
                    );
                },
            );
    }

    getProcedures(id: number) {
        this.service.loading(true);
        const ref = CONSTANTS.ENDPOINTS.specialty.filterBySpecialty.replace(
            '#id',
            id.toString(),
        );
        this.service.httpGET(ref + this.convenioId).subscribe(
            data => {
                this.procedures = data.body as ProcedureSpecialties[];
                this.filteredProcedures = this.procedures;

                if (!isEmpty(this.procedures)) {
                    this.selectedProcedures = [this.procedures[0]];

                    this.procedure.valor = this.procedures[0].preco;
                    this.procedure.convenioEspecialidadeProcedimentoId = this.procedures[0].id;
                    this.procedure.convenioEspecialidadeProcedimentoNome = this.procedures[0].procedimentoNome;
                }

                this.service.loading(false);
            },
            err => {

                this.service.loading(false);
                this.service.notification.error(
                    this.service.translate.instant('COMMON.ERROR.SEARCH'),
                    err.error.error,
                );
            },
        );
    }

    selectProcedures(event) {
        const id = event.selected[0].id;
        this.getProcedures(id);
    }

    markProcedure(event) {
        this.procedure.valor = event.selected[0].preco;
        this.procedure.convenioEspecialidadeProcedimentoId = event.selected[0].id;
        this.procedure.convenioEspecialidadeProcedimentoNome =
            event.selected[0].procedimentoNome;

        if (event.selected[0].procedimentoIsAceitaFace) {
            this.isFace = true;
        } else {
            this.isFace = false;
        }

        this.procedure.isFaceDistal = false;
        this.procedure.isFaceLingualPalatina = false;
        this.procedure.isFaceMesial = false;
        this.procedure.isFaceOclusalIncisal = false;
        this.procedure.isFaceVestibular = false;

    }

    insertProcedure(close = true) {
        this.procedure.tratamentoId = this.tratamentoId;

        const professional = this.profissionais.find(
            item => item.id === this.procedure.profissionalId,
        );
        this.procedure.profissionalNome = professional.nome;

        if (this.isFace) {
            let count = 0;
            for (const face of this.listFace) {
                if (this.procedure[face.porp]) {
                    count++;
                }
            }

            if (count > 1) {
                this.multipleFaces(count, close);
            } else {
                const selectedProcedures = this.prepareReturnProcedures();
                this.close(selectedProcedures, close);
            }
        } else {
            const selectedProcedures = this.prepareReturnProcedures();
            this.close(selectedProcedures, close);
        }
    }

    prepareReturnProcedures() {
        let selectedProcedures: ProceduresModel[] = [];
        for (const dente of this.listaDentes) {
            const item = classToPlain(this.procedure) as any;
            item.emDente = dente.value ? dente.value : dente;
            selectedProcedures = [...selectedProcedures, item];
        }

        return selectedProcedures;
    }

    multipleFaces(nFaces: number, close = true) {
        const translate = this.service.translate;
        const refSWAL = 'PATIENTS.MODAL_PROCEDURE.SWAL';

        Swal.fire({
                      title: translate.instant(`${ refSWAL }.TITLE`),
                      text: translate.instant(`${ refSWAL }.TEXT`),
                      type: 'question',
                      showCancelButton: true,
                      confirmButtonText: translate.instant(`${ refSWAL }.CONFIRM_BUTTON`),
                      cancelButtonText: translate.instant(`${ refSWAL }.CANCEL_BUTTON`),
                  }).then(result => {
            if (result.value || result.dismiss === Swal.DismissReason.cancel) {
                this.procedure.valor = result.value ?
                    nFaces * parseFloat(this.procedure.valor.toString()) : this.procedure.valor;
                const selectedProcedures = this.prepareReturnProcedures();
                this.close(selectedProcedures, close);
            }
        });
    }

    clearProcedureFilter() {
        this.procedureFilter = '';
        this.filterProcedures();
    }

    filterProcedures() {
        this.filteredProcedures = this.procedures.filter(procedure => procedure.procedimentoNome.toUpperCase()
            .includes(this.procedureFilter.toUpperCase()));

        if (this.procedureFilter.trim().length <= 0) {
            this.selectedProcedures = [];
            this.procedure = new ProceduresModel();
        }
    }

    close(procedures: ProceduresModel[] = undefined, close = true) {
        this.emitSave.emit(procedures);

        if (close) {
            this._activeModal.close();
        } else {
            this.listaDentes = [];
            this.procedure.valor = undefined;
        }
    }

}
