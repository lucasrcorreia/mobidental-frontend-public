import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_CONFIGURATION, ApiConfiguration } from '../../api/api-configuration';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { UtilsService } from '../../services/utils/utils.service';
import { SchedulingsStatisticsModel } from './schedulings-report/scheduling-statistics/schedulings-statistics-model';
import { BirdthdayReportModel } from './birthday-report/birthday-report.component';

export enum Periodocity { MENSAL = 'MENSAL', ANUAL = 'ANUAL'}

export interface BirthDate {
	dia?: number;
	mes?: number;
}

export interface ReceiptsReportFilters {
	dataInicio?: string | NgbDateStruct;
	dataFim?: string | NgbDateStruct;
	profissionalId?: number;
	professionalName?: string;
	convenioId?: number;
	covenantName?: string;
	pacienteId?: number;
	patientName?: string;
	listaStatusTratamento?: string[];
	listaStatusTratamentoName?: string[];
	listaStatusAgendamento?: string[];
	listaStatusAgendamentoName?: string[];
	statusTratamento?: string[];
	statusTratamentoName?: string[];
	indicacoes?: string[];
	indicacoesName?: string[];
	situacaoPaciente?: number[];
	situacaoPacienteName?: string[];
	especialidades?: number[];
	especialidadesName?: string[];
	empresaId?: number;
	empresaName?: string;
	tipoPeriodo?: Periodocity;
	nascimentoFim?: BirthDate;
	nascimentoInicio?: BirthDate;
	mes?: number;
	ano?: number;
}

@Injectable({
	providedIn: 'root',
})
export class ReportsService {

	constructor(
			@Inject(API_CONFIGURATION) private readonly _config: ApiConfiguration,
			private readonly _http: HttpClient,
			private readonly _service: UtilsService,
	) {
	}

	private _findAll(path: string, filters: ReceiptsReportFilters): Promise<unknown[]> {
		filters.dataInicio = filters.dataInicio ? this._service.parseDateFromDatePicker(filters.dataInicio) : null;
		filters.dataFim = filters.dataFim ? this._service.parseDateFromDatePicker(filters.dataFim) : null;
		return this._http
				.post<unknown[]>(`${ this._config.apiBasePath }` + path, filters)
				.toPromise();
	}

	findAllReceipts(filters: ReceiptsReportFilters): Promise<unknown[]> {
		return this._findAll('/recibo/findAll', filters);
	}

	findAllTreatments(filters: ReceiptsReportFilters): Promise<unknown[]> {
		return this._findAll('/tratamento/findAll', filters);
	}

	findAllAgeRange(): Promise<any[]> {
		return this._http
				.get<any[]>(`${ this._config.apiBasePath }` + '/paciente/getQuantidadePacientesByFaixaEtaria')
				.toPromise();
	}

	findAllBirthday(): Promise<BirdthdayReportModel[]> {
		return this._http
				.get<BirdthdayReportModel[]>(`${ this._config.apiBasePath }` + '/paciente/getQuantidadeAniversariantesMensal')
				.toPromise();
	}

	findAllAgeRangeByRange(idadeInicial: number, idadeFinal: number): Promise<unknown[]> {
		return this._http
				.post<unknown[]>(`${ this._config.apiBasePath }` + '/paciente/getPacientesByFaixaEtaria', { idadeInicial, idadeFinal })
				.toPromise();
	}

	findAllBirthdayByRange(id: number): Promise<unknown[]> {
		return this._http
				.post<unknown[]>(`${ this._config.apiBasePath }` + '/paciente/getPacientesRelatorioByMesNascimento', { id })
				.toPromise();
	}

	findAllIndications(filters: ReceiptsReportFilters): Promise<unknown[]> {
		return this._findAll('/paciente/getQuantidadeIndicacoesByPeriodo', filters);
	}

	findAllSchedulings(filters: ReceiptsReportFilters): Promise<unknown[]> {
		return this._findAll('/eventoAgenda/findAgendamentosRelatorio', filters);
	}

	fetchSchegulindStatistics(filters: ReceiptsReportFilters): Promise<SchedulingsStatisticsModel> {
		return this._http
				.post<SchedulingsStatisticsModel>(`${ this._config.apiBasePath }` + '/eventoAgenda/getEstatisticasAgendamento', filters)
				.toPromise()
	}

	findAllPatients(filters: ReceiptsReportFilters): Promise<unknown[]> {
		return this._findAll('/paciente/findPacientesRelatorio', filters);
	}
}
