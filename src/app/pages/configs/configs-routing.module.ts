import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../services/guard/auth.guard';

const routes: Routes = [
  {
    path: 'financial',
    loadChildren: './financial/financial.module#FinancialModule',
    data: {
      id: 5,
      children: true,
    },
    canActivate: [AuthGuard],
  },
  {
    path: 'covenant',
    loadChildren: './covenant/covenant.module#CovenantModule',
    data: {
      id: 1,
      children: true,
    },
    canActivate: [AuthGuard],
	},
	{
    path: 'comunication',
    loadChildren: './comunication/comunication.module#ComunicationModule',
    data: {
      id: 26,
      children: true,
    },
    canActivate: [AuthGuard],
  },
  {
    path: 'professionals',
    loadChildren: './professionals/professionals.module#ProfessionalsModule',
    data: {
      id: 3,
      children: true,
    },
    canActivate: [AuthGuard],
  },
  {
    path: 'medical-record',
    loadChildren: './medical-record/medical-record.module#MedicalRecordModule',
  },
  {
    path: 'prosthetic',
    loadChildren: './prosthetic/prosthetic.module#ProstheticModule',
    data: {
      id: 13,
      children: true,
    },
    canActivate: [AuthGuard],
  },
  {
    path: 'permissions',
    loadChildren: './permissions/permissions.module#PermissionsModule',
    data: {
      id: 2,
      children: true,
    },
    canActivate: [AuthGuard],
  },
  {
    path: 'conta',
    loadChildren: './conta-config/conta-config.module#ContaConfigModule',
  },
  {
    path: 'perfil',
    loadChildren: './perfil-config/perfil-config.module#PerfilConfigModule',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigsRoutingModule {}
