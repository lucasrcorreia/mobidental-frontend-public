import { Uso } from './enums/uso';

export interface Medicine {
	id?: number;
	nome?: string;
	posologia?: string;
	tipoUso?: Uso;
	dataCadastro?: string;
}
