import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalCcostCategoryComponent } from './modal-ccost-category.component';
import { SharedModule } from '../../../shared/shared.module';
import { NgxTranslateModule } from '../../../core/translate/translate.module';
import { TreeviewModule } from 'ngx-treeview';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ModalCcostCategoryComponent],
  imports: [
    CommonModule,
    SharedModule,
    NgxTranslateModule,
    TreeviewModule.forRoot(),
    FormsModule
  ],
  exports: [ModalCcostCategoryComponent]
})
export class ModalCcostCategoryModule {}
