import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulingsReportComponent } from './schedulings-report.component';

describe('ScheudlingsReportComponent', () => {
  let component: SchedulingsReportComponent;
  let fixture: ComponentFixture<SchedulingsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedulingsReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulingsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
