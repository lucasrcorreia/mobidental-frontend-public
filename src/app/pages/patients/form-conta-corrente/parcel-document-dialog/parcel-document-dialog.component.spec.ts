import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParcelDocumentDialogComponent } from './parcel-document-dialog.component';

describe('ParcelDocumentDialogComponent', () => {
  let component: ParcelDocumentDialogComponent;
  let fixture: ComponentFixture<ParcelDocumentDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParcelDocumentDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParcelDocumentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
