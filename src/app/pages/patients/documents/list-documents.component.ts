import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { DocumentsService } from '../../../api/documents.service';
import { DocumentModel } from '../../../api/model/image';
import { flatten, groupBy, isEmpty, orderBy } from 'lodash-es';
import { UtilsService } from '../../../services/utils/utils.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsService } from 'angular2-notifications';
import { UploadDocumentComponent, UploadFile } from './upload-document/upload-document.component';
import { CameraUploadComponent, WebCamFile } from './upload-document/camera-upload/camera-upload.component';
import { DocumentsCommonService } from './documents-common.service';
import { VisualizeDocumentComponent } from './visualize-document/visualize-document.component';
import { Subscription } from 'rxjs';
import { MultipleCameraUploadComponent } from './upload-document/multiple-camera-upload/multiple-camera-upload.component';

const GROUP_BY_PROPERTY = 'data';
const MIME_TYPE_FOR_WEBCAM_IMAGE = { type: 'image/jpeg' };

@Component({
	selector: 'app-list-documents',
	templateUrl: './list-documents.component.html',
	styleUrls: ['./list-documents.component.scss'],
})
export class ListDocumentsComponent implements OnInit, OnDestroy {

	@Input() patientId: number;

	groupedDocs: { [key: string]: DocumentModel[] } = {};
	private _subscriptions = new Subscription();

	constructor(
			private readonly _documentsService: DocumentsService,
			private readonly _loadingService: UtilsService,
			private readonly _modalService: NgbModal,
			private readonly _notificationsService: NotificationsService,
			private readonly _documentsCommonService: DocumentsCommonService) {
	}

	ngOnInit() {
		this._fetchDocuments();
		this._initSubscribes();
	}

	ngOnDestroy(): void {
		this._subscriptions.unsubscribe();
	}

	private _initSubscribes() {
		const sub = this._documentsCommonService.$data.subscribe((trigger: boolean) => {
			if (trigger) {
				this._fetchDocuments();
			}
		});

		this._subscriptions.add(sub);
	}

	private async _fetchDocuments() {
		try {
			this._loadingService.loading(true);
			const images = await this._documentsService.list(this.patientId);
			this.groupedDocs = groupBy(this._formatValueFromDocuments(images), GROUP_BY_PROPERTY);
		} finally {
			this._loadingService.loading(false);
		}
	}

	private _formatValueFromDocuments(documents: DocumentModel[]): DocumentModel[] {
		documents.forEach(document => {
			if (document.data) {
				document.data = document.data.split(' ')[0];
			}

			this._documentsCommonService.getCorrectUrlFromDocument(document);
		});

		documents = orderBy(documents, [(document: DocumentModel) => {
			return this.dateFromString(document.data);
		}], ['desc']);

		return documents;
	}

	get groups() {
		return Object.keys(this.groupedDocs);
	}

	get documents(): DocumentModel[] {
		return flatten(Object.values(this.groupedDocs));
	}

	documentsFromGroup(group: string) {
		return Object.values(this.groupedDocs[group]);
	}

	dateFromString(date: string) {
		const defaultDateFormat = date.split('/').reverse().join('-');

		// Sem a hora 00 ele traz 1 dia a menos
		return new Date(defaultDateFormat + ' 00:00:00');
	}

	getLabel(label: string) {
		return this._documentsCommonService.getLabel(label);
	}

	async multipleCameraUpload() {
		const modalRef = this._modalService.open(MultipleCameraUploadComponent, { size: 'xl' as 'lg' });

		const files: WebCamFile[] = await modalRef.result;

		if (files && files.length > 0) {
			try {
				for (const file of files) {
					const document: DocumentModel = this._buildDocumentToUpload(file);
					const imageBlob = this.dataURItoBlob(file.file.imageAsBase64);
					const imageFile = new File([imageBlob], this._createNameForSnapShots(), MIME_TYPE_FOR_WEBCAM_IMAGE);


					this._loadingService.loading(true);
					await this._documentsService.upload(document, imageFile);
				}
				this._notificationsService.success('Arquivos enviados.');
			} catch (error) {
				this._notificationsService.error(`Ocorreu um erro ao fazer o upload de um ou mais arquivos.`);
			} finally {
				this._loadingService.loading(false);
				this._fetchDocuments();
			}
		}
	}

	async cameraUpload() {
		const modalRef = this._modalService.open(CameraUploadComponent, { size: 'lg' });

		const file: WebCamFile = await modalRef.result;

		if (file) {
			const document: DocumentModel = this._buildDocumentToUpload(file);
			const imageBlob = this.dataURItoBlob(file.file.imageAsBase64);
			const imageFile = new File([imageBlob], this._createNameForSnapShots(), MIME_TYPE_FOR_WEBCAM_IMAGE);

			try {
				this._loadingService.loading(true);
				await this._documentsService.upload(document, imageFile);
				this._notificationsService.success(`Arquivo ${ document.descricao } enviado.`);
			} catch (error) {
				this._notificationsService.error('Ocorreu um erro ao fazer o upload do arquivo.');
			} finally {
				this._loadingService.loading(false);
				this._fetchDocuments();
			}
		}

	}

	async uploadDocument() {
		const modalRef = this._modalService.open(UploadDocumentComponent, { size: 'lg' });

		const files: UploadFile[] = await modalRef.result;

		if (files) {
			// Guarda arquivos que deram erro no upload
			const errorOnUpload: string[] = [];

			this._loadingService.loading(true);
			const promises = files.map(async (item: UploadFile) => {
				try {
					const document: DocumentModel = this._buildDocumentToUpload(item);
					await this._documentsService.upload(document, item.file);
				} catch (error) {
					errorOnUpload.push(item.description);
				}
			});

			await Promise.all(promises).finally(() => {
				this._loadingService.loading(false);
				if (files.length !== errorOnUpload.length) {
					this._notificationsService.success('Arquivos enviados.');
				}
				if (!isEmpty(errorOnUpload)) {
					this._notificationsService.error(`Ocorreu um erro ao fazer o upload do(s) arquivo(s): ${ errorOnUpload.toString() }.`);
				}
				this._fetchDocuments();
			});
		}
	}

	private _buildDocumentToUpload(item: UploadFile | WebCamFile): DocumentModel {
		return {
			descricao: item.description,
			observacao: item.note,
			pacienteId: this.patientId,
		} as DocumentModel;
	}

	async visualize(document: DocumentModel) {
		const modalRef = this._modalService.open(VisualizeDocumentComponent, { size: 'xl' as 'lg' });
		modalRef.componentInstance.documents = this.documents;
		modalRef.componentInstance.currentDocument = document;

		const editionOccurred = await modalRef.result;

		if (editionOccurred) {
			this._fetchDocuments();
		}
	}

	edit(document: DocumentModel) {
		this._documentsCommonService.edit(document);
	}

	download(document: DocumentModel) {
		this._documentsCommonService.download(document);
	}

	async delete(document: DocumentModel) {
		this._documentsCommonService.delete(document);
	}

	dataURItoBlob(dataURI: string): Blob {
		const byteString = window.atob(dataURI);
		const arrayBuffer = new ArrayBuffer(byteString.length);
		const int8Array = new Uint8Array(arrayBuffer);
		for (let i = 0; i < byteString.length; i++) {
			int8Array[i] = byteString.charCodeAt(i);
		}
		return new Blob([int8Array], MIME_TYPE_FOR_WEBCAM_IMAGE);
	}

	private _createNameForSnapShots() {
		return 'snapshot_' + new Date().toLocaleDateString().replace(/\//g, '_') + '_patient_' + this.patientId + '.jpeg';
	}

}
