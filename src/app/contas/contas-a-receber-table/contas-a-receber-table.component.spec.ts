import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContasAReceberTableComponent } from './contas-a-receber-table.component';

describe('ContasAReceberTableComponent', () => {
  let component: ContasAReceberTableComponent;
  let fixture: ComponentFixture<ContasAReceberTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContasAReceberTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContasAReceberTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
