import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../../services/utils/utils.service';
import { ColumnsTableModel } from '../../../core/models/table/columns.model';
import { CONSTANTS } from '../../../core/constants/constants';
import { PaginationResponseModel } from '../../../core/models/response/paginationResponse.model';
import {
  CovenanteCreate,
  CovenantModel,
  ProceduresCovenantModel,
  SpecialtiesCovenantModel,
} from '../../../core/models/config/covenant/covenantCreate.model';
import { ConveniosApiService } from '../../../api/convenios-api.service';
import Swal from 'sweetalert2';
import { NotificationsService } from 'angular2-notifications';
import { errorToString } from '../../../lib/helpers/error-to-string';
import { GlobalSpinnerService } from '../../../lib/global-spinner.service';
import { load } from '@angular/core/src/render3';

@Component({
  selector: 'app-comunication',
  templateUrl: './comunication.component.html',
  styleUrls: [
    './comunication.component.scss',
    './../../../../assets/icon/icofont/css/icofont.scss',
  ],
})
export class ComunicationComponent implements OnInit {

	comunicationSettings: any;

  constructor(
    public service: UtilsService,
    private readonly _conveniosApiService: ConveniosApiService,
    private readonly _notificationsService: NotificationsService,
    private readonly _globalSpinnerService: GlobalSpinnerService,
  ) {
  }

  ngOnInit() {
		this.getSettings();
  }


  getSettings() {
    this.service.loading(true);
		this.service
		.httpGET(CONSTANTS.ENDPOINTS.config.comunication.getSettings)
		.subscribe(
			data => {
				this.service.loading(false);
				this.comunicationSettings = data.body;
			},
			err => {
				this.service.loading(false);
				this.service.notification.error(
					this.service.translate.instant('COMMON.ERROR.SEARCH'),
					err.error.error,
				);
			},
		);
  }

}
