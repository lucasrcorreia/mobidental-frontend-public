import { Injectable } from '@angular/core';
import { TreeviewItem } from 'ngx-treeview';
import { CategoryInterface, CenterCostInterface } from '../../core/models/config/financial/financial.model';

@Injectable({
  providedIn: 'root',
})
export class TreeviewService {
  centerCost = 'centerCost';

  createTreeView(data: (CategoryInterface | CenterCostInterface)[] | null | undefined): TreeviewItem[] {
    return (data || [])
      .map(d =>
        'categoriaPaiId' in d || 'categoriaChilds' in d
          ? this._categoryCreateTreeView([d])[0]
          : this._buildTreeView([d])[0],
      );
  }

  private _categoryCreateTreeView(cs: CategoryInterface[] | null | undefined): TreeviewItem[] {
    return (cs || [])
      .filter(c => !c.excluido)
      .map(c => new TreeviewItem({
        text: c.descricao,
        value: c,
        children: this._categoryCreateTreeView(c.categoriaChilds).map(f => {
          f.value.parent = c;
          return f;
        }),
      }));
  }

  private _buildTreeView(cs: CenterCostInterface[] | null | undefined): TreeviewItem[] {
    return (cs || [])
      .filter(c => !c.excluido)
      .map(c => new TreeviewItem({
        text: c.descricao,
        value: c,
        children: this._buildTreeView(c.centroCustoChilds).map(f => {
          f.value.parent = c;
          return f;
        }),
      }));
  }

  findItemById(id: number, childrenNodeName: string, currentNode) {
    let result;

    for (let i = 0; i < currentNode.length; i++) {
      const currChild = currentNode[i];

      if (currChild.id === id) {
        return currChild;
      } else {
        if (currChild[childrenNodeName].length > 0) {
          for (let j = 0; j < currChild[childrenNodeName].length; j++) {
            const currChildInner = currChild[childrenNodeName][j];
            if (currChildInner.id === id) {
              result = currChildInner;
            } else {
              this.findItemById(id, childrenNodeName, currChildInner);
            }
          }
        }
      }
    }

    if (result) {
      return result;
    } else {
      return null;
    }
  }

  findNoChildren(childrenNodeName: string, currentNode: object[], noChildren: Array<any>) {
    for (const currChild of currentNode) {
      if (!currChild[childrenNodeName] || currChild[childrenNodeName].length === 0) {
        noChildren.push(currChild);
      } else {
        if (currChild[childrenNodeName].length > 0) {
          for (const currChildInner of currChild[childrenNodeName]) {
            if (!currChildInner[childrenNodeName] || currChildInner[childrenNodeName].length === 0) {
              noChildren.push(currChildInner);
            } else {
              this.findNoChildren(childrenNodeName, currChildInner[childrenNodeName], noChildren);
            }
          }
        } else {
          noChildren.push(currChild);
        }
      }
    }

  }

  getAllNodes(childrenNodeName: string, currentNode, nodes: Array<any>) {
    for (const currChild of currentNode) {
      nodes.push(currChild);
      if (currChild[childrenNodeName].length > 0) {
        for (const currChildInner of currChild[childrenNodeName]) {
          if (!currChildInner[childrenNodeName] || currChildInner[childrenNodeName].length === 0) {
            nodes.push(currChildInner);
          } else {
            this.getAllNodes(childrenNodeName, currChildInner[childrenNodeName], nodes);
          }
        }
      } else {
        nodes.push(currChild);
      }
    }
  }

  getClassItem(item: TreeviewItem, typeComp: string) {
    let result = 'simpleText';
    if (item.children) {
      result = 'nodeText';
    } else {
      if (typeComp === 'centerCost') {
        if (!item.value.centroCustoPaiId) {
          result = 'nodeText';
        }
      } else {
        if (!item.value.categoriaPaiId) {
          result = 'nodeText';
        }
      }
    }

    return result;
  }

  getClassIconItem(item: TreeviewItem, typeComp: string) {
    let result = '';
    if (!item.children) {
      if (typeComp === 'centerCost') {
        if (!item.value.centroCustoPaiId) {
          result = 'icofont-minus';
        }
      } else {
        if (!item.value.categoriaPaiId) {
          result = 'icofont-minus';
        }
      }
    }

    return result;
  }
}
