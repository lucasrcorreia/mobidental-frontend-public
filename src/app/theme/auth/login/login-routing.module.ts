import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AcceptInviteComponent } from './accept-invite/accept-invite.component';

const routes: Routes = [
  {
    path: 'invite',
    component: AcceptInviteComponent,
  },
  {
    path: '',
    data: {
      title: 'Login',
      status: false,
    },
    children: [
      {
        path: 'basic',
        loadChildren: './basic-login/basic-login.module#BasicLoginModule',
      },
      {
        path: 'header-footer',
        loadChildren: './header-footer-login/header-footer-login.module#HeaderFooterLoginModule',
      },
      {
        path: 'social',
        loadChildren: './social-login/social-login.module#SocialLoginModule',
      },
      {
        path: 'social-header-footer',
        loadChildren: './social-header-footer-login/social-header-footer-login.module#SocialHeaderFooterLoginModule',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginRoutingModule {}
