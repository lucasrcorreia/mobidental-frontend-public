import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FluxoDeCaixa } from '../../../../api/fluxo-de-caixa-api.service';
import { descricoesPagamentos, FormaPagamento } from '../../../../api/model/forma-pagamento';
import { getTipoParcelaInfo, TipoParcela } from '../../../../api/model/tipo-parcela';
import { PATIENT_RECORD_LINK } from '../../../agenda/agendamento/scheduling/scheduling.component';
import { Router } from '@angular/router';
import { ReportPrintType } from '../../../reports/report-header-actions/report-header-actions.component';

export type Aba = 'entradas' | 'saidas';

@Component({
               selector: 'app-fluxo-caixa-listagem',
               templateUrl: './fluxo-caixa-listagem.component.html',
               styleUrls: ['./fluxo-caixa-listagem.component.scss'],
           })
export class FluxoCaixaListagemComponent implements OnInit {

    readonly limite = Number.MAX_SAFE_INTEGER;

    aba: Aba = 'entradas';

    @Input() fluxo?: FluxoDeCaixa;
    @Output() generateReport = new EventEmitter<ReportPrintType>();

    constructor(private readonly _router: Router) {
    }

    ngOnInit() {
    }

    descricaoFormaPagamento(f: FormaPagamento) {
        return descricoesPagamentos[f];
    }

    tipoParcela(t: TipoParcela) {
        return getTipoParcelaInfo(t);
    }

    goToPatientPage(id: number): void {
        this._router.navigate(PATIENT_RECORD_LINK(id));
    }

    print(type: ReportPrintType): void {
        this.generateReport.emit(type);
    }

}
