export interface PatientReportRowModel {
	tipoLancamento?: string;
	dataVencimento?: string;
	valor?: number;
	numeroCheque?: string;
	dataRecebimento?: string;
	formaRecebimento?: string;
	descricao?: string;
}
