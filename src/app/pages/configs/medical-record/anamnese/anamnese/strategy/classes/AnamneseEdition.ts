import { AnamneseStrategy } from '../AnamneseStrategy';
import { AnamnesePerguntaModel } from '../../../models/AnamnesePerguntaModel';
import { AnamneseModel } from '../../../models/AnamneseModel';

export class AnamneseEdition implements AnamneseStrategy {
  operation = 'alterada';
  title = 'CONFIG.ANAMNESE.LABELS.EDIT_QUESTION';
  readonly question: AnamnesePerguntaModel;

  constructor(question: AnamnesePerguntaModel) {
    this.question = question;
  }

  workWithQuestion(anamnese: AnamneseModel, question: AnamnesePerguntaModel, index: number): void {
    anamnese.perguntas[index] = question;
  }

}
