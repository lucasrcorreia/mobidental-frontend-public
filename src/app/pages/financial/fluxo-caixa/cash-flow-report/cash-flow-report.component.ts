import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../../../services/utils/utils.service';
import { NotificationsService } from 'angular2-notifications';
import { GlobalSpinnerService } from '../../../../lib/global-spinner.service';
import { CashFlowService } from './service/cash-flow.service';
import { ReportPrintType } from '../../../reports/report-header-actions/report-header-actions.component';
import * as moment from 'moment';
import { CashFlowStoreService } from './service/cash-flow-store.service';

export interface CashFlowFilter {
    ano: number,
    mes: string
}

enum CashFlowPeriodocity {
    DAILY,
    MONTHLY
}

const CASH_FLOW_PERIODICITY: { label: string, value: CashFlowPeriodocity }[] = [
    { label: 'Diário', value: CashFlowPeriodocity.DAILY },
    { label: 'Mensal', value: CashFlowPeriodocity.MONTHLY },
];

export const CASH_FLOW_MONTHS: { label: string, value: string }[] = [
    { label: 'Janeiro', value: 'JANEIRO' },
    { label: 'Fevereiro', value: 'FEVEREIRO' },
    { label: 'Março', value: 'MARCO' },
    { label: 'Abril', value: 'ABRIL' },
    { label: 'Maio', value: 'MAIO' },
    { label: 'Junho', value: 'JUNHO' },
    { label: 'Julho', value: 'JULHO' },
    { label: 'Agosto', value: 'AGOSTO' },
    { label: 'Setembro', value: 'SETEMBRO' },
    { label: 'Outubro', value: 'OUTUBRO' },
    { label: 'Novembro', value: 'NOVEMBRO' },
    { label: 'Dezembro', value: 'DEZEMBRO' },
];

export const numberToBRLCurrency = (value: number) => {
    return value !== undefined ?
        'R$ ' + new Intl.NumberFormat(
        'pr-BR',
        { maximumFractionDigits: 2, minimumFractionDigits: 2 }
        ).format(value) : '';
};

@Component({
               selector: 'app-cash-flow-report',
               templateUrl: './cash-flow-report.component.html',
               styleUrls: ['./cash-flow-report.component.scss']
           })
export class CashFlowReportComponent implements OnInit {

    readonly periodicityList = CASH_FLOW_PERIODICITY;
    currentPeriodicity: CashFlowPeriodocity = CashFlowPeriodocity.DAILY;

    readonly months = CASH_FLOW_MONTHS;
    readonly years: number[] = [];

    filter: CashFlowFilter = { ano: moment().year(), mes: this.months[moment().month()].value };
    sheetTitle = this._service.translate.instant('REPORTS.CASH_FLOW.DAILY.TITLE');

    constructor(
        private readonly _cashFlowService: CashFlowService,
        readonly _service: UtilsService,
        private readonly _notificationsService: NotificationsService,
        private readonly _globalSpinnerService: GlobalSpinnerService,
        private readonly _cashFlowStoreService: CashFlowStoreService,
    ) {
    }

    ngOnInit() {
        this._generateYearsList();
        this._fetchCashFlow();
    }

    async _fetchCashFlow() {
        this._cashFlowStoreService.fireLoadingEvent(true);
        try {
            this._dailyCashFlow();
            this._monthlyCashFlow();
        } catch (e) {
            this._notificationsService.error('Ocorreu um erro ao listar os dados.');
        } finally {
            setTimeout(() => this._cashFlowStoreService.fireLoadingEvent(false), 500);
        }
    }

    get dailyCashFlow() {
        return this.currentPeriodicity === CashFlowPeriodocity.DAILY;
    }

    private async _dailyCashFlow() {
        const cashFlow = await this._cashFlowService.fetchDailyCashFlow(this.filter);
        this._cashFlowStoreService.fireDailyCashFlowEvent(cashFlow);
    }

    private async _monthlyCashFlow() {
        const cashFlow = await this._cashFlowService.fetchMonthlyCashFlow(this.filter);
        this._cashFlowStoreService.fireMonthlyCashFlowEvent(cashFlow);
    }

    print(type: ReportPrintType) {
        if (this.dailyCashFlow) {
            this._cashFlowStoreService.fireDailyCashFlowPrintEvent(type);
        } else {
            this._cashFlowStoreService.fireMonthlyCashFlowPrintEvent(type);
        }
    }

    private _generateYearsList(): void {
        let minYear = 2010;
        const currentYear = moment().year();
        for (; minYear <= currentYear; minYear++) {
            this.years.push(minYear);
        }
    }

    changeVisualization(period: CashFlowPeriodocity) {
        if (period != this.currentPeriodicity) {
            this.sheetTitle = period === CashFlowPeriodocity.DAILY ? this._service.translate.instant(
                'REPORTS.CASH_FLOW.DAILY.TITLE') :
                this._service.translate.instant('REPORTS.CASH_FLOW.MONTHLY.TITLE');
            this._cashFlowStoreService.fireLoadingEvent(true);
            setTimeout(() => this._cashFlowStoreService.fireLoadingEvent(false), 500);
        }
    }

}
