export enum TipoLancamento {
  A_RECEBER = 'A_RECEBER',
  RECEBIDO = 'RECEBIDO',
  ATRASADO = 'ATRASADO',
}

export const descricoesLancamentos: { [k in TipoLancamento]: string } = {
  A_RECEBER: 'A receber',
  ATRASADO: 'Atrasado',
  RECEBIDO: 'Recebido',
};
