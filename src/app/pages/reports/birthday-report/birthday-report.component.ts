import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { ReceiptsReportFilters, ReportsService } from '../reports.service';
import { ReportDatatableColumn, Type } from '../report-datatable/report-datatable.component';
import { AbstractReport } from '../abstract-report';
import { UtilsService } from '../../../services/utils/utils.service';
import { SheetGeneratorService } from '../../../services/sheets-generator/sheet-generator.service';
import { MaskFormatService } from '../../../services/utils/mask-format.service';
import { AsyncTemplate } from '../../../lib/helpers/async-template';
import { NotificationsService } from 'angular2-notifications';
import { GlobalSpinnerService } from '../../../lib/global-spinner.service';
import { CommonsReportBuildService } from '../../../services/report/commons-report-build.service';
import { PdfGeneratorService } from '../../../services/pdf-generator/pdf-generator.service';
import { DropdownGenericService } from '../../patients/form-patients/services/dropdown-generic.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NgxChartView } from '../indications-report/indications-report.component';
import { FULL_CELLPHONE_LENGTH } from '../../agenda/agenda.component';
import { Router } from "@angular/router";
import { DashboardQueryService } from "../../../theme/dashboard/default/service/dashboard-query.service";

declare let $: any;

export interface BirdthdayReportModel {
	quantidade?: number;
	mes?: string;
	mesId?: number;
}

const buildWhatsappMessage = (patientCellPhone: string,
							  patientName: string): string =>
		`https://api.whatsapp.com/send?phone=55${patientCellPhone}&text=Feliz aniversário${patientName}. Que você tenha cada vez mais saúde, paz, felicidade, alegria, prosperidade e sabedoria. Tudo de melhor para você neste dia tão especial! Parabéns!!!`

const goToWhatsappMessage = (row) => {
	const patientCellPhone = row['celular'];
	const patientName = row['nomePaciente'];
	if (!patientCellPhone || patientCellPhone.length < FULL_CELLPHONE_LENGTH) {
		return;
	}

	const firstPatientName = patientName ? ', ' + patientName.match(/(\w+)/)[0] : '';
	const whatsappLink = buildWhatsappMessage(patientCellPhone, firstPatientName);

	// Usar window.open() pode não funcionar de acordo com browser e/ou configuração do usuário
	$('<a />', { href: whatsappLink, target: '_blank' }).get(0).click();
}

const TABLE_COLUMNS: ReportDatatableColumn[] = [
	{ prop: 'nomePaciente', translation: 'REPORTS.TABLE.PATIENT_NAME' },
	{
		function: (row) => goToWhatsappMessage(row),
		translation: 'REPORTS.TABLE.WHATSAPP',
		cellClassName: 'text-center',
		type: Type.TOOLTIP,
		iconClass: 'icofont icofont-brand-whatsapp font-size-20 pointer text-green',
		tooltipText: 'Enviar mensagem'
	},
	{ prop: 'dataNascimento', translation: 'REPORTS.TABLE.BIRTH_DATE', type: Type.DATE },
	{ prop: 'idade', translation: 'REPORTS.TABLE.AGE', pdfAlignment: 'center' },
	{ prop: 'sexo', translation: 'REPORTS.TABLE.GENDER' },
	{ prop: 'email', translation: 'REPORTS.TABLE.EMAIL' },
	{ prop: 'celular', translation: 'REPORTS.TABLE.CELL', type: Type.CELL },
	{ prop: 'telefone', translation: 'REPORTS.TABLE.PHONE', type: Type.PHONE },
];

const REPORT_COLUMNS: ReportDatatableColumn[] = [
	{ prop: 'nomePaciente', translation: 'REPORTS.TABLE.PATIENT_NAME' },
	{ prop: 'dataNascimento', translation: 'REPORTS.TABLE.BIRTH_DATE', type: Type.DATE },
	{ prop: 'idade', translation: 'REPORTS.TABLE.AGE', pdfAlignment: 'center' },
	{ prop: 'sexo', translation: 'REPORTS.TABLE.GENDER' },
	{ prop: 'email', translation: 'REPORTS.TABLE.EMAIL' },
	{ prop: 'celular', translation: 'REPORTS.TABLE.CELL', type: Type.CELL },
	{ prop: 'telefone', translation: 'REPORTS.TABLE.PHONE', type: Type.PHONE },
];

const SHEET_COLUMNS: ReportDatatableColumn[] = [
	{ prop: 'nomePaciente', translation: 'REPORTS.TABLE.PATIENT_NAME' },
	{ prop: 'dataNascimento', translation: 'REPORTS.TABLE.BIRTH_DATE', type: Type.DATE },
	{ prop: 'idade', translation: 'REPORTS.TABLE.AGE', pdfAlignment: 'center' },
	{ prop: 'sexo', translation: 'REPORTS.TABLE.GENDER' },
	{ prop: 'email', translation: 'REPORTS.TABLE.EMAIL' },
	{ prop: 'celular', translation: 'REPORTS.TABLE.CELL', type: Type.CELL },
	{ prop: 'telefone', translation: 'REPORTS.TABLE.PHONE', type: Type.PHONE },
	{ prop: 'endereco', translation: 'REPORTS.TABLE.ADDRESS' },
	{ prop: 'numeroEndereco', translation: 'REPORTS.TABLE.N_ADDRESS' },
	{ prop: 'cep', translation: 'REPORTS.TABLE.CEP' },
	{ prop: 'bairro', translation: 'REPORTS.TABLE.NEIGHBORHOOD' },
	{ prop: 'cidade', translation: 'REPORTS.TABLE.CITY' },
	{ prop: 'ufCidade', translation: 'REPORTS.TABLE.UF' },
];

@Component({
	selector: 'app-age-range-report',
	templateUrl: './birthday-report.component.html',
	styleUrls: ['./birthday-report.component.scss'],
})
export class BirthdayReportComponent extends AbstractReport implements OnInit {

	@ViewChild('barVerticalComponent') barVerticalComponent?: ElementRef;

	filters: ReceiptsReportFilters;
	ownData: any[];

	currentMonth: string;

	pureResults: BirdthdayReportModel[];
	chartResults: NgxChartView[];
	colorScheme = {
		domain: ['#dc461a'],
	};
	barChartView: [number, number];

	sheetFileName = 'relatorio_aniversariantes';
	sheetTitle = this._service.translate.instant('REPORTS.BIRTHDAY.TITLE');

	reportTableBodyWidths: number[] = [110, 50, 30, 50, 120, 65, 65];

	readonly columns = TABLE_COLUMNS;
	readonly pdfColumns = REPORT_COLUMNS;
	readonly sheetColumns = SHEET_COLUMNS;

	private readonly _destroy$ = new Subject();

	constructor(private readonly _reportsService: ReportsService,
				private readonly _router: Router,
				private readonly _dashboardQueryService: DashboardQueryService,
				@Inject(UtilsService)  _service: UtilsService,
				@Inject(DropdownGenericService) _dropdownGenericService: DropdownGenericService,
				@Inject(NotificationsService) _notificationsService: NotificationsService,
				@Inject(GlobalSpinnerService) _globalSpinnerService: GlobalSpinnerService,
				@Inject(CommonsReportBuildService) _commonsReportService: CommonsReportBuildService,
				@Inject(PdfGeneratorService) _pdfGeneratorService: PdfGeneratorService,
				@Inject(SheetGeneratorService) _sheetGeneratorService: SheetGeneratorService,
				@Inject(MaskFormatService) _maskFormatService: MaskFormatService,
				@Inject(AsyncTemplate) _asyncTemplate: AsyncTemplate) {
		super(_service, _dropdownGenericService, _notificationsService, _globalSpinnerService, _commonsReportService, _pdfGeneratorService, _sheetGeneratorService, _maskFormatService, _asyncTemplate);
		this._service.toggleMenu.pipe(takeUntil(this._destroy$)).subscribe(data => {
			if (data) {
				setTimeout(() => this.resizeChart());
			}
		});
	}

	async ngOnInit() {
		await this._initChart();
		this._checkForExtraQueryParams();
	}

	ngOnDestroy() {
		this._destroy$.next();
		this._destroy$.complete();
	}

	private async _initChart() {
		this.pureResults = await this.fetchOwnData(() => this._reportsService.findAllBirthday())
		this.chartResults = this.pureResults.map(r => {
			return { name: r.mes, value: r.quantidade }
		});
		this.resizeChart();
	}

	private _checkForExtraQueryParams() {
		const currentMonth = this._dashboardQueryService._currentMonth;
		if (currentMonth) {
			this.fetchByRange(currentMonth);
		}
	}

	resizeChart(): void {
		// -15 para tirar o padding direito
		this.barChartView = [this.barVerticalComponent.nativeElement.offsetWidth - 15, 250];
	}

	async fetchByRange($event: NgxChartView) {
		this.currentMonth = $event.name;
		const mesId = this.pureResults.find(r => r.mes === $event.name).mesId;

		this.ownData = await this.fetchOwnData(() => this._reportsService.findAllBirthdayByRange(mesId));
	}

	async buildReportHeader() {
		return [
			{ ...await this._commonsReportService.buildFinancialReportHeader(this.sheetTitle) },
			{
				text: this.reportFilterLabel,
				fontSize: 9,
				margin: [0, 10, 0, 10],
			},
		]
	}

	private get reportFilterLabel() {
		return [
			{ text: 'Aniversariantes no mês de: ', bold: true },
			this.currentMonth,
		];
	}

	async search(): Promise<any> {
		return Promise.resolve(undefined);
	}

}
