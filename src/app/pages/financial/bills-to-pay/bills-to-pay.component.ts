import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { UtilsService } from '../../../services/utils/utils.service';
import { LISTAS } from '../lists';
import * as moment from 'moment';
import { ColumnsTableModel } from '../../../core/models/table/columns.model';
import { FINANCIAL_CONST } from '../constants';
import { CONSTANTS } from '../../../core/constants/constants';
import { forkJoin, Subject } from 'rxjs';
import { TreeviewService } from '../../../services/treeview/treeview.service';
import { AccountBillsToPayModel, BillsToPayTableModel } from '../../../core/models/financial/billsToPay/billsToPay.model';
import { CustomDatepickerI18n, I18n } from '../../../shared/ngb-datepicker/datepicker-i18n';
import { NgbDateFRParserFormatter } from '../../../shared/ngb-datepicker/ngb-date-fr-parser-formatter';
import { CategoryInterface, CenterCostInterface } from '../../../core/models/config/financial/financial.model';
import { NgbAccordion, NgbDateParserFormatter, NgbDatepickerI18n, NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { takeUntil } from 'rxjs/operators';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { TotalizersInfo } from '../../../components/totalizers/totalizers.component';
import { GlobalSpinnerService } from '../../../lib/global-spinner.service';
import { ContasAPagarApiService, TipoExclusaoContaAPagar } from '../../../api/contas-a-pagar.api.service';
import { NotificationsService } from 'angular2-notifications';
import { ModalWithOptionsComponent } from '../../../shared/modal-with-options/modal-with-options.component';
import { errorToString } from '../../../lib/helpers/error-to-string';
import { PdfGeneratorService, PDFMake } from '../../../services/pdf-generator/pdf-generator.service';
import { BLUE_COLOR, CommonsReportBuildService, GREEN_COLOR, GREY_COLOR, REPORT_STYLES } from '../../../services/report/commons-report-build.service';
import { descricoesPagamentos, FormaPagamento } from '../../../api/model/forma-pagamento';
import { TipoRecebimento } from '../../../api/model/tipo-recebimento';
import { forEach, groupBy } from 'lodash-es';
import { Dictionary } from 'ramda';

export const BY_RANGE = 'BY_RANGE';

enum ReportType {
	ANALYTICAL,
	SYNTHETIC,
}

interface ContasAPagarFilters {
	dataInicio: NgbDateStruct | string;
	dataFim: NgbDateStruct | string;
	formaDeRecebimento?: FormaPagamento[];
	tipoLancamento?: TipoRecebimento[];
}

@Component({
	selector: 'app-bills-to-pay',
	templateUrl: './bills-to-pay.component.html',
	styleUrls: [
		'./bills-to-pay.component.scss',
		'./../../../../assets/icon/icofont/css/icofont.scss',
	],
	providers: [
		I18n,
		{ provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
		{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
	],
})
export class BillsToPayComponent implements OnInit, OnDestroy {

	@ViewChild('a') ngbAccordion?: NgbAccordion;

	@ViewChild(DatatableComponent) table: DatatableComponent;

	private destroy$ = new Subject();
	readonly reportType = ReportType;

	locale = this.service.translate.instant('COMMON.LOCALE');
	localLists = new LISTAS(this.service.translate);
	searchRange = 'TODAY';
	typeComp = 'centerCost';

	categories = [];
	cCost = [];

	localConst = FINANCIAL_CONST;
	centerCost: CenterCostInterface[];
	category: CategoryInterface[];
	treeview = {
		category: [],
		cCost: [],
	};

	showModal = true;
	moreFilter = false;
	itemsTreeview = [];
	selectedCategory = [];
	categoryToDisplay = this.service.translate.instant(
			'FINANCIAL.BILLS_TO_PAY.ALL_CAT',
	);

	rowsPage: AccountBillsToPayModel[] = [];
	searchParam = {
		categorias: [],
		centroCustos: [],
		dataInicio: null,
		dataFim: null,
		formaDeRecebimento: [],
		tipoLancamento: [],
		descricao: null,
		numeroDocumento: null,
	};

	findBy: 'descricao' | 'documento' = 'descricao';
	findByValue?: string;

	page = {
		size: 10,
		totalElements: 0,
		totalPages: 0,
		pageNumber: 0,
	};

	account: AccountBillsToPayModel;
	accountTable: BillsToPayTableModel;
	showAccountPay = false;
	showAccount = false;
	currentAccountIsPaid = false;

	totals?: TotalizersInfo;
	toDeleteBill?: AccountBillsToPayModel;

	columns: ColumnsTableModel[] = [
		{
			action: true,
			name: this.service.translate.instant('FINANCIAL.BILLS_TO_PAY.TABLE.COL1'),
			prop: 'id',
			sortable: false,
			width: 150,
		},
		{
			action: false,
			name: this.service.translate.instant('FINANCIAL.BILLS_TO_PAY.TABLE.COL2'),
			prop: 'dataVencimento',
			sortable: false,
			width: 130,
		},
		{
			action: false,
			name: this.service.translate.instant('FINANCIAL.BILLS_TO_PAY.TABLE.COL3'),
			prop: 'descricao',
			sortable: true,
			width: 250,
		},
		{
			action: false,
			name: this.service.translate.instant('FINANCIAL.BILLS_TO_PAY.TABLE.COL4'),
			prop: 'parcel',
			sortable: true,
			width: 80,
		},
		{
			action: false,
			name: this.service.translate.instant('FINANCIAL.BILLS_TO_PAY.TABLE.COL5'),
			prop: 'valorTotal',
			sortable: true,
			currency: true,
			width: 100,
		},
		{
			action: false,
			name: this.service.translate.instant('FINANCIAL.BILLS_TO_PAY.TABLE.COL6'),
			prop: 'dataPagamento',
			sortable: false,
			width: 130,
		},
		{
			action: false,
			name: this.service.translate.instant('FINANCIAL.BILLS_TO_PAY.TABLE.COL7'),
			prop: 'categoria',
			sortable: true,
			width: 150,
		},
		{
			action: false,
			name: this.service.translate.instant('FINANCIAL.BILLS_TO_PAY.TABLE.COL8'),
			prop: 'status',
			sortable: false,
			width: 100,
		},
	];

	constructor(
			public service: UtilsService,
			private treeviewService: TreeviewService,
			private cd: ChangeDetectorRef,
			private readonly _globalSpinnerService: GlobalSpinnerService,
			private readonly _modalService: NgbModal,
			private readonly _contasAPagarApiService: ContasAPagarApiService,
			private readonly _notificationsService: NotificationsService,
			private readonly _pdfGeneratorService: PdfGeneratorService,
			private readonly _commonsReportService: CommonsReportBuildService,
	) {
		this.service.toggleMenu.pipe(takeUntil(this.destroy$)).subscribe(data => {
			if (data) {
				this.updateDataTable();
			}
		});
	}


	ngOnInit() {
		this.searchParam.dataFim = moment().format('DD/MM/YYYY');
		this.searchParam.dataInicio = moment().format('DD/MM/YYYY');
		this.initDropdowns();
	}

	ngOnDestroy() {
		this.destroy$.next();
		this.destroy$.complete();
	}

	updateDataTable() {
		this.cd.markForCheck();
		setTimeout(() => {
			this.table.recalculate();
			(this.table as any).cd.markForCheck();
		});
	}

	initDropdowns() {
		this.service.loading(true);

		const refCCost = this.service.httpGET(
				CONSTANTS.ENDPOINTS.config.financial.centerCost,
		);
		const refCat = this.service.httpGET(
				CONSTANTS.ENDPOINTS.config.financial.category,
		);

		forkJoin([refCCost, refCat]).subscribe(
				list => {
					this.centerCost = list[0].body as CenterCostInterface[];
					this.category = list[1].body as CategoryInterface[];

					this.treeviewService.getAllNodes('categoriaChilds', this.category, this.categories);
					this.treeviewService.getAllNodes('centroCustoChilds', this.centerCost, this.cCost);

					this.treeview.cCost = this.treeviewService.createTreeView(this.centerCost);

					this.treeview.category = this.treeviewService.createTreeView(this.category);

					this.populateTable();
				},
				err => {

					this.service.loading(false);
					this.service.notification.error(
							this.service.translate.instant('COMMON.ERROR.SEARCH'),
							err.error.error,
					);
				},
		);
	}

	onrangeChange() {
		switch (this.searchRange) {
			case 'TODAY':
				this.searchParam.dataInicio = moment().format('DD/MM/YYYY');
				this.searchParam.dataFim = moment().format('DD/MM/YYYY');
				this.populateTable();
				break;
			case 'WEEK':
				this.searchParam.dataInicio = moment()
						.startOf('week')
						.format('DD/MM/YYYY');
				this.searchParam.dataFim = moment()
						.endOf('week')
						.format('DD/MM/YYYY');
				this.populateTable();
				break;
			case 'MONTH':
				this.searchParam.dataInicio = moment()
						.startOf('month')
						.format('DD/MM/YYYY');
				this.searchParam.dataFim = moment()
						.endOf('month')
						.format('DD/MM/YYYY');
				this.populateTable();
				break;
			case 'PREV_MONTH':
				this.searchParam.dataInicio = moment()
						.subtract(1, 'month')
						.startOf('month')
						.format('DD/MM/YYYY');
				this.searchParam.dataFim = moment()
						.subtract(1, 'month')
						.endOf('month')
						.format('DD/MM/YYYY');
				this.populateTable();
				break;
			case BY_RANGE:
				this.searchParam.dataInicio = this.service.parseDatePicker(
						moment()
								.startOf('month')
								.format('DD/MM/YYYY'),
				);
				this.searchParam.dataFim = this.service.parseDatePicker(
						moment().format('DD/MM/YYYY'),
				);
				this.populateTable();
				break;

			default:
				break;
		}
	}

	populateTable() {
		const param = { ...this.searchParam };

		if (this.findBy === 'descricao') {
			param.descricao = this.findByValue;
		} else {
			param.numeroDocumento = this.findByValue;
		}

		if (this.searchRange === BY_RANGE) {
			param.dataInicio = this.service.parseDateFromDatePicker(
					this.searchParam.dataInicio,
			);
			param.dataFim = this.service.parseDateFromDatePicker(
					this.searchParam.dataFim,
			);
		}

		const loadingToken = this._globalSpinnerService.loadingManager.start();

		this.service
				.httpPOST(CONSTANTS.ENDPOINTS.financial.billsToPay.searchTable, param)
				.subscribe(
						data => {
							this.rowsPage = data.body as AccountBillsToPayModel[];
							this.page.pageNumber = 0;
							this.page.size = this.rowsPage.length;
							this.page.totalElements = this.rowsPage.length;
							this.page.totalPages = 1;

							this.totals = this._buildTotals();

							this.rowsPage.sort(
									(a, b) => this.sortRows(a, b),
							);

							this.rowsPage.forEach(row => {
								const itemCat = this.treeviewService.findItemById(row.categoriaId, 'categoriaChilds', this.category);
								row.categoria = itemCat && itemCat.descricao;
							});
						},
						err => {

							this.service.notification.error(
									this.service.translate.instant('COMMON.ERROR.SEARCH'),
									err.error.error,
							);
						},
						() => loadingToken.finished(),
				);

		this.ngbAccordion.collapse('panel');
	}

	private _buildTotals() {
		return (this.rowsPage || []).reduce<TotalizersInfo>((
				totals = { total: 0, percent: 0, received: 0, vanquished: 0 },
				data,
		) => ({
			...totals,
			total: totals.total + data.valorTotal,
			received: totals.received + (data.dataPagamento ? data.valorTotal : 0),
			vanquished: totals.vanquished + this._verifyVanquished(data),
		}), undefined);
	}

	private _verifyVanquished(bill: AccountBillsToPayModel): number {
		const today = moment();
		const vanquished = today > moment(bill.dataVencimento, 'DD/MM/YYYY');
		return !bill.dataPagamento && vanquished ? bill.valorTotal : 0;
	}

	async removeBill(row: AccountBillsToPayModel) {
		let choosed = TipoExclusaoContaAPagar.SOMENTE_ESTE_LANCAMENTO;
		if (row.recorrenciaId) {
			const modalRef = this._modalService.open(ModalWithOptionsComponent, { size: 'lg', centered: true });
			modalRef.componentInstance.title = 'Remover conta a pagar';
			modalRef.componentInstance.message = 'Como você prefere remover esta conta a pagar?';
			modalRef.componentInstance.confirmText = 'Remover'
			modalRef.componentInstance.options = [
				{ value: TipoExclusaoContaAPagar.SOMENTE_ESTE_LANCAMENTO, label: 'Remover apenas este lançamento' },
				{ value: TipoExclusaoContaAPagar.ESTE_E_PROXIMOS_LANCAMENTOS, label: 'Remover este e os próximos lançamentos' },
				{ value: TipoExclusaoContaAPagar.TODOS, label: 'Remover todos os lançamentos' },
			];

			choosed = await modalRef.result;
		}

		if (choosed) {
			const { value } = await Swal.fire({
				title: 'Confirmação de exclusão',
				text: 'Tem certeza que deseja remover?',
				type: 'question',
				showCancelButton: true,
				confirmButtonText: 'Sim. Remova',
				cancelButtonText: 'Cancelar',
			});

			if (!value) {
				return false;
			}

			this._removeBill(choosed as TipoExclusaoContaAPagar, row)
		}
	}

	private async _removeBill(tipo: TipoExclusaoContaAPagar, bill: AccountBillsToPayModel) {
		const { id, recorrenciaId } = bill;

		const loadingToken = this._globalSpinnerService.loadingManager.start();

		try {
			await this._contasAPagarApiService.removerContaAPagar({
				id,
				recorrenciaId,
				emTipoExclusaoContaPagar: tipo,
			});
			this._notificationsService.success('Conta a pagar removida');
			this.processDeleteModal(true);
		} catch (e) {
			this._notificationsService.error('Erro ao remover conta a pagar', errorToString(e));
		} finally {
			loadingToken.finished();
		}
	}

	sortRows(a: AccountBillsToPayModel, b: AccountBillsToPayModel) {
		return moment(b.dataVencimento, 'DD/MM/YYYY').toDate().getTime()
				- moment(a.dataVencimento, 'DD/MM/YYYY').toDate().getTime();
	}

	selectAllListTypeBill() {
		this.searchParam.tipoLancamento = this.localLists.listTypeBill.map(
				item => item.value,
		);
	}

	selectAllListTypePayment() {
		this.searchParam.formaDeRecebimento = this.localLists.listTypePayment.map(
				item => item.value,
		);
	}

	validateDates() {
		const init = moment(
				this.service.parseDateFromDatePicker(this.searchParam.dataInicio),
				'DD/MM/YYYY',
		);
		const end = moment(
				this.service.parseDateFromDatePicker(this.searchParam.dataFim),
				'DD/MM/YYYY',
		);

		if (end.isBefore(init)) {
			return false;
		} else {
			return true;
		}
	}

	applyDate(event) {
		if (
				['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'].includes(event.key) &&
				this.searchParam.dataFim &&
				this.searchParam.dataFim.year &&
				this.searchParam.dataFim.year > 2010 &&
				this.searchParam.dataInicio &&
				this.searchParam.dataInicio.year &&
				this.searchParam.dataInicio.year > 2010
		) {
			if (this.validateDates) {
				this.populateTable();
			}
		}
	}

	onDateSelect() {
		if (this.validateDates()) {
			this.populateTable();
		}
	}

	getStatus(row: BillsToPayTableModel) {
		const now = moment();
		const expirationDate = moment(row.dataVencimento, 'DD/MM/YYYY');
		const paymentDate = row.dataPagamento
				? moment(row.dataPagamento, 'DD/MM/YYYY')
				: null;
		const translate = this.service.translate;
		const STATUS_LIST = 'FINANCIAL.LIST_SEARCH.STATUS_LIST.';

		if (paymentDate) {
			return translate.instant(STATUS_LIST + 'RECEIVED_ALL');
		} else {
			if (now.isBefore(expirationDate, 'day')) {
				return translate.instant(STATUS_LIST + 'ON_DATE');
			} else if (now.isAfter(expirationDate, 'day')) {
				return translate.instant(STATUS_LIST + 'LATE_PAYMENT');
			} else {
				return translate.instant(STATUS_LIST + 'TODAY');
			}
		}
	}

	getStatusColor(row: BillsToPayTableModel) {
		const now = moment();
		const expirationDate = moment(row.dataVencimento, 'DD/MM/YYYY');
		const paymentDate = row.dataPagamento
				? moment(row.dataPagamento, 'DD/MM/YYYY')
				: null;
		const STATUS_LIST_COLOR = CONSTANTS.STATUS_LIST_COLOR;

		if (paymentDate) {
			return STATUS_LIST_COLOR.RECEIVED_ALL;
		} else {
			if (now.isBefore(expirationDate, 'day')) {
				return STATUS_LIST_COLOR.ON_DATE;
			} else if (now.isAfter(expirationDate, 'day')) {
				return STATUS_LIST_COLOR.LATE_PAYMENT;
			} else {
				return STATUS_LIST_COLOR.TODAY;
			}
		}
	}

	// se estiver pago então é somente para visualizar o registro, seta informações de visualização
	formAccount(account: AccountBillsToPayModel, paid = false) {
		if (!account) {
			this.account = new AccountBillsToPayModel();
			this.account.tipoCusto = FINANCIAL_CONST.VARIABLE;
			this.account.numeroTotalParcelas = 1;
			this.account.dataCompetencia = this.service.parseDatePicker(
					moment().format('DD/MM/YYYY'),
			);
			this.account.dataVencimento = this.service.parseDatePicker(
					moment().format('DD/MM/YYYY'),
			);
			this.account.formaDeRecebimento = this.localConst.DINHEIRO;

			this.showAccount = true;
			setTimeout(() => {
				document.querySelector('#modal-account-pay').classList.add('md-show');
			}, 500);
		} else {
			this.service.loading(true);
			this.service
					.httpGET(
							CONSTANTS.ENDPOINTS.financial.billsToPay.findOne +
							account.id,
					)
					.subscribe(
							data => {
								this.account = (data.body) as AccountBillsToPayModel;

								this.service.loading(false);
								this.showAccount = true;
								this.currentAccountIsPaid = paid;
								setTimeout(() => {
									document
											.querySelector('#modal-account-pay')
											.classList.add('md-show');
								}, 500);
							},
							err => {

								this.service.loading(false);
								this.service.notification.error(
										this.service.translate.instant('COMMON.ERROR.SEARCH'),
										err.error.error,
								);
							},
					);
		}
	}

	haveFilters() {
		return this.searchParam.formaDeRecebimento.length > 0 ||
				this.searchParam.tipoLancamento.length > 0 ||
				(this.searchParam.categorias && this.searchParam.categorias.length > 0) ||
				this.searchParam.descricao ||
				this.searchParam.numeroDocumento;
	}

	processResultModal(event: any[]) {
		this.searchParam.categorias = [];
		if (event.length > 0) {
			this.categoryToDisplay =
					event.length +
					' ' +
					this.service.translate.instant(
							'FINANCIAL.BILLS_TO_PAY.CATEGORY_SELECTED',
					);

			event.forEach(row => {
				this.searchParam.categorias = [
					...this.searchParam.categorias,
					row.id,
				];
			});
		} else {
			this.categoryToDisplay = this.service.translate.instant(
					'FINANCIAL.BILLS_TO_PAY.ALL_CAT',
			);
		}
		this.selectedCategory = event;
	}

	togglePay(account: BillsToPayTableModel) {
		if (account.dataPagamento) {
			this.reversePayment(account);
		} else {
			this.pay(account);
		}
	}

	reversePayment(account: BillsToPayTableModel) {
		const translate = this.service.translate;
		const refSWAL = 'FINANCIAL.BILLS_TO_PAY.SWAL';

		Swal.fire({
			title: translate.instant(`${refSWAL}.TITLE`),
			text: translate.instant(`${refSWAL}.TEXT`),
			type: 'question',
			showCancelButton: true,
			confirmButtonText: translate.instant(`${refSWAL}.CONFIRM_BUTTON`),
			cancelButtonText: translate.instant(`${refSWAL}.CANCEL_BUTTON`),
		}).then(result => {
			if (result.value) {
				this.service.loading(true);
				const ref =
						CONSTANTS.ENDPOINTS.financial.billsToPay.reverse;

				this.service.httpPUT(ref, { id: account.id }).subscribe(
						data => {
							this.service.loading(false);
							this.service.notification.info(
									this.service.translate.instant(
											'PATIENTS.FORM_CHECKING_ACCOUNT.REVERSE_TITLE',
									),
									this.service.translate.instant(
											'PATIENTS.FORM_CHECKING_ACCOUNT.REVERSE_MSG',
									),
							);
							this.populateTable();
						},
						err => {

							this.service.loading(false);
							this.service.notification.error(
									this.service.translate.instant('COMMON.ERROR.SEARCH'),
									err.error.error,
							);
						},
				);
			}
		});
	}

	pay(account: BillsToPayTableModel) {
		this.accountTable = account;
		this.showAccountPay = true;
		setTimeout(() => {
			document.querySelector('#modal-account-payment').classList.add('md-show');
		}, 500);
	}

	processResultModalPayment(event) {
		if (event) {
		}

		this.showAccountPay = false;
	}

	processDeleteModal(refresh: boolean | null | undefined) {
		if (refresh) {
			this.populateTable();
		}
	}

	async processResultModalAccount(event) {
		const refCat = await this.service.httpGET(
				CONSTANTS.ENDPOINTS.config.financial.category,
		).toPromise();
		this.category = refCat.body as CategoryInterface[];
		if (event) {
			this.populateTable();
		}

		this.showAccount = false;
		this.currentAccountIsPaid = false;
	}

	removeAllFilters() {
		this.searchParam.categorias = [];
		this.searchParam.formaDeRecebimento = [];
		this.searchParam.tipoLancamento = [];
		this.searchParam.numeroDocumento = null;
		this.searchParam.descricao = null;
		this.populateTable();
	}

	disableAccountFormsIfNecessary(account) {

	}

	accountIsPaid(account): boolean {
		return this.getStatus(account) === 'Pago' && account.dataPagamento;
	}

	async generateReport(type: ReportType) {
		if (!this._commonsReportService.checkIfPrintIsAvaliable(this.rowsPage)) {
			return;
		}

		const loading = this._globalSpinnerService.loadingManager.start();
		try {
			const pdfMake = await BillsToPayComponent.buildReport(this.rowsPage, this.searchParam, this.totals, this._commonsReportService, type);
			const pdfMakeDefinition = await this._pdfGeneratorService.exportPDFMakeAsPDF(pdfMake, '', 'ContasAPagar.pdf');
			pdfMakeDefinition.open();
		} catch (e) {
			this._notificationsService.error('Ocorreu um erro ao emitir o relatório')
		} finally {
			loading.finished();
		}
	}

	static async buildReport(bills: AccountBillsToPayModel[], filters: ContasAPagarFilters, billsResumeValue: TotalizersInfo, commonsReportService: CommonsReportBuildService, type: ReportType): Promise<PDFMake> {
		let title = 'Relatório de Contas a Pagar';
		const pdfMake: PDFMake = { afterContent: [], pageOrientation: 'landscape' };
		pdfMake.styles = REPORT_STYLES;

		if (type === ReportType.ANALYTICAL) {
			pdfMake.styles.title.fontSize = 18;
			pdfMake.pageOrientation = 'portrait';
			title = 'Relatório de Contas a Pagar - Categoria';
		}

		pdfMake.afterContent.push(await BillsToPayComponent._buildReportCommonHeader(title, filters, commonsReportService));
		pdfMake.afterContent.push(await BillsToPayComponent._buildReportBody(bills, type));
		pdfMake.afterContent.push(await BillsToPayComponent._buildReportResume(billsResumeValue, type));

		return pdfMake;
	}

	private static _buildReportBody(bills: AccountBillsToPayModel[], type: ReportType) {
		if (type === ReportType.ANALYTICAL) {
			const groupedBills = groupBy(bills, 'categoria');
			return this._buildAnalyticalReportBody(groupedBills);
		} else {
			return this._buildSyntheticReportBody(bills)
		}
	}

	private static _buildAnalyticalReportBody(groupedBills: Dictionary<AccountBillsToPayModel[]>) {
		let content = [];
		forEach(groupedBills, (bills: AccountBillsToPayModel[], categoria: string) => {
			const totalCliente = bills.map(bill => bill.valor).reduce((sum, value) => sum + value);
			content.push(
					{ canvas: [{ type: 'line', x1: 0, y1: -1, x2: 555, y2: -1, lineWidth: 1 }], margin: [0, 5, 0, 10] },
					{
						text: [
							{ text: 'Categoria: ', bold: true, color: GREY_COLOR },
							categoria,
						], margin: [0, 0, 0, 10],
					},
					{
						table: {
							widths: [60, 60, 170, 45, 100, 70],
							body: [
								[
									{ text: 'Vencimento', style: 'tableHeader', color: GREY_COLOR },
									{ text: 'Pagamento', style: 'tableHeader', color: GREY_COLOR },
									{ text: 'Descrição', style: 'tableHeader', color: GREY_COLOR, alignment: 'left' },
									{ text: 'Parcela', style: 'tableHeader', color: GREY_COLOR },
									{ text: 'Forma Pagamento', style: 'tableHeader', color: GREY_COLOR, alignment: 'left' },
									{ text: 'Valor', style: 'tableHeader', color: GREY_COLOR, alignment: 'right' },
								],
								...this._buildAnalyticalReportRows(bills),
							],
						},
						layout: 'noBorders',
					},
					{
						columns: [
							{
								stack: [
									{ text: 'Quantidade de parcelas:', style: 'resumeHeader', color: BLUE_COLOR },
								],
							},
							{
								width: 50,
								alignment: 'right',
								stack: [
									{ text: bills.length, style: 'resumeRow' },
								],
							},
							{
								width: 250,
								alignment: 'right',
								stack: [
									{ text: 'Total Categoria (R$): ', style: 'resumeHeader', color: BLUE_COLOR },
								],
							},
							{
								alignment: 'right',
								stack: [
									{ text: totalCliente.toFixed(2), style: 'resumeRow' },
								],
							},
						],
						margin: [0, 10, 10, 0],
					},
			);
		})

		return content;
	}

	private static _buildAnalyticalReportRows(bills: AccountBillsToPayModel[]) {
		return bills.map(bill =>
				[
					{ text: bill.dataVencimento, style: 'tableRow', alignment: 'center' },
					{ text: bill.dataPagamento, style: 'tableRow', alignment: 'center' },
					{ text: bill.descricao, style: 'tableRow' },
					{ text: bill.numeroParcela ? bill.numeroParcela + ' / ' + bill.numeroTotalParcelas : ' ', style: 'tableRow', alignment: 'center' },
					{ text: descricoesPagamentos[bill.formaDeRecebimento], style: 'tableRow' },
					{ text: bill.valor.toFixed(2), style: 'tableRow', alignment: 'right' },
				],
		);
	}

	private static _buildSyntheticReportBody(bills: AccountBillsToPayModel[]) {
		return {
			table: {
				widths: [60, 60, 200, 200, 45, 105, 65],
				body: [
					[
						{ text: 'Vencimento', style: 'tableHeader' },
						{ text: 'Pagamento', style: 'tableHeader' },
						{ text: 'Categoria', style: 'tableHeader' },
						{ text: 'Descrição', style: 'tableHeader' },
						{ text: 'Parcela', style: 'tableHeader' },
						{ text: 'Forma Pagamento', style: 'tableHeader' },
						{ text: 'Valor', style: 'tableHeader' },
					],
					...this._buildSyntheticReportRows(bills),
				],
			},
		}
	}

	private static _buildSyntheticReportRows(bills: AccountBillsToPayModel[]) {
		return bills.map(bill =>
				[
					{ text: bill.dataVencimento, style: 'tableRow', alignment: 'center' },
					{ text: bill.dataPagamento, style: 'tableRow', alignment: 'center' },
					{ text: bill.categoria, style: 'tableRow' },
					{ text: bill.descricao, style: 'tableRow' },
					{ text: bill.numeroParcela ? bill.numeroParcela + ' / ' + bill.numeroTotalParcelas : ' ', style: 'tableRow', alignment: 'center' },
					{ text: descricoesPagamentos[bill.formaDeRecebimento], style: 'tableRow' },
					{ text: bill.valor.toFixed(2), style: 'tableRow', alignment: 'right' },
				],
		);
	}

	private static async _buildReportCommonHeader(title: string, filters: ContasAPagarFilters, commonsReportService: CommonsReportBuildService) {
		return [
			await commonsReportService.buildFinancialReportHeader(title),
			CommonsReportBuildService.buildPeriodo([this._ngbDateStructToDate(filters.dataInicio), this._ngbDateStructToDate(filters.dataFim)]),
			CommonsReportBuildService.buildContasTipoRecebimento(filters.tipoLancamento),
			{ ...CommonsReportBuildService.buildFormaPagamento(filters.formaDeRecebimento), margin: [0, 0, 0, 10] },
		]
	}

	private static _ngbDateStructToDate(date: NgbDateStruct | string): Date {
		const { year, day, month } = typeof date === 'string' ?
				{ year: +date.split('/')[2], month: +date.split('/')[1], day: +date.split('/')[0] } :
				date;
		return new Date(year, month - 1, day);
	}

	private static _buildReportResume(billsResumeValue: TotalizersInfo, type: ReportType) {
		return type === ReportType.ANALYTICAL ? this._buildAnalyticalReportResume(billsResumeValue) : this._buildSyntheticReportResume(billsResumeValue);
	}

	private static _buildAnalyticalReportResume(billsResumeValue: TotalizersInfo) {
		return [
			{
				columns: [
					{
						width: 500,
						alignment: 'right',
						stack: [
							{ text: 'Total Geral: ', style: 'resumeHeader', color: BLUE_COLOR },
						],
					},
					{
						alignment: 'right',
						stack: [
							{ text: billsResumeValue.received.toFixed(2), style: 'resumeRow' },
						],
					},
				],
				margin: [0, 30, 0, 0],
			},
			{ canvas: [{ type: 'line', x1: 450, y1: -1, x2: 555, y2: -1, lineWidth: 1 }], margin: [0, 5, 0, 0] },
		]
	}

	private static _buildSyntheticReportResume(billsResumeValue: TotalizersInfo) {
		return {
			columns: [
				{
					width: 700,
					alignment: 'right',
					stack: [
						{ text: 'Pagas (R$):', style: 'resumeHeader', color: GREEN_COLOR },
						{ text: 'A Pagar (R$):', style: 'resumeHeader', color: BLUE_COLOR },
						{ text: 'Total despesas (R$):', style: 'resumeHeader' },
					],
				},
				{
					alignment: 'right',
					stack: [
						{ text: billsResumeValue.received.toFixed(2), style: 'resumeRow', color: GREEN_COLOR },
						{ text: (billsResumeValue.total - billsResumeValue.received).toFixed(2), style: 'resumeRow', color: BLUE_COLOR },
						{ text: billsResumeValue.total.toFixed(2), style: 'resumeRow' },
					],
				},
			],
			margin: [0, 30, 0, 0],
		}
	}
}
