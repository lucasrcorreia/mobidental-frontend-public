import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProstheticComponent } from './prosthetic.component';

describe('ProstheticComponent', () => {
  let component: ProstheticComponent;
  let fixture: ComponentFixture<ProstheticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProstheticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProstheticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
