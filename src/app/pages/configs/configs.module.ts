import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigsRoutingModule } from './configs-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, ConfigsRoutingModule]
})
export class ConfigsModule {}
