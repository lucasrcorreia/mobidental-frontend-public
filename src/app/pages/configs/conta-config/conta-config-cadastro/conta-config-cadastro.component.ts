import { Component, Input } from '@angular/core';
import { ContaModel } from '../../../../api/conta-api.service';
import { imasks } from '../../../../lib/input-masks';
import { values } from 'ramda';
import { Uf } from '../../../../lib/uf';

@Component({
  selector: 'app-conta-config-cadastro',
  templateUrl: './conta-config-cadastro.component.html',
  styleUrls: ['./conta-config-cadastro.component.scss'],
})
export class ContaConfigCadastroComponent {

  readonly cpfCnpjMask = imasks.cpfCnpj();
  readonly phoneNumberMask = imasks.phoneNumber();
  readonly cepMask = imasks.cep();

  readonly ufs = values(Uf);

  @Input() conta: Partial<ContaModel> = {};

}
