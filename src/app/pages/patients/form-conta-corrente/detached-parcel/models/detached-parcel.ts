import { FormaPagamento } from "../../../../../api/model/forma-pagamento";
import { TipoParcela } from "../../../../../api/model/tipo-parcela";

export interface DetachedParcel {
	id?: number;
	pacienteId?: number;
	dataEmissao?: string;
	descricao?: string;
	valor?: number;
	dataVencimento?: string;
	convenioId?: number;
	formaDeRecebimento?: FormaPagamento;
	tipoParcelaReceber?: TipoParcela;
	numeroParcela?: null;
	recorrenciaId?: null;
	usuarioId?: null;
	juros?: number;
	tipoRecorrencia?: string;
	quantidadeOcorrencia?: number
}
