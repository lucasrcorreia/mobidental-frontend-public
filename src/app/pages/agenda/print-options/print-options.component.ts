import { LISTS } from './../lists';
import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { UtilsService } from './../../../services/utils/utils.service';
import { ShortListItem } from '../../../core/models/forms/common/common.model';
import * as moment from 'moment';
import { CONSTANTS } from '../../../core/constants/constants';
import { classToPlain } from 'class-transformer';
import { environment } from '../../../../environments/environment';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-print-options',
  templateUrl: './print-options.component.html',
  styleUrls: [
    './print-options.component.scss',
    './../../../../assets/icon/icofont/css/icofont.scss'
  ]
})
export class PrintOptionsComponent implements OnInit, OnChanges {
  @Input() professionals: ShortListItem[] = [];
  @Input() initDate: NgbDateStruct;
  @Input() endDate: NgbDateStruct;

  searchParam = {
    dataInicial: this.service.parseDatePicker(moment().format('DD/MM/YYYY')),
    dataFinal: this.service.parseDatePicker(moment().format('DD/MM/YYYY')),
    profissionalId: null,
    pacientePessoaNome: null,
    listaStatusAgendamento: [
      'MARCADO',
      'REMARCADO',
      'AGUARDANDO',
      'CONFIRMADO',
      'ATENDIDO',
      'EM_ATENDIMENTO',
      'FALTOU',
      'PACIENTE_DESMARCOU',
      'PROFISSIONAL_DESMARCOU'
    ]
  };

  localLists = new LISTS(this.service.translate);

  constructor(public service: UtilsService) {}

  ngOnInit() {}

  ngOnChanges() {
    this.searchParam.dataInicial = this.initDate;
    this.searchParam.dataFinal = this.endDate;


  }

  print() {
    this.service.loading(true);

    /*
    const objStatus = {};
    this.searchParam.listaStatusAgendamento.map((item, index) => {
      objStatus[index] = item;
    });
    */

    const params = classToPlain(this.searchParam) as any;
    // params.listaStatusAgendamento = objStatus;

    params.dataInicial = this.service.parseDateFromDatePicker(
      this.searchParam.dataInicial
    );
    params.dataFinal = this.service.parseDateFromDatePicker(
      this.searchParam.dataFinal
    );



    this.service.httpPOST(CONSTANTS.ENDPOINTS.agenda.print, params).subscribe(
      data => {
        window.open(
          environment.host +
            CONSTANTS.ENDPOINTS.agenda.urlAgendaPdf +
            (data.body as any).token
        );
        this.service.loading(false);
      },
      err => {

        this.service.loading(false);
        this.service.notification.error(
          this.service.translate.instant('COMMON.ERROR.SEARCH'),
          err.error.error
        );
      }
    );
  }
}
