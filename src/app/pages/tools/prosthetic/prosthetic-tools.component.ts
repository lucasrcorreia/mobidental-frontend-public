import { AccountBillsToPayModel } from "./../../../core/models/financial/billsToPay/billsToPay.model";
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { UtilsService } from "../../../services/utils/utils.service";
import { ColumnsTableModel } from "../../../core/models/table/columns.model";
import { CONSTANTS } from "../../../core/constants/constants";
import { forkJoin, Subject } from "rxjs";
import { ShortListItem } from "../../../core/models/forms/common/common.model";
import {
	ProstheticServiceModel,
	ProstheticServiceObjectModel,
} from "../../../core/models/tools/prosthetic/prostheticService.model";
import { CustomDatepickerI18n, I18n } from "../../../shared/ngb-datepicker/datepicker-i18n";
import { NgbDateFRParserFormatter } from "../../../shared/ngb-datepicker/ngb-date-fr-parser-formatter";
import { NgbDateParserFormatter, NgbDatepickerI18n } from "@ng-bootstrap/ng-bootstrap";
import {
	CategoryInterface,
	CenterCostInterface,
} from "../../../core/models/config/financial/financial.model";
import * as moment from "moment";
import Swal from "sweetalert2";
import { FINANCIAL_CONST } from "../../financial/constants";
import { takeUntil } from "rxjs/operators";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { ProteticosApiService } from "../../../api/proteticos.api.service";
import { GlobalSpinnerService } from "../../../lib/global-spinner.service";
import { NotificationsService } from "angular2-notifications";
import { errorToString } from "../../../lib/helpers/error-to-string";

const STATUS_ABERTO = "ABERTO";

@Component({
	selector: "app-prosthetic",
	templateUrl: "./prosthetic-tools.component.html",
	styleUrls: [
		"./prosthetic-tools.component.scss",
		"./../../../../assets/icon/icofont/css/icofont.scss",
	],
	providers: [
		I18n,
		{ provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
		{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
	],
})
export class ProstheticToolsComponent implements OnInit, OnDestroy {
	@ViewChild(DatatableComponent) table: DatatableComponent;

	searchRange = "TODAY";
	BY_RANGE = "BY_RANGE";

	locale = this.service.translate.instant("COMMON.LOCALE");

	columns: ColumnsTableModel[] = [
		{
			action: true,
			name: this.service.translate.instant("TOOLS.PROSTHETIC.TABLE.COL1"),
			prop: "id",
			sortable: false,
			width: 150,
		},
		{
			action: false,
			name: this.service.translate.instant("TOOLS.PROSTHETIC.TABLE.COL2"),
			prop: "dataSolicitacao",
			sortable: true,
			width: 120,
		},
		{
			action: false,
			name: this.service.translate.instant("TOOLS.PROSTHETIC.TABLE.COL3"),
			prop: "itemServicoProteticoNome",
			sortable: true,
			width: 200,
		},
		{
			action: false,
			name: this.service.translate.instant("TOOLS.PROSTHETIC.TABLE.COL4"),
			prop: "valor",
			sortable: true,
			width: 120,
			currency: true,
		},
		{
			action: false,
			name: this.service.translate.instant("TOOLS.PROSTHETIC.TABLE.COL5"),
			prop: "pacientePessoaNome",
			sortable: true,
			width: 250,
		},
		{
			action: false,
			name: this.service.translate.instant("TOOLS.PROSTHETIC.TABLE.COL6"),
			prop: "proteticoNome",
			sortable: true,
			width: 150,
		},
		{
			action: false,
			name: this.service.translate.instant("TOOLS.PROSTHETIC.TABLE.COL7"),
			prop: "profissionalNome",
			sortable: true,
			width: 150,
		},
		{
			action: false,
			name: this.service.translate.instant("TOOLS.PROSTHETIC.TABLE.COL8"),
			prop: "dataPrevistaEntrega",
			sortable: true,
			width: 150,
		},
		{
			action: false,
			name: "Data Entrega",
			prop: "dataEntrega",
			sortable: true,
			width: 150,
		},
	];

	listSearchRange = [
		{
			label: this.service.translate.instant("FINANCIAL.LIST_SEARCH.RANGE.TODAY"),
			value: "TODAY",
		},
		{
			label: this.service.translate.instant("FINANCIAL.LIST_SEARCH.RANGE.WEEK"),
			value: "WEEK",
		},
		{
			label: this.service.translate.instant("FINANCIAL.LIST_SEARCH.RANGE.MONTH"),
			value: "MONTH",
		},
		{
			label: this.service.translate.instant("FINANCIAL.LIST_SEARCH.RANGE.PREV_MONTH"),
			value: "PREV_MONTH",
		},
		{
			label: this.service.translate.instant("FINANCIAL.LIST_SEARCH.RANGE.BY_RANGE"),
			value: "BY_RANGE",
		},
	];

	rows = {};
	rowsPage = [];

	searchParam = {
		dataFim: null,
		dataInicio: null,
		pacienteNome: "",
		profissionalId: null,
		proteticoId: null,
		statusServicoProtetico: "ABERTO",
	};

	listStatus = [
		{
			label: this.service.translate.instant("TOOLS.PROSTHETIC.ABERTO"),
			value: "ABERTO",
		},
		{
			label: this.service.translate.instant("TOOLS.PROSTHETIC.RECEBIDO"),
			value: "RECEBIDO",
		},
	];

	page = {
		size: 100,
		totalElements: 0,
		totalPages: 0,
		pageNumber: 0,
	};

	showModalCreate = false;
	showModalConfirm = false;
	showAccount = false;
	justPay = false;
	listProsthetic: Array<ShortListItem> = [];
	listProferssional: Array<ShortListItem> = [];
	serviceProsthetic: ProstheticServiceObjectModel;

	centerCost: CenterCostInterface[];
	category: CategoryInterface[];
	account: AccountBillsToPayModel;
	private destroy$ = new Subject();

	constructor(
		public service: UtilsService,
		private cd: ChangeDetectorRef,
		private readonly _proteticosApiService: ProteticosApiService,
		private readonly _globalSpinnerService: GlobalSpinnerService,
		private readonly _notificationsService: NotificationsService
	) {
		this.service.toggleMenu.pipe(takeUntil(this.destroy$)).subscribe((data) => {
			if (data) {
				this.updateDataTable();
			}
		});
	}

	ngOnInit() {
		this.searchParam = {
			dataFim: moment().format("DD/MM/YYYY"),
			dataInicio: moment().format("DD/MM/YYYY"),
			pacienteNome: "",
			profissionalId: null,
			proteticoId: null,
			statusServicoProtetico: "ABERTO",
		};

		this.initLists();
	}

	ngOnDestroy() {
		this.destroy$.next();
		this.destroy$.complete();
	}

	updateDataTable() {
		this.cd.markForCheck();
		setTimeout(() => {
			this.table.recalculate();
			(this.table as any).cd.markForCheck();
		});
	}

	initLists() {
		const prosthetic = this.service.httpGET(
			CONSTANTS.ENDPOINTS.tools.prosthetic.dropdownProsthetic
		);
		const professionals = this.service.httpGET(CONSTANTS.ENDPOINTS.professional.active);

		forkJoin([prosthetic, professionals]).subscribe(
			(list) => {
				this.listProsthetic = list[0].body as Array<ShortListItem>;
				this.listProferssional = list[1].body as Array<ShortListItem>;
				this.populateTable();
			},
			(err) => {
				this.service.loading(false);
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			}
		);
	}

	changeStatus() {
		this.rows = {};
		this.rowsPage = [];
		this.populateTable();
	}

	populateTable() {
		this.service.loading(true);
		const param = { ...this.searchParam };

		if (this.searchRange === this.BY_RANGE) {
			param.dataInicio = this.service.parseDateFromDatePicker(this.searchParam.dataInicio);
			param.dataFim = this.service.parseDateFromDatePicker(this.searchParam.dataFim);
		}

		this.service.httpPOST(CONSTANTS.ENDPOINTS.tools.prosthetic.table, param).subscribe(
			(data) => {
				this.rowsPage = data.body as ProstheticServiceModel[];
				this.page.pageNumber = 0;
				this.page.size = this.rowsPage.length;
				this.page.totalElements = this.rowsPage.length;
				this.page.totalPages = 1;
				this.service.loading(false);
			},
			(err) => {
				this.service.loading(false);
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			}
		);
	}

	onSelectChange() {
		this.populateTable();
	}

	onrangeChange() {
		switch (this.searchRange) {
			case "TODAY":
				this.searchParam.dataInicio = moment().format("DD/MM/YYYY");
				this.searchParam.dataFim = moment().format("DD/MM/YYYY");
				this.populateTable();
				break;
			case "WEEK":
				this.searchParam.dataInicio = moment().startOf("week").format("DD/MM/YYYY");
				this.searchParam.dataFim = moment().endOf("week").format("DD/MM/YYYY");
				this.populateTable();
				break;
			case "MONTH":
				this.searchParam.dataInicio = moment().startOf("month").format("DD/MM/YYYY");
				this.searchParam.dataFim = moment().endOf("month").format("DD/MM/YYYY");
				this.populateTable();
				break;
			case "PREV_MONTH":
				this.searchParam.dataInicio = moment()
					.subtract(1, "month")
					.startOf("month")
					.format("DD/MM/YYYY");
				this.searchParam.dataFim = moment()
					.subtract(1, "month")
					.endOf("month")
					.format("DD/MM/YYYY");
				this.populateTable();
				break;
			case this.BY_RANGE:
				this.searchParam.dataInicio = this.service.parseDatePicker(
					moment().startOf("month").format("DD/MM/YYYY")
				);
				this.searchParam.dataFim = this.service.parseDatePicker(moment().format("DD/MM/YYYY"));
				this.populateTable();
				break;

			default:
				break;
		}
	}

	validateDates() {
		const init = moment(
			this.service.parseDateFromDatePicker(this.searchParam.dataInicio),
			"DD/MM/YYYY"
		);
		const end = moment(
			this.service.parseDateFromDatePicker(this.searchParam.dataFim),
			"DD/MM/YYYY"
		);

		if (end.isBefore(init)) {
			return false;
		} else {
			return true;
		}
	}

	applyDate(event) {
		if (
			["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"].includes(event.key) &&
			this.searchParam.dataFim &&
			this.searchParam.dataFim.year &&
			this.searchParam.dataFim.year > 2010 &&
			this.searchParam.dataInicio &&
			this.searchParam.dataInicio.year &&
			this.searchParam.dataInicio.year > 2010
		) {
			if (this.validateDates) {
				this.populateTable();
			}
		}
	}

	onDateSelect() {
		if (this.validateDates()) {
			this.populateTable();
		}
	}

	formService(serviceProsthetic: ProstheticServiceModel) {
		if (serviceProsthetic) {
			this.service.loading(true);
			this.service
				.httpGET(CONSTANTS.ENDPOINTS.tools.prosthetic.findOne + serviceProsthetic.id)
				.subscribe(
					(data) => {
						this.serviceProsthetic = data.body as ProstheticServiceObjectModel;
						this.showForm();
						this.service.loading(false);
					},
					(err) => {
						this.service.loading(false);
						this.service.notification.error(
							this.service.translate.instant("COMMON.ERROR.SEARCH"),
							err.error.error
						);
					}
				);
		} else {
			this.serviceProsthetic = new ProstheticServiceObjectModel();
			this.showForm();
		}
	}

	showForm() {
		this.showModalCreate = true;
		setTimeout(() => {
			document.querySelector("#modal-create-service-prosthetic").classList.add("md-show");
		}, 500);
	}

	doneService(id: number) {
		this.service.loading(true);
		this.service.httpGET(CONSTANTS.ENDPOINTS.tools.prosthetic.findOne + id.toString()).subscribe(
			(data) => {
				this.serviceProsthetic = data.body as ProstheticServiceObjectModel;
				this.showModalConfirm = true;
				this.service.loading(false);
				setTimeout(() => {
					document.querySelector("#modal-confirm-service-prosthetic").classList.add("md-show");
				}, 500);
			},
			(err) => {
				this.service.loading(false);
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			}
		);
	}

	initDropdownsAccount() {
		this.service.loading(true);

		const refCCost = this.service.httpGET(CONSTANTS.ENDPOINTS.config.financial.centerCost);
		const refCat = this.service.httpGET(CONSTANTS.ENDPOINTS.config.financial.category);

		forkJoin([refCCost, refCat]).subscribe(
			(list) => {
				this.centerCost = list[0].body as any;
				this.category = list[1].body as any;

				this.service.loading(false);

				setTimeout(() => {
					document.querySelector("#modal-account-pay").classList.add("md-show");
				}, 500);
			},
			(err) => {
				this.service.loading(false);
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			}
		);
	}

	processResultModalCreate(event) {
		if (!event) {
			return;
		}

		this.service.notification.success(
			this.service.translate.instant("TOOLS.PROSTHETIC.TOAST.SUCCESS.TITLE"),
			this.service.translate.instant("TOOLS.PROSTHETIC.TOAST.SUCCESS.SUBTITLE")
		);

		this.showModalCreate = false;
		this.rowsPage = [];
		this.populateTable();
	}

	processResultModalConfirm(event) {
		if (event) {
			this.serviceProsthetic = event as ProstheticServiceObjectModel;

			if (this.serviceProsthetic.dataPagamento) {
				this.justPay = false;
				this.prepareAccount();
			} else {
				this.service.notification.success(
					this.service.translate.instant("TOOLS.PROSTHETIC.TOAST.SUCCESS.TITLE"),
					this.service.translate.instant("TOOLS.PROSTHETIC.TOAST.SUCCESS.SUBTITLE")
				);

				this.rowsPage = [];
				this.populateTable();
			}
		}

		this.showModalConfirm = false;
	}

	prepareAccount() {
		this.showAccount = true;
		this.account = new AccountBillsToPayModel();

		this.account.dataCompetencia = this.service.parseDatePicker(
			this.serviceProsthetic.dataPagamento
		);

		this.account.dataVencimento = this.service.parseDatePicker(
			this.serviceProsthetic.dataPagamento
		);

		this.account.dataPagamento = this.service.parseDatePicker(this.serviceProsthetic.dataPagamento);

		this.account.descricao =
			this.service.translate.instant("TOOLS.PROSTHETIC.TITLE_ACCOUNT") +
			" - " +
			this.serviceProsthetic.proteticoNome +
			" - " +
			this.serviceProsthetic.itemServicoProteticoNome;
		this.account.numeroTotalParcelas = 1;
		this.account.valor = this.serviceProsthetic.valor;
		this.account.juros = 0;
		this.account.desconto = 0;
		this.account.valorTotal = this.serviceProsthetic.valor;
		this.account.servicoProteticoId = this.serviceProsthetic.id;
		this.account.formaDeRecebimento = FINANCIAL_CONST.DINHEIRO;
		this.account.tipoCusto = FINANCIAL_CONST.VARIABLE;

		this.initDropdownsAccount();
	}

	async processResultModalAccount(event) {
		const refCat = await this.service
			.httpGET(CONSTANTS.ENDPOINTS.config.financial.category)
			.toPromise();
		this.category = refCat.body as CategoryInterface[];
		if (event) {
			this.service.notification.success(
				this.service.translate.instant("TOOLS.PROSTHETIC.TOAST.SUCCESS.TITLE"),
				this.service.translate.instant("TOOLS.PROSTHETIC.TOAST.SUCCESS.SUBTITLE")
			);

			this.rowsPage = [];
			this.populateTable();
		} else {
			if (!this.justPay) {
				this.serviceProsthetic.dataPagamento = null;
				this.service.loading(true);
				this.service
					.httpPOST(CONSTANTS.ENDPOINTS.tools.prosthetic.save, this.serviceProsthetic)
					.subscribe(
						(data) => {
							this.service.loading(false);
							this.service.notification.warn(
								this.service.translate.instant("TOOLS.PROSTHETIC.TOAST.WARN_ACCOUNT.TITLE"),
								this.service.translate.instant("TOOLS.PROSTHETIC.TOAST.WARN_ACCOUNT.SUBTITLE")
							);

							this.rowsPage = [];
							this.populateTable();
						},
						(err) => {
							this.service.loading(false);
							this.service.notification.error(
								this.service.translate.instant("COMMON.ERROR.SEARCH"),
								err.error.error
							);
						}
					);
			}
		}

		this.showAccount = false;
	}

	pay(id: number) {
		this.service.loading(true);
		this.service.httpGET(CONSTANTS.ENDPOINTS.tools.prosthetic.findOne + id.toString()).subscribe(
			(data) => {
				this.justPay = true;
				this.serviceProsthetic = data.body as ProstheticServiceObjectModel;
				this.serviceProsthetic.dataPagamento = moment().format("DD/MM/YYYY");
				this.prepareAccount();
			},
			(err) => {
				this.service.loading(false);
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			}
		);
	}

	async delete(service: ProstheticServiceObjectModel) {
		const result = await Swal.fire({
			title: "Apagar serviço protético",
			text: "Tem certeza que quer apagar esse serviço protético?",
			type: "question",
			showCancelButton: true,
			confirmButtonText: "Sim, apagar",
			cancelButtonText: "Não, cancelar",
		});

		if (!result.value) {
			return;
		}

		const loadingToken = this._globalSpinnerService.loadingManager.start();

		try {
			await this._proteticosApiService.apagar(service.id);
			this.populateTable();
		} catch (e) {
			this._notificationsService.error("Erro ao apagar serviço protético", errorToString(e));
		} finally {
			loadingToken.finished();
		}
	}

	async removeDelivery(service: ProstheticServiceObjectModel) {
		const refSWAL = "TOOLS.PROSTHETIC.SWAL";

		const result = await Swal.fire({
			title: this.service.translate.instant(`${refSWAL}.TITLE_REMOVE`),
			text: this.service.translate.instant(`${refSWAL}.TEXT_REMOVE`),
			type: "question",
			showCancelButton: true,
			confirmButtonText: this.service.translate.instant(`${refSWAL}.CONFIRM_BUTTON_REMOVE`),
			cancelButtonText: this.service.translate.instant(`${refSWAL}.CANCEL_BUTTON`),
		});

		if (!result.value) {
			return;
		}

		const token = this._globalSpinnerService.loadingManager.start();

		this.service
			.httpGET(CONSTANTS.ENDPOINTS.tools.prosthetic.findOne + service.id.toString())
			.subscribe(
				(data) => {
					this.serviceProsthetic = data.body as ProstheticServiceObjectModel;
					this.serviceProsthetic.dataEntrega = null;
					this.serviceProsthetic.statusServicoProtetico = STATUS_ABERTO;

					this.service
						.httpPOST(CONSTANTS.ENDPOINTS.tools.prosthetic.save, this.serviceProsthetic)
						.subscribe(
							() => {
								this.populateTable();
								this.service.notification.info(
									this.service.translate.instant("TOOLS.PROSTHETIC.TOAST.REMOVED.TITLE"),
									this.service.translate.instant(
										"TOOLS.PROSTHETIC.TOAST.REMOVED.SUBTITLE_WITHOUT_PAYMENT"
									)
								);
							},
							(err) => {
								this.service.notification.error(
									this.service.translate.instant("COMMON.ERROR.SEARCH"),
									err.error.error
								);
							},
							() => token.finished()
						);
				},
				(err) => {
					this.service.notification.error(
						this.service.translate.instant("COMMON.ERROR.SEARCH"),
						err.error.error
					);
				},
				() => token.finished()
			);
	}
}
