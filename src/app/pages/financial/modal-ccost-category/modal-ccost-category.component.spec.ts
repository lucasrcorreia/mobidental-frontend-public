import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCcostCategoryComponent } from './modal-ccost-category.component';

describe('ModalCcostCategoryComponent', () => {
  let component: ModalCcostCategoryComponent;
  let fixture: ComponentFixture<ModalCcostCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCcostCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCcostCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
