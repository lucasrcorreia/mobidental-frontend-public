import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationRoutingModule } from './registration-routing.module';
import { SharedModule } from '../../../shared/shared.module';
import { UserRegistrationComponent } from './user-registration.component';
import { AppLibModule } from '../../../lib/app-lib.module';
import { NgxMaskModule } from 'ngx-mask';
import { FormsModule } from '@angular/forms';
import { IMaskModule } from 'angular-imask';

@NgModule({
  declarations: [UserRegistrationComponent],
  imports: [
    CommonModule,
    AppLibModule,
    NgxMaskModule,
    FormsModule,
    RegistrationRoutingModule,
    SharedModule,
    IMaskModule,
  ],
})
export class RegistrationModule {}
