import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountComponent } from './account.component';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { SharedModule } from '../../../../shared/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxSelectModule } from 'ngx-select-ex';
import { NgxTranslateModule } from '../../../../core/translate/translate.module';
import { NgxCurrencyModule } from 'ngx-currency';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputMaskModule } from 'racoon-mask-raw';
import { ModalCcostCategoryModule } from '../../modal-ccost-category/modal-ccost-category.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PipesModule } from '../../../../core/pipes/pipes.module';
import { DirectivesModule } from '../../../../core/directive/directives.module';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [AccountComponent],
  imports: [
    CommonModule,
    SimpleNotificationsModule,
    SharedModule,
    NgxSelectModule,
    NgSelectModule,
    NgxTranslateModule,
    NgxCurrencyModule,
    FormsModule,
    ReactiveFormsModule,
    InputMaskModule,
    ModalCcostCategoryModule,
    NgxDatatableModule,
    PipesModule,
    DirectivesModule,
    NgSelectModule,
    NgxMaskModule.forRoot(),

  ],
  exports: [AccountComponent]
})
export class AccountBillToPayModule { }
