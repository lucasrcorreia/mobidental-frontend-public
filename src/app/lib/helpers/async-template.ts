import { Injectable } from "@angular/core";
import { NotificationsService } from "angular2-notifications";
import { GlobalSpinnerService } from "../global-spinner.service";
import { errorToString } from "./error-to-string";

export const delay = (ms) => new Promise((resolve) => window.setTimeout(() => resolve(), ms));

export const timeout = (ms, rejectionProvider = () => Error("timeout")) =>
	delay(ms).then(() => {
		throw rejectionProvider();
	});

@Injectable({
	providedIn: "root",
})
export class AsyncTemplate {
	constructor(
		private readonly _spinner: GlobalSpinnerService,
		private readonly _notifications: NotificationsService
	) {}

	async wrapUserFeedback<R>(errorMessage: string, fn: () => Promise<R>): Promise<R> {
		const loadingToken = this._spinner.loadingManager.start();

		try {
			return await fn();
		} catch (e) {
			this._notifications.error(errorMessage, errorToString(e));
			throw e;
		} finally {
			loadingToken.finished();
		}
	}
}
