import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdutosFormModalComponent } from './produtos-form-modal.component';

describe('ProdutosFormModalComponent', () => {
  let component: ProdutosFormModalComponent;
  let fixture: ComponentFixture<ProdutosFormModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdutosFormModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdutosFormModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
