import { TestBed } from '@angular/core/testing';

import { ConveniosApiService } from './convenios-api.service';

describe('ConveniosApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConveniosApiService = TestBed.get(ConveniosApiService);
    expect(service).toBeTruthy();
  });
});
