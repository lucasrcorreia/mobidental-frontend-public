import { Injectable } from "@angular/core";
import * as XLSX from "xlsx";
import { Table2SheetOpts } from "xlsx";

@Injectable({
	providedIn: "root",
})
export class SheetGeneratorService {
	constructor() {}

	static toExportFileName(excelFileName: string): string {
		return `${excelFileName}_export_${new Date().getTime()}.xlsx`;
	}

	public exportJsonAsExcelFileTyped(json: ({ [coluna: string]: string })[], excelFileName: string): void {
		return this.exportJsonAsExcelFile(json, excelFileName);
	}

	/**
	 * @deprecated use {@link #exportJsonAsExcelFileTyped}
	 */
	public exportJsonAsExcelFile(json: any[], excelFileName: string): void {
		const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
		this._generateSheetFile(worksheet, excelFileName);
	}

	public exportDOMTableAsExcelFile(
		id: string,
		excelFileName: string,
		opts?: Table2SheetOpts
	): void {
		const worksheet: XLSX.WorkSheet = XLSX.utils.table_to_sheet(document.getElementById(id), opts);
		this._generateSheetFile(worksheet, excelFileName);
	}

	private _generateSheetFile(worksheet: XLSX.WorkSheet, excelFileName: string) {
		const workbook: XLSX.WorkBook = { Sheets: { Planilha: worksheet }, SheetNames: ["Planilha"] };
		XLSX.writeFile(workbook, SheetGeneratorService.toExportFileName(excelFileName));
	}
}
