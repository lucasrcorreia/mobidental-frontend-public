import { Component } from '@angular/core';
import { DashboardApiService, TotaisDashboard } from '../../../api/dashboard.api.service';
import * as moment from 'moment';
import { NgxChartView } from "../../../pages/reports/indications-report/indications-report.component";
import { Router } from "@angular/router";
import { DashboardQueryService } from "./service/dashboard-query.service";

@Component({
	selector: 'app-default',
	templateUrl: './default.component.html',
	styleUrls: [
		'./default.component.scss',
	],
})
export class DefaultComponent {

	readonly dado$: Promise<TotaisDashboard> = this._dashboardApiService.fetchDashboard();

	constructor(
			private readonly _dashboardApiService: DashboardApiService,
			private readonly _router: Router,
			private readonly _dashboardQueryService: DashboardQueryService,
	) {
	}

	navigateToBirthDayReport(): void {
		const now = moment();
		const ngxChartView: NgxChartView = { name: now.format('MMMM'), value: now.month() + 1 }

		this._dashboardQueryService._currentMonth = ngxChartView;
		this._router.navigate(['/reports/birthday-report']);
	}
}

