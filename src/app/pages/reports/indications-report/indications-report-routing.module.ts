import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndicationsReportComponent } from './indications-report.component';

const routes: Routes = [
  {
    path: '',
    component: IndicationsReportComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IndicationsReportRoutingModule { }
