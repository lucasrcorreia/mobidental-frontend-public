export const environment = {
  production: false,
  hmr: true,
  host: 'https://app.mobidental.com.br'
};
