import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PermissionsRoutingModule } from './permissions-routing.module';
import { PermissionsComponent } from './permissions.component';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NgxTranslateModule } from '../../../core/translate/translate.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [PermissionsComponent],
  imports: [
    CommonModule,
    SimpleNotificationsModule,
    NgxTranslateModule,
    NgSelectModule,
    FormsModule,
    PermissionsRoutingModule,
    ReactiveFormsModule,
  ],
})
export class PermissionsModule { }
