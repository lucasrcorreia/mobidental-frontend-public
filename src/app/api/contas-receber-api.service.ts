import { Inject, Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { API_CONFIGURATION, ApiConfiguration } from "./api-configuration";
import { FormaPagamento } from "./model/forma-pagamento";
import { TipoLancamento } from "./model/tipo-lancamento";
import * as moment from "moment";
import { ContentBillTreatmentModel } from "../core/models/patients/patients.model";
import { PaymentClientModel } from "../core/models/patients/payment/payment.model";

interface ContasAReceberListingApiOptions {
	[k: string]: string | string[];

	descricao?: string;
	convenioId?: string;
	dataInicio: string;
	dataFim: string;
	formaDeRecebimento: FormaPagamento[];
	tipoLancamento: TipoLancamento[];
}

export interface ContasAReceberListingOptions {
	descricao?: string;
	nomePaciente?: string;
	idConvenio?: number;
	dataInicio: Date;
	dataFim: Date;
	formaDePagamento?: FormaPagamento[];
	tipoLancamento?: TipoLancamento[];
}

export interface ContasAReceberListingResult {
	content: ContentBillTreatmentModel[];
	valorTotal: number;
	valorTotalARecer: number;
	valorTotalRecebido: number;
}

export interface ContasAReceberReport {
	content?: ContasAReceberReportRow[];
	quantidadeParcelasAPagar?: number;
	quantidadeParcelasPagas?: number;
	valorTotal?: number;
	valorTotalAReceber?: number;
	valorTotalRecebido?: number;
}

export interface ContasAReceberReportRow {
	dataRecebimento?: string;
	dataVencimento?: string;
	descricao?: string;
	formaRecebimento?: string;
	numeroCheque?: string;
	paciente?: string;
	tipoLancamento?: string;
	valor?: number;
}

export interface ParcelasRecebidasConta {
	agencia: string;
	banco: string;
	chequeConta: string;
	cpfCnpj: string;
	dataBomPara: unknown;
	dataRecebimento: string;
	formaDeRecebimentoDescricao: string;
	id: number;
	numCheque: unknown;
	observacao: string;
	reciboId: number;
	titular: string;
	valorRecebido: number;
}

export interface Totais {
	A_RECEBER: number | null;
	RECEBIDO: number | null;
}

const API_DATE_FORMAT = "DD/MM/YYYY";

@Injectable({
	providedIn: "root",
})
export class ContasReceberApiService {
	constructor(
		private readonly _http: HttpClient,
		@Inject(API_CONFIGURATION) private readonly _config: Readonly<ApiConfiguration>
	) {}

	fetchContasAReceber(options: ContasAReceberListingOptions): Promise<ContasAReceberListingResult> {
		const params = this._buildParams(options);

		return this._http
			.post<ContasAReceberListingResult>(
				`${this._config.apiBasePath}/contaReceber/contasReceberFinanceiro`,
				params
			)
			.toPromise();
	}

	fetchContasAReceberReport(options: ContasAReceberListingOptions): Promise<ContasAReceberReport> {
		const params = this._buildParams(options);

		return this._http
			.post<ContasAReceberReport>(
				`${this._config.apiBasePath}/contaReceber/buildListaContasReceberReport`,
				params
			)
			.toPromise();
	}

	fetchDialogData(paymentId: number): Promise<PaymentClientModel> {
		return this._http
			.get<PaymentClientModel>(
				`${this._config.apiBasePath}/contaReceber/dialog/carregar/${paymentId}`
			)
			.toPromise();
	}

	estornar(contaId: number): Promise<void> {
		return this._http
			.put<void>(`${this._config.apiBasePath}/contaReceberRecebido/${contaId}/remover`, null)
			.toPromise();
	}

	consultarItensRecebidos(contaReceberId: number): Promise<ParcelasRecebidasConta[]> {
		return this._http
			.get<ParcelasRecebidasConta[]>(
				`${this._config.apiBasePath}/contaReceber/getListaItensRecebidosByContaReceber/`,
				{
					params: { contaReceberId: String(contaReceberId) },
				}
			)
			.toPromise();
	}

	private _buildParams({
		idConvenio,
		dataInicio,
		formaDePagamento,
		tipoLancamento,
		dataFim,
		descricao,
		nomePaciente,
	}: ContasAReceberListingOptions): Record<string, string | string[]> {
		return {
			descricao,
			nomePaciente,
			dataFim: moment(dataFim).format(API_DATE_FORMAT),
			dataInicio: moment(dataInicio).format(API_DATE_FORMAT),
			convenioId: idConvenio ? String(idConvenio) : undefined,
			formaDeRecebimento: formaDePagamento ? formaDePagamento : [],
			tipoLancamento: tipoLancamento ? tipoLancamento : [],
		};
	}
}
