export interface ModalRef<T = any> {
  close(data?: T);
}
