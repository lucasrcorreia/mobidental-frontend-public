import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcedureDialogComponent } from './procedure-dialog.component';

describe('ModalProcedimentoComponent', () => {
  let component: ProcedureDialogComponent;
  let fixture: ComponentFixture<ProcedureDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProcedureDialogComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
