import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommitmentDatetimePickerComponent } from './commitment-datetime-picker.component';

describe('DatetimePickerComponent', () => {
  let component: CommitmentDatetimePickerComponent;
  let fixture: ComponentFixture<CommitmentDatetimePickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommitmentDatetimePickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommitmentDatetimePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
