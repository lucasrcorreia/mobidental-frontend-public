import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UtilsService } from '../../../../services/utils/utils.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProceduresCovenantModel } from '../../../../core/models/config/covenant/covenantCreate.model';

@Component({
  selector: 'app-modal-add-procedure',
  templateUrl: './modal-add-procedure.component.html',
  styleUrls: [
    './modal-add-procedure.component.scss',
    './../../../../../assets/icon/icofont/css/icofont.scss',
  ],
})
export class ModalAddProcedureComponent implements OnInit {

  @Input() modalId: string;
  @Input() model?: Partial<ProceduresCovenantModel>;
  @Input() editando = false;

  @Output() resultModal = new EventEmitter<any>();

  formProcedure: FormGroup;

  readonly percentageMask = {
    mask: Number,
    scale: 2,
    thousandsSeparator: '.',
    radix: ',',
    max: 100,
    padFractionalZeros: true,
    normalizeZeros: true,
  };

  _protetico = '';
  _comissao = '';

  constructor(
    public service: UtilsService,
    private readonly _fb: FormBuilder,
  ) {
  }

  get formValue(): ProceduresCovenantModel {
    return this.formProcedure && this.formProcedure.value;
  }

  patchForm(data: Partial<ProceduresCovenantModel>) {
    if (this.formProcedure) {
      this.formProcedure.patchValue(data);
    }
  }

  ngOnInit() {
    if (!this.model) {
      this.model = {};
    }

    if (this.model.procedimentoIsAceitaFace == null) {
      this.model.procedimentoIsAceitaFace = false;
    }

    if (this.model.procedimentoStatus == null) {
      this.model.procedimentoStatus = false;
    }

    this.formProcedure = this._buildForm(this.model);

    this._comissao = this.model.procedimentoPorcentagemComissao != null
      ? this.model.procedimentoPorcentagemComissao.toLocaleString('pt-BR')
      : '0';

    this._protetico = this.model.procedimentoValorCustoProtetico != null
      ? this.model.procedimentoValorCustoProtetico.toLocaleString('pt-BR')
      : '0';
  }

  private _buildForm(data: Partial<ProceduresCovenantModel>) {
    const formGroup: { [k in keyof ProceduresCovenantModel]?: any[] } = {
      procedimentoStatus: [data.procedimentoStatus],
      procedimentoIsAceitaFace: [data.procedimentoIsAceitaFace],
      procedimentoValorCustoProtetico: [data.procedimentoValorCustoProtetico],
      procedimentoNome: [data.procedimentoNome],
      preco: [data.preco],
      procedimentoPorcentagemComissao: [data.procedimentoPorcentagemComissao],
    };

    return this._fb.group(formGroup);
  }

  add() {
    const result = { ...this.model, ...this.formValue };
    this.resultModal.emit(result);
  }

  closeModal() {
    this.resultModal.emit(null);
  }

  clampPercentagem(n: number) {
    return Math.min(n, 100);
  }
}
