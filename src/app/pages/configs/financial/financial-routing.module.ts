import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FinancialComponent } from './financial.component';

const routes: Routes = [
  {
    path: 'centerCost',
    data: {
      type: 'centerCost',
      id: 9,
      children: true
    },
    component: FinancialComponent
  },
  {
    path: 'category',
    data: {
      type: 'category',
      id: 10,
      children: true
    },
    component: FinancialComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinancialRoutingModule { }
