import { Component, Input, OnInit } from '@angular/core';
import { UtilsService } from "../../../../services/utils/utils.service";
import { AnnotationsService } from "../service/annotations.service";
import { GlobalSpinnerService } from "../../../../lib/global-spinner.service";
import { NotificationsService } from "angular2-notifications";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { Annotation } from "../models/annotation";
import { PdfGeneratorService } from "../../../../services/pdf-generator/pdf-generator.service";
import { PatientModel } from "../../../../core/models/patients/patients.model";
import * as moment from 'moment';

@Component({
	selector: 'app-annotation',
	templateUrl: './annotation.component.html',
	styleUrls: ['./annotation.component.scss']
})
export class AnnotationComponent implements OnInit {

	@Input() annotationId?: number;
	@Input() patient: PatientModel;

	annotation: Annotation = {};

	constructor(readonly _service: UtilsService,
				private readonly _activeModal: NgbActiveModal,
				private readonly _annotationsService: AnnotationsService,
				private readonly _globalSpinnerService: GlobalSpinnerService,
				private readonly _notificationService: NotificationsService,
				private readonly _pdfGeneratorService: PdfGeneratorService) { }

	ngOnInit() {
		this.annotation.pacienteId = this.patient.id;
		if (this.annotationId) {
			this._getOne();
		}
	}

	async _getOne() {
		try {
			this.annotation = await this._annotationsService.getOne(this.annotationId);
		} catch (e) {
			this._notificationService.error('Ocorreu um erro ao obter a anotação.');
			this.close();
		}
	}

	async save() {
		const loading = this._globalSpinnerService.loadingManager.start();
		try {
			await this._annotationsService.save(this.annotation);
			this._notificationService.success('Anotação' + (this.annotationId ? ' editada.' : 'criada.'));
			this._activeModal.close(true);
		} catch (e) {
			this._notificationService.error('Ocorreu um erro ao ' + (this.annotationId ? ' editar' : 'criar') + ' a anotação.');
		} finally {
			loading.finished();
		}
	}

	async print() {
		const loading = this._globalSpinnerService.loadingManager.start();
		try {
			const html = await this._buildPrintHeader();
			const pdfMakeDefinition = await this._pdfGeneratorService.exportPDFMakeAsPDF(
					{ pageMargins: [30, 20, 30, 20] },
					html,
					this.patient.pessoaNome + ' - ' + this.annotation.assunto + '.pdf'
			);
			pdfMakeDefinition.open();
		} catch (e) {
			this._notificationService.error('Ocorreu um erro ao gerar o PDF');
		} finally {
			loading.finished();
		}
	}

	private _buildPrintHeader(): string {
		return `
				<span style="margin-bottom: 5px"><b>Paciente: </b>${this.patient.pessoaNome}</span>
				<span style="margin-bottom: 5px"><b>Assunto: </b>${this.annotation.assunto}</span>
				<span><b>Data de Emissão: </b>${moment().format('L')}</span>
				<hr>
		` + this.annotation.descricao;
	}

	close() {
		this._activeModal.close();
	}

}
