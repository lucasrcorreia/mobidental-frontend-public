import { Component, ElementRef, Inject, OnInit, ViewChild } from "@angular/core";
import { NotificationsService } from "angular2-notifications";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { GlobalSpinnerService } from "../../../lib/global-spinner.service";
import { AsyncTemplate } from "../../../lib/helpers/async-template";
import { PdfGeneratorService } from "../../../services/pdf-generator/pdf-generator.service";
import { CommonsReportBuildService } from "../../../services/report/commons-report-build.service";
import { SheetGeneratorService } from "../../../services/sheets-generator/sheet-generator.service";
import { MaskFormatService } from "../../../services/utils/mask-format.service";
import { UtilsService } from "../../../services/utils/utils.service";
import { DropdownGenericService } from "../../patients/form-patients/services/dropdown-generic.service";
import { AbstractReport } from "../abstract-report";
import { NgxChartView } from "../indications-report/indications-report.component";
import { ReportDatatableColumn, Type } from "../report-datatable/report-datatable.component";
import { ReceiptsReportFilters, ReportsService } from "../reports.service";

const TABLE_COLUMNS: ReportDatatableColumn[] = [
	{ prop: "nomePaciente", translation: "REPORTS.TABLE.PATIENT_NAME" },
	{ prop: "dataNascimento", translation: "REPORTS.TABLE.BIRTH_DATE", type: Type.DATE },
	{ prop: "idade", translation: "REPORTS.TABLE.AGE", pdfAlignment: "center" },
	{ prop: "sexo", translation: "REPORTS.TABLE.GENDER" },
	{ prop: "email", translation: "REPORTS.TABLE.EMAIL" },
	{ prop: "celular", translation: "REPORTS.TABLE.CELL", type: Type.CELL },
	{ prop: "telefone", translation: "REPORTS.TABLE.PHONE", type: Type.PHONE },
];

const SHEET_COLUMNS: ReportDatatableColumn[] = [
	{ prop: "nomePaciente", translation: "REPORTS.TABLE.PATIENT_NAME" },
	{ prop: "dataNascimento", translation: "REPORTS.TABLE.BIRTH_DATE", type: Type.DATE },
	{ prop: "idade", translation: "REPORTS.TABLE.AGE", pdfAlignment: "center" },
	{ prop: "sexo", translation: "REPORTS.TABLE.GENDER" },
	{ prop: "email", translation: "REPORTS.TABLE.EMAIL" },
	{ prop: "celular", translation: "REPORTS.TABLE.CELL", type: Type.CELL },
	{ prop: "telefone", translation: "REPORTS.TABLE.PHONE", type: Type.PHONE },
	{ prop: "endereco", translation: "REPORTS.TABLE.ADDRESS" },
	{ prop: "numeroEndereco", translation: "REPORTS.TABLE.N_ADDRESS" },
	{ prop: "cep", translation: "REPORTS.TABLE.CEP" },
	{ prop: "bairro", translation: "REPORTS.TABLE.NEIGHBORHOOD" },
	{ prop: "cidade", translation: "REPORTS.TABLE.CITY" },
	{ prop: "ufCidade", translation: "REPORTS.TABLE.UF" },
];

@Component({
	selector: "app-age-range-report",
	templateUrl: "./age-range-report.component.html",
	styleUrls: ["./age-range-report.component.scss"],
})
export class AgeRangeReportComponent extends AbstractReport implements OnInit {
	@ViewChild("barVerticalComponent") barVerticalComponent?: ElementRef;

	filters: ReceiptsReportFilters;

	search() {
		throw new Error("Method not implemented.");
	}

	currentAgeRange: string;

	ownData: any[];
	results: NgxChartView[];
	colorScheme = {
		domain: ["#dc461a"],
	};
	barChartView: [number, number];

	sheetFileName = "relatorio_faixa_etaria";
	sheetTitle = this._service.translate.instant("REPORTS.AGE_RANGE.TITLE");
	reportTableBodyWidths: number[] = [110, 50, 30, 50, 120, 65, 65];
	readonly columns = TABLE_COLUMNS;
	readonly sheetColumns = SHEET_COLUMNS;

	private readonly _destroy$ = new Subject();

	constructor(
		private readonly _reportsService: ReportsService,
		@Inject(UtilsService) _service: UtilsService,
		@Inject(DropdownGenericService) _dropdownGenericService: DropdownGenericService,
		@Inject(NotificationsService) _notificationsService: NotificationsService,
		@Inject(GlobalSpinnerService) _globalSpinnerService: GlobalSpinnerService,
		@Inject(CommonsReportBuildService) _commonsReportService: CommonsReportBuildService,
		@Inject(PdfGeneratorService) _pdfGeneratorService: PdfGeneratorService,
		@Inject(SheetGeneratorService) _sheetGeneratorService: SheetGeneratorService,
		@Inject(MaskFormatService) _maskFormatService: MaskFormatService,
		@Inject(AsyncTemplate) _asyncTemplate: AsyncTemplate
	) {
		super(
			_service,
			_dropdownGenericService,
			_notificationsService,
			_globalSpinnerService,
			_commonsReportService,
			_pdfGeneratorService,
			_sheetGeneratorService,
			_maskFormatService,
			_asyncTemplate
		);
		this._service.toggleMenu.pipe(takeUntil(this._destroy$)).subscribe((data) => {
			if (data) {
				setTimeout(() => this.resizeChart());
			}
		});
	}

	ngOnInit() {
		this._initChart();
	}

	ngOnDestroy() {
		this._destroy$.next();
		this._destroy$.complete();
	}

	resizeChart(): void {
		// -15 para tirar o padding direito
		this.barChartView = [this.barVerticalComponent.nativeElement.offsetWidth - 15, 250];
	}

	private async _initChart() {
		this.results = (await this.fetchOwnData(() => this._reportsService.findAllAgeRange())).map(
			(r) => {
				return { name: r["faixaEtaria"], value: r["quantidade"] };
			}
		);
		this.resizeChart();
	}

	async fetchByAgeRange($event) {
		this.currentAgeRange = $event.name;
		const colletedAges = $event.name.match(/\d+/g);

		// Regra de negócio, quando + na idade, o máximo é 120
		const initialAge = colletedAges ? +colletedAges[0] : 0;
		const finalAge = colletedAges ? +colletedAges[1] || 120 : 0;
		this.ownData = await this.fetchOwnData(() =>
			this._reportsService.findAllAgeRangeByRange(initialAge, finalAge)
		);
	}

	async buildReportHeader() {
		return [
			{ ...(await this._commonsReportService.buildFinancialReportHeader(this.sheetTitle)) },
			{
				text: this.reportFilterLabel,
				fontSize: 9,
				margin: [0, 10, 0, 10],
			},
		];
	}

	private get reportFilterLabel() {
		if (this.currentAgeRange !== "Em Branco") {
			return [{ text: "Pacientes com idade entre: ", bold: true }, this.currentAgeRange];
		}

		return [{ text: "Pacientes com data de nascimento em branco", bold: true }];
	}
}
