import { FormsModel } from '../models/forms/forms.model';
import { Validators } from '@angular/forms';

export const FormProcedureCreate: FormsModel[] = [
  {
    field: 'procedimentoNome',
    validators: [Validators.required]
  },
  {
    field: 'preco',
    validators: [Validators.required]
  }
];
