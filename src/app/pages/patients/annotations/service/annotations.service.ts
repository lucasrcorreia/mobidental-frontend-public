import { Inject, Injectable } from '@angular/core';
import { API_CONFIGURATION, ApiConfiguration } from "../../../../api/api-configuration";
import { HttpClient } from "@angular/common/http";
import { Annotation } from "../models/annotation";

@Injectable({
	providedIn: 'root'
})
export class AnnotationsService {

	constructor(
			@Inject(API_CONFIGURATION) private readonly _config: ApiConfiguration,
			private readonly _http: HttpClient,
	) {
	}

	listAll(patientId: number): Promise<Annotation[]> {
		return this._http
				.post<Annotation[]>(`${this._config.apiBasePath}/anotacaoPaciente/findAllAnotacoes`, { pacienteId: patientId })
				.toPromise();
	}

	getOne(id: number): Promise<Annotation> {
		return this._http
				.get<Annotation>(`${this._config.apiBasePath}/anotacaoPaciente/findOne/` + id)
				.toPromise();
	}

	save(annotation: Annotation): Promise<Annotation> {
		const { id, assunto, descricao, pacienteId } = annotation;
		const body = { id, assunto, descricao, pacienteId };
		return this._http
				.post<Annotation>(`${this._config.apiBasePath}/anotacaoPaciente/save`, body)
				.toPromise();
	}

	remove(id: number): Promise<void> {
		return this._http
				.delete<void>(`${this._config.apiBasePath}/anotacaoPaciente/` + id)
				.toPromise();
	}
}
