import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulingsReportPageComponent } from './schedulings-report-page.component';

describe('SchedulingsReportPageComponent', () => {
  let component: SchedulingsReportPageComponent;
  let fixture: ComponentFixture<SchedulingsReportPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedulingsReportPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulingsReportPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
