import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { ContentPatientServicesTableModel } from "../../../../core/models/patients/procedures/procedures.model";
import { UtilsService } from "../../../../services/utils/utils.service";
import { SelectItemStatus, ShortListItem } from "../../../../core/models/forms/common/common.model";

@Component({
	selector: 'app-edit-procedure',
	templateUrl: './edit-procedure.component.html',
	styleUrls: ['./edit-procedure.component.scss']
})
export class EditProcedureComponent implements OnInit {

	@Input() procedure: ContentPatientServicesTableModel;
	@Input() professionals: ShortListItem[];
	@Input() statuses: SelectItemStatus[];

	constructor(private readonly _activeModal: NgbActiveModal,
				readonly _service: UtilsService) { }

	ngOnInit() {
	}

	onStatusChange($event) {
		this.procedure.statusTratamentoServicoDescricao = $event[0].text;
	}

	onProfessionalChange($event) {
		this.procedure.profissionalNome = $event[0].text;
	}

	save() {
		this._activeModal.close(this.procedure);
	}

	close() {
		this._activeModal.close();
	}

}
