import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FluxoDeCaixa, FluxoDeCaixaApiService } from '../../../api/fluxo-de-caixa-api.service';
import { distinctUntilChanged, filter, flatMap, shareReplay, take } from 'rxjs/operators';
import { equals } from 'ramda';
import { AsyncTemplate } from '../../../lib/helpers/async-template';
import { PdfGeneratorService, PDFMake } from '../../../services/pdf-generator/pdf-generator.service';
import { descricoesPagamentos } from '../../../api/model/forma-pagamento';
import { GlobalSpinnerService } from '../../../lib/global-spinner.service';
import { NotificationsService } from 'angular2-notifications';
import { ListaFluxoCaixaReport } from './models/lista-fluxo-caixa-report';
import {
    BLUE_COLOR,
    CommonsReportBuildService,
    GREEN_COLOR,
    RED_COLOR,
    REPORT_STYLES
} from '../../../services/report/commons-report-build.service';
import { ReportPrintType } from '../../reports/report-header-actions/report-header-actions.component';
import { SheetGeneratorService } from '../../../services/sheets-generator/sheet-generator.service';

const RESUME_COLUMN = (column: string) => {
    return {
        width: 90,
        stack: [
            { text: column, style: 'resumeHeader' },
            { text: 'Dinheiro (R$):', style: 'resumeRow' },
            { text: 'Cartão Crédito (R$):', style: 'resumeRow' },
            { text: 'Cartão Débito (R$):', style: 'resumeRow' },
            { text: 'Cheque (R$):', style: 'resumeRow' },
            { text: 'Boleto (R$):', style: 'resumeRow' },
            { text: 'Déb. Automático (R$):', style: 'resumeRow' },
            { text: 'Transf. Bancária (R$):', style: 'resumeRow' },
            { text: 'Permuta (R$):', style: 'resumeRow' },
        ],
    };
};

const RESUME_ROWS = (color: string, totals: any) => {
    return {
        width: '*',
        color: color,
        alignment: 'right',
        stack: [
            { text: ' ', style: 'resumeRow' },
            { text: totals.totalDinheiro.toFixed(2), style: 'resumeRow' },
            { text: totals.totalCartaoCredito.toFixed(2), style: 'resumeRow' },
            { text: totals.totalCartaoDebito.toFixed(2), style: 'resumeRow' },
            { text: totals.totalCheque.toFixed(2), style: 'resumeRow' },
            { text: totals.totalBoleto.toFixed(2), style: 'resumeRow' },
            { text: totals.totalDebitoAutomatico.toFixed(2), style: 'resumeRow' },
            { text: totals.totalTransferenciaBancaria.toFixed(2), style: 'resumeRow' },
            { text: totals.permuta.toFixed(2), style: 'resumeRow' },
        ],
    };
};

const EMPTY_SPACE = {
    width: 10,
    text: '',
};

const VALUE_TIPO_CONTA = (conta: ListaFluxoCaixaReport, tipoConta: string): string => {
    return (conta.tipoConta !== tipoConta || !conta.valor) ? ' ' : conta.valor.toFixed(2);
};

@Component({
               selector: 'app-fluxo-caixa',
               templateUrl: './fluxo-caixa.component.html',
               styleUrls: ['./fluxo-caixa.component.scss'],
           })

export class FluxoCaixaComponent implements OnInit {

    private _currentDates: [Date, Date];
    private readonly _datesSubject = new BehaviorSubject<[Date, Date]>(undefined);

    readonly fluxoDeCaixa$ = this._datesSubject.pipe(
        filter(it => !!it),
        distinctUntilChanged(equals),
        flatMap(([inicio, fim]) => this._asyncTemplate.wrapUserFeedback(
            'Problema ao consultar fluxo de caixa',
            () => this._fluxoDeCaixaApiService.listarFluxoDeCaixa(inicio, fim),
        )),
        shareReplay(1),
    );

    constructor(
        private readonly _fluxoDeCaixaApiService: FluxoDeCaixaApiService,
        private readonly _asyncTemplate: AsyncTemplate,
        private readonly _pdfGeneratorService: PdfGeneratorService,
        private readonly _globalSpinnerService: GlobalSpinnerService,
        private readonly _notificationsService: NotificationsService,
        private readonly _commonsReportService: CommonsReportBuildService,
        private readonly _sheetGeneratorService: SheetGeneratorService,
    ) {
    }

    ngOnInit() {
    }

    atualizarDates(dates: [Date, Date]) {
        this._currentDates = dates;
        this._datesSubject.next(dates);
    }

    async generateReport(type: ReportPrintType): Promise<void> {
        const loading = this._globalSpinnerService.loadingManager.start();
        try {
            const fluxoDeCaixa = await this.fluxoDeCaixa$.pipe(take(1)).toPromise();
            const { entradas, saidas } = fluxoDeCaixa;
            if (!this._commonsReportService.checkIfPrintIsAvaliable([...entradas, ...saidas])) {
                return;
            }

            const listaFluxoCaixaReports = await this._fluxoDeCaixaApiService.getListaFluxoCaixaReport({
                                                                                                           entradas,
                                                                                                           saidas
                                                                                                       });
            switch (type) {
                case ReportPrintType.SHEETS:
                    this._generateSheetsReport(listaFluxoCaixaReports);
                    break;
                default:
                    this._generatePdfReport(fluxoDeCaixa, listaFluxoCaixaReports);
                    break;
            }
        } catch (e) {
            this._notificationsService.error('Ocorreu um erro ao emitir o relatório.');
        } finally {
            loading.finished();
        }
    }

    private _generateSheetsReport(listaFluxoCaixaReports: ListaFluxoCaixaReport[]) {
        this._sheetGeneratorService.exportJsonAsExcelFile(
            this._buildRowsToSheet(listaFluxoCaixaReports),
            'Fluxo de Caixa'
        );
    }

    private _buildRowsToSheet(listaFluxoCaixaReports: ListaFluxoCaixaReport[]): unknown[] {
        return listaFluxoCaixaReports.map(conta => {
            return {
                'Data': conta.data,
                'Paciente': conta.cliente,
                'Descrição': conta.descricao,
                'Forma Pagamento': descricoesPagamentos[conta.formaDeRecebimento],
                'Entradas': VALUE_TIPO_CONTA(conta, 'CONTA_PAGAR'),
                'Saídas': VALUE_TIPO_CONTA(conta, 'CONTA_RECEBER'),
            };
        });
    }

    private async _generatePdfReport(
        fluxoDeCaixa: FluxoDeCaixa,
        listaFluxoCaixaReports: ListaFluxoCaixaReport[],
    ) {
        const pdfMake = await FluxoCaixaComponent.buildReport(
            fluxoDeCaixa,
            listaFluxoCaixaReports,
            this._currentDates,
            this._commonsReportService
        );
        const pdfMakeDefinition = await this._pdfGeneratorService.exportPDFMakeAsPDF(
            pdfMake,
            '',
            'FluxoDeCaixa.pdf'
        );
        pdfMakeDefinition.open();
    }

    static async buildReport(
        fluxoDeCaixa: FluxoDeCaixa,
        listaFluxoCaixaReports: ListaFluxoCaixaReport[],
        dates: [Date, Date],
        commonsReportService: CommonsReportBuildService
    ): Promise<PDFMake> {
        const pdfMake: PDFMake = { afterContent: [] };
        pdfMake.styles = REPORT_STYLES;
        pdfMake.afterContent.push(await FluxoCaixaComponent._buildReportHeader(dates, commonsReportService));
        pdfMake.afterContent.push(FluxoCaixaComponent._buildReportBody(listaFluxoCaixaReports));
        pdfMake.afterContent.push(FluxoCaixaComponent._buildReportResume(fluxoDeCaixa));

        return pdfMake;
    }

    private static async _buildReportHeader(dates: [Date, Date], commonsReportService: CommonsReportBuildService) {
        return [
            await commonsReportService.buildFinancialReportHeader('Relatório Fluxo de Caixa'),
            {
                ...CommonsReportBuildService.buildPeriodo(dates),
                margin: [0, 20, 0, 10],
            }];
    }

    private static _buildReportBody(listaFluxoCaixaReports: ListaFluxoCaixaReport[]) {
        return {
            table: {
                widths: [50, 130, 130, 90, 50, 50],
                body: [
                    [
                        { text: 'Data', style: 'tableHeader' },
                        { text: 'Paciente', style: 'tableHeader' },
                        { text: 'Descrição', style: 'tableHeader' },
                        { text: 'Forma Pagamento', style: 'tableHeader' },
                        { text: 'Entradas', style: 'tableHeader' },
                        { text: 'Saídas', style: 'tableHeader' },
                    ],
                    ...this._buildReportRows(listaFluxoCaixaReports),
                ],
            },
        };
    }

    private static _buildReportResume(fluxoDeCaixa: FluxoDeCaixa) {
        return [
            {
                columns: [
                    RESUME_COLUMN('ENTRADAS'),
                    RESUME_ROWS('green', fluxoDeCaixa.totais.entradas),
                    EMPTY_SPACE,
                    RESUME_COLUMN('SAÍDAS'),
                    RESUME_ROWS(RED_COLOR, fluxoDeCaixa.totais.saidas),
                    EMPTY_SPACE,
                    {
                        width: 80,
                        stack: [
                            { text: 'TOTAIS', style: 'resumeHeader' },
                            { text: 'Total Entradas (R$):', style: 'resumeRow' },
                            { text: 'Total Saídas (R$):', style: 'resumeRow' },
                            { text: 'Saldo (R$):', style: 'resumeRow' },
                        ],
                    },
                    {
                        alignment: 'right',
                        stack: [
                            { text: ' ', style: 'resumeRow' },
                            {
                                text: fluxoDeCaixa.totais.totalEntradas.toFixed(2),
                                style: 'resumeRow',
                                color: GREEN_COLOR
                            },
                            { text: fluxoDeCaixa.totais.totalSaidas.toFixed(2), style: 'resumeRow', color: RED_COLOR },
                            {
                                text: (fluxoDeCaixa.totais.totalEntradas - fluxoDeCaixa.totais.totalSaidas).toFixed(2),
                                style: 'resumeRow',
                                color: BLUE_COLOR
                            },
                        ],
                    },
                    EMPTY_SPACE,
                ],
                margin: [20, 30, 0, 0],
            },
            {
                canvas: [
                    {
                        type: 'rect',
                        x: 0,
                        y: -105,
                        w: 555,
                        h: 115,
                        lineWidth: 1,
                    },

                ],
            },
        ];
    }

    private static _buildReportRows(listaFluxoCaixaReports: ListaFluxoCaixaReport[]) {
        return listaFluxoCaixaReports.map(conta =>
                                              [
                                                  { text: conta.data, style: 'tableRow', alignment: 'center' },
                                                  { text: conta.cliente, style: 'tableRow' },
                                                  { text: conta.descricao, style: 'tableRow' },
                                                  {
                                                      text: descricoesPagamentos[conta.formaDeRecebimento],
                                                      style: 'tableRow'
                                                  },
                                                  this._checkTipoConta(conta, 'CONTA_PAGAR'),
                                                  this._checkTipoConta(conta, 'CONTA_RECEBER'),
                                              ],
        );
    }

    private static _checkTipoConta(conta: ListaFluxoCaixaReport, tipoConta: string) {
        return {
            text: VALUE_TIPO_CONTA(conta, tipoConta),
            style: 'tableRow',
            alignment: 'right'
        };
    }

}
