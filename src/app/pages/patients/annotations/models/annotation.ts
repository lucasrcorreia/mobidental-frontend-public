export interface Annotation {
	id?: number,
	assunto?: string,
	descricao?: string,
	pacienteId?: number;
	dataCriacao?: string;
	dataUltimaAlteracao?: string;
}
