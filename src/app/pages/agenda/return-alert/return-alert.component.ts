import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ReturnAlertService } from './service/return-alert.service';
import { ReturnAlert } from './model/return-alert';
import { ShortListItem } from '../../../core/models/forms/common/common.model';
import { NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateReturnAlertComponent } from './create-alert/create-return-alert.component';
import { UtilsService } from '../../../services/utils/utils.service';
import * as moment from 'moment';
import { MaskFormatService } from '../../../services/utils/mask-format.service';
import { GlobalSpinnerService } from '../../../lib/global-spinner.service';
import { NotificationsService } from 'angular2-notifications';
import { DEFAULT_AVATAR_PROFILE_PATH } from '../../../layout/admin/account-chooser/account-chooser.component';
import Swal from 'sweetalert2';
import { PatientModel } from '../../../core/models/patients/patients.model';
import { FULL_CELLPHONE_LENGTH, MARCADO, SCHEDULING } from '../agenda.component';
import { ConvenantModel } from '../../../core/models/patients/convenio.model';
import { ServicesListModel } from '../../../core/models/agenda/agenda.model';
import { CONSTANTS } from '../../../core/constants/constants';
import { ModalAnimationComponent } from '../../../shared/modal-animation/modal-animation.component';
import { ReturnAlertStatus } from './model/enum/return-alert-status';
import { PATIENT_RECORD_LINK } from '../agendamento/scheduling/scheduling.component';

declare let $: any;

const MOMENT_NOW = moment().format('L');
const NON_AVALIABLE_LABEL = 'Não Disponível';
const DEFAULT_SCHEDULE = (patientId: number, professionalId: number, duracao: number, returnAlertId: number) => {
	return {
		id: null,
		pacienteId: patientId,
		profissionalId: professionalId,
		convenioId: null,
		celular: null,
		telefoneFixo: null,
		email: null,
		dataHoraInicialEvento: moment().format('L HH:mm:ss'),
		duracao: duracao,
		emStatusAgendamento: MARCADO,
		anotacoes: null,
		pacienteCodigoProntuario: null,
		tipoAtendimentoId: null,
		dataHoraCadastro: null,
		dataHoraAlteracao: null,
		usuarioCadastroNome: null,
		usuarioAlteracaoNome: null,
		emTipoAgendamento: SCHEDULING,
		alertaRetornoId: returnAlertId,
	}
};
const buildWhatsappMessage = (patientCellPhone: string,
							  patientName: string): string =>
		`https://api.whatsapp.com/send?phone=55${ patientCellPhone }&text=${ patientName } você tem um 
		retorno marcado com o seu Dentista. Por favor entre em contato para agendar o melhor dia e horário?`;

@Component({
	selector: 'app-return-alert',
	templateUrl: './return-alert.component.html',
	styleUrls: ['./return-alert.component.scss'],
})
export class ReturnAlertComponent implements OnInit {

	@ViewChild('scheduleDialog') scheduleDialog?: ModalAnimationComponent;

	@Input() professionals: ShortListItem[] = [];
	@Input() convenantsList: ConvenantModel[] = [];
	@Input() servicesList: ServicesListModel[] = [];
	@Output() refreshLabel = new EventEmitter<void>();

	readonly returnAlertStatus = ReturnAlertStatus;
	readonly selectDateOption = 'Selecionar Período';
	readonly expiredAlertsOption = 'Alertas Expirados';
	readonly todayOption = 'Alertas que expiram Hoje';
	readonly datableOptions = [this.selectDateOption];
	readonly fifteenDaysToExpire = 'Expiram nos próximos 15 dias';

	maxDate: NgbDateStruct;
	selectedExibition = this.fifteenDaysToExpire;
	initialDate: NgbDateStruct;
	finalDate: NgbDateStruct;
	exibitionOptionList = [
		this.todayOption,
		'Expiram nos próximos 7 dias',
		this.fifteenDaysToExpire,
		'Expiram nos próximos 30 dias',
		this.expiredAlertsOption,
		this.selectDateOption,
	];

	openScheduleDialog = false;
	currentPatient: PatientModel;
	defaultSchedule = null;

	returnAlerts: ReturnAlert[];

	constructor(private readonly _returnAlertService: ReturnAlertService,
				private readonly _modalService: NgbModal,
				private readonly _notificationsService: NotificationsService,
				private readonly _globalSpinnerService: GlobalSpinnerService,
				public readonly _maskFormatService: MaskFormatService,
				public readonly service: UtilsService) { }

	ngOnInit() {
		this.selectExibition();
		this.maxDate = this.service.parseDatePicker(moment().format('L'));
	}

	async fetchExpiredReturnAlerts() {
		if (!this.initialDate) {
			return;
		}

		const initialDate = this.service.parseDateFromDatePicker(this.initialDate);

		this._fetchReturnAlerts(initialDate, '', true);
	}

	async fetchReturnAlertsByDate() {
		if (!ReturnAlertComponent._validNgbDateStruct(this.initialDate) || !ReturnAlertComponent._validNgbDateStruct(this.finalDate)) {
			return;
		}

		const initialDate = this.service.parseDateFromDatePicker(this.initialDate);
		const finalDate = this.service.parseDateFromDatePicker(this.finalDate);

		this._fetchReturnAlerts(initialDate, finalDate);
	}

	private static _validNgbDateStruct(ngbDateStruct: NgbDateStruct) {
		return ngbDateStruct && !isNaN(ngbDateStruct.year) && !isNaN(ngbDateStruct.month) && !isNaN(ngbDateStruct.day);
	}

	async selectExibition() {
		switch (this.selectedExibition) {
			case this.todayOption:
				this._fetchReturnAlerts(MOMENT_NOW, MOMENT_NOW);
				break;
			case this.expiredAlertsOption:
				this.fetchExpiredReturnAlerts();
				break;
			case this.selectDateOption:
				this.fetchReturnAlertsByDate();
				break;
			default:
				const range = this.selectedExibition.match(/(\d+)/);
				this._fetchReturnAlerts(MOMENT_NOW, moment().add(range[0], 'days').format('L'));
				break;
		}
	}

	private async _fetchReturnAlerts(initialDate: string, finalDate: string, isExpirados?: boolean, status?: string) {
		const loading = this._globalSpinnerService.loadingManager.start();
		try {
			this.returnAlerts = await this._returnAlertService.listAll(initialDate, finalDate, isExpirados, status);
			this.fillDates(initialDate, finalDate);
		} catch (e) {
			this._notificationsService.error('Ocorreu um erro ao carregar os Alertas de Retorno.');
		} finally {
			loading.finished();
		}
	}

	private static _parseStringToNgbDate(date: string): NgbDateStruct {
		const momentDate = moment(date.split('/').reverse().join('-'));
		return { year: momentDate.year(), month: momentDate.month() + 1, day: momentDate.date() }
	}

	private fillDates(initialDate: string, finalDate: string): void {
		this.initialDate = ReturnAlertComponent._parseStringToNgbDate(initialDate);
		this.finalDate = ReturnAlertComponent._parseStringToNgbDate(finalDate);
	}

	getPatientNumber(returnAlert: ReturnAlert) {
		if (!returnAlert.pacientePessoaCelular && !returnAlert.pacientePessoaTelefone) {
			return NON_AVALIABLE_LABEL;
		}
		return returnAlert.pacientePessoaCelular ? this._maskFormatService.getFormatedCellPhone(returnAlert.pacientePessoaCelular) :
				this._maskFormatService.getFormatedPhone(returnAlert.pacientePessoaTelefone);
	}

	goToWhatsappMessage(returnAlert: ReturnAlert) {
		if (!returnAlert.pacientePessoaCelular || returnAlert.pacientePessoaCelular.length !== FULL_CELLPHONE_LENGTH) {
			this._notificationsService.alert('O paciente não possui um número de telefone válido! Deve ter 11 dígitos.');
		}

		const { pacientePessoaCelular, pacientePessoaNome } = returnAlert
		const whatsappLink = buildWhatsappMessage(pacientePessoaCelular, pacientePessoaNome);

		// Usar window.open() pode não funcionar de acordo com browser e/ou configuração do usuário
		$('<a />', { href: whatsappLink, target: '_blank' }).get(0).click();
	}

	getPatientProfileImage(imageUrl: string) {
		return !imageUrl || !imageUrl.trim() ? DEFAULT_AVATAR_PROFILE_PATH : imageUrl;
	}

	getLabel(label: string) {
		return !label || !label.trim() ? NON_AVALIABLE_LABEL : label;
	}

	async createNewReturnAlert(returnAlert: ReturnAlert = { status: ReturnAlertStatus.ABERTO } as ReturnAlert) {
		const modalRef = this._modalService.open(CreateReturnAlertComponent);
		modalRef.componentInstance.professionals = this.professionals;
		modalRef.componentInstance.returnAlert = returnAlert;

		const result = await modalRef.result;

		if (result) {
			this._refreshReturnAlerts();
		}
	}

	_refreshReturnAlerts() {
		this.selectExibition();
		this.refreshLabel.emit();
	}

	async schedule(returnAlert: ReturnAlert) {
		const loading = this._globalSpinnerService.loadingManager.start()
		const data = await this.service.httpGET(CONSTANTS.ENDPOINTS.patients.general.findOne + returnAlert.pacienteId).toPromise();
		this.currentPatient = data.body as PatientModel;
		const professionalConfig = await this._fetchProfessionalConfigs(returnAlert.profissionalId);
		this.defaultSchedule = DEFAULT_SCHEDULE(returnAlert.pacienteId, returnAlert.profissionalId, professionalConfig, returnAlert.id);
		loading.finished();

		this.scheduleDialog.open();
		this.openScheduleDialog = true;
	}

	async _fetchProfessionalConfigs(id: number): Promise<number> {
		const professionalConfig = await this.service.httpGET(CONSTANTS.ENDPOINTS.agenda.professionalConfig, { profissionalId: id }).toPromise();
		return professionalConfig.body['slotLabelInterval'];
	}

	async removeReturnAlert(id: number) {
		const { value } = await Swal.fire({
			title: 'Confirmação de exclusão',
			text: 'Tem certeza que deseja remover este Alerta de Retorno?',
			type: 'question',
			showCancelButton: true,
			confirmButtonText: 'Sim. Remover',
			cancelButtonText: 'Cancelar',
		});

		if (!value) {
			return;
		}

		const loading = this._globalSpinnerService.loadingManager.start();
		try {
			await this._returnAlertService.remove(id);
			this._refreshReturnAlerts();
		} catch (e) {
			this._notificationsService.error('Ocorreu um erro ao remover o Alerta de Retorno.')
		} finally {
			loading.finished();
		}
	}

	getPatientRecordLink(patientId: number) {
		return PATIENT_RECORD_LINK(patientId);
	}

}
