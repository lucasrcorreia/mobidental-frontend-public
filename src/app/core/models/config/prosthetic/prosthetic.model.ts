export class ProstheticModel {
  bairro: string = null;
  celular: string = null;
  cep: string = null;
  cidade: string = null;
  complemento: string = null;
  dataCadastro: string = null;
  email: string = null;
  endereco: string = null;
  id: number = null;
  nome: string = null;
  numeroEndereco: string = null;
  observacao: string = null;
  telefone: string = null;
  ufCidade: string = null;
};

