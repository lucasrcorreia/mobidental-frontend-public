import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CovenanteCreate } from '../../../../core/models/config/covenant/covenantCreate.model';
import { FormCovenantCreate } from '../../../../core/forms/createCovenant';
import { UtilsService } from '../../../../services/utils/utils.service';

@Component({
  selector: 'app-modal-create',
  templateUrl: './modal-create.component.html',
  styleUrls: [
    './modal-create.component.scss',
    './../../../../../assets/icon/icofont/css/icofont.scss'
  ]
})
export class ModalCreateComponent implements OnInit, OnChanges {
  @Input() modalId: string;
  @Input() covenantList: Array<any>;
  @Input() idDefault: number;
  @Output() resultModal = new EventEmitter<any>();

  formCovenant: FormGroup;

  constructor(public service: UtilsService) {}

  ngOnInit() {}

  ngOnChanges() {
    const covenant = new CovenanteCreate();
    covenant.isCopiaProcedimentos = 1;
    covenant.convenioId = this.idDefault;
    this.formCovenant = this.service.createForm(FormCovenantCreate, covenant);
  }

  closeModal() {
    this.resultModal.emit(null);
    this.service.closeModal(this.modalId, null);
  }

  create() {
    if (this.formCovenant.get('isCopiaProcedimentos').value === 0) {
      this.formCovenant.get('convenioId').setValue(this.idDefault);
    }

    this.resultModal.emit(this.formCovenant.value);
    this.service.closeModal(this.modalId, null);
  }
}
