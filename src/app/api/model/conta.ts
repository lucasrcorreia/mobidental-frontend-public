export interface Conta {
  email: string;
  senha: string;
  nomeDoResponsavel: string;
  razaoSocial: string;
  cpfCnpj: string;
  celular: string;
  telefone: string;
}


