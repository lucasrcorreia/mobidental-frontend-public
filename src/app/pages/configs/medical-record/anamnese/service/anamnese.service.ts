import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AnamneseModel } from '../models/AnamneseModel';
import { API_CONFIGURATION, ApiConfiguration } from "../../../../../api/api-configuration";
import { AnamneseRespostaModel } from "../../../../patients/anamnese/models/AnamneseRespostaModel";

@Injectable({
  providedIn: 'root'
})
export class AnamneseService {

  constructor(
    @Inject(API_CONFIGURATION) private readonly _config: ApiConfiguration,
    private readonly _http: HttpClient,
  ) {
  }

  listAll(): Promise<AnamneseModel[]> {
    return this._http
      .get<AnamneseModel[]>(`${this._config.apiBasePath}/anamnese/all/modelos`)
      .toPromise();
  }

  getOne(id: number): Promise<AnamneseModel> {
    return this._http
      .get<AnamneseModel>(`${this._config.apiBasePath}/anamnese/findOneModelo/` + id)
      .toPromise();
  }

  save(anamnese: AnamneseModel): Promise<AnamneseModel> {
    return this._http
      .post<AnamneseModel>(`${this._config.apiBasePath}/anamnese/salvarModelo`, anamnese)
      .toPromise();
  }

  delete(id: number): Promise<void> {
    return this._http
      .delete<void>(`${this._config.apiBasePath}/anamnese/deleteModelo/` + id)
      .toPromise();
  }

  saveAnswer(anamneseResposta: AnamneseRespostaModel[]): Promise<AnamneseRespostaModel[]> {
    return this._http
      .post<AnamneseRespostaModel[]>(`${this._config.apiBasePath}/anamnese/salvarRespostas`, anamneseResposta)
      .toPromise();
  }

  listAnswerByAnamneseAndPatient(anamneseId: number, patientId: number): Promise<AnamneseRespostaModel[]> {
    return this._http
      .get<AnamneseRespostaModel[]>(
        `${this._config.apiBasePath}/anamnese/obterRespostasByModeloAndPaciente/?modeloId=${anamneseId}&pacienteId=${patientId}`
      )
      .toPromise();
  }

  updateDefaultPatientAnamnese(anamneseId: number, patientId: number) {
    const params = {
      modeloId: anamneseId.toString(),
      pacienteId: patientId.toString(),
    };
    return this._http
      .put(`${this._config.apiBasePath}/paciente/atualizaUltimoModeloAnamneseSalvo`, null, {params}).toPromise();
  }
}
