import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryPatientSchedulingComponent } from './history-patient-scheduling.component';

describe('HistoryPatientSchedulingComponent', () => {
  let component: HistoryPatientSchedulingComponent;
  let fixture: ComponentFixture<HistoryPatientSchedulingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryPatientSchedulingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryPatientSchedulingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
