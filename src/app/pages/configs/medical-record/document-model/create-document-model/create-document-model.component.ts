import { Component, OnInit } from "@angular/core";
import { UtilsService } from "../../../../../services/utils/utils.service";
import { NotificationsService } from "angular2-notifications";
import { ActivatedRoute, Router } from "@angular/router";
import { DocumentModelService } from "../service/document-model.service";
import { DocumentModel } from "../models/DocumentModel";
import { Tag } from "../models/Tag";
import { AsyncTemplate } from "../../../../../lib/helpers/async-template";

const IMPOSSIBLE_ID = "novo";

@Component({
	selector: "app-create-document-model",
	templateUrl: "./create-document-model.component.html",
	styleUrls: ["./create-document-model.component.scss"],
})
export class CreateDocumentModelComponent implements OnInit {
	title = "CONFIG.DOCUMENT_MODEL.LABELS.NEW";
	tags?: Tag[];
	tagNames?: string[];
	documentModel: DocumentModel = {
		nome: "",
		texto: "",
	};

	constructor(
		private readonly _documentModelService: DocumentModelService,
		private readonly _service: UtilsService,
		private readonly _notificationsService: NotificationsService,
		private readonly _route: ActivatedRoute,
		private readonly _asyncTemplate: AsyncTemplate,
		private readonly _router: Router
	) {}

	ngOnInit() {
		this._initDocumentModel();
	}

	navigateBack() {
		this._router.navigate([".."], { relativeTo: this._route });
	}

	async save() {
		const modelId = this._route.snapshot.params.id;
		const operation = modelId === IMPOSSIBLE_ID ? "criado" : "editado";

		try {
			this._service.loading(true);

			await this._documentModelService.save(this.documentModel);

			this.navigateBack();
			this._notificationsService.success(`Modelo ${operation}`);
		} catch (e) {
			this._notificationsService.error(`Houve um erro ao ${operation} o modelo`);
		} finally {
			this._service.loading(false);
		}
	}

	tagIncludesOnText(tag: string) {
		const regex = new RegExp(tag + "(?!_)");
		return this.documentModel.texto.match(regex);
	}

	private async _initDocumentModel() {
		const modelId = this._route.snapshot.params.id;

		if (modelId !== IMPOSSIBLE_ID) {
			try {
				this.title = "CONFIG.DOCUMENT_MODEL.LABELS.EDIT";
				this.documentModel = await this._documentModelService.findOne(modelId);
			} catch (e) {
				this._notificationsService.error("Ocorreu um erro ao obter o modelo");
			}
		}

		this.tags = await this._asyncTemplate.wrapUserFeedback("Falha ao carregar documento", () =>
			this._documentModelService.listTags()
		);
		this.tagNames = this.tags.map((tag) => tag.tag.replace(/^#/, ""));
	}
}
