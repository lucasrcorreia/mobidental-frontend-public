import { Component, OnDestroy, ViewChild } from "@angular/core";
import { CustomDatepickerI18n, I18n } from "../../../shared/ngb-datepicker/datepicker-i18n";
import { NgbDateParserFormatter, NgbDatepickerI18n } from "@ng-bootstrap/ng-bootstrap";
import { NgbDateFRParserFormatter } from "../../../shared/ngb-datepicker/ngb-date-fr-parser-formatter";
import { combineLatest, Observable, Subject, Subscription } from "rxjs";
import {
	BillsToReceiveFilters,
	ngbToDate,
} from "./bills-to-receive-filters/bills-to-receive-filters.component";
import {
	catchError,
	distinctUntilChanged,
	filter,
	flatMap,
	map,
	shareReplay,
	startWith,
	take,
} from "rxjs/operators";
import {
	ContasAReceberListingOptions,
	ContasAReceberListingResult,
	ContasAReceberReport,
	ContasAReceberReportRow,
	ContasReceberApiService,
} from "../../../api/contas-receber-api.service";
import { ContentBillTreatmentModel } from "../../../core/models/patients/patients.model";
import { GlobalSpinnerService } from "../../../lib/global-spinner.service";
import { NotificationsService } from "angular2-notifications";
import { errorToString } from "../../../lib/helpers/error-to-string";
import * as moment from "moment";
import { TranslateService } from "@ngx-translate/core";
import { CONSTANTS } from "../../../core/constants/constants";
import { ModalAnimationComponent } from "../../../shared/modal-animation/modal-animation.component";
import { PaymentClientModel } from "../../../core/models/patients/payment/payment.model";
import Swal from "sweetalert2";
import {
	PdfGeneratorService,
	PDFMake,
} from "../../../services/pdf-generator/pdf-generator.service";
import {
	BLUE_COLOR,
	CommonsReportBuildService,
	GREEN_COLOR,
	REPORT_STYLES,
} from "../../../services/report/commons-report-build.service";

export interface BillsValueResume {
	total: number;
	received: number;
	percent: number;
}

const componentFiltersToApiOptions = (
	filters: BillsToReceiveFilters
): ContasAReceberListingOptions => {
	return {
		dataInicio: ngbToDate(filters.startDate),
		dataFim: ngbToDate(filters.endDate),
		tipoLancamento: filters.lancamento,
		formaDePagamento: filters.pagamento,
		idConvenio: filters.idConvenio,
		descricao: filters.descricao,
		nomePaciente: filters.nomePaciente,
	};
};

@Component({
	selector: "app-bills-to-receive",
	templateUrl: "./bills-to-receive.component.html",
	styleUrls: [
		"./bills-to-receive.component.scss",
		"./../../../../assets/icon/icofont/css/icofont.scss",
	],
	providers: [
		I18n,
		{ provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
		{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
	],
})
export class BillsToReceiveComponent implements OnDestroy {
	private readonly _subs = new Subscription();

	private readonly _forcedRefreshSubject = new Subject<undefined>();

	readonly filtersSubject = new Subject<Partial<BillsToReceiveFilters>>();

	private readonly filters = combineLatest([
		this.filtersSubject.pipe(distinctUntilChanged()),
		this._forcedRefreshSubject.pipe(startWith(undefined)),
	]).pipe(
		map(([filters]) => filters),
		filter((filters) => filters.startDate != null && filters.endDate != null),
		map(componentFiltersToApiOptions)
	);

	private _currentFilters: ContasAReceberListingOptions;

	private readonly contasInfo = this.filters.pipe(
		flatMap((options) => this._fetchContas(options)),
		shareReplay(1)
	);

	readonly bills: Observable<ContentBillTreatmentModel[]> = this.contasInfo.pipe(
		map((it) => it.content),
		catchError(() => [])
	);

	readonly totals: Observable<BillsValueResume> = this.contasInfo.pipe(
		map((info) => ({
			total: info.valorTotal,
			received: info.valorTotalRecebido,
			percent: 0,
		}))
	);

	billPaymentDialogData?: {
		count: number;
		bill: ContentBillTreatmentModel;
		dialogData: PaymentClientModel;
		status: { text: string; color: string };
	};

	billEditDialogData?: {
		count: number;
		bill: ContentBillTreatmentModel;
		status: { text: string; color: string };
	};

	@ViewChild("payParcelModal") payParcelModalRef?: ModalAnimationComponent;
	@ViewChild("billEditModal") billEditModalRef?: ModalAnimationComponent;

	constructor(
		private readonly _contasReceberApiService: ContasReceberApiService,
		private readonly _globalSpinner: GlobalSpinnerService,
		private readonly _notificationsService: NotificationsService,
		private readonly _translate: TranslateService,
		private readonly _pdfGeneratorService: PdfGeneratorService,
		private readonly _commonsReportService: CommonsReportBuildService
	) {
		// Force shareReplay to cache
		this._subs.add(this.bills.subscribe());
		this._subs.add(this.totals.subscribe());
	}

	ngOnDestroy(): void {
		this._subs.unsubscribe();
	}

	refreshContas() {
		this._forcedRefreshSubject.next();
	}

	private async _fetchContas(
		options: ContasAReceberListingOptions
	): Promise<ContasAReceberListingResult> {
		this._currentFilters = options;
		const loadingToken = this._globalSpinner.loadingManager.start();

		try {
			return await this._contasReceberApiService.fetchContasAReceber(options);
		} catch (e) {
			this._notificationsService.error("Erro ao consultar contas", errorToString(e));

			throw e;
		} finally {
			loadingToken.finished();
		}
	}

	async openParcelPayment(row: ContentBillTreatmentModel, total: number) {
		const loadingToken = this._globalSpinner.loadingManager.start();

		try {
			const dialogData = await this._contasReceberApiService.fetchDialogData(row.id);

			this.billPaymentDialogData = {
				dialogData,
				count: total,
				bill: row,
				status: {
					text: this._getStatusDesc(row),
					color: this._getStatusColor(row),
				},
			};

			setTimeout(() => {
				if (this.payParcelModalRef) {
					this.payParcelModalRef.open();
				}
			});
		} finally {
			loadingToken.finished();
		}
	}

	async maybeParcelPayed(dataModal: unknown) {
		if (!dataModal) {
			return;
		}

		this._forcedRefreshSubject.next();
	}

	async estornarPagamento(row: ContentBillTreatmentModel) {
		const confirmation = await Swal.fire({
			title: "Estorno de parcela",
			text: "Deseja realmente estornar essa parcela?",
			type: "question",
			showCancelButton: true,
			confirmButtonText: "Sim, estorne!",
			cancelButtonText: "Não, cancele",
		});

		if (!confirmation.value) {
			return;
		}

		const loadingToken = this._globalSpinner.loadingManager.start();

		try {
			await this._contasReceberApiService.estornar(row.id);
			this._forcedRefreshSubject.next();
			this._notificationsService.success("Parcela estornada com sucesso!");
		} catch (e) {
			this._notificationsService.error("Erro ao estornar parcela", errorToString(e));
		} finally {
			loadingToken.finished();
		}
	}

	async openEditModal(row: ContentBillTreatmentModel, total: number) {
		this.billEditDialogData = {
			count: total,
			bill: row,
			status: {
				text: this._getStatusDesc(row),
				color: this._getStatusColor(row),
			},
		};

		setTimeout(() => {
			if (this.billEditModalRef) {
				this.billEditModalRef.open();
			}
		});
	}

	async maybeEdited(editedData: unknown | null) {
		if (editedData) {
			this._forcedRefreshSubject.next();
		}
	}

	private _getStatusDesc(row: ContentBillTreatmentModel) {
		const expiration = moment(row.dataVencimento, "DD/MM/YYYY");
		const now = moment();
		const amount = row.valor;
		const received = row.valorRecebido;

		if (received) {
			if (received < amount) {
				return this._translate.instant(
					"PATIENTS.FORM_CHECKING_ACCOUNT.STATUS_LIST.RECEIVED_PARTIAL"
				);
			} else {
				return this._translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.STATUS_LIST.RECEIVED_ALL");
			}
		} else {
			if (now.isBefore(expiration)) {
				return this._translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.STATUS_LIST.ON_DATE");
			} else if (now.isSame(expiration, "day")) {
				return this._translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.STATUS_LIST.TODAY");
			} else {
				return this._translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.STATUS_LIST.LATE_PAYMENT");
			}
		}
	}

	private _getStatusColor(row: ContentBillTreatmentModel) {
		const STATUS_LIST_COLOR = CONSTANTS.STATUS_LIST_COLOR;

		const expiration = moment(row.dataVencimento, "DD/MM/YYYY");
		const now = moment();
		const amount = row.valor;
		const received = row.valorRecebido;

		if (received) {
			if (received < amount) {
				return STATUS_LIST_COLOR.RECEIVED_PARTIAL;
			} else {
				return STATUS_LIST_COLOR.RECEIVED_ALL;
			}
		} else {
			if (now.isBefore(expiration)) {
				return STATUS_LIST_COLOR.ON_DATE;
			} else if (now.isSame(expiration, "day")) {
				return STATUS_LIST_COLOR.TODAY;
			} else {
				return STATUS_LIST_COLOR.LATE_PAYMENT;
			}
		}
	}

	async generateReport() {
		const bills = await this._contasReceberApiService.fetchContasAReceberReport(
			this._currentFilters
		);
		const billsValueResume = await BillsToReceiveComponent._observableValue(this.totals);
		const loading = this._globalSpinner.loadingManager.start();
		try {
			const pdfMake = await BillsToReceiveComponent.buildReport(
				bills,
				billsValueResume,
				this._currentFilters,
				this._commonsReportService
			);
			const pdfMakeDefinition = await this._pdfGeneratorService.exportPDFMakeAsPDF(
				pdfMake,
				"",
				"ContasAReceber.pdf"
			);
			pdfMakeDefinition.open();
		} catch (e) {
			this._notificationsService.error("Ocorreu um erro ao emitir o relatório");
		} finally {
			loading.finished();
		}
	}

	static async buildReport(
		contasAReceberReport: ContasAReceberReport,
		billsValueResume: BillsValueResume,
		filters: ContasAReceberListingOptions,
		commonsReportService: CommonsReportBuildService
	): Promise<PDFMake> {
		const pdfMake: PDFMake = { afterContent: [], pageOrientation: "landscape" };
		pdfMake.styles = REPORT_STYLES;
		pdfMake.afterContent.push(
			await BillsToReceiveComponent._buildReportHeader(filters, commonsReportService)
		);
		pdfMake.afterContent.push(await BillsToReceiveComponent._buildReportBody(contasAReceberReport));
		pdfMake.afterContent.push(await BillsToReceiveComponent._buildReportResume(billsValueResume));

		return pdfMake;
	}

	private static async _buildReportHeader(
		filters: ContasAReceberListingOptions,
		commonsReportService: CommonsReportBuildService
	) {
		return [
			await commonsReportService.buildFinancialReportHeader("Relatório de Contas a Receber"),
			{
				...CommonsReportBuildService.buildPeriodo([filters.dataInicio, filters.dataFim]),
				margin: [0, 20, 0, 0],
			},
			CommonsReportBuildService.buildContasTipoLancamento(filters.tipoLancamento),
			{
				...CommonsReportBuildService.buildFormaPagamento(filters.formaDePagamento),
				margin: [0, 0, 0, 10],
			},
		];
	}

	private static _buildReportBody(contasAReceberReport: ContasAReceberReport) {
		return {
			table: {
				widths: [50, 55, 140, 140, 140, 100, 50, 50],
				body: [
					[
						{ text: "Vencimento", style: "tableHeader" },
						{ text: "Recebimento", style: "tableHeader" },
						{ text: "Lançamento", style: "tableHeader" },
						{ text: "Cliente", style: "tableHeader" },
						{ text: "Descrição", style: "tableHeader" },
						{ text: "Forma Pagamento", style: "tableHeader" },
						{ text: "Cheque", style: "tableHeader" },
						{ text: "Valor", style: "tableHeader" },
					],
					...this._buildReportRows(contasAReceberReport.content),
				],
			},
		};
	}

	private static _buildReportRows(bills: ContasAReceberReportRow[]) {
		return bills.map((bill) => [
			{ text: bill.dataVencimento, style: "tableRow", alignment: "center" },
			{ text: bill.dataRecebimento, style: "tableRow", alignment: "center" },
			{ text: bill.tipoLancamento, style: "tableRow" },
			{ text: bill.paciente, style: "tableRow" },
			{ text: bill.descricao, style: "tableRow" },
			{ text: bill.formaRecebimento, style: "tableRow" },
			{ text: bill.numeroCheque, style: "tableRow", alignment: "center" },
			{ text: bill.valor.toFixed(2), style: "tableRow", alignment: "right" },
		]);
	}

	private static _buildReportResume(billsValueResume: BillsValueResume) {
		return {
			columns: [
				{
					width: 695,
					alignment: "right",
					stack: [
						{ text: "Recebidas (R$):", style: "resumeHeader", color: GREEN_COLOR },
						{ text: "A Receber (R$):", style: "resumeHeader", color: BLUE_COLOR },
						{ text: "Total receitas (R$):", style: "resumeHeader" },
					],
				},
				{
					alignment: "right",
					stack: [
						{ text: billsValueResume.received.toFixed(2), style: "resumeRow", color: GREEN_COLOR },
						{
							text: (billsValueResume.total - billsValueResume.received).toFixed(2),
							style: "resumeRow",
							color: BLUE_COLOR,
						},
						{ text: billsValueResume.total.toFixed(2), style: "resumeRow" },
					],
				},
			],
			margin: [0, 30, 0, 0],
		};
	}

	private static async _observableValue<T>(observable: Observable<T>): Promise<T> {
		return observable.pipe(take(1)).toPromise();
	}
}
