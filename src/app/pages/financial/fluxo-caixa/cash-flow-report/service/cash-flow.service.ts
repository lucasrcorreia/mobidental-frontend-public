import { Inject, Injectable } from '@angular/core';
import { API_CONFIGURATION, ApiConfiguration } from "../../../../../api/api-configuration";
import { HttpClient } from "@angular/common/http";
import { DailyCashFlow } from "../models/daily-cash-flow";
import { SellReport } from '../../../sell-report/models/sell-report';
import { MonthlyCashFlow } from '../models/monthly-cash-flow';

@Injectable({
  providedIn: 'root'
})
export class CashFlowService {

  constructor(
    @Inject(API_CONFIGURATION) private readonly _config: ApiConfiguration,
    private readonly _http: HttpClient,
  ) {
  }

  fetchDailyCashFlow(params: { ano: number, mes: string }): Promise<DailyCashFlow> {
    return this._http
      .post<DailyCashFlow>(`${this._config.apiBasePath}/fluxoCaixa/obterFluxoCaixaDiario`, params)
      .toPromise();
  }

  fetchMonthlyCashFlow(params: { ano: number, mes: string }): Promise<MonthlyCashFlow> {
    return this._http
      .post<MonthlyCashFlow>(`${this._config.apiBasePath}/fluxoCaixa/obterFluxoCaixaMensal`, params)
      .toPromise();
  }

  fetchSellReport(params: { ano: number, mes: string }): Promise<SellReport> {
    return this._http
      .post<any>(`${this._config.apiBasePath}/fluxoCaixa/obterRelatorioContaCorrenteDiario`, params)
      .toPromise();
  }
}
