import { TestBed } from '@angular/core/testing';

import { ContaApiService } from './conta-api.service';

describe('ContaApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContaApiService = TestBed.get(ContaApiService);
    expect(service).toBeTruthy();
  });
});
