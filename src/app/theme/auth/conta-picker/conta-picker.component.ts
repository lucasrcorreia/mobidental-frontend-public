import { Component, OnInit } from '@angular/core';
import { ContaApiService } from '../../../api/conta-api.service';
import { ContaUserModel, UserModel } from '../../../core/models/user/user.model';
import { ActivatedRoute, Router } from '@angular/router';
import { PREV_ROUTE_PARAM } from '../choosen-conta-guard.service';
import { LoginService } from '../../../auth/login.service';
import { NotificationsService } from 'angular2-notifications';
import { GlobalSpinnerService } from '../../../lib/global-spinner.service';
import { errorToString } from '../../../lib/helpers/error-to-string';

@Component({
  selector: 'app-conta-picker',
  templateUrl: './conta-picker.component.html',
  styleUrls: ['./conta-picker.component.scss'],
})
export class ContaPickerComponent implements OnInit {

  readonly accounts = this._loadContas();

  constructor(
    private readonly _contaApiService: ContaApiService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _loginService: LoginService,
    private readonly _router: Router,
    private readonly _notificationsService: NotificationsService,
    private readonly _globalSpinnerService: GlobalSpinnerService,
  ) {
  }

  ngOnInit() {
  }

  private _loadContas() {
    const loading = this._globalSpinnerService.loadingManager.start();

    try {
      return this._contaApiService.listarTodasContas();
    } catch (e) {
      this._notificationsService.error('Erro ao carregar contas', errorToString(e));
      throw e;
    } finally {
      loading.finished();
    }
  }

  async selectAccount(conta: ContaUserModel): Promise<ContaUserModel[]> {
    const loadingToken = this._globalSpinnerService.loadingManager.start();

    try {
      await this._loginService.escolherConta(conta.usuarioPerfilContaModuloId);
    } catch (e) {
      this._notificationsService.error('Erro ao escolher conta', errorToString(e));
      return [];
    } finally {
      loadingToken.finished();
    }

    const route = this._activatedRoute.snapshot.queryParams[PREV_ROUTE_PARAM] || '/';

    await this._router.navigate([route]);
  }

}
