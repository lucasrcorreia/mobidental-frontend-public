import { AnamnesePerguntaModel } from '../../models/AnamnesePerguntaModel';
import { AnamneseModel } from '../../models/AnamneseModel';

export interface AnamneseStrategy {
  title: string;
  operation: string;
  question: AnamnesePerguntaModel;
  workWithQuestion(anamnese: AnamneseModel, question: AnamnesePerguntaModel, index): void;
}
