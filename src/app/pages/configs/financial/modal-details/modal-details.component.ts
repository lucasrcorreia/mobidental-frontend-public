import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { UtilsService } from '../../../../services/utils/utils.service';
import { CONSTANTS } from '../../../../core/constants/constants';
import { CategoryInterface, CenterCostInterface } from '../../../../core/models/config/financial/financial.model';
import { ModalAnimationComponent } from '../../../../shared/modal-animation/modal-animation.component';
import { GlobalSpinnerService } from '../../../../lib/global-spinner.service';

@Component({
  selector: 'app-modal-details',
  templateUrl: './modal-details.component.html',
  styleUrls: [
    './modal-details.component.scss',
    './../../../../../assets/icon/icofont/css/icofont.scss',
  ],
})
export class ModalDetailsComponent implements OnInit, OnChanges {
  @Input() typeComp: string;
  @Input() object;
  @Input() titulo;
  @Input() modal: ModalAnimationComponent;

  @Output() reloadItems = new EventEmitter<boolean>();

  item = {
    description: '',
    active: true,
  };

  constructor(
    public service: UtilsService,
    private readonly _globalSpinnerService: GlobalSpinnerService,
  ) {
  }

  ngOnChanges() {
    this.item.description = this.object.descricao;
    this.item.active = this.object.status;
  }

  ngOnInit() {}

  add() {
    this.service.loading(true);
    let ref;
    let ccost: CenterCostInterface = null;
    let category: CategoryInterface = null;

    if (this.typeComp === 'centerCost') {
      ref = CONSTANTS.ENDPOINTS.config.financial.saveCCost;
      ccost = {
        id: this.object.id,
        descricao: this.item.description,
        status: this.item.active,
        excluido: this.object.excluido || false,
        centroCustoPaiId: this.object.centroCustoPaiId,
        centroCustoChilds: null,
      };
      delete ccost.centroCustoChilds;
    } else {
      ref = CONSTANTS.ENDPOINTS.config.financial.saveCat;
      category = {
        id: this.object.id,
        descricao: this.item.description,
        status: this.item.active,
        excluido: this.object.excluido || false,
        categoriaPaiId: this.object.categoriaPaiId,
        categoriaChilds: null,
      };
      delete category.categoriaChilds;
    }

    const loadingToken = this._globalSpinnerService.loadingManager.start();

    this.service
      .httpPOST(ref, this.typeComp === 'centerCost' ? ccost : category)
      .subscribe(
        data => {
          this.reloadItems.emit(true);
          if (this.modal) {
            this.modal.close(false);
          }
          this.service.closeModal('modal-config-financial', false);
          loadingToken.finished();
        },
        err => {

          loadingToken.finished();
          this.service.notification.error(
            this.service.translate.instant('COMMON.ERROR.SEARCH'),
            err.error.error,
          );
        },
      );
  }
}
