import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { InviteInfo, InvitesApiService } from '../../../../api/invites.api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, flatMap, map, shareReplay, take } from 'rxjs/operators';
import { prop } from 'ramda';
import { NotificationsService } from 'angular2-notifications';
import { GlobalSpinnerService } from '../../../../lib/global-spinner.service';
import { errorToString } from '../../../../lib/helpers/error-to-string';
import { LoginService } from '../../../../auth/login.service';

const notNullable = <T>(o: T): o is Exclude<T, null | undefined> => o != null;

export const INVITE_KEY_PARAM = 'inviteKey';

@Component({
  selector: 'app-accept-invite',
  templateUrl: './accept-invite.component.html',
  styleUrls: ['./accept-invite.component.scss'],
})
export class AcceptInviteComponent implements OnInit {

  readonly inviteInfo: Observable<InviteInfo> = this._route.queryParams.pipe(
    map(prop(INVITE_KEY_PARAM)),
    filter(notNullable),
    flatMap(inviteKey => this._fetchInviteInfo(String(inviteKey))),
    shareReplay(1),
  );

  password?: string;
  passwordConfirmation?: string;

  constructor(
    private readonly _route: ActivatedRoute,
    private readonly _router: Router,
    private readonly _invitesApiService: InvitesApiService,
    private readonly _notificationsService: NotificationsService,
    private readonly _globalSpinnerService: GlobalSpinnerService,
    private readonly _loginService: LoginService,
  ) {
  }

  ngOnInit() {
    this._checkUrlParam();
  }

  private async _fetchInviteInfo(key: string): Promise<InviteInfo> {
    const loadingToken = this._globalSpinnerService.loadingManager.start();

    try {
      return await this._invitesApiService.fetchInviteInfo(key);
    } catch (e) {
      await this._router.navigate(['/']);
      this._notificationsService.error('Erro ao carregar convite', errorToString(e));
      throw e;
    } finally {
      loadingToken.finished();
    }
  }

  private async _checkUrlParam() {
    if (!this._route.snapshot.queryParams[INVITE_KEY_PARAM]) {
      await this._router.navigate(['/']);
      this._notificationsService.error('Erro de carregamento', 'Chave de convite não encontrada');
    }
  }

  async aceitarConvite() {
    const inviteInfo = await this.inviteInfo.pipe(take(1)).toPromise();

    if (!inviteInfo.isUsuarioNovo) {
      this.passwordConfirmation = this.password;
    }

    if (this.password.length === 0) {
      this._notificationsService.error('Erro ao aceitar convite', 'Senha não preenchida');
      return;
    }

    if (this.password !== this.passwordConfirmation) {
      this._notificationsService.error('Erro ao aceitar convite', 'Senhas não são iguais');
      return;
    }

    const loadingToken = this._globalSpinnerService.loadingManager.start();

    try {
      const user = await this._invitesApiService.acceptInvite(
        inviteInfo.usuarioEmail,
        this.password,
        inviteInfo.chaveConvite,
      );

      await this._loginService.logAsUser(user);

      if (user.usuarioPerfilContaModuloSelecionado) {
        await this._loginService.escolherConta(
          user.usuarioPerfilContaModuloSelecionado.id,
        );
      }

      await this._router.navigate(['/']);
    } catch (e) {
      this._notificationsService.error('Erro ao aceitar convite', errorToString(e));
    } finally {
      loadingToken.finished();
    }
  }
}
