import { Directive, ElementRef, Input } from "@angular/core";
import { DocumentModel } from "../../../../api/model/image";
import { DocumentsCommonService } from "../documents-common.service";

const finishLoad = (el: HTMLImageElement) =>
	new Promise((resolve, reject) => {
		if (el.complete) return resolve();
		el.addEventListener("load", () => resolve());
		el.addEventListener("error", () => reject());
	});

export interface AppGalleryPhotoswipeItem extends PhotoSwipe.Item {
	title?: string;
	caption?: string;
	document: DocumentModel;
}

@Directive({
	selector: "[appGalleryItem]",
})
export class GalleryItemDirective {
	@Input() document?: DocumentModel;

	private _imgEl?: HTMLImageElement;

	constructor({ nativeElement }: ElementRef, private readonly _docService: DocumentsCommonService) {
		if (nativeElement instanceof HTMLImageElement) {
			this._imgEl = nativeElement;
		}
	}

	@Input()
	set galleryImageSrc(imageUrl: string | null | undefined) {
		if (imageUrl != null && imageUrl.trim()) {
			const img = new Image();
			img.src = imageUrl;

			this._imgEl = img;
		}
	}

	get imageElement() {
		return this._imgEl;
	}

	async toPhotoSwipeItem(): Promise<AppGalleryPhotoswipeItem> {
		const img = this._imgEl;

		await finishLoad(img);

		return {
			src: img.src,
			h: img.naturalHeight,
			w: img.naturalWidth,
			title: this._docService.getLabel(this.document.descricao),
			document: this.document
		};
	}
}
