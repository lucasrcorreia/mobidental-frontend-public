import { Component, OnInit, ViewChild } from '@angular/core';
import { Produto } from '../../../api/model/produto';
import { ModalAnimationComponent } from '../../../shared/modal-animation/modal-animation.component';
import { clone } from 'ramda';
import { ProdutosApiService } from '../../../api/produtos-api.service';
import { GlobalSpinnerService } from '../../../lib/global-spinner.service';
import { NotificationsService } from 'angular2-notifications';
import { errorToString } from '../../../lib/helpers/error-to-string';
import { ProdutosListTableComponent } from './produtos-list-table/produtos-list-table.component';
import { BehaviorSubject } from 'rxjs';
import Swal from 'sweetalert2';
import { AsyncTemplate } from '../../../lib/helpers/async-template';
import { AbaEstoque } from './produtos-list-table/produtos-list-table.component';

@Component({
  selector: 'app-produtos-list',
  templateUrl: './produtos-list.component.html',
  styleUrls: ['./produtos-list.component.scss']
})
export class ProdutosListComponent implements OnInit {
  readonly Pagina = AbaEstoque;

  readonly paginaAtualSubject = new BehaviorSubject(AbaEstoque.TODOS);

  editing?: Produto;
  produtoAAdicionarMovimentacao?: Produto;

  query?: string;

  @ViewChild('table') tableRef?: ProdutosListTableComponent;
  @ViewChild('modalEdit') modalEdit?: ModalAnimationComponent;

  @ViewChild('modalAddMovimentacao')
  modalAddMovimentacaoRef?: ModalAnimationComponent;

  constructor(
    private readonly _produtosApi: ProdutosApiService,
    private readonly _notificationsService: NotificationsService,
    private readonly _asyncTemplate: AsyncTemplate
  ) {}

  ngOnInit() {}

  editProduto(prod: Produto) {
    this.editing = clone(prod);
    if (this.modalEdit) this.modalEdit.open();
  }

  async deleteProduto({ id }: Produto) {
    if (!id) return;

    const result = await Swal.fire({
      title: 'Apagar produto',
      text: 'Tem certeza que deseja apagar esse produto?',
      type: 'question',
      showCancelButton: true,
      confirmButtonText: 'Sim, apagar',
      cancelButtonText: 'Não, cancelar'
    });

    if (!result.value) return;

    this._asyncTemplate.wrapUserFeedback(
      'Ocorreu um erro ao apagar o produto',
      async () => {
        await this._produtosApi.apagarProduto(id);

        this.notificarSucesso('Produto apagado com sucesso');

        this.reload();
      }
    );
  }

  atualizarAba(a: AbaEstoque) {
    this.paginaAtualSubject.next(a);
  }

  reload() {
    if (this.tableRef) this.tableRef.reload();
  }

  notificarEReload(s: string) {
    this.reload();
    this.notificarSucesso(s);
  }

  notificarSucesso(s: string) {
    this._notificationsService.success(s);
  }

  async abrirAdicionarMovimentacao(produto: Produto) {
    this.produtoAAdicionarMovimentacao = produto;
    this.modalAddMovimentacaoRef.open();
  }
}
