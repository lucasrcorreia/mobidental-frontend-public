export enum TipoPerguntaAnamnese {
  SIM_NAO = 'SIM_NAO',
  SIM_NAO_TEXTO = 'SIM_NAO_TEXTO',
  TEXTO = 'TEXTO',
}

export const TIPO_PERGUNTA_ANAMNESE_TRANSLATE: { [key in TipoPerguntaAnamnese]: string } = {
  SIM_NAO: 'Sim ou não',
  SIM_NAO_TEXTO: 'Sim ou não mais texto',
  TEXTO: 'Texto',
};
