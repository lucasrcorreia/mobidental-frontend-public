export interface Produto {
  id?: number | null;
  nome: string;
  unidadeMedida: string;
  produtoEstoqueQuantidade?: number | null;
  estoqueMinimo?: number | null;
}
