import { SellReportItem } from './sell-report-item';

export interface SellReport {
  items?: SellReportItem[];
  totalEntrada?: number;
  totalSaida?: number;
  totalVenda?: number;
}
