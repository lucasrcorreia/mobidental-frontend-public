import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Chart } from 'chart.js';
import { CashFlowStoreService } from '../service/cash-flow-store.service';
import { NotificationsService } from 'angular2-notifications';
import { GlobalSpinnerService } from '../../../../../lib/global-spinner.service';
import { CommonsReportBuildService } from '../../../../../services/report/commons-report-build.service';
import { PdfGeneratorService } from '../../../../../services/pdf-generator/pdf-generator.service';
import { SheetGeneratorService } from '../../../../../services/sheets-generator/sheet-generator.service';
import { ReportPrintType } from '../../../../reports/report-header-actions/report-header-actions.component';
import { CASH_FLOW_MONTHS, CashFlowFilter, numberToBRLCurrency } from '../cash-flow-report.component';
import { MonthlyCashFlow } from '../models/monthly-cash-flow';
import { isEmpty } from 'lodash-es';

@Component({
               selector: 'app-monthly-cash-flow',
               templateUrl: './monthly-cash-flow.component.html',
               styleUrls: ['./monthly-cash-flow.component.scss']
           })
export class MonthlyCashFlowComponent implements OnInit {

    @Input() filter: CashFlowFilter;

    readonly months = CASH_FLOW_MONTHS;
    chart: any;
    cashFlow: MonthlyCashFlow;

    numberToBRLCurrency = numberToBRLCurrency;

    loading;
    readonly portugueseMessages = {
        emptyMessage: 'Nenhum dado cadastrado...',
        totalMessage: 'registro(s)',
        selectedMessage: 'selecionado'
    };

    private _subscriptions: Subscription = new Subscription();

    constructor(
        private readonly _cashFlowStoreService: CashFlowStoreService,
        private readonly _notificationsService: NotificationsService,
        private readonly _globalSpinnerService: GlobalSpinnerService,
        private readonly _commonsReportService: CommonsReportBuildService,
        private readonly _pdfGeneratorService: PdfGeneratorService,
        private readonly _sheetGeneratorService: SheetGeneratorService,
    ) {
    }

    ngOnInit() {
        this._monthlyCashFlowStoreSubscribe();
    }

    ngOnDestroy() {
        this._subscriptions.unsubscribe();
    }

    private _monthlyCashFlowStoreSubscribe() {
        const sub = this._cashFlowStoreService.$monthlyCashFlowObservable.subscribe((cashFlow: MonthlyCashFlow) => {
            this.cashFlow = cashFlow;
            if (cashFlow && !isEmpty(cashFlow.items)) {
                this._buildChart();
            }
        });
        const printSub = this._cashFlowStoreService.$monthlyCashFlowPrintEmitter.subscribe((type: ReportPrintType) => {
            if (type !== undefined) {
                this._generateSheetsReport();
            }
        });
        const loadingSub = this._cashFlowStoreService.$loadingEmitter.subscribe((loading: boolean) => {
            this.loading = loading;
        });
        this._subscriptions.add(sub);
        this._subscriptions.add(printSub);
        this._subscriptions.add(loadingSub);
    }

    private _buildChart() {
        this.chart = new Chart('monthly-canvas', {
            type: 'bar',
            data: {
                datasets: [
                    {
                        label: 'Recebimentos Previstos',
                        data: this.cashFlow.items.map(item => item.recebimentoPrevisto),
                        order: 6,
                        backgroundColor: '#6da97b'
                    },
                    {
                        label: 'Recebimentos Realizados',
                        data: this.cashFlow.items.map(item => -item.recebimentoRealizado),
                        order: 5,
                        backgroundColor: '#28a745'
                    },
                    {
                        label: 'Pagamentos Previstos',
                        data: this.cashFlow.items.map(item => -item.saidaPrevisto),
                        order: 4,
                        backgroundColor: '#de939b'
                    },
                    {
                        label: 'Recebimentos Realizados',
                        data: this.cashFlow.items.map(item => -item.saidaRealizado),
                        order: 3,
                        backgroundColor: '#dc3545'
                    },
                    {
                        label: 'Saldo Previsto',
                        data: this.cashFlow.items.map(item => item.lucroPrejuizoPrevisto),
                        type: 'line',
                        order: 2,
                        backgroundColor: 'transparent',
                        borderColor: '#73777c',
                        pointBackgroundColor: '#73777c'
                    },
                    {
                        label: 'Saldo Realizado',
                        data: this.cashFlow.items.map(item => item.lucroPrejuizoRealizado),
                        type: 'line',
                        order: 1,
                        backgroundColor: 'transparent',
                        borderColor: '#174a7e',
                        pointBackgroundColor: '#174a7e'
                    }
                ],
                labels: this.cashFlow.items.map(item => item.mes.slice(0, 3) + '/' + this.filter.ano)
            },
            options: {
                maintainAspectRatio: false,
                legend: {
                    display: true
                },
                scales: {
                    xAxes: [{
                        display: true
                    }],
                    yAxes: [{
                        display: true
                    }],
                },
                tooltips: {
                    titleFontSize: 15,
                    titleAlign: 'center',
                    backgroundColor: 'DARK_BLUE',
                    callbacks: {
                        title: (tooltipItem) => {
                            const month = CASH_FLOW_MONTHS.find(month => month.value === this.cashFlow.items[tooltipItem[0].index].mes);
                            return (month ? month.label + ' ' : '') + this.filter.ano;
                        },
                        label: function (tooltipItem, data) {
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + numberToBRLCurrency(
                                tooltipItem.yLabel);
                        }
                    }
                },
            }
        });
    }

    private _generateSheetsReport() {
        const loading = this._globalSpinnerService.loadingManager.start();
        try {
            this._sheetGeneratorService.exportDOMTableAsExcelFile(
                'monthly-report-table',
                'relatório-fluxo-caixa-mensal',
                { raw: true }
            );
        } catch (e) {
            this._notificationsService.error('Ocorreu um erro ao emitir o relatório.');
        } finally {
            loading.finished();
        }
    }

}
