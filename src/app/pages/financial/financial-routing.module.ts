import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FluxoCaixaComponent } from './fluxo-caixa/fluxo-caixa.component';
import {SellReportComponent} from './sell-report/sell-report.component';

const routes: Routes = [
  {
    path: 'bills-to-pay',
    loadChildren: './bills-to-pay/bills-to-pay.module#BillsToPayModule',
    data: {
      id: 11,
      children: true,
    },

  },
  {
    path: 'bills-to-receive',
    loadChildren: './bills-to-receive/bills-to-receive.module#BillsToReceiveModule',
    data: {
      id: 12,
      children: true,
      NO_WRAPPER: true,
    },
  },
  {
    path: 'commission',
    loadChildren: './commission/commission.module#CommissionModule',
    data: {
      id: 15,
      children: true,
    },
  },
  {
    path: 'fluxo-caixa',
    component: FluxoCaixaComponent,
    data: {
      // TODO: Adicionar permissão correta
      id: -1,
      children: true,
      NO_WRAPPER: true,
    },
  },
  {
    path: 'sell-report',
    component: SellReportComponent,
    data: {
      // TODO: Adicionar permissão correta
      id: -1,
      children: true,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FinancialRoutingModule {}
