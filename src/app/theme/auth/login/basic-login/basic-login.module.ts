import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasicLoginComponent } from './basic-login.component';
import { BasicLoginRoutingModule } from './basic-login-routing.module';
import { SharedModule } from '../../../../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxTranslateModule } from '../../../../core/translate/translate.module';
import { SimpleNotificationsModule } from 'angular2-notifications';

@NgModule({
  imports: [
    CommonModule,
    BasicLoginRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    NgxTranslateModule,
    SimpleNotificationsModule
  ],
  declarations: [BasicLoginComponent]
})

export class BasicLoginModule {
}


