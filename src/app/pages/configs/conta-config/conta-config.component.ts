import { Component, OnInit } from '@angular/core';
import { ContaApiService, ContaModel } from '../../../api/conta-api.service';
import { LoginService } from '../../../auth/login.service';
import { NotificationsService } from 'angular2-notifications';
import { errorToString } from '../../../lib/helpers/error-to-string';
import { GlobalSpinnerService } from '../../../lib/global-spinner.service';
import { UtilsService } from '../../../services/utils/utils.service';
import { fromEvent } from 'file-selector';
import { DomSanitizer } from '@angular/platform-browser';

export enum ContaConfigForm {
  CADASTRO,
  AGENDA,
  FINANCEIRO,
}

@Component({
  selector: 'app-conta-config',
  templateUrl: './conta-config.component.html',
  styleUrls: ['./conta-config.component.scss'],
})
export class ContaConfigComponent implements OnInit {

  readonly ContaConfigForm = ContaConfigForm;

  paginaAtual: ContaConfigForm = ContaConfigForm.CADASTRO;
  conta: Partial<ContaModel> = {};
  imageFile?: File;

  constructor(
    private readonly _loginService: LoginService,
    private readonly _contaApiService: ContaApiService,
    private readonly _notificationsService: NotificationsService,
    private readonly _globalSpinnerService: GlobalSpinnerService,
    private readonly _domSanitizer: DomSanitizer,
    private readonly _utilsService: UtilsService,
  ) {
  }

  ngOnInit() {
    const { currentConta } = this._loginService;
    if (currentConta) {
      this._fetchConta(currentConta.perfilContaModuloContaId);
    }
  }

  switchPage(page: ContaConfigForm) {
    this.paginaAtual = page;
  }

  async pickFile(event: Event) {
    const [file] = await fromEvent(event);

    if (!(file instanceof File)) {
      throw Error('file is not File');
    }

    this.imageFile = file;
    this.conta.urlImagem = URL.createObjectURL(file);
  }

  profileBackgroundUrl(url: string) {
    return this._domSanitizer.bypassSecurityTrustStyle(
      `url(${url || 'assets/images/avatar-blank.jpg'})`,
    );
  }

  async save() {
    const conta = this.conta as ContaModel;

    const loadingToken = this._globalSpinnerService.loadingManager.start();

    try {
      await this._contaApiService.salvarConta(conta, this.imageFile);
      await this._loginService.reloadConta();
      this._notificationsService.success('Conta salva com sucesso');
    } catch (e) {
      this._notificationsService.error('Erro ao salvar conta', errorToString(e));
    } finally {
      loadingToken.finished();
    }
  }

  private async _fetchConta(id: number) {
    const loadingToken = this._globalSpinnerService.loadingManager.start();

    try {
      this.conta = (await this._contaApiService.fetchContaCompleta(id)) || {};
    } catch (e) {
      this._notificationsService.error('Erro ao carregar conta', errorToString(e));
    } finally {
      loadingToken.finished();
    }
  }

}
