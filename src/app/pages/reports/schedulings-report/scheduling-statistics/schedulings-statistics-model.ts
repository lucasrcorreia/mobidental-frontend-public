export interface SchedulingsStatisticsModel {
	itens: SchedulingsStatisticsItemsModel[];
	porcentagemComparecimento?: number;
	porcentagemFalta?: number;
	totalComparecimento?: number;
	totalFalta?: number;
}

export interface SchedulingsStatisticsItemsModel {
	diaMes?: string;
	faltantes?: number;
	presentes?: number;
}
