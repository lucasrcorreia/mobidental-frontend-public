import { Validators, ValidatorFn } from '@angular/forms';

export interface FormsModel {
  field: string;
  validators: ValidatorFn[];
  group?: FormsModel[];
}
