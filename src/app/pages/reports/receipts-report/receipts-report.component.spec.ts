import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiptsReportComponent } from './receipts-report.component';

describe('ReceiptsReportComponent', () => {
  let component: ReceiptsReportComponent;
  let fixture: ComponentFixture<ReceiptsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiptsReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
