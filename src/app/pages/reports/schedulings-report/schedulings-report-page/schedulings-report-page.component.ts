import { Component, Inject, OnInit } from '@angular/core';
import { ReceiptsReportFilters, ReportsService } from '../../reports.service';
import { UtilsService } from '../../../../services/utils/utils.service';
import { NotificationsService } from 'angular2-notifications';
import { GlobalSpinnerService } from '../../../../lib/global-spinner.service';
import { CommonsReportBuildService } from '../../../../services/report/commons-report-build.service';
import { PdfGeneratorService, PdfMakePageOrientation } from '../../../../services/pdf-generator/pdf-generator.service';
import { SheetGeneratorService } from '../../../../services/sheets-generator/sheet-generator.service';
import { MaskFormatService } from '../../../../services/utils/mask-format.service';
import { AsyncTemplate } from '../../../../lib/helpers/async-template';
import { AbstractReport } from '../../abstract-report';
import { ReportDatatableColumn, Type } from '../../report-datatable/report-datatable.component';
import { CustomDatepickerI18n, I18n } from '../../../../shared/ngb-datepicker/datepicker-i18n';
import { NgbDateParserFormatter, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateFRParserFormatter } from '../../../../shared/ngb-datepicker/ngb-date-fr-parser-formatter';
import { isEmpty } from 'lodash-es';
import { DropdownGenericService } from '../../../patients/form-patients/services/dropdown-generic.service';

const TABLE_COLUMNS: ReportDatatableColumn[] = [
	{ prop: 'dataHoraInicialEvento', translation: 'REPORTS.TABLE.DATE', type: Type.DATE },
	{ prop: 'profissionalNome', translation: 'REPORTS.TABLE.PROFESSIONAL' },
	{ prop: 'pacientePessoaNome', translation: 'REPORTS.TABLE.PATIENT' },
	{ prop: 'celular', translation: 'REPORTS.TABLE.CELL', type: Type.CELL },
	{ prop: 'telefoneFixo', translation: 'REPORTS.TABLE.PHONE', type: Type.PHONE },
	{ prop: 'convenioNome', translation: 'REPORTS.TABLE.COVENANT' },
	{ prop: 'tipoAtendimentoDescricao', translation: 'REPORTS.TABLE.PROCEDURE' },
	{ prop: 'anotacoes', translation: 'REPORTS.TABLE.OBS' },
];

const SHEET_COLUMNS: ReportDatatableColumn[] = [
	{ prop: 'dataHoraInicialEvento', translation: 'REPORTS.TABLE.DATE_TIME_EVENT' },
	{ prop: 'pacientePessoaNome', translation: 'REPORTS.TABLE.PATIENT_NAME' },
	{ prop: 'celular', translation: 'REPORTS.TABLE.CELL', type: Type.CELL },
	{ prop: 'profissionalNome', translation: 'REPORTS.TABLE.PROFESSIONAL_NAME' },
	{ prop: 'tipoAtendimentoDescricao', translation: 'REPORTS.TABLE.ATTENDANCE_TYPE' },
	{ prop: 'emStatusAgendamento', translation: 'REPORTS.TABLE.SCHEDULING_STATUS' },
	{ prop: 'convenioNome', translation: 'REPORTS.TABLE.COVENANT' },
	{ prop: 'telefoneFixo', translation: 'REPORTS.TABLE.PHONE', type: Type.PHONE },
	{ prop: 'anotacoes', translation: 'REPORTS.TABLE.ANOTATIONS' },
];


@Component({
	selector: 'app-schedulings-report-page',
	templateUrl: './schedulings-report-page.component.html',
	styleUrls: ['./schedulings-report-page.component.scss'],
	providers: [
		I18n,
		{ provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
		{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
	],
})
export class SchedulingsReportPageComponent extends AbstractReport implements OnInit {

	readonly columns: ReportDatatableColumn[] = TABLE_COLUMNS;
	readonly sheetColumns = SHEET_COLUMNS;
	filters: ReceiptsReportFilters = { profissionalId: null, convenioId: null };
	ownData: any[];
	sheetTitle = this._service.translate.instant('REPORTS.SCHEDULINGS.REPORT_TITLE');
	sheetFileName = 'relatorio_agendamentos';
	reportPageOrientation: PdfMakePageOrientation = 'landscape';
	reportTableBodyWidths = [50, 130, 130, 60, 60, 95, 100, 100];

	constructor(private readonly _reportsService: ReportsService,
				@Inject(UtilsService)  _service: UtilsService,
				@Inject(DropdownGenericService) _dropdownGenericService: DropdownGenericService,
				@Inject(NotificationsService) _notificationsService: NotificationsService,
				@Inject(GlobalSpinnerService) _globalSpinnerService: GlobalSpinnerService,
				@Inject(CommonsReportBuildService) _commonsReportService: CommonsReportBuildService,
				@Inject(PdfGeneratorService) _pdfGeneratorService: PdfGeneratorService,
				@Inject(SheetGeneratorService) _sheetGeneratorService: SheetGeneratorService,
				@Inject(MaskFormatService) _maskFormatService: MaskFormatService,
				@Inject(AsyncTemplate) _asyncTemplate: AsyncTemplate) {
		super(_service, _dropdownGenericService, _notificationsService, _globalSpinnerService, _commonsReportService, _pdfGeneratorService, _sheetGeneratorService, _maskFormatService, _asyncTemplate);
	}

	ngOnInit() {
		this.onChandePeriod();
		this.initProfessionalDropdown();
		this.initCovenantDropdown();
	}

	async buildReportHeader(): Promise<any> {
		const behaviorFiltersValue = this._behaviorFilters$.value;
		const dates: [Date, Date] = [this._getDateFromFilter(behaviorFiltersValue.dataInicio), this._getDateFromFilter(behaviorFiltersValue.dataFim)];
		return [
			await this._commonsReportService.buildFinancialReportHeader(this.sheetTitle),
			{ ...CommonsReportBuildService.buildPeriodo(dates) },
			{ ...CommonsReportBuildService.buildProfessional(behaviorFiltersValue.professionalName) },
			{ ...CommonsReportBuildService.buildCovenant(behaviorFiltersValue.covenantName) },
			{
				...CommonsReportBuildService.buildStatus(behaviorFiltersValue.listaStatusAgendamentoName),
				margin: [0, 0, 0, 10],
			},
		]
	}

	async search() {
		if (!this.requiredFiltersArePresent) {
			return;
		}
		const filters = { ...this.filters };
		filters.listaStatusAgendamento = isEmpty(filters.listaStatusAgendamento) ? [] : [filters.listaStatusAgendamento.toString()];
		this._behaviorFilters$.next({
			...filters,
			covenantName: this.covenantName,
			professionalName: this.professionalName,
			listaStatusAgendamentoName: this.listaStatusName(this.filters.listaStatusAgendamento, this.statusAgenda),
		});
		this.ownData = await this.fetchOwnData(() => this._reportsService.findAllSchedulings(filters));
	}

}
