import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProstheticToolsComponent } from './prosthetic-tools.component';

describe('ProstheticComponent', () => {
  let component: ProstheticToolsComponent;
  let fixture: ComponentFixture<ProstheticToolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProstheticToolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProstheticToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
