import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AnamnesePerguntaModel } from '../../models/AnamnesePerguntaModel';
import { ATIVO_INATIVO_TRANSLATE, AtivoInativo } from '../../models/enums/AtivoInativo';
import { TIPO_PERGUNTA_ANAMNESE_TRANSLATE, TipoPerguntaAnamnese } from '../../models/enums/TipoPerguntaAnamnese';
import { TIPO_ALERTA_PERGUNTA_ANAMNESE_TRANSLATE, TipoAlertaPerguntaAnamnese } from '../../models/enums/TipoAlertaPerguntaAnamnese';
import { UtilsService } from '../../../../../../services/utils/utils.service';

@Component({
  selector: 'app-create-question',
  templateUrl: './create-question.component.html',
  styleUrls: ['./create-question.component.scss']
})
export class CreateQuestionComponent implements OnInit {

  @Input() title: string;
  @Input() question: AnamnesePerguntaModel;

  readonly questionAlertTranslate = TIPO_ALERTA_PERGUNTA_ANAMNESE_TRANSLATE;
  readonly questionTypeTranslate = TIPO_PERGUNTA_ANAMNESE_TRANSLATE;
  readonly questionActiveTranslate = ATIVO_INATIVO_TRANSLATE;

  readonly statuses = Object.values(AtivoInativo);
  readonly tipoPerguntaAnamnese = TipoPerguntaAnamnese;
  readonly questionTypes = Object.values(TipoPerguntaAnamnese);
  readonly questionAlerts = Object.values(TipoAlertaPerguntaAnamnese);

  constructor(public activeModal: NgbActiveModal,
              readonly _service: UtilsService) {
  }

  ngOnInit() {
  }

}
