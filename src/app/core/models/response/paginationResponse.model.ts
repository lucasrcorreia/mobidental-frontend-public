export interface PaginationResponseModel {
  totalElements: number;
  totalPages: number;
  size: number;
  content: any;
}
