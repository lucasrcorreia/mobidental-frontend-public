import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTreatmentComponent } from './form-treatment.component';

describe('FormTreatmentComponent', () => {
  let component: FormTreatmentComponent;
  let fixture: ComponentFixture<FormTreatmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormTreatmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTreatmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
