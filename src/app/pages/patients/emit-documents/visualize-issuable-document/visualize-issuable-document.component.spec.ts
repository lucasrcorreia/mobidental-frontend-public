import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizeIssuableDocumentComponent } from './visualize-issuable-document.component';

describe('VisualizeIssuableDocumentComponent', () => {
  let component: VisualizeIssuableDocumentComponent;
  let fixture: ComponentFixture<VisualizeIssuableDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizeIssuableDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizeIssuableDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
