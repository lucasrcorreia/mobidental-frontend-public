export interface DetachedTreatment {
	id?: number;
	tratamentoId?: number;
	pacienteId?: number;
	isFaceVestibular?: boolean;
	isFaceMesial?: boolean;
	isFaceLingualPalatina?: boolean;
	isFaceDistal?: boolean;
	isFaceOclusalIncisal?: boolean;
	convenioEspecialidadeProcedimentoId?: number;
	statusTratamentoServico?: string;
	emDente?: string;
	profissionalId?: number;
	profissionalNome?: string;
	valor?: number;
	convenioEspecialidadeProcedimentoNome?: string;
}
