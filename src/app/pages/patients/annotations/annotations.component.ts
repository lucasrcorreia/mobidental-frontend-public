import { Component, Input, OnInit } from '@angular/core';
import { AnnotationsService } from "./service/annotations.service";
import { GlobalSpinnerService } from "../../../lib/global-spinner.service";
import { NotificationsService } from "angular2-notifications";
import { UtilsService } from "../../../services/utils/utils.service";
import { Annotation } from "./models/annotation";
import { PatientModel } from "../../../core/models/patients/patients.model";
import { CreateReturnAlertComponent } from "../../agenda/return-alert/create-alert/create-return-alert.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AnnotationComponent } from "./annotation/annotation.component";
import Swal from "sweetalert2";

@Component({
	selector: 'app-annotations',
	templateUrl: './annotations.component.html',
	styleUrls: ['./annotations.component.scss']
})
export class AnnotationsComponent implements OnInit {

	@Input() patient: PatientModel;

	annotations: Annotation[];
	readonly portugueseMessages = {
		// Message to show when array is presented
		// but contains no values
		emptyMessage: 'Nenhuma anotação cadastrada...',

		// Footer total message
		totalMessage: 'registro(s)',

		// Footer selected message
		selectedMessage: 'selecionado'
	};

	constructor(readonly _service: UtilsService,
				private readonly _annotationsService: AnnotationsService,
				private readonly _globalSpinnerService: GlobalSpinnerService,
				private readonly _notificationService: NotificationsService,
				private readonly _modalService: NgbModal) { }

	ngOnInit() {
		this._fetchAnnotations();
	}

	private async _fetchAnnotations() {
		this.annotations = await this._annotationsService.listAll(this.patient.id);
	}

	async createAnnotation(id?: number) {
		const modalRef = this._modalService.open(AnnotationComponent, { size: 'lg', centered: true });
		modalRef.componentInstance.annotationId = id;
		modalRef.componentInstance.patient = this.patient;

		const result = await modalRef.result;

		if (result) {
			this._fetchAnnotations();
		}
	}

	async remove(id: number) {
		const confirmation = await Swal.fire({
			title: 'Remover Anotação',
			text: 'Deseja realmente remover esta anotação?',
			type: 'question',
			showCancelButton: true,
			confirmButtonText: 'Sim, remover!',
			cancelButtonText: 'Não',
		});

		if (!confirmation.value) {
			return;
		}

		const loading = this._globalSpinnerService.loadingManager.start();
		try {
			await this._annotationsService.remove(id);
			this._notificationService.success('Anotação removida.');
			this._fetchAnnotations();
		} catch (e) {
			this._notificationService.error('Ocorreu um erro ao remover a anotação.');
		} finally {
			loading.finished();
		}
	}

}
