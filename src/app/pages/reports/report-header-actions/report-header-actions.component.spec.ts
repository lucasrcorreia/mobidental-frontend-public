import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportHeaderActionsComponent } from './report-header-actions.component';

describe('ReportHeaderActionsComponent', () => {
  let component: ReportHeaderActionsComponent;
  let fixture: ComponentFixture<ReportHeaderActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportHeaderActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportHeaderActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
