import { TestBed } from '@angular/core/testing';

import { FluxoDeCaixaApiService } from './fluxo-de-caixa-api.service';

describe('FluxoDeCaixaApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FluxoDeCaixaApiService = TestBed.get(FluxoDeCaixaApiService);
    expect(service).toBeTruthy();
  });
});
