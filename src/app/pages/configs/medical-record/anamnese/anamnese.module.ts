import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnamneseComponent } from './anamnese/anamnese.component';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NgxTranslateModule } from '../../../../core/translate/translate.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AnamneseRoutingModule } from './anamnese-routing.module';
import { NgbModalModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SingleAnamneseComponent } from './anamnese/single-anamnese/single-anamnese.component';
import { DragulaModule } from 'ng2-dragula';
import { CreateQuestionComponent } from './anamnese/create-question/create-question.component';
import { UiSwitchModule } from 'ngx-ui-switch';
import { NgxSelectModule } from 'ngx-select-ex';
import { AppLibModule } from "../../../../lib/app-lib.module";

@NgModule({
  declarations: [AnamneseComponent, SingleAnamneseComponent, CreateQuestionComponent],
	imports: [
		CommonModule,
		SimpleNotificationsModule,
		NgxTranslateModule,
		NgSelectModule,
		FormsModule,
		AnamneseRoutingModule,
		ReactiveFormsModule,
		NgbTooltipModule,
		NgxDatatableModule,
		DragulaModule,
		NgbModalModule,
		UiSwitchModule,
		NgxSelectModule,
		AppLibModule,
	],
  entryComponents: [CreateQuestionComponent],
})
export class AnamneseModule {
}
