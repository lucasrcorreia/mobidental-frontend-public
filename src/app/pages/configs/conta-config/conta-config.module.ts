import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContaConfigComponent } from './conta-config.component';
import { ContaConfigCadastroComponent } from './conta-config-cadastro/conta-config-cadastro.component';
import { ContaConfigAgendaComponent } from './conta-config-agenda/conta-config-agenda.component';
import { ContaConfigFinanceiroComponent } from './conta-config-financeiro/conta-config-financeiro.component';
import { ContaConfigRoutingModule } from './conta-config-routing.module';
import { FormsModule } from '@angular/forms';
import { AppLibModule } from '../../../lib/app-lib.module';
import { NgxMaskModule } from 'ngx-mask';
import { NgxCurrencyModule } from 'ngx-currency';
import { IMaskModule } from 'angular-imask';

@NgModule({
  declarations: [
    ContaConfigComponent,
    ContaConfigCadastroComponent,
    ContaConfigAgendaComponent,
    ContaConfigFinanceiroComponent,
  ],
  imports: [
    ContaConfigRoutingModule,
    CommonModule,
    FormsModule,
    AppLibModule,
    NgxMaskModule,
    NgxCurrencyModule,
    IMaskModule,
  ],
})
export class ContaConfigModule {}
