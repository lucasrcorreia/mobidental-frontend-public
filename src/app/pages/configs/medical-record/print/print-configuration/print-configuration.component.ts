import { Component, OnInit } from '@angular/core';
import { MOBI_DENTAL_PATH, PrintConfigurationsService } from '../service/print-configurations.service';
import { PrintConfiguration } from '../model/print-configuration';
import { NotificationsService } from 'angular2-notifications';
import { UtilsService } from '../../../../../services/utils/utils.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LOGO_POSITION_TRANSLATE, LogoPosition } from '../model/enum/logo-position';

@Component({
	selector: 'app-emit-documents',
	templateUrl: './print-configuration.component.html',
	styleUrls: ['./print-configuration.component.scss'],
})
export class PrintConfigurationComponent implements OnInit {

	footerLines;
	printConfiguration: PrintConfiguration;
	readonly MOBI_DENTAL_PATH = MOBI_DENTAL_PATH;
	readonly _logoPositions = Object.values(LogoPosition);
	logoPositionTranslated = LOGO_POSITION_TRANSLATE;

	constructor(readonly _printService: PrintConfigurationsService,
				private readonly _service: UtilsService,
				private readonly _notificationsService: NotificationsService,
				private readonly _route: ActivatedRoute,
				private readonly _router: Router,
	) { }

	ngOnInit() {
		this._initPrintConfigurations();
	}

	private async _initPrintConfigurations() {
		try {
			this.printConfiguration = await this._printService.getConfigurations();
			this.footerLines = this._printService.getRodapePlaceholder();
		} catch (e) {
			this._notificationsService.error('Ocorreu um erro ao exibir as configurações de impressão');
		}
	}

	async save() {
		try {
			this._service.loading(true);
			this.printConfiguration = await this._printService.update(this.printConfiguration);
			this._notificationsService.success('Configurações atualizadas.');
		} catch (e) {
			this._notificationsService.error('Ocorreu um erro ao atualizar as configurações.');
		} finally {
			this._service.loading(false);
		}
	}

}
