export interface RespostaPaginada<T> {
  totalElements: number;
  totalPages: number;
  size: number;
  content: T[];
}
