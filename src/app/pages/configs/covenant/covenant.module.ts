import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CovenantRoutingModule } from './covenant-routing.module';
import { CovenantComponent } from './covenant.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from '../../../shared/shared.module';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxTranslateModule } from '../../../core/translate/translate.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormCovenantComponent } from './form-covenant/form-covenant.component';
import { ModalCreateComponent } from './modal-create/modal-create.component';
import { DirectivesModule } from '../../../core/directive/directives.module';
import { CurrencyMaskConfig, NgxCurrencyModule } from 'ngx-currency';
import { CONSTANTS } from '../../../core/constants/constants';
import { CURRENCY_MASK_CONFIG } from 'ngx-currency/src/currency-mask.config';
import { ModalAddProcedureComponent } from './modal-add-procedure/modal-add-procedure.component';
import { AppLibModule } from '../../../lib/app-lib.module';
import { PipesModule } from '../../../core/pipes/pipes.module';
import { IMaskModule } from 'angular-imask';
import { UiSwitchModule } from "ngx-ui-switch";

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: 'right',
  allowNegative: true,
  allowZero: true,
  decimal: CONSTANTS.COMMONS_VALUES.DECIMAL,
  precision: 2,
  prefix: CONSTANTS.COMMONS_VALUES.CURRENCY_SYMBOL + ' ',
  suffix: '',
  thousands: CONSTANTS.COMMONS_VALUES.THOUSANDS,
  nullable: false
};

@NgModule({
  declarations: [
    CovenantComponent,
    FormCovenantComponent,
    ModalCreateComponent,
    ModalAddProcedureComponent
  ],
	imports: [
		CommonModule,
		NgxDatatableModule,
		SimpleNotificationsModule,
		SharedModule,
		NgSelectModule,
		NgxTranslateModule,
		FormsModule,
		ReactiveFormsModule,
		DirectivesModule,
		NgxCurrencyModule,
		CovenantRoutingModule,
		AppLibModule,
		PipesModule,
		IMaskModule,
		UiSwitchModule,
	],
  providers: [
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig }
  ]
})
export class CovenantModule {}
