import { FormsModel } from '../models/forms/forms.model';
import { Validators } from '@angular/forms';

export const FormPatient: FormsModel[] = [
  {
    field: 'pessoaNome',
    validators: [Validators.required]
  }
];

export const FormPatientTreatment: FormsModel[] = [
  {
    field: 'descricao',
    validators: [Validators.required]
  },
  {
    field: 'profissionalId',
    validators: [Validators.required]
  },
  {
    field: 'dataAbertura',
    validators: [Validators.required]
  },
  {
    field: 'convenioId',
    validators: [Validators.required]
  },
  {
    field: 'statusTratamentoDescricao',
    validators: [Validators.required]
  },
];

