export interface ConvenantModel {
  id: number;
  nome: string;
  isPadrao: boolean;
}
