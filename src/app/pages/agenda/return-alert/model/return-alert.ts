import { ReturnAlertStatus } from './enum/return-alert-status';

export interface ReturnAlert {
	id?: number,
	status?: ReturnAlertStatus,
	dataCadastro?: string,
	dataRetorno?: string,
	usuarioCadastroId?: number,
	motivoId?: number,
	motivoNome?: string,
	pacienteId?: number,
	pacientePessoaCelular?: string,
	pacientePessoaNome?: string,
	pacientePessoaTelefone?: string,
	pacienteUrlImagemPerfil?: string,
	profissionalId?: number,
	profissionalNome?: string,
	observacao?: string
}
