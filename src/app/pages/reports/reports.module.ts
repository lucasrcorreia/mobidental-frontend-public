import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsRoutingModule } from './reports-routing.module';
import { ReportHeaderActionsComponent } from './report-header-actions/report-header-actions.component';
import { ReportDatatableComponent } from './report-datatable/report-datatable.component';
import { AppLibModule } from '../../lib/app-lib.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxTranslateModule } from '../../core/translate/translate.module';
import { NgbDropdownModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [ReportHeaderActionsComponent, ReportDatatableComponent],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    AppLibModule,
    NgxDatatableModule,
    NgxTranslateModule,
    NgbDropdownModule,
    NgbTooltipModule,
  ],
  exports: [
    ReportDatatableComponent,
    ReportHeaderActionsComponent,
  ],
})
export class ReportsModule {
}
