import { FormsModel } from '../models/forms/forms.model';
import { Validators } from '@angular/forms';

export const FormLogin: FormsModel[] = [
  {
    field: 'login',
    validators: [Validators.required, Validators.email]
  },
  {
    field: 'senha',
    validators: [Validators.required]
  }
];

