import { NgModule } from '@angular/core';
import { DecimalWithLocalePipe } from './decimal/decimal';

@NgModule({
  declarations: [
    DecimalWithLocalePipe,
  ],
  imports: [

  ],
  exports: [
    DecimalWithLocalePipe,
  ]
})
export class PipesModule { }
