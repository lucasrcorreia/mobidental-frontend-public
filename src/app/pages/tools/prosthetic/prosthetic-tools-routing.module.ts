import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProstheticToolsComponent } from './prosthetic-tools.component';

const routes: Routes = [
  {
    path: '',
    component: ProstheticToolsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProstheticRoutingModule { }
