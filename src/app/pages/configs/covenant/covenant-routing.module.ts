import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CovenantComponent } from './covenant.component';
import { FormCovenantComponent } from './form-covenant/form-covenant.component';

const routes: Routes = [
  {
    path: '',
    component: CovenantComponent
  },
  {
    path: 'form',
    component: FormCovenantComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CovenantRoutingModule {}
