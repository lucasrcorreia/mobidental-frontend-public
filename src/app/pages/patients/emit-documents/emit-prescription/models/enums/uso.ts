export enum Uso {
  USO_INTERNO = 'USO_INTERNO',
  USO_EXTERNO = 'USO_EXTERNO',
}

export const USO_TRANSLATE: { [key in Uso]: string } = {
  USO_INTERNO: 'Uso interno',
  USO_EXTERNO: 'Uso externo',
};
