import { IssuableDocumentType } from './enum/IssuableDocumentType';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';

export interface IssuableDocument {
	id?: number;
	nomeModelo?: string;
	texto?: string;
	data?: string | NgbDate;
	profissionalId?: string;
	pacienteId?: string;
	tipoDocumento?: IssuableDocumentType;
}
