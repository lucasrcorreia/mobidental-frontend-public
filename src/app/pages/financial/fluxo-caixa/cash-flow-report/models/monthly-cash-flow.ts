import { MonthlyCashFlowItem } from './monthly-cash-flow-item';

export interface MonthlyCashFlow {
    items?: MonthlyCashFlowItem[];
    saldoFinalPrevisto?: number;
    saldoFinalRealizado?: number;
    totalRecebimentoPrevisto?: number;
    totalRecebimentoRealizado?: number;
    totalSaidaPrevisto?: number;
    totalSaidaRealizado?: number;
}
