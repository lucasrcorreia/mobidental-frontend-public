import { SellReportItemChildren } from './sell-report-item-children';

export interface SellReportItem {
  dia?: number;
  data?: string;
  totalVenda?: number;
  totalEntrada?: number;
  totalSaida?: number;
  childrens?: SellReportItemChildren[];

  /* Campos criados no front-end, não conhecidos pelo back-end */
  dayOfWeek?: string;
  dayOfMonth?: string;
  monthAbbreviation?: string;
}
