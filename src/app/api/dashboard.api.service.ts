import { Inject, Injectable } from '@angular/core';
import { API_CONFIGURATION, ApiConfiguration } from './api-configuration';
import { HttpClient } from '@angular/common/http';

export interface TotaisDashboard {
  totalAgendamentosMes: number;
  quantidadePacientesAtivos: number;
  quantidadeAniversariantesMes: number;
  quantidadeDebitos: number;
}

@Injectable({
  providedIn: 'root',
})
export class DashboardApiService {

  constructor(
    @Inject(API_CONFIGURATION) private readonly _config: ApiConfiguration,
    private readonly _http: HttpClient,
  ) {
  }

  fetchDashboard(): Promise<TotaisDashboard> {
    return this._http
      .get<TotaisDashboard>(`${this._config.apiBasePath}/dashboard/getDadosDashboard`)
      .toPromise();
  }

}
