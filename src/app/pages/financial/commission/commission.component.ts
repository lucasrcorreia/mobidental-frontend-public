import { I18n } from "./../../../shared/ngb-datepicker/datepicker-i18n";
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import {
	NgbDateParserFormatter,
	NgbDatepickerI18n,
	NgbDate,
	NgbDateStruct,
} from "@ng-bootstrap/ng-bootstrap";
import { NgbDateFRParserFormatter } from "../../../shared/ngb-datepicker/ngb-date-fr-parser-formatter";
import { CustomDatepickerI18n } from "../../../shared/ngb-datepicker/datepicker-i18n";
import { UtilsService } from "../../../services/utils/utils.service";
import { ColumnsTableModel } from "../../../core/models/table/columns.model";
import { ComissionFinancial } from "../../../core/models/financial/commission/commission.model";
import { CONSTANTS } from "../../../core/constants/constants";
import { forkJoin, Subject, Subscription } from "rxjs";
import { ShortListItem } from "../../../core/models/forms/common/common.model";
import { classToPlain } from "class-transformer";
import * as moment from "moment";
import Swal from "sweetalert2";
import { DatatableComponent, SortType } from "@swimlane/ngx-datatable";
import { takeUntil } from "rxjs/operators";
import {
	CategoryInterface,
	CenterCostInterface,
} from "../../../core/models/config/financial/financial.model";
import { AccountBillsToPayModel } from "../../../core/models/financial/billsToPay/billsToPay.model";
import { FINANCIAL_CONST } from "../constants";
import { SheetGeneratorService } from "../../../services/sheets-generator/sheet-generator.service";
import { NotificationsService } from "angular2-notifications";

// Se o id for null, o select não inicia com "Todos", consta como se não houvesse nenhum selecionado
const TODOS: ShortListItem = {
	id: -1,
	nome: "Todos",
};

const dumbCompare = (a: any, b: any): number => (a < b ? -1 : a > b ? 1 : 0);

const REPORT_NAME = "commisões";

const formatCurrency = (o: unknown) =>
	Number(o || 0).toLocaleString("pt-BR", { style: "currency", currency: "BRL" });

@Component({
	selector: "app-commission",
	templateUrl: "./commission.component.html",
	styleUrls: ["./commission.component.scss"],
	providers: [
		I18n,
		{ provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
		{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
	],
})
export class CommissionComponent implements OnInit, OnDestroy {
	readonly SortType = SortType;

	@ViewChild(DatatableComponent) table: DatatableComponent;
	private destroy$ = new Subject();
	private readonly _subs = new Subscription();

	searchRange = "TODAY";
	BY_RANGE = "BY_RANGE";
	checkAll = false;

	showAccount = false;
	centerCost: CenterCostInterface[];
	account: AccountBillsToPayModel;
	category: CategoryInterface[];
	readonly localConst = FINANCIAL_CONST;

	locale = this.service.translate.instant("COMMON.LOCALE");

	columns: ColumnsTableModel[] = [
		{
			action: true,
			name: this.service.translate.instant("FINANCIAL.COMMISSION.TABLE.COL1"),
			prop: "id",
			sortable: false,
			width: 30,
		},
		{
			action: false,
			name: this.service.translate.instant("FINANCIAL.COMMISSION.TABLE.COL2"),
			prop: "data",
			sortable: false,
			width: 120,
		},
		{
			action: false,
			name: this.service.translate.instant("FINANCIAL.COMMISSION.TABLE.COL3"),
			prop: "profissionalNome",
			sortable: true,
			comparator: dumbCompare,
			width: 150,
		},
		{
			action: false,
			name: this.service.translate.instant("FINANCIAL.COMMISSION.TABLE.COL4"),
			prop: "tratamentoServicoPacientePessoaNome",
			sortable: true,
			comparator: dumbCompare,
			width: 150,
		},
		{
			action: false,
			name: this.service.translate.instant("FINANCIAL.COMMISSION.TABLE.COL5"),
			prop: "tratamentoServicoConvenioEspecialidadeProcedimentoProcedimentoNome",
			sortable: true,
			comparator: dumbCompare,
			width: 250,
		},
		{
			action: false,
			name: this.service.translate.instant("FINANCIAL.COMMISSION.TABLE.COL6"),
			prop: "tratamentoServicoConvenioEspecialidadeProcedimentoConvenioEspecialidadeConvenioNome",
			sortable: true,
			comparator: dumbCompare,
			width: 150,
		},
		{
			action: false,
			name: this.service.translate.instant("FINANCIAL.COMMISSION.TABLE.COL7"),
			prop: "tratamentoServicoValorComDesconto",
			sortable: false,
			width: 140,
			currency: true,
		},
		{
			action: false,
			name: this.service.translate.instant("FINANCIAL.COMMISSION.TABLE.COL8"),
			prop: "valor",
			sortable: false,
			width: 120,
			currency: true,
		},
		{
			action: false,
			name: this.service.translate.instant("FINANCIAL.COMMISSION.TABLE.COL9"),
			prop: "tratamentoServicoPorcentagemComissao",
			sortable: false,
			width: 100,
		},
		{
			action: false,
			name: this.service.translate.instant("FINANCIAL.COMMISSION.TABLE.COL10"),
			prop: "valorPagar",
			sortable: false,
			width: 100,
			currency: true,
		},
	];

	listSearchRange = [
		{
			label: this.service.translate.instant("FINANCIAL.LIST_SEARCH.RANGE.TODAY"),
			value: "TODAY",
		},
		{
			label: this.service.translate.instant("FINANCIAL.LIST_SEARCH.RANGE.WEEK"),
			value: "WEEK",
		},
		{
			label: this.service.translate.instant("FINANCIAL.LIST_SEARCH.RANGE.MONTH"),
			value: "MONTH",
		},
		{
			label: this.service.translate.instant("FINANCIAL.LIST_SEARCH.RANGE.PREV_MONTH"),
			value: "PREV_MONTH",
		},
		{
			label: this.service.translate.instant("FINANCIAL.LIST_SEARCH.RANGE.BY_RANGE"),
			value: "BY_RANGE",
		},
	];

	rows = {};
	rowsPage: Array<ComissionFinancial> = [];

	searchParam = {
		convenioId: -1,
		dataFim: null,
		dataInicio: null,
		profissionalId: -1,
		statusComissao: "ABERTO",
		tipoDoPagamento: "PROCEDIMENTO_FINALIZADO_E_RECEBIMENTO",
	};

	listConvenant: Array<ShortListItem> = [];
	listProfessional: Array<ShortListItem> = [];

	listPaymentType = [
		{
			display: "Procedimento finalizado e recebido",
			value: "PROCEDIMENTO_FINALIZADO_E_RECEBIMENTO",
		},
		{
			display: "Procedimento finalizado",
			value: "PROCEDIMENTO_FINALIZADO",
		},
	];

	listStatus = [
		{
			label: this.service.translate.instant("FINANCIAL.COMMISSION.ABERTO"),
			value: "ABERTO",
		},
		{
			label: this.service.translate.instant("FINANCIAL.COMMISSION.RECEBIDO"),
			value: "RECEBIDO",
		},
	];

	page = {
		size: 100,
		totalElements: 0,
		totalPages: 0,
		pageNumber: 0,
	};

	constructor(
		public service: UtilsService,
		private cd: ChangeDetectorRef,
		private readonly _sheetGeneratorService: SheetGeneratorService,
		private readonly _notificationsService: NotificationsService
	) {
		this.service.toggleMenu.pipe(takeUntil(this.destroy$)).subscribe((data) => {
			if (data) {
				this.updateDataTable();
			}
		});
	}

	ngOnInit() {
		this.initLists();
	}

	ngOnDestroy() {
		this._subs.unsubscribe();
		this.destroy$.next();
		this.destroy$.complete();
	}

	updateDataTable() {
		this.cd.markForCheck();
		setTimeout(() => {
			this.table.recalculate();
			(this.table as any).cd.markForCheck();
		});
	}

	imprimir() {
		const rows = this.rowsPage || [];

		if (rows.length < 1) {
			this._notificationsService.info("Nenhum resultado para ser exportado");
			return;
		}

		const tabelas = rows.map((row) =>
			this.columns.reduce(
				(obj, col) =>
					col.action
						? obj
						: {
								...obj,
								[col.name]: col.currency ? formatCurrency(row[col.prop]) : row[col.prop],
						  },
				{} as Record<string, string>
			)
		);

		this._sheetGeneratorService.exportJsonAsExcelFileTyped(tabelas, REPORT_NAME);
	}

	initLists() {
		const convenant = this.service.httpGET(CONSTANTS.ENDPOINTS.patients.treatment.convenant);
		const professionals = this.service.httpGET(CONSTANTS.ENDPOINTS.professional.active);

		const sub = forkJoin([convenant, professionals]).subscribe(
			(list) => {
				this.listConvenant = list[0].body as Array<ShortListItem>;
				this.listConvenant.unshift(TODOS);
				this.listProfessional = list[1].body as Array<ShortListItem>;
				this.listProfessional.unshift(TODOS);
				this.populateTable();
			},
			(err) => {
				this.service.loading(false);
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			}
		);

		this._subs.add(sub);
	}

	onrangeChange() {
		switch (this.searchRange) {
			case "TODAY":
				this.searchParam.dataInicio = moment().format("DD/MM/YYYY");
				this.searchParam.dataFim = moment().format("DD/MM/YYYY");
				break;

			case "WEEK":
				this.searchParam.dataInicio = moment().startOf("week").format("DD/MM/YYYY");
				this.searchParam.dataFim = moment().endOf("week").format("DD/MM/YYYY");
				break;

			case "MONTH":
				this.searchParam.dataInicio = moment().startOf("month").format("DD/MM/YYYY");
				this.searchParam.dataFim = moment().endOf("month").format("DD/MM/YYYY");
				break;

			case "PREV_MONTH":
				this.searchParam.dataInicio = moment()
					.subtract(1, "month")
					.startOf("month")
					.format("DD/MM/YYYY");
				this.searchParam.dataFim = moment()
					.subtract(1, "month")
					.endOf("month")
					.format("DD/MM/YYYY");
				break;

			case this.BY_RANGE:
				this.searchParam.dataInicio = this._coerceToDatepickerStruct(
					this.searchParam.dataInicio || moment().startOf("month").format("DD/MM/YYYY")
				);
				this.searchParam.dataFim = this._coerceToDatepickerStruct(
					this.searchParam.dataFim || moment().format("DD/MM/YYYY")
				);
				break;

			default:
				break;
		}
	}

	private _coerceToDatepickerStruct(data: string | object): NgbDateStruct {
		if (typeof data === "string") {
			return this.service.parseDatePicker(data);
		}

		return data as NgbDateStruct;
	}

	async populateTable() {
		this.service.loading(true);
		const param = { ...this.searchParam };
		param.convenioId = param.convenioId === -1 ? null : param.convenioId;
		param.profissionalId = param.profissionalId === -1 ? null : param.profissionalId;

		if (this.searchRange === this.BY_RANGE) {
			param.dataInicio = this.service.parseDateFromDatePicker(this.searchParam.dataInicio);
			param.dataFim = this.service.parseDateFromDatePicker(this.searchParam.dataFim);
		}

		try {
			const data = await this.service
				.httpPOST(CONSTANTS.ENDPOINTS.financial.commission.table, param)
				.toPromise();
			this.rowsPage = data.body as ComissionFinancial[];

			this.page.pageNumber = 0;
			this.page.size = this.rowsPage.length;
			this.page.totalElements = this.rowsPage.length;
			this.page.totalPages = 1;
			this.clearAllItens();
		} catch (err) {
			this.service.notification.error(
				this.service.translate.instant("COMMON.ERROR.SEARCH"),
				err.error.error
			);
		} finally {
			this.service.loading(false);
		}
	}

	clearAllItens(): void {
		this.rowsPage.map((row) => (row.checked = false));
		this.checkAll = false;
	}

	checkAllItens() {
		this.rowsPage.map((row) => {
			row.checked = this.checkAll;
		});
	}

	getStatusAllItens() {
		let result = true;
		this.rowsPage.map((row) => {
			if (!row.checked) {
				result = false;
			}
		});

		this.checkAll = result;
	}

	changeStatus() {
		this.rowsPage = [];
		this.rows = {};
		this.populateTable();
	}

	async authorizeSelecteds(remove = false, translatePath: string) {
		const confirm = await this._confirmOperationDialog(translatePath);
		if (!confirm) {
			return;
		}
		let list = [];
		this.rowsPage.map((item) => {
			if (item.checked) {
				const itemList = classToPlain(item) as ComissionFinancial;
				delete itemList.checked;
				list.push(itemList);
			}
		});

		if (list.length > 0) {
			this.service
				.httpPOST(
					remove
						? CONSTANTS.ENDPOINTS.financial.commission.unauthorize
						: CONSTANTS.ENDPOINTS.financial.commission.authorize,
					list
				)
				.subscribe(
					() => {
						this.rowsPage = [];
						this.rows = {};
						this.checkAll = false;
						this.service.notification.success(
							this.service.translate.instant(
								remove
									? "FINANCIAL.COMMISSION.SUCCESS.UNAUTHORIZE"
									: "FINANCIAL.COMMISSION.SUCCESS.AUTHORIZE"
							)
						);
						this.populateTable();
					},
					(err) => {
						this.service.loading(false);
						this.service.notification.error(
							this.service.translate.instant("COMMON.ERROR.SEARCH"),
							err.error.error
						);
					}
				);
		}
	}

	private async _confirmOperationDialog(translatePath: string): Promise<boolean> {
		const translate = this.service.translate;
		const refSWAL = "FINANCIAL.COMMISSION." + translatePath;

		const { value } = await Swal.fire({
			title: translate.instant(`${refSWAL}.TITLE`),
			text: translate.instant(`${refSWAL}.TEXT`),
			type: "question",
			showCancelButton: true,
			confirmButtonText: translate.instant(`${refSWAL}.CONFIRM_BUTTON`),
			cancelButtonText: translate.instant(`${refSWAL}.CANCEL_BUTTON`),
		});

		return value;
	}

	paySelecteds() {
		if (!this.searchParam.profissionalId || this.searchParam.profissionalId === -1) {
			this.service.notification.info(
				"Atenção",
				"É necessário informar um dentista para realizar o pagamento."
			);
			return;
		}

		const translate = this.service.translate;
		const refSWAL = "FINANCIAL.COMMISSION.SWAL";

		Swal.fire({
			title: translate.instant(`${refSWAL}.TITLE`),
			text: translate.instant(`${refSWAL}.TEXT`),
			type: "question",
			showCancelButton: true,
			confirmButtonText: translate.instant(`${refSWAL}.CONFIRM_BUTTON`),
			cancelButtonText: translate.instant(`${refSWAL}.CANCEL_BUTTON`),
		}).then((result) => {
			if (result.value) {
				let list = [];
				this.rowsPage.map((item) => {
					if (item.checked) {
						const itemList = classToPlain(item) as ComissionFinancial;
						delete itemList.checked;
						list.push(itemList);
					}
				});

				if (list.length > 0) {
					this.service.httpPOST(CONSTANTS.ENDPOINTS.financial.commission.pay, list).subscribe(
						async () => {
							this.rowsPage = [];
							this.rows = {};
							this.checkAll = false;
							await this.populateTable();

							const confirm = await this._confirmOperationDialog("SWAL_SEND_TO_FINANCIAL_FLOW");
							if (!confirm) {
								return;
							}

							this.initSendPaymentToFinancialFlowDialog(list);
						},
						(err) => {
							this.service.loading(false);
							this.service.notification.error(
								this.service.translate.instant("COMMON.ERROR.SEARCH"),
								err.error.error
							);
						}
					);
				}
			}
		});
	}

	findProfessionalById(id: number): string {
		const professional = this.listProfessional.find((professional) => professional.id === id);
		return professional ? professional.nome : "N/A";
	}

	async initSendPaymentToFinancialFlowDialog(list: ComissionFinancial[]) {
		await this.initDropdownsForSendPaymentToFinancialFlow();

		this.account = new AccountBillsToPayModel();
		this.account.descricao =
			"Pagamento de profissionais - " + this.findProfessionalById(this.searchParam.profissionalId);
		this.account.valor = list.map((p) => p.valorPagar).reduce((sum, value) => sum + value);
		this.account.tipoCusto = FINANCIAL_CONST.VARIABLE;
		this.account.numeroTotalParcelas = 1;
		this.account.dataCompetencia = this.service.parseDatePicker(moment().format("DD/MM/YYYY"));
		this.account.dataVencimento = this.service.parseDatePicker(moment().format("DD/MM/YYYY"));
		this.account.formaDeRecebimento = this.localConst.DINHEIRO;

		this.showAccount = true;
		setTimeout(() => {
			document.querySelector("#modal-account-pay").classList.add("md-show");
		}, 500);
	}

	async initDropdownsForSendPaymentToFinancialFlow() {
		this.service.loading(true);

		const refCCost = this.service.httpGET(CONSTANTS.ENDPOINTS.config.financial.centerCost);
		const refCat = this.service.httpGET(CONSTANTS.ENDPOINTS.config.financial.category);

		try {
			this.service.loading(true);
			const list = await forkJoin([refCCost, refCat]).toPromise();
			this.centerCost = list[0].body as CenterCostInterface[];
			this.category = list[1].body as CategoryInterface[];
		} catch (err) {
			this.service.notification.error(
				this.service.translate.instant("COMMON.ERROR.SEARCH"),
				err.error.error
			);
		} finally {
			this.service.loading(false);
		}
	}

	async processResultModalAccount(event: unknown) {
		const refCat = await this.service
			.httpGET(CONSTANTS.ENDPOINTS.config.financial.category)
			.toPromise();

		this.category = refCat.body as CategoryInterface[];

		if (event) {
			this.populateTable();
		}

		this.showAccount = false;
	}
}
