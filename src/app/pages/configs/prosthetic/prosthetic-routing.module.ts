import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProstheticComponent } from './prosthetic.component';
import { FormProstheticComponent } from "./form-prosthetic/form-prosthetic.component";

const routes: Routes = [
  {
    path: '',
    component: ProstheticComponent
  },
  {
    path: ':id',
    component: FormProstheticComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProstheticRoutingModule { }
