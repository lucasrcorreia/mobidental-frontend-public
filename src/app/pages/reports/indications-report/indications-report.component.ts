import { Component, Inject, OnInit } from '@angular/core';
import { ReportDatatableColumn } from '../report-datatable/report-datatable.component';
import { ReceiptsReportFilters, ReportsService } from '../reports.service';
import { AbstractReport } from '../abstract-report';
import { UtilsService } from '../../../services/utils/utils.service';
import { SheetGeneratorService } from '../../../services/sheets-generator/sheet-generator.service';
import { MaskFormatService } from '../../../services/utils/mask-format.service';
import { AsyncTemplate } from '../../../lib/helpers/async-template';
import { NgbDateParserFormatter, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { CustomDatepickerI18n, I18n } from '../../../shared/ngb-datepicker/datepicker-i18n';
import { NgbDateFRParserFormatter } from '../../../shared/ngb-datepicker/ngb-date-fr-parser-formatter';
import { NotificationsService } from 'angular2-notifications';
import { GlobalSpinnerService } from '../../../lib/global-spinner.service';
import { CommonsReportBuildService } from '../../../services/report/commons-report-build.service';
import { PdfGeneratorService } from '../../../services/pdf-generator/pdf-generator.service';
import { DropdownGenericService } from '../../patients/form-patients/services/dropdown-generic.service';

const AMOUNMT_PROP = 'quantidade';
const NAME_PROP = 'indicacao';

const TABLE_COLUMNS: ReportDatatableColumn[] = [
	{ prop: NAME_PROP, translation: 'REPORTS.TABLE.NAME' },
	{ prop: AMOUNMT_PROP, translation: 'REPORTS.TABLE.AMOUNT', cellClassName: 'text-center', pdfAlignment: 'center' },
];

interface Indications {
	quantidade?: number;
	indicacao?: string;
	porcentagem?: number;
}

export interface NgxChartView {
	name: string;
	value: number;
}

let formattedLabels: { [key: string]: string };

const fetchFormattedLabel = (label): string => {
	return formattedLabels[label];
}

@Component({
	selector: 'app-indications-report',
	templateUrl: './indications-report.component.html',
	styleUrls: ['./indications-report.component.scss'],
	providers: [
		I18n,
		{ provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
		{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
	],
})
export class IndicationsReportComponent extends AbstractReport implements OnInit {

	filters: ReceiptsReportFilters = {};
	ownData: any[];
	sheetFileName = 'relatorio_indicacoes';
	readonly columns = TABLE_COLUMNS;

	sheetTitle = this._service.translate.instant('REPORTS.INDICATIONS.TITLE');
	reportTableBodyWidths: number[] = [400, 135];

	results: NgxChartView[];
	percents: { [key: string]: string };
	colorScheme = {
		domain: [],
	};

	constructor(private readonly _reportsService: ReportsService,
				@Inject(UtilsService)  _service: UtilsService,
				@Inject(DropdownGenericService) _dropdownGenericService: DropdownGenericService,
				@Inject(NotificationsService) _notificationsService: NotificationsService,
				@Inject(GlobalSpinnerService) _globalSpinnerService: GlobalSpinnerService,
				@Inject(CommonsReportBuildService) _commonsReportService: CommonsReportBuildService,
				@Inject(PdfGeneratorService) _pdfGeneratorService: PdfGeneratorService,
				@Inject(SheetGeneratorService) _sheetGeneratorService: SheetGeneratorService,
				@Inject(MaskFormatService) _maskFormatService: MaskFormatService,
				@Inject(AsyncTemplate) _asyncTemplate: AsyncTemplate) {
		super(_service, _dropdownGenericService, _notificationsService, _globalSpinnerService, _commonsReportService, _pdfGeneratorService, _sheetGeneratorService, _maskFormatService, _asyncTemplate);
	}

	ngOnInit() {
		this.onChandePeriod();
	}

	labelFormatting(label) {
		return fetchFormattedLabel(label);
	}

	async search() {
		this._behaviorFilters$.next({ ...this.filters });
		this.ownData = await this.fetchOwnData(() => this._reportsService.findAllIndications({ ...this.filters }));
		this.results = this.ownData.map((i: Indications) => {return { name: i.indicacao, value: i.quantidade }});
		this.colorScheme.domain = IndicationsReportComponent.generateRandomColors(this.colorScheme.domain, this.results);
		this._buildPieChartCustomLabels();
	}

	private _buildPieChartCustomLabels() {
		this.ownData.forEach((i: Indications) => {
			const name = i.indicacao;
			const value = i.quantidade;
			const percent = i.porcentagem;
			formattedLabels = { ...formattedLabels, [name]: name + ', ' + value + ' (' + percent + '%)' };
			this.percents = { ...this.percents, [name]: percent + '%' }
		});
	}

	async buildReportHeader() {
		const behaviorFiltersValue = this._behaviorFilters$.value;
		const dates: [Date, Date] = [this._getDateFromFilter(behaviorFiltersValue.dataInicio), this._getDateFromFilter(behaviorFiltersValue.dataFim)];
		return [
			await this._commonsReportService.buildFinancialReportHeader(this.sheetTitle),
			{
				...CommonsReportBuildService.buildPeriodo(dates),
				margin: [0, 20, 0, 10],
			},
		]
	}

	static generateRandomColors(colors: any[], results: any[]): string[] {
		const letters = '0123456789ABCDEF';
		colors = [];
		results.forEach(v => {
			let color = '#';
			for (var i = 0; i < 6; i++) {
				color += letters[Math.floor(Math.random() * 16)];
			}
			colors.push(color);
		});

		return colors;
	}

}
