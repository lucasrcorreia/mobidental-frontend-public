import {
	Component,
	ElementRef,
	EventEmitter,
	HostBinding,
	Input,
	OnInit,
	Output,
	ViewChild,
} from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import {
	ContextmenuType,
	DatatableComponent,
	DatatableRowDetailDirective,
} from "@swimlane/ngx-datatable";
import * as moment from "moment";
import compose from "ramda/es/compose";
import equals from "ramda/es/equals";
import not from "ramda/es/not";
import { Subject } from "rxjs";
import Swal from "sweetalert2";
import { ContasReceberApiService } from "../../api/contas-receber-api.service";
import { TipoParcela } from "../../api/model/tipo-parcela";
import { CONSTANTS } from "../../core/constants/constants";
import { ContentBillTreatmentModel, PatientModel } from "../../core/models/patients/patients.model";
import { ColumnsTableModel } from "../../core/models/table/columns.model";
import { AsyncTemplate } from "../../lib/helpers/async-template";
import { UtilsService } from "../../services/utils/utils.service";
import { PaymentReceipt } from "../../shared/payment-receipt/model/paymentReceipt";
import { PaymentReceiptComponent } from "../../shared/payment-receipt/payment-receipt.component";

// TODO: Melhorar componente, o atual foi escrito a partir de um componente legado
@Component({
	selector: "app-contas-a-receber-table",
	templateUrl: "./contas-a-receber-table.component.html",
	styleUrls: ["./contas-a-receber-table.component.scss"],
})
export class ContasAReceberTableComponent implements OnInit {
	@ViewChild("tableParcels") tableParcels?: DatatableComponent;
	@ViewChild(DatatableRowDetailDirective) rowDetailRef?: DatatableRowDetailDirective;

	@Output() readonly parcelPayment = new Subject<ContentBillTreatmentModel>();
	@Output() readonly parcelEdit = new Subject<ContentBillTreatmentModel>();
	@Output() readonly reversePayment = new Subject<ContentBillTreatmentModel>();
	@Output() readonly refreshBills = new EventEmitter<void>();

	@HostBinding("style.position") readonly _hostPosition = "relative";

	activeDetail?: ContentBillTreatmentModel;

	readonly parcelTypeCannotDelete = TipoParcela.PARCELA_TRATAMENTO;

	dropdownPos: [number, number] = [0, 0];

	readonly numeroGuiaColumnDefinition = {
		action: false,
		name: "Número Guia",
		prop: "guiaConvenio",
		sortable: false,
		width: 120,
	};

	readonly detailColumns: ColumnsTableModel[] = [
		{
			action: true,
			name: this.translateService.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_QUOTA.COL1"),
			prop: " id",
			sortable: false,
			width: 105,
		},
		{
			action: false,
			name: this.translateService.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_DETAILS.COL2"),
			prop: "dataRecebimento",
			sortable: true,
			width: 100,
		},
		{
			action: false,
			name: this.translateService.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_DETAILS.COL3"),
			prop: "valorRecebido",
			sortable: true,
			currency: true,
			width: 65,
		},
		{
			action: false,
			name: this.translateService.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_DETAILS.COL4"),
			prop: "formaDeRecebimentoDescricao",
			sortable: true,
			width: 150,
		},
	];

	columns: ColumnsTableModel[] = [
		{
			action: true,
			name: this.translateService.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_QUOTA.COL1"),
			prop: " id",
			sortable: false,
			width: 105,
		},
		{
			action: false,
			name: this.translateService.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_QUOTA.COL3"),
			prop: "dataVencimento",
			sortable: true,
			width: 90,
		},
		{
			action: false,
			name: "Paciente",
			prop: "pacientePessoaNome",
			sortable: true,
			width: 200,
		},
		{
			action: false,
			name: "Descrição",
			prop: "descricao",
			sortable: true,
			width: 200,
		},
		{
			action: false,
			name: this.translateService.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_QUOTA.COL2"),
			prop: "numeroParcela",
			sortable: true,
			width: 70,
		},
		{
			action: false,
			name: this.translateService.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_QUOTA.COL4"),
			prop: "valor",
			sortable: true,
			currency: true,
			width: 130,
		},
		{
			action: false,
			name: this.translateService.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_QUOTA.COL5"),
			prop: "valorRecebido",
			sortable: true,
			currency: true,
			width: 130,
		},
		{
			action: false,
			name: this.translateService.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_QUOTA.COL6"),
			prop: "formaDeRecebimentoDescricao",
			sortable: true,
			width: 200,
		},
		{
			action: false,
			name: this.translateService.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_QUOTA.COL7"),
			prop: "status",
			sortable: true,
			width: 150,
		},
	];

	readonly locale = this.translateService.instant("COMMON.LOCALE");

	private _bills: ContentBillTreatmentModel[] = [];

	constructor(
		readonly translateService: TranslateService,
		private readonly _modalService: NgbModal,
		private readonly _contasReceberApiService: ContasReceberApiService,
		private readonly _asyncTemplate: AsyncTemplate,
		private readonly _hostElementRef: ElementRef<HTMLElement>,
		public readonly service: UtilsService
	) {}

	ngOnInit() {}

	@Input()
	set bills(bills: ContentBillTreatmentModel[]) {
		this._bills = bills;
		this.activeDetail = undefined;
	}

	get bills(): ContentBillTreatmentModel[] {
		return this._bills;
	}

	getStatusDesc(row: ContentBillTreatmentModel) {
		const expiration = moment(row.dataVencimento, "DD/MM/YYYY");
		const now = moment();
		const amount = row.valor;
		const received = row.valorRecebido;

		if (received) {
			if (received < amount) {
				return this.translateService.instant(
					"PATIENTS.FORM_CHECKING_ACCOUNT.STATUS_LIST.RECEIVED_PARTIAL"
				);
			} else {
				return this.translateService.instant(
					"PATIENTS.FORM_CHECKING_ACCOUNT.STATUS_LIST.RECEIVED_ALL"
				);
			}
		} else {
			if (now.isBefore(expiration)) {
				return this.translateService.instant("PATIENTS.FORM_CHECKING_ACCOUNT.STATUS_LIST.ON_DATE");
			} else if (now.isSame(expiration, "day")) {
				return this.translateService.instant("PATIENTS.FORM_CHECKING_ACCOUNT.STATUS_LIST.TODAY");
			} else {
				return this.translateService.instant(
					"PATIENTS.FORM_CHECKING_ACCOUNT.STATUS_LIST.LATE_PAYMENT"
				);
			}
		}
	}

	getStatusColor(row: ContentBillTreatmentModel) {
		const STATUS_LIST_COLOR = CONSTANTS.STATUS_LIST_COLOR;

		const expiration = moment(row.dataVencimento, "DD/MM/YYYY");
		const now = moment();
		const amount = row.valor;
		const received = row.valorRecebido;

		if (received) {
			if (received < amount) {
				return STATUS_LIST_COLOR.RECEIVED_PARTIAL;
			} else {
				return STATUS_LIST_COLOR.RECEIVED_ALL;
			}
		} else {
			if (now.isBefore(expiration)) {
				return STATUS_LIST_COLOR.ON_DATE;
			} else if (now.isSame(expiration, "day")) {
				return STATUS_LIST_COLOR.TODAY;
			} else {
				return STATUS_LIST_COLOR.LATE_PAYMENT;
			}
		}
	}

	doReversePayment(rowDetail: ContentBillTreatmentModel) {
		this.reversePayment.next(rowDetail);
	}

	async toggleExpandRow(row: ContentBillTreatmentModel) {
		if (row.children == null) {
			row.children = await this._loadChildrenOfConta(row.id);
		}

		if (!row.children || !row.children.length) {
			return;
		}

		const rowDetail = this.rowDetailRef;

		if (!rowDetail) {
			return;
		}

		if (this.activeDetail === row) {
			rowDetail.toggleExpandRow(row);
		} else {
			rowDetail.collapseAllRows();
			rowDetail.toggleExpandRow(row);
		}

		this.activeDetail = row === this.activeDetail ? undefined : row;
	}

	private async _loadChildrenOfConta(contaId: number) {
		try {
			let error: unknown;

			const result = await this._asyncTemplate.wrapUserFeedback("", () =>
				this._contasReceberApiService.consultarItensRecebidos(contaId).catch((err) => (error = err))
			);

			if (error) throw error;

			return result || [];
		} catch {
			return [];
		}
	}

	async emitPaymentReceipt(row: any, rowDetatil: any) {
		const patient = await this._findPatientById(row.pacienteId);
		const modalRef = this._modalService.open(PaymentReceiptComponent, { size: "lg" });
		modalRef.componentInstance.paymentReceipt = {
			id: rowDetatil.reciboId,
			itemRecebidoId: rowDetatil.id,
			valor: rowDetatil.valorRecebido,
			data: rowDetatil.dataRecebimento,
			pacienteId: row.pacienteId,
			nomePaciente: patient.pessoaNome,
			cpfCnpjPaciente: patient.pessoaCpfCnpj,
			referente: "Tratamento odontológico",
			status: "EMITIDO",
		} as PaymentReceipt;

		const result = await modalRef.result;

		if (result) {
			this.refreshBills.emit();
		}
	}

	private async _findPatientById(id: number): Promise<PatientModel> {
		const data = await this.service
			.httpGET(CONSTANTS.ENDPOINTS.patients.general.findOne + id)
			.toPromise();
		return data.body as PatientModel;
	}

	async deleteBill(bill: any) {
		const { value } = await Swal.fire({
			title: "Confirmação de exclusão",
			text: "Tem certeza que deseja apagar esta conta?",
			type: "question",
			showCancelButton: true,
			confirmButtonText: "Sim. Apague",
			cancelButtonText: "Cancelar",
		});

		if (!value) {
			return false;
		}

		this.service.loading(true);
		const content = {
			id: bill.id,
			recorrenciaId: bill.recorrenciaId,
			tipoExclusao: "SOMENTE_ESTE_LANCAMENTO",
		};
		try {
			await this.service
				.httpPUT(CONSTANTS.ENDPOINTS.patients.financial.removeParcel, content)
				.toPromise();
			this.service.notification.success("Conta removida.");
			this.refreshBills.emit();
		} catch (e) {
			this.service.notification.error("Ocorreu um erro ao remover a Conta.");
		} finally {
			this.service.loading(false);
		}
	}

	handleContextMenu({ event, type }: { event: MouseEvent; type: ContextmenuType }) {
		if (type !== ContextmenuType.header) return;

		event.preventDefault();

		const { left, top } = this._hostElementRef.nativeElement.getBoundingClientRect();

		this.dropdownPos = [event.x - left, event.y - top];
	}

	toggleNumeroGuiaColumn() {
		const newColumns = this.columns.filter(compose(not, equals(this.numeroGuiaColumnDefinition)));

		if (newColumns.length === this.columns.length) {
			newColumns.splice(newColumns.length - 2, 0, this.numeroGuiaColumnDefinition);
		}

		this.columns = newColumns;
	}
}
