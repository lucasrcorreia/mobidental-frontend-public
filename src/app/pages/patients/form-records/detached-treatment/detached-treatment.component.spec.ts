import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetachedTreatmentComponent } from './detached-treatment.component';

describe('DetachedTreatmentComponent', () => {
  let component: DetachedTreatmentComponent;
  let fixture: ComponentFixture<DetachedTreatmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetachedTreatmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetachedTreatmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
