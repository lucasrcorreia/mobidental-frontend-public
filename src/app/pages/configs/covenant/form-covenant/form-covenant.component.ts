import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CovenantModel, ProceduresCovenantModel } from '../../../../core/models/config/covenant/covenantCreate.model';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilsService } from '../../../../services/utils/utils.service';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormCovenant } from '../../../../core/forms/covenant';
import { CONSTANTS } from '../../../../core/constants/constants';
import { ConveniosApiService } from '../../../../api/convenios-api.service';
import { NotificationsService } from 'angular2-notifications';
import { errorToString } from '../../../../lib/helpers/error-to-string';
import { GlobalSpinnerService } from '../../../../lib/global-spinner.service';
import { debounce } from 'lodash-es';
import Swal from 'sweetalert2';
import { ModalAnimationComponent } from '../../../../shared/modal-animation/modal-animation.component';

// TODO: Refatorar esse módulo inteiro de convênios, o código legado é surreal (em um mal sentido)
@Component({
	selector: 'app-form-covenant',
	templateUrl: './form-covenant.component.html',
	styleUrls: [
		'./form-covenant.component.scss',
		'./../../../../../assets/icon/icofont/css/icofont.scss',
	],
})
export class FormCovenantComponent implements OnInit {
	@ViewChild('searchProcedure') searchProcedure: ElementRef;
	@ViewChild('formModal') _formModalRef?: ModalAnimationComponent;

	covenant: CovenantModel;
	covenantIsPadrao = false;
	formCovenant: FormGroup;
	procedures: FormArray;
	allProcedures: FormArray;
	specialSelected = 0;
	searchProc = '';
	showModal = false;
	editMode = false;
	itemEditing = -1;

	editingProcedimento?: Partial<ProceduresCovenantModel>;
	editingControl?: AbstractControl;
	modalEditando = false;

	constructor(
			public service: UtilsService,
			private route: ActivatedRoute,
			private readonly fb: FormBuilder,
			private readonly _router: Router,
			private readonly _conveniosApiService: ConveniosApiService,
			private readonly _notificationsService: NotificationsService,
			private readonly _globalSpinnerService: GlobalSpinnerService,
			private readonly _formBuilder: FormBuilder,
			private readonly _changeDetectorRef: ChangeDetectorRef,
	) {
		this.route.params.subscribe(params => {
			this.editMode = params.editing === 'true';
		});
	}

	ngOnInit() {
		this._loadForm();
		this._loadProcedures().then(() => this.createForm()); // Força buscar procedimentos toda vez
	}

	private _loadForm() {
		this.covenant = this.service.getStorage(CONSTANTS.STORAGE.COVENANT) as CovenantModel;
		this.covenantIsPadrao = this.covenant.isPadrao;
		this.createForm();
	}

	createForm() {
		let control;
		let value;
		let result;

		this.formCovenant = this.fb.group({});
		this.formCovenant.addControl('id', this.fb.control(this.covenant.id));
		this.formCovenant.addControl(
				'nome',
				this.fb.control(this.covenant.nome, [Validators.required]),
		);

		const arraySpecialities = this.fb.array([]);
		let index = 0;
		for (const especial of this.covenant.especialidades) {
			const innerGroup = this.fb.group({});
			result = this.service.createControl(especial, 'nome', FormCovenant);
			control = result.control;
			value = result.value;

			innerGroup.addControl('id', this.fb.control(especial.id));
			innerGroup.addControl('nome', control ? control : this.fb.control(value));

			const arrayProcedures = this.fb.array([]);
			let indexProcedure = 0;

			for (const procedure of especial.procedimentos) {
				const innerGroup2 = this.fb.group({});
				innerGroup2.addControl('id', this.fb.control(procedure.id));
				innerGroup2.addControl(
						'procedimentoId',
						this.fb.control(procedure.procedimentoId),
				);
				innerGroup2.addControl(
						'especialidadeId',
						this.fb.control(procedure.especialidadeId),
				);
				innerGroup2.addControl(
						'procedimentoIsAceitaFace',
						this.fb.control(procedure.procedimentoIsAceitaFace),
				);
				innerGroup2.addControl(
						'procedimentoNome',
						this.fb.control(procedure.procedimentoNome, [Validators.required]),
				);
				innerGroup2.addControl(
						'procedimentoStatus',
						this.fb.control(procedure.procedimentoStatus, [Validators.required]),
				);
				innerGroup2.addControl(
						'preco',
						this.fb.control(procedure.preco),
				);
				innerGroup2.addControl(
						'procedimentoPorcentagemComissao',
						this.fb.control(procedure.procedimentoPorcentagemComissao),
				);
				innerGroup2.addControl(
						'procedimentoValorCustoProtetico',
						this.fb.control(procedure.procedimentoValorCustoProtetico),
				);

				arrayProcedures.setControl(indexProcedure, innerGroup2);
				indexProcedure++;
			}

			innerGroup.setControl('procedimentos', arrayProcedures);
			arraySpecialities.setControl(index, innerGroup);
			index++;
		}

		this.formCovenant.setControl('especialidades', arraySpecialities);

		this._changeDetectorRef.detectChanges();
	}

	listenToKeyEvent = debounce((event: KeyboardEvent) => {
		if (event.key.toLowerCase().indexOf('arrow') === -1) {
			this.filterProcedures();
		}
	}, 300);

	edit(proc: ProceduresCovenantModel, ctrl: AbstractControl) {
		this.editingProcedimento = proc;
		this.editingControl = ctrl;
		this.modalEditando = true;
		this._openProcModal();
	}

	filterProcedures() {
		const specialties = this.formCovenant.get('especialidades') as FormArray;
		const procedures = specialties.controls[this.specialSelected].get(
				'procedimentos',
		) as FormArray;

		const result: FormArray = this.fb.array([]);
		if (this.searchProc) {
			procedures.controls.forEach(item => {
				if (
						item
								.get('procedimentoNome')
								.value.toLowerCase()
								.indexOf(this.searchProc) > -1
				) {
					result.controls.push(item);
				}
			});

			this.procedures = result;
		} else {
			this.procedures = procedures;
		}
	}

	selectSpecialty(index: number) {
		this.searchProc = '';
		this.specialSelected = index;

		if (this.editMode) {
			this.getProcedures();
		} else {
			this.selectProcedures(index);
		}
	}

	async deleteProc(proc: ProceduresCovenantModel) {
		const confirmation = await Swal.fire({
			title: 'Confirmação de exclusão',
			text: 'Tem certeza que quer apagar o procedimento?',
			type: 'question',
			showCancelButton: true,
			confirmButtonText: 'Sim. Apague',
			cancelButtonText: 'Cancelar',
		});

		if (!confirmation.value) {
			return;
		}

		const loadingToken = this._globalSpinnerService.loadingManager.start();

		try {
			await this._conveniosApiService.apagarProcedimentoDeConvenio(proc.id);
			this._notificationsService.success('Procedimento apagado com sucesso.');

			try {
				await this._loadProcedures();
				await this._loadForm();
			} catch {
			}
		} catch (e) {
			this._notificationsService.error('Erro ao apagar procedimento', errorToString(e));
		} finally {
			loadingToken.finished();
		}
	}

	private async _loadProcedures(): Promise<void> {

		const specialties = this.formCovenant.get('especialidades') as FormArray;

		if (this.covenant && this.covenant.id) {
			const ref =
					CONSTANTS.ENDPOINTS.config.covenant.getProcedures.replace(
							'#specialtyId',
							specialties.controls[this.specialSelected].get('id').value.toString(),
					) + this.covenant.id;

			const loadingToken = this._globalSpinnerService.loadingManager.start();

			try {
				const dataProc = await this.service.httpGET(ref).toPromise();
				const proceduresData = dataProc.body as ProceduresCovenantModel[];

				const procedures = specialties.controls[this.specialSelected].get('procedimentos') as FormArray;

				while (procedures.get([0])) {
					procedures.removeAt(0);
				}

				for (const procedure of proceduresData) {
					const innerGroup = this.createProcedureControl(procedure);
					procedures.push(innerGroup);
				}

				this.service.setStorage(
						CONSTANTS.STORAGE.COVENANT,
						{ isPadrao: this.covenant.isPadrao, ...this.formCovenant.value },
				);
			} catch (e) {

				this.service.notification.error(
						this.service.translate.instant('COMMON.ERROR.SEARCH'),
						errorToString(e),
				);
			} finally {
				loadingToken.finished();
			}
		}

		this.selectProcedures(this.specialSelected);
		this._loadForm();
	}

	async getProcedures() {
		await this._loadProcedures();
	}

	selectProcedures(index: number) {
		const specialties = this.formCovenant.get('especialidades') as FormArray;

		this.procedures = specialties.controls[index].get(
				'procedimentos',
		) as FormArray;

		this.allProcedures = specialties.controls[index].get(
				'procedimentos',
		) as FormArray;
	}

	addProc() {
		this.editingProcedimento = { procedimentoStatus: true };
		this.editingControl = undefined;
		this.modalEditando = false;
		this._openProcModal();
	}

	receiveEditModel(proc: ProceduresCovenantModel) {
		this.updateProcedure(proc);
	}

	private _openProcModal() {
		this.showModal = true;
		setTimeout(() => {
			document.querySelector('#modal-create-proc').classList.add('md-show');
		});
	}

	async processResultModalCreate(procedure: ProceduresCovenantModel) {
		this.showModal = false;

		if (!procedure) {
			return;
		}

		const loadingToken = this._globalSpinnerService.loadingManager.start();
		const { covenant } = this;
		if (covenant) {
			procedure.id = covenant ? covenant.id : undefined;
			procedure.especialidadeId = covenant
					? covenant.especialidades[this.specialSelected].id
					: undefined;
		}

		try {
			await this._conveniosApiService.saveConvenioProcedimento(procedure);
			this._closeModal();
		} catch (e) {
			this._notificationsService.error('Erro ao salvar procedimento', errorToString(e));
		} finally {
			loadingToken.finished();
		}

		await this._loadProcedures();
		this._loadForm();
	}

	createProcedureControl(procedure: ProceduresCovenantModel): FormGroup {
		const innerGroup = this.fb.group({});
		innerGroup.addControl('id', this.fb.control(procedure.id));
		innerGroup.addControl(
				'procedimentoId',
				this.fb.control(procedure.procedimentoId),
		);
		innerGroup.addControl(
				'especialidadeId',
				this.fb.control(this.specialSelected),
		);
		innerGroup.addControl(
				'procedimentoIsAceitaFace',
				this.fb.control(procedure.procedimentoIsAceitaFace),
		);
		innerGroup.addControl(
				'procedimentoNome',
				this.fb.control(procedure.procedimentoNome, [Validators.required]),
		);
		innerGroup.addControl(
				'procedimentoStatus',
				this.fb.control(procedure.procedimentoStatus, [Validators.required]),
		);
		innerGroup.addControl(
				'preco',
				this.fb.control(procedure.preco, [Validators.required]),
		);
		innerGroup.addControl(
				'procedimentoPorcentagemComissao',
				this.fb.control(procedure.procedimentoPorcentagemComissao),
		);
		innerGroup.addControl(
				'procedimentoValorCustoProtetico',
				this.fb.control(procedure.procedimentoValorCustoProtetico),
		);

		return innerGroup;
	}

	back() {
		this._router.navigate(['/config/covenant']);
	}

	async save() {
		try {
			this.service.loading(true);
			if (!this.editMode) {
				await this.service.httpPOST(CONSTANTS.ENDPOINTS.config.covenant.save, this.formCovenant.value).toPromise();
				this.service.notification.success('Convênio criado.');
			} else {
				// as duas linhas de baixo podem também pegar de formCovenant.value, mas depois teria que setar isPadrao manualmente, ou seja, dá na mesma
				const covenant = { ...this.covenant };
				covenant.nome = this.formCovenant.get('nome').value;
				// remove especialidades pois não vai na requisição
				delete covenant.especialidades;
				await this.service.httpPUT(CONSTANTS.ENDPOINTS.config.covenant.update, covenant).toPromise();
				this.service.notification.success('Convênio atualizado.');
			}
			this.service.back();
		} catch (e) {

			this.service.notification.error(this.service.translate.instant('COMMON.ERROR.SEARCH'), e.error.error);
		} finally {
			this.service.loading(false);
		}
	}

	async updateProcedure(procedure: ProceduresCovenantModel) {
		this.service.loading(true);

		if (!this.modalEditando) {
			this.covenant = this.service.getStorage(CONSTANTS.STORAGE.COVENANT) as CovenantModel;
			this.covenantIsPadrao = this.covenant.isPadrao;
			procedure.convenioId = this.covenant.id;
			const specialties = this.formCovenant.get('especialidades') as FormArray;
			procedure.especialidadeId = specialties.controls[this.specialSelected].get('id').value.toString()
		}

		this.service
				.httpPOST(CONSTANTS.ENDPOINTS.config.covenant.updateProcedure, procedure)
				.subscribe(
						() => {
							if (this.editingControl) {
								this.editingControl.patchValue(procedure);
							}

							this.service.loading(false);
							this.service.notification.success(
									this.service.translate.instant('CONFIG.COVENANT.FORM.SAVE_PROCEDURE.TITLE'),
									this.service.translate.instant('CONFIG.COVENANT.FORM.SAVE_PROCEDURE.' + (this.modalEditando ? 'SUB' : 'NEW')),
							);

							this.service.setStorage(
									CONSTANTS.STORAGE.COVENANT,
									this.formCovenant.value,
							);

							this.itemEditing = -1;
							this._closeModal();
							this._loadProcedures();
						},
						err => {

							this.service.loading(false);
							this.service.notification.error(
									this.service.translate.instant('COMMON.ERROR.SEARCH'),
									err.error.error,
							);
						},
				);

	}

	saveProcedure(procedure: ProceduresCovenantModel) {

		this.service.loading(true);

		this.covenant = this.service.getStorage(CONSTANTS.STORAGE.COVENANT) as CovenantModel;
		this.covenantIsPadrao = this.covenant.isPadrao;
		procedure.convenioId = this.covenant.id;
		this.service
				.httpPOST(CONSTANTS.ENDPOINTS.config.covenant.saveProcedure, procedure)
				.subscribe(
						() => {
							if (this.editingControl) {
								this.editingControl.patchValue(procedure);
							}

							this.service.loading(false);
							this.service.notification.success(
									this.service.translate.instant('CONFIG.COVENANT.FORM.SAVE_PROCEDURE.TITLE'),
									this.service.translate.instant('CONFIG.COVENANT.FORM.SAVE_PROCEDURE.SUB'),
							);

							this.service.setStorage(
									CONSTANTS.STORAGE.COVENANT,
									this.formCovenant.value,
							);

							this.itemEditing = -1;
							this._loadForm();
							this._closeModal();
						},
						err => {

							this.service.loading(false);
							this.service.notification.error(
									this.service.translate.instant('COMMON.ERROR.SEARCH'),
									err.error.error,
							);
						},
				);
	}

	private _closeModal() {
		if (this._formModalRef) {
			this._formModalRef.close();
		}
	}
}
