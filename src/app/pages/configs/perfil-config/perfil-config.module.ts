import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfilConfigComponent } from './perfil-config.component';
import { PerfilConfigRoutingModule } from './perfil-config-routing.module';
import { AppLibModule } from '../../../lib/app-lib.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [PerfilConfigComponent],
  imports: [
    CommonModule,
    FormsModule,
    PerfilConfigRoutingModule,
    AppLibModule,
  ],
})
export class PerfilConfigModule {}
