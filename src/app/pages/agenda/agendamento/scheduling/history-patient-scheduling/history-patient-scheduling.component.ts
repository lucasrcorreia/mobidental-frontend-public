import { Component, EventEmitter, Input, OnDestroy, OnInit } from "@angular/core";
import { AgendaApiService } from "../../../../../api/agenda.api.service";
import { AgendaModel } from "../../../../../core/models/agenda/agenda.model";
import { UtilsService } from "../../../../../services/utils/utils.service";
import { LISTS } from "../../../lists";
import { isEmpty } from "lodash-es";
import { NO_LABEL } from "../../../agenda.component";
import { distinctUntilChanged, map } from "rxjs/operators";
import { AutoUnsubscribe } from "ngx-auto-unsubscribe";
import { BehaviorSubject } from "rxjs";

@AutoUnsubscribe()
@Component({
	selector: "app-history-patient-scheduling",
	templateUrl: "./history-patient-scheduling.component.html",
	styleUrls: ["./history-patient-scheduling.component.scss"],
})
export class HistoryPatientSchedulingComponent implements OnInit, OnDestroy {
	@Input() refresh?: BehaviorSubject<number>;

	readonly noLabel = NO_LABEL;
	readonly locallists = new LISTS(this._utilsService.translate);
	public _schedulingHistory: AgendaModel[];

	pageSize = 8;
	currentPage = 1;

	constructor(
		private readonly _agendaApiService: AgendaApiService,
		private readonly _utilsService: UtilsService
	) {}

	ngOnInit() {
		if (this.refresh) {
			this.refresh
				.pipe(
					distinctUntilChanged(),
					map((patientId) => this._fetchShedulings(patientId))
				)
				.subscribe();
		}
	}

	ngOnDestroy() {}

	private async _fetchShedulings(patientId: number) {
		if (patientId)
			this._schedulingHistory = await this._agendaApiService.listHistoryPatientScheduling(
				patientId
			);
	}

	get schedulings(): AgendaModel[] {
		if (!this._schedulingHistory) {
			return [];
		}

		return this._schedulingHistory.slice(
			(this.currentPage - 1) * this.pageSize,
			(this.currentPage - 1) * this.pageSize + this.pageSize
		);
	}

	getLabel(label: string): string {
		return isEmpty(label) ? this.noLabel : label;
	}

	translateStatus(status: string): string {
		const translatedStatus = this.locallists.listStatusAgenda.find((stats) => {
			return stats.value === status;
		});

		return translatedStatus ? translatedStatus.label : this.noLabel;
	}
}
