import {SimNaoResposta} from './SimNaoResposta';

export interface AnamneseRespostaModel {
  id?: number;
  resposta?: string;
  respostaSimNao?: SimNaoResposta;
  perguntaId?: number;
  perguntaModeloId?: number;
  pacienteId?: number;
}
