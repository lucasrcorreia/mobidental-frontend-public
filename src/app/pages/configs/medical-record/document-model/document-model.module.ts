import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DocumentModelComponent } from "./document-model/document-model.component";
import { DocumentModelRoutingModule } from "./document-model-routing.module";
import { CreateDocumentModelComponent } from "./create-document-model/create-document-model.component";
import { NgxTranslateModule } from "../../../../core/translate/translate.module";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { NgbTooltipModule, NgbTabsetModule } from "@ng-bootstrap/ng-bootstrap";
import { AppLibModule } from "../../../../lib/app-lib.module";
import { FormsModule } from "@angular/forms";
import { CKEditorModule } from "ckeditor4-angular";
import { ContractDocumentModelComponent } from "./contract-document-model/contract-document-model.component";

@NgModule({
	declarations: [
		DocumentModelComponent,
		CreateDocumentModelComponent,
		ContractDocumentModelComponent,
	],
	imports: [
		CommonModule,
		DocumentModelRoutingModule,
		NgxTranslateModule,
		NgxDatatableModule,
		NgbTooltipModule,
		NgbTabsetModule,
		AppLibModule,
		FormsModule,
		CKEditorModule,
	],
})
export class DocumentModelModule {}
