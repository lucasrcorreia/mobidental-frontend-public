import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BirthdayReportComponent } from './birthday-report.component';

const routes: Routes = [
	{
		path: '',
		component: BirthdayReportComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class BirthdayReportRoutingModule {}
