import { AnamneseStrategy } from '../AnamneseStrategy';
import { AnamnesePerguntaModel } from '../../../models/AnamnesePerguntaModel';
import { TipoAlertaPerguntaAnamnese } from '../../../models/enums/TipoAlertaPerguntaAnamnese';
import { TipoPerguntaAnamnese } from '../../../models/enums/TipoPerguntaAnamnese';
import { AtivoInativo } from '../../../models/enums/AtivoInativo';
import { AnamneseModel } from '../../../models/AnamneseModel';
import { last } from 'lodash-es';

export class AnamneseCreation implements AnamneseStrategy {
  operation = 'adicionada';
  title = 'CONFIG.ANAMNESE.LABELS.NEW_QUESTION';
  readonly question: AnamnesePerguntaModel = {
    tipoAlerta: TipoAlertaPerguntaAnamnese.SEM_ALERTA,
    tipoPergunta: TipoPerguntaAnamnese.SIM_NAO,
    status: AtivoInativo.ATIVO,
  };

  workWithQuestion(anamnese: AnamneseModel, question: AnamnesePerguntaModel): void {
    // Caso seja a primeira pergunta, a posição é a primeira (0)
    question.posicao = anamnese.perguntas.length > 0 ? (last(anamnese.perguntas)).posicao + 1 : 0;
    anamnese.perguntas.push(question);
  }

}
