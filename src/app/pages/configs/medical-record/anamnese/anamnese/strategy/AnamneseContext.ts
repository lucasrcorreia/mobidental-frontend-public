import { AnamneseStrategy } from './AnamneseStrategy';
import { AnamnesePerguntaModel } from '../../models/AnamnesePerguntaModel';
import { AnamneseModel } from '../../models/AnamneseModel';

export class AnamneseContext {

  private strategy: AnamneseStrategy;

  constructor(strategy: AnamneseStrategy) {
    this.strategy = strategy;
  }

  getTitle(): string {
    return this.strategy.title;
  }

  getOperation(): string {
    return this.strategy.operation;
  }

  getQuestion(): AnamnesePerguntaModel {
    return this.strategy.question;
  }

  workWithQuestion(anamnese: AnamneseModel, question: AnamnesePerguntaModel, index = 0): void {
    this.strategy.workWithQuestion(anamnese, question, index);
  }
}
