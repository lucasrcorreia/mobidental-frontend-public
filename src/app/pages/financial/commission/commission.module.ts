import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { CommissionRoutingModule } from "./commission-routing.module";
import { CommissionComponent } from "./commission.component";
import { SharedModule } from "./../../../shared/shared.module";
import { NgxTranslateModule } from "../../../core/translate/translate.module";
import { NgSelectModule } from "@ng-select/ng-select";
import { NgxCurrencyModule } from "ngx-currency";
import { NgxMaskModule } from "ngx-mask";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { InputMaskModule } from "racoon-mask-raw";
import { DirectivesModule } from "../../../core/directive/directives.module";
import { PipesModule } from "../../../core/pipes/pipes.module";
import { SimpleNotificationsModule } from "angular2-notifications";
import { AccountBillToPayModule } from "../bills-to-pay/account/account.module";
import { NgbTooltipModule } from "@ng-bootstrap/ng-bootstrap";

@NgModule({
	declarations: [CommissionComponent],
	imports: [
		CommonModule,
		NgxTranslateModule,
		SharedModule,
		NgSelectModule,
		NgxTranslateModule,
		NgxCurrencyModule,
		NgxMaskModule.forRoot(),
		NgxDatatableModule,
		InputMaskModule,
		NgbTooltipModule,
		FormsModule,
		ReactiveFormsModule,
		DirectivesModule,
		PipesModule,
		SimpleNotificationsModule,
		CommissionRoutingModule,
		AccountBillToPayModule,
	],
})
export class CommissionModule {}
