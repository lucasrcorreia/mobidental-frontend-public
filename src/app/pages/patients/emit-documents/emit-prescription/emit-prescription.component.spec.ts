import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmitPrescriptionComponent } from './emit-prescription.component';

describe('EmitPrescriptionComponent', () => {
  let component: EmitPrescriptionComponent;
  let fixture: ComponentFixture<EmitPrescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmitPrescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmitPrescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
