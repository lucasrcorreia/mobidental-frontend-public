import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreatmentsReportComponent } from './treatments-report.component';

describe('TreatmentsReportComponent', () => {
  let component: TreatmentsReportComponent;
  let fixture: ComponentFixture<TreatmentsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreatmentsReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreatmentsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
