import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TreatmentsReportRoutingModule } from './treatments-report-routing.module';
import { TreatmentsReportComponent } from './treatments-report.component';
import { NgxTranslateModule } from '../../../core/translate/translate.module';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { ReportsModule } from '../reports.module';
import { NgxSelectModule } from 'ngx-select-ex';

@NgModule({
	declarations: [
		TreatmentsReportComponent,
	],
	imports: [
		CommonModule,
		TreatmentsReportRoutingModule,
		NgxTranslateModule,
		NgbDatepickerModule,
		FormsModule,
		ReportsModule,
		NgxSelectModule,
	],
})
export class TreatmentsReportModule {}
