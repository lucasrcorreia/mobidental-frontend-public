import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { UtilsService } from '../../../services/utils/utils.service';
import { ColumnsTableModel } from '../../../core/models/table/columns.model';
import { CONSTANTS } from '../../../core/constants/constants';
import { PaginationResponseModel } from '../../../core/models/response/paginationResponse.model';
import { ProfessionalModel } from '../../../core/models/config/professional/professional.model';
import Swal from 'sweetalert2';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

export const AGUARDANDO_ATIVACAO = 'AGUARDANDO_ATIVACAO';
export const ATIVO = 'ATIVO';
export const INATIVO = 'INATIVO';

export const PROFESSIONAL_STATUS_LIST = (service: UtilsService) => [
	{
		label: service.translate.instant('CONFIG.PROFESSIONAL.STATUS.ATIVO'),
		value: ATIVO,
		showOnSelect: true,
	},
	{
		label: service.translate.instant('CONFIG.PROFESSIONAL.STATUS.AGUARDANDO_ATIVACAO'),
		value: AGUARDANDO_ATIVACAO,
		showOnSelect: false,
	},
	{
		label: service.translate.instant('CONFIG.PROFESSIONAL.STATUS.INATIVO'),
		value: INATIVO,
		showOnSelect: true,
	},
];

@Component({
	selector: 'app-professionals',
	templateUrl: './professionals.component.html',
	styleUrls: ['./professionals.component.scss',
		'./../../../../assets/icon/icofont/css/icofont.scss']
})
export class ProfessionalsComponent implements OnInit, OnDestroy {
	@ViewChild(DatatableComponent) table: DatatableComponent;

	columns: ColumnsTableModel[] = [
		{
			action: true,
			name: this.service.translate.instant('CONFIG.PROFESSIONAL.TABLE.COL1'),
			prop: 'id',
			sortable: false,
			width: 150
		},
		{
			action: false,
			name: this.service.translate.instant('CONFIG.PROFESSIONAL.TABLE.COL2'),
			prop: 'profissionalNome',
			sortable: true,
			width: 350
		},
		{
			action: false,
			name: this.service.translate.instant('CONFIG.PROFESSIONAL.TABLE.COL3'),
			prop: 'perfilContaModuloDescricao',
			sortable: true,
			width: 200
		},
		{
			action: false,
			name: this.service.translate.instant('CONFIG.PROFESSIONAL.TABLE.COL4'),
			prop: 'usuarioEmail',
			sortable: true,
			width: 200
		},
		{
			action: false,
			name: this.service.translate.instant('CONFIG.PROFESSIONAL.TABLE.COL5'),
			prop: 'emStatusUsuarioPerfilContaModulo',
			sortable: true,
			width: 150
		}
	];

	rows = {};
	rowsPage = [];
	searchParam = {
		page: 0,
		emStatusUsuarioPerfilContaModulo: ATIVO || [],
		size: 100,
		sorting: {
			undefined: 'asc'
		}
	};

	listStatus = PROFESSIONAL_STATUS_LIST(this.service);
	filteredStatusList = PROFESSIONAL_STATUS_LIST(this.service).filter(status => status.showOnSelect);

	page = {
		size: 100,
		totalElements: 0,
		totalPages: 0,
		pageNumber: 0
	};

	professional: ProfessionalModel;
	private destroy$ = new Subject();

	constructor(
			public service: UtilsService,
			private cd: ChangeDetectorRef) {
		this.service.toggleMenu.pipe(takeUntil(this.destroy$)).subscribe(data => {
			if (data) {
				this.updateDataTable();
			}
		});

	}

	ngOnInit() {
		this.populateTable();
	}

	ngOnDestroy() {
		this.destroy$.next();
		this.destroy$.complete();
	}

	updateDataTable() {
		this.cd.markForCheck();
		setTimeout(() => {
			this.table.recalculate();
			(this.table as any).cd.markForCheck();
		});
	}

	populateTable() {
		this.service.loading(true);
		this.searchParam.page = this.page.pageNumber;
		const searchParam = { ...this.searchParam };
		if (searchParam.emStatusUsuarioPerfilContaModulo === ATIVO) {
			searchParam.emStatusUsuarioPerfilContaModulo = [ATIVO, AGUARDANDO_ATIVACAO];
		}
		this.service
				.httpPOST(CONSTANTS.ENDPOINTS.config.professional.table, searchParam)
				.subscribe(
						data => {
							const result = data.body as PaginationResponseModel;
							if (this.page.pageNumber === 0) {
								this.page.totalElements = result.totalElements;
								this.page.totalPages = result.totalPages;
							}

							this.rows[this.page.pageNumber] = result.content;
							this.rowsPage = result.content;
							this.service.loading(false);
						},
						err => {

							this.service.loading(false);
							this.service.notification.error(
									this.service.translate.instant('COMMON.ERROR.SEARCH'),
									err.error.error
							);
						}
				);
	}

	setPage(pageInfo) {
		this.page.pageNumber = pageInfo.offset;
		if (this.rows[this.page.pageNumber]) {
			this.rowsPage = this.rows[this.page.pageNumber];
		} else {
			this.populateTable();
		}
	}

	getStatus(value: string): string {
		return this.listStatus.find(status => status.value === value).label;
	}

	delete(id: number) {
		const translate = this.service.translate;
		const refSWAL = 'CONFIG.PROSTHETIC.SWAL';

		Swal.fire({
			title: translate.instant(`${ refSWAL }.TITLE_DELETE`),
			text: translate.instant(`${ refSWAL }.TEXT_DELETE`),
			type: 'question',
			showCancelButton: true,
			confirmButtonText: translate.instant(`${ refSWAL }.CONFIRM_BUTTON`),
			cancelButtonText: translate.instant(`${ refSWAL }.CANCEL_BUTTON`)
		}).then(result => {
			if (result.value) {
				this.service.loading(true);
				this.service
						.httpDELETE(
								CONSTANTS.ENDPOINTS.config.professional.delete + id
						)
						.subscribe(
								data => {
									this.service.notification.success(
											this.service.translate.instant('CONFIG.PROFESSIONAL.TOAST.DELETE.TITLE'),
											this.service.translate.instant('CONFIG.PROFESSIONAL.TOAST.DELETE.SUBTITLE')
									);
									this.service.loading(false);
									this.page.pageNumber = 0;
									this.rows = {};
									this.rowsPage = [];
									this.populateTable();
								},
								err => {

									this.service.loading(false);
									this.service.notification.error(
											this.service.translate.instant('COMMON.ERROR.SEARCH'),
											err.error.error
									);
								}
						);
			}
		});
	}

	sendMail(id: number) {
		const translate = this.service.translate;
		const refSWAL = 'CONFIG.PROSTHETIC.SWAL';

		Swal.fire({
			title: translate.instant(`${ refSWAL }.TITLE_MAIL`),
			text: translate.instant(`${ refSWAL }.TEXT_MAIL`),
			type: 'question',
			showCancelButton: true,
			confirmButtonText: translate.instant(`${ refSWAL }.CONFIRM_BUTTON`),
			cancelButtonText: translate.instant(`${ refSWAL }.CANCEL_BUTTON`)
		}).then(result => {
			if (result.value) {
				this.service.loading(true);
				this.service
						.httpPUT(
								CONSTANTS.ENDPOINTS.config.professional.resendMail, { id }
						)
						.subscribe(
								data => {
									this.service.notification.info(
											this.service.translate.instant('CONFIG.PROFESSIONAL.TOAST.MAIL.TITLE'),
											this.service.translate.instant('CONFIG.PROFESSIONAL.TOAST.MAIL.SUBTITLE')
									);
									this.service.loading(false);
								},
								err => {

									this.service.loading(false);
									this.service.notification.error(
											this.service.translate.instant('COMMON.ERROR.SEARCH'),
											err.error.error
									);
								}
						);
			}
		});
	}

	awaitingActivation(professional: ProfessionalModel) {
		return professional.emStatusUsuarioPerfilContaModulo === AGUARDANDO_ATIVACAO;
	}

	log(status) {

	}

}
