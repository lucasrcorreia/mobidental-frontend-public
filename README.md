# Mobidental

### Formatação

Esse projeto passou por várias configurações de formatação e diversas partes de
código estão incosistentes, por causa disso foi configurado um hook para que
todo commit passe por formatação automática pelo [Prettier](//prettier.io/).
Regras e lembretes sobre:

- Configure o plugin do prettier no seu editor de escolha para evitar problemas na
  hora do commit
- Evite alterar configurações do prettier, elas são estritas por design para
  evitar discussões desnecessárias, apenas "aceite" como todo mundo (mais
  explicações no site do prettier)
- Esteja ciente que sempre que editar um arquivo antigo pré configuração do
  pretter o hook automaticamente formatará outras partes do código, a decisão é
  proposital
