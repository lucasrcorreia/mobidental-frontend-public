import { FormaPagamento } from '../../../../api/model/forma-pagamento';
import { SellReportItemType } from './enums/sell-report-item-type';

export interface SellReportItemChildren {
  nomePaciente?: string;
  pacienteId?: number;
  dia?: number;
  valor?: number;
  descricao?: string;
  emFormaDeRecebimento?: FormaPagamento;
  emTipoItemContaCorrente?: SellReportItemType;

  /* Campos criados no front-end, não conhecidos pelo back-end */
  data?: string;
}
