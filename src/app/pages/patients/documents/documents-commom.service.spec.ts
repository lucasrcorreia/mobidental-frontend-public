import { TestBed } from '@angular/core/testing';

import { DocumentsCommonService } from './documents-common.service';

describe('DocumentsCommomService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DocumentsCommonService = TestBed.get(DocumentsCommonService);
    expect(service).toBeTruthy();
  });
});
