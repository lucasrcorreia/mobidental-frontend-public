// tslint:disable: quotemark
import { ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import {
    ContentBillTreatmentModel,
    ContentTreatmentModel,
    ParcelasModel,
    ProceduresModel
} from '../../../core/models/patients/patients.model';
import { FormGroup } from '@angular/forms';
import { UtilsService } from '../../../services/utils/utils.service';
import { FormPatientTreatment } from '../../../core/forms/patient';
import { ConvenantModel } from '../../../core/models/patients/convenio.model';
import * as moment from 'moment';
import { CONSTANTS } from '../../../core/constants/constants';
import { SelectItemStatus, ShortListItem } from '../../../core/models/forms/common/common.model';
import { LISTAS } from '../lists';
import { ContentPatientServicesTableModel } from '../../../core/models/patients/procedures/procedures.model';
import { BehaviorSubject, forkJoin, Subject } from 'rxjs';
import { TableColumnsTreatment } from './tables';
import Swal from 'sweetalert2';
import { classToPlain } from 'class-transformer';
import { takeUntil, takeWhile } from 'rxjs/operators';
import { toothsList } from '../tooths';
import { LOCAL_CONSTS } from '../constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { EditProcedureComponent } from './edit-procedure/edit-procedure.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProcedureDialogService } from '../procedure-dialog/procedure-dialog.service';
import { NotificationsService } from 'angular2-notifications';

@Component({
               selector: 'app-form-treatment',
               templateUrl: './form-treatment.component.html',
               styleUrls: [
                   './form-treatment.component.scss',
                   './../../../../assets/icon/icofont/css/icofont.scss',
               ],
           })
export class FormTreatmentComponent implements OnInit, OnDestroy {
    @ViewChild(DatatableComponent) table: DatatableComponent;

    @Input() convenios: ConvenantModel[] = [];
    @Input() treatment: ContentTreatmentModel = new ContentTreatmentModel();
    @Input() profissionais: ShortListItem[] = [];
    @Input() editMode: string;
    @Output() close = new EventEmitter();

    paymentMethodsWithoutParcels = ['A_VISTA', 'NUM_PROCEDIMENTO'];
    AGUARDANDO_APROVACAO = LOCAL_CONSTS.AGUARDANDO_APROVACAO;
    A_VISTA = LOCAL_CONSTS.A_VISTA;

    vendedores: ShortListItem[] = [];
    procedures: ContentPatientServicesTableModel[] = [];
    proceduresTable = [];
    financialTable = [];
    parcelas = [];

    resume = {
        valorComDesconto: 0,
        valorDesconto: 0,
        valorTotal: 0,
    };

    bills: ContentBillTreatmentModel[] = [];
    formulario: FormGroup;
    minDate = { year: 2019, month: 1, day: 1 };
    treatmentStep = 'form';
    showApprovedsProcedures = true;
    showModalParcela = false;
    doRefreshFinancial = true;
    paid = 0;
    selectedParcel: ParcelasModel;

    toggleStateInputs = new BehaviorSubject<boolean>(false);
    destroy = false;
    LISTAS_LOCAIS = new LISTAS(this.service.translate);
    columnsTables: TableColumnsTreatment;
    locale = this.service.translate.instant('COMMON.LOCALE');
    situations: SelectItemStatus[] = new LISTAS(this.service.translate).listaStatusTratamento;
    private destroy$ = new Subject();

    constructor(
        public service: UtilsService,
        private cd: ChangeDetectorRef,
        private readonly _modalService: NgbModal,
        private readonly _procedureDialogService: ProcedureDialogService,
        private readonly _notificationsService: NotificationsService,
    ) {
        for (let i = 1; i <= 50; i++) {
            const item = {
                label: i === 1 ? `${ i } Parcela` : `${ i } Parcelas`,
                value: i,
            };

            this.parcelas = [...this.parcelas, item];
        }

        this.service.toggleMenu.pipe(takeUntil(this.destroy$)).subscribe(data => {
            if (data) {
                this.updateDataTable();
            }
        });
    }

    get currentPaymentMethodCanHavePartials() {
        return !this.paymentMethodsWithoutParcels.includes(this.formulario.get('formaPagamento').value);
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
        if (this.treatmentStep === 'tables') {
            this.close.emit('underEdit');
        }
    }

    ngOnInit() {
        this.createForm();
        this.createColumns();
        this.populateData();
        this.toggleStateInputs.pipe(takeUntil(this.destroy$)).subscribe(state => {
            if (state) {
                this.formulario.get('descricao').disable();
                this.formulario.get('convenioId').disable();
                this.formulario.get('operadorVendedorId').disable();
                this.formulario.get('profissionalId').disable();
                this.formulario.get('formaDeRecebimento').disable();
                this.formulario.get('formaPagamento').disable();
                this.formulario.get('numeroTotalParcelas').disable();
                this.formulario.get('tipoDesconto').disable();
                this.formulario.get('valorDesconto').disable();
                this.formulario.get('guiaConvenio').disable();
            } else {
                this.formulario.get('descricao').enable();
                this.formulario.get('convenioId').enable();
                this.formulario.get('operadorVendedorId').enable();
                this.formulario.get('profissionalId').enable();
                this.formulario.get('formaDeRecebimento').enable();
                this.formulario.get('formaPagamento').enable();
                this.formulario.get('numeroTotalParcelas').enable();
                this.formulario.get('tipoDesconto').enable();
                this.formulario.get('valorDesconto').enable();
                this.formulario.get('valorPercentualDesconto').enable();
                this.formulario.get('guiaConvenio').enable();
            }
        });
    }

    updateDataTable() {
        this.cd.markForCheck();
        setTimeout(() => {
            this.table.recalculate();
            (this.table as any).cd.markForCheck();
        });
    }

    createColumns() {
        this.columnsTables = new TableColumnsTreatment(this.service.translate);

    }

    createForm() {
        if (!this.treatment.dataAbertura) {
            this.treatment.dataAbertura = this.service.parseDatePicker(
                moment().format('YYYY-MM-DD'),
                'YYYY-MM-DD',
            );
        } else {
            if (!this.treatment.dataAbertura.month) {
                this.treatment.dataAbertura = this.service.parseDatePicker(
                    this.treatment.dataAbertura,
                    'DD/MM/YYYY',
                );
            }
        }

        if (!this.treatment.descricao) {
            this.treatment.descricao = this.service.translate.instant(
                'PATIENTS.FORM_TREATMENT.descricaoDefault',
            );
        }

        if (!this.treatment.statusTratamento) {
            this.treatment.statusTratamento = this.LISTAS_LOCAIS.listaStatusTratamento[0].value;
            this.treatment.statusTratamentoDescricao = this.LISTAS_LOCAIS.listaStatusTratamento[0].label;
        }

        if (!this.treatment.formaDeRecebimento) {
            this.treatment.formaDeRecebimento = LOCAL_CONSTS.DEFAULT_FINANCIAL.FORMA_RECEBIMENTO;
        }

        if (!this.treatment.formaPagamento) {
            this.treatment.formaPagamento = LOCAL_CONSTS.DEFAULT_FINANCIAL.FORMA_PAGAMENTO;
        }

        if (!this.treatment.numeroTotalParcelas) {
            this.treatment.numeroTotalParcelas = LOCAL_CONSTS.DEFAULT_FINANCIAL.PARCELAS;
        }


        this.formulario = this.service.createForm(
            FormPatientTreatment,
            this.treatment,
        );

        if (this.treatment.statusTratamento !== this.AGUARDANDO_APROVACAO) {
            this.toggleStateInputs.next(true);
        }
    }

    populateData() {
        if (this.editMode === 'yes') {
            this.getTreatment(true);
        } else {
            if (this.editMode === 'partial') {

                if (this.treatment.servicos && this.treatment.servicos.length > 0) {
                    this.applyToothNames(this.treatment.servicos);
                    this.applyStatusName(this.treatment.servicos);
                    this.getProcedures();
                }

                if (this.treatment.parcelas && this.treatment.parcelas.length > 0) {
                    this.financialTable = this.treatment.parcelas;
                    this.refreshFinancialTableByPaymentMethod();
                }

                this.treatmentStep = 'tables';
            }
            this.getVendedores();
        }
    }

    getProcedures() {
        if (this.treatment.servicos) {
            if (this.showApprovedsProcedures) {
                this.proceduresTable = classToPlain(
                    this.treatment.servicos.filter(
                        procedure => procedure.statusTratamentoServico !== LOCAL_CONSTS.NAO_AUTORIZADO,
                    ),
                ) as ProceduresModel[];
            } else {
                this.proceduresTable = classToPlain(
                    this.treatment.servicos.filter(
                        procedure => procedure.statusTratamentoServico === LOCAL_CONSTS.NAO_AUTORIZADO,
                    ),
                ) as ProceduresModel[];
            }

            this.proceduresTable.map(procedure => {
                let desc = '(';

                if (procedure.isFaceDistal) {
                    desc += `${ this.service.translate.instant('PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceDistal') }, `;
                }

                if (procedure.isFaceOclusalIncisal) {
                    desc += `${ this.service.translate.instant('PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceOclusalIncisal') }, `;
                }

                if (procedure.isFaceLingualPalatina) {
                    desc += `${ this.service.translate.instant(
                        'PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceLingualPalatina') }, `;
                }

                if (procedure.isFaceMesial) {
                    desc += `${ this.service.translate.instant('PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceMesial') }, `;
                }

                if (procedure.isFaceVestibular) {
                    desc += `${ this.service.translate.instant('PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceVestibular') } `;
                }

                if (desc.trim().substr(desc.trim().length - 1, 1) === ',') {
                    desc = desc.trim().substr(0, desc.trim().length - 1);
                }

                if (desc !== '(') {
                    procedure.convenioEspecialidadeProcedimentoNome = procedure.convenioEspecialidadeProcedimentoNome + ' ' + desc + ')';
                }
            });
        }
    }

    getVendedores() {
        this.service.loading(true);
        this.service.httpGET(CONSTANTS.ENDPOINTS.operator.active).subscribe(
            data => {
                this.vendedores = data.body as ShortListItem[];
                this.service.loading(false);
            },
            err => {

                this.service.loading(false);
                this.service.notification.error(
                    this.service.translate.instant('COMMON.ERROR.SEARCH'),
                    err.error.error,
                );
            },
        );
    }

    closeTreatment() {
        this.close.emit('close');
    }

    saveTreatmentLocal() {
        this.service.setStorage('treatment', this.treatment);
    }

    initTreatment() {
        this.editMode = 'partial';
        this.populateData();

        this.service.parseFormToObject(this.formulario, this.treatment);
        this.treatmentStep = 'tables';
        this.saveTreatmentLocal();
    }

    toggleAprove(row: ContentPatientServicesTableModel) {
        const translate = this.service.translate;
        const refSWAL = 'PATIENTS.FORM_TREATMENT.SWAL';

        Swal.fire({
                      title: translate.instant(`${ refSWAL }.TITLE`),
                      text: translate.instant(
                          `${ refSWAL +
                          (this.showApprovedsProcedures
                              ? '.TEXT_DISAPPROVE'
                              : '.TEXT_APPROVE') }`,
                      ),
                      type: 'question',
                      showCancelButton: true,
                      confirmButtonText: translate.instant(`${ refSWAL }.CONFIRM_BUTTON`),
                      cancelButtonText: translate.instant(`${ refSWAL }.CANCEL_BUTTON`),
                  }).then(result => {


            if (result.value) {
                const index = this.treatment.servicos.findIndex(
                    each =>
                        each.convenioEspecialidadeProcedimentoId ===
                        row.convenioEspecialidadeProcedimentoId &&
                        each.emDente.toString() === row.emDente,
                );


                if (this.showApprovedsProcedures) {

                    this.treatment.servicos[
                        index
                        ].statusTratamentoServico = LOCAL_CONSTS.NAO_AUTORIZADO;
                    this.treatment.servicos[
                        index
                        ].statusTratamentoServicoDescricao = this.LISTAS_LOCAIS.listaStatusTratamentoServico.find(
                        item => item.value === LOCAL_CONSTS.NAO_AUTORIZADO,
                    ).label;
                } else {


                    this.treatment.servicos[index].statusTratamentoServico = LOCAL_CONSTS.REALIZAR;
                    this.treatment.servicos[
                        index
                        ].statusTratamentoServicoDescricao = this.LISTAS_LOCAIS.listaStatusTratamentoServico.find(
                        item => item.value === LOCAL_CONSTS.REALIZAR,
                    ).label;
                }

                this.saveTreatmentLocal();
                this.getProcedures();
                this.refreshFinancialTableByPaymentMethod();

            }
        });
    }

    applyToothNames(list: ProceduresModel[]) {
        list.map(item => {
            const value = item.emDente.value ? item.emDente.value : item.emDente;
            const tooth = toothsList.find(each => each.value === value);
            item.emDenteDescricao = tooth.label;
        });
    }

    applyStatusName(list: ProceduresModel[]) {
        list.map(item => {
            const status = this.LISTAS_LOCAIS.listaStatusTratamentoServico.find(
                each => each.value === item.statusTratamentoServico,
            );
            if (status) {
                item.statusTratamentoServicoDescricao = status.label;
            }
        });
    }

    getTreatment(vendedores = false) {
        this.service.loading(true);

        const ref =
            CONSTANTS.ENDPOINTS.patients.treatment.getTreatment + this.treatment.id;
        const treatment = this.service.httpGET(ref);
        const refPaid =
            CONSTANTS.ENDPOINTS.patients.financial.checkIfPaid + this.treatment.id;
        const paid = this.service.httpGET(refPaid);

        const joinList = [treatment, paid];
        let vendedoresRef = null;
        if (vendedores) {
            vendedoresRef = this.service.httpGET(
                CONSTANTS.ENDPOINTS.operator.active,
            );
            joinList.push(vendedoresRef);
        }


        forkJoin(joinList).subscribe(
            list => {
                this.treatment = list[0].body as ContentTreatmentModel;
                this.financialTable = this.treatment.parcelas;
                this.applyToothNames(this.treatment.servicos);
                this.applyStatusName(this.treatment.servicos);
                this.getProcedures();

                this.paid = (list[1].body as any).valor as number;
                if (this.paid > 0) {
                    this.doRefreshFinancial = false;
                }

                if (vendedores) {
                    this.vendedores = list[2].body as ShortListItem[];
                }

                this.treatmentStep = 'tables';
                this.service.loading(false);


            },
            err => {

                this.service.loading(false);
                this.service.notification.error(
                    this.service.translate.instant('COMMON.ERROR.SEARCH'),
                    err.error.error,
                );
            },
        );
    }

    createFinancialParts(replaceAll = true, numberOfProcedures = false) {
        let parcelaValor = +parseFloat((
                                           this.treatment.valorComDesconto / this.treatment.numeroTotalParcelas
                                       ).toString()).toFixed(2);

        if (replaceAll || !this.treatment.parcelas || this.treatment.parcelas.length === 0) {
            this.treatment.parcelas = [];

            for (let i = 0; i < this.treatment.numeroTotalParcelas; i++) {
                const parcela = new ParcelasModel();
                parcela.dataEmissao = moment().format('DD/MM/YYYY');
                parcela.formaDeRecebimento = this.treatment.formaDeRecebimento;
                parcela.numeroParcela = i + 1;
                parcela.descricao = numberOfProcedures ? this.treatment.servicos[i].convenioEspecialidadeProcedimentoNome : this.treatment.descricao;

                if (this.treatment.formaDeRecebimento === LOCAL_CONSTS.CARTAO_CREDITO) {
                    parcela.dataVencimento = moment()
                        .add(i + 1, 'months')
                        .format('DD/MM/YYYY');
                } else {
                    parcela.dataVencimento = moment()
                        .add(i, 'months')
                        .format('DD/MM/YYYY');
                }

                if (i === this.treatment.numeroTotalParcelas - 1) {
                    parcela.valor = this.treatment.valorComDesconto - (parcelaValor * (this.treatment.numeroTotalParcelas - 1));
                } else {
                    parcela.valor = parcelaValor;
                }

                // caso o método de pagamento seja "Número de Procedimentos", muda o valor da parcela escrito no começo do método
                if (numberOfProcedures) {
                    const proceduresTable = classToPlain(
                        this.treatment.servicos.filter(
                            procedure => procedure.statusTratamentoServico !== LOCAL_CONSTS.NAO_AUTORIZADO,
                        ),
                    ) as ProceduresModel[];
                    parcela.valor = proceduresTable[i].valor;
                    parcela.dataVencimento = moment().format('DD/MM/YYYY');
                }

                this.treatment.parcelas = [...this.treatment.parcelas, parcela];
            }
        } else {
            let countNonEdited = 0;
            const valueEdited = this.treatment.parcelas.reduce((prev, curr) => {
                if (curr.edited) {
                    return prev + parseFloat(curr.valor.toString());
                } else {
                    countNonEdited++;
                    return prev;
                }
            }, 0);

            parcelaValor = +parseFloat(
                ((this.treatment.valorComDesconto - valueEdited) / countNonEdited).toString()).toFixed(2);

            this.treatment.parcelas.filter(parcela => !parcela.edited).map((parcela, index) => {
                if (index === this.treatment.parcelas.length - 1) {
                    parcela.valor =
                        this.treatment.valorComDesconto - valueEdited -
                        parcelaValor * (countNonEdited - 1);
                } else {
                    parcela.valor = parcelaValor;
                }
            });

        }

        this.financialTable = classToPlain(this.treatment.parcelas) as ParcelasModel[];
        this.saveTreatmentLocal();
    }

    refreshFinancial(replaceAll = true, numberOfProcedures = false) {
        this.doRefreshFinancial = true;
        if (this.paid > 0) {
            this.doRefreshFinancial = false;
        }

        if (this.doRefreshFinancial) {
            this.toggleStateInputs.next(false);

            const approvedProcedures = this.treatment.servicos.filter(
                procedure => procedure.statusTratamentoServico !== LOCAL_CONSTS.NAO_AUTORIZADO,
            );
            const totalValue = approvedProcedures.reduce((prev, curr) => {
                return prev + parseFloat(curr.valor.toString());
            }, 0);

            this.treatment.valor = totalValue;

            if (
                this.formulario.get('valorDesconto').value ||
                this.formulario.get('valorPercentualDesconto').value
            ) {
                if (this.formulario.get('tipoDesconto').value === LOCAL_CONSTS.PORCENTAGEM) {
                    this.treatment.valorPercentualDesconto = parseFloat(
                        this.formulario.get('valorPercentualDesconto').value,
                    );
                    this.treatment.valorDesconto =
                        (parseFloat(this.formulario.get('valorPercentualDesconto').value) *
                            totalValue) /
                        100;
                    this.treatment.valorComDesconto =
                        totalValue - this.treatment.valorDesconto;
                } else {
                    this.treatment.valorDesconto = parseFloat(
                        this.formulario.get('valorDesconto').value,
                    );
                    this.treatment.valorPercentualDesconto =
                        parseFloat(this.formulario.get('valorDesconto').value) / totalValue;
                    this.treatment.valorComDesconto =
                        totalValue - parseFloat(this.formulario.get('valorDesconto').value);
                }
            } else {
                this.treatment.valorComDesconto = totalValue;
            }

            this.formulario.get('valor').setValue(totalValue);
            this.formulario
                .get('valorComDesconto')
                .setValue(this.treatment.valorComDesconto);

            this.service.parseFormToObject(this.formulario, this.treatment);


            if (
                this.treatment.formaDeRecebimento &&
                this.treatment.formaPagamento &&
                this.treatment.numeroTotalParcelas
            ) {
                this.createFinancialParts(replaceAll, numberOfProcedures);
            }
        } else {
            this.toggleStateInputs.next(true);
        }

    }

    openProcedureDialog(button: HTMLButtonElement) {
        button.blur();
        this.treatment.profissionalId = (this.formulario.get('profissionalId').value);

        const event = this._procedureDialogService.open(
            this.treatment.convenioId,
            this.treatment.profissionalId,
            this.treatment.id,
            this.profissionais
        );

        event.subscribe(procedures => {
            if (procedures) {
                this._notificationsService.success('','Novo procedimento incluído');
                this.treatment.servicos = this.treatment.servicos ? [...this.treatment.servicos, ...procedures] : procedures;
                this.showApprovedsProcedures = true;
                this.applyToothNames(this.treatment.servicos);
                this.applyStatusName(this.treatment.servicos);
                this.getProcedures();
                this.saveTreatmentLocal();
                this.refreshFinancialTableByPaymentMethod();
            }
        });
    }

    deleteProcedure(row: ContentPatientServicesTableModel) {
        const refSWAL = 'PATIENTS.FORM_TREATMENT.SWAL';
        const translate = this.service.translate;

        Swal.fire({
                      title: translate.instant(`${ refSWAL }.TITLE`),
                      text: translate.instant(`${ refSWAL }.TEXT_DELETE`),
                      type: 'question',
                      showCancelButton: true,
                      confirmButtonText: translate.instant(`${ refSWAL }.CONFIRM_BUTTON`),
                      cancelButtonText: translate.instant(`${ refSWAL }.CANCEL_BUTTON`),
                  }).then(result => {
            if (result.value) {
                const index = this.treatment.servicos.findIndex(each =>
                                                                    each.convenioEspecialidadeProcedimentoId ===
                                                                    row.convenioEspecialidadeProcedimentoId &&
                                                                    each.emDente.toString() === row.emDente,
                );

                this.treatment.servicos.splice(index, 1);
                this.getProcedures();
                this.refreshFinancialTableByPaymentMethod();
                this.saveTreatmentLocal();
            }
        });
    }

    salvarTratamento(approve = false): Promise<void> {
        this.service.loading(true);

        this.saveTreatmentLocal();
        this.service.parseFormToObject(this.formulario, this.treatment);
        this.refreshFinancialTableByPaymentMethod(false);

        const treatmentSave = classToPlain(this.treatment) as ContentTreatmentModel;
        delete treatmentSave.statusTratamentoDescricao;
        delete treatmentSave.posicaoTratamento;
        treatmentSave.dataAbertura = this.service.parseDateFromDatePicker(treatmentSave.dataAbertura);

        treatmentSave.servicos.map(servico => {
            delete servico.emDenteDescricao;
            delete servico.statusTratamentoServicoDescricao;

            if (servico.isFaceDistal === null) {
                servico.isFaceDistal = false;
            }

            if (servico.isFaceLingualPalatina === null) {
                servico.isFaceLingualPalatina = false;
            }

            if (servico.isFaceMesial === null) {
                servico.isFaceMesial = false;
            }

            if (servico.isFaceOclusalIncisal === null) {
                servico.isFaceOclusalIncisal = false;
            }

            if (servico.isFaceVestibular === null) {
                servico.isFaceVestibular = false;
            }
        });

        treatmentSave.parcelas.map(parcela => {
            delete parcela.recebido;
            delete parcela.edited;
        });


        return new Promise((resolve, reject) => {
            this.service.httpPOST(CONSTANTS.ENDPOINTS.patients.treatment.saveTreatment, treatmentSave)
                .subscribe(
                    data => {

                        const parcels = this.treatment.parcelas;
                        this.treatment = data.body as ContentTreatmentModel;
                        this.treatment.parcelas.forEach((parcel, i) => {
                            parcel.edited = parcels[i].edited;
                        });
                        this.service.loading(false);
                        if (!approve) {
                            this.service.notification.success(
                                this.service.translate.instant('PATIENTS.FORM_TREATMENT.descricaoDefault'),
                                this.service.translate.instant('PATIENTS.FORM_TREATMENT.TREATMENT_SAVE'),
                            );
                        }
                        this.service.removeStorage('treatment');
                        if (approve) {
                            this.aprovarDesaprovar();
                        }
                        resolve();
                    },
                    err => {

                        this.service.loading(false);
                        if (!approve) {
                            this.service.notification.error(
                                this.service.translate.instant('COMMON.ERROR.SEARCH'),
                                err.error.error
                            );
                        }
                        reject();
                    },
                );
        });
    }

    async aprovarDesaprovar() {
        if (this.treatment.statusTratamento === LOCAL_CONSTS.AGUARDANDO_APROVACAO) {
            if (this.treatment.id) {
                await this.salvarTratamento();
                this.executeAprovarDesaprovar();
            } else {
                this.salvarTratamento(true);
            }
        } else {
            const refSWAL = 'PATIENTS.FORM_TREATMENT.SWAL';
            const translate = this.service.translate;

            Swal.fire({
                          title: translate.instant(`${ refSWAL }.TITLE`),
                          text: translate.instant(`${ refSWAL }.TEXT_DISAPPROVE_TREATMENT`),
                          type: 'question',
                          showCancelButton: true,
                          confirmButtonText: translate.instant(`${ refSWAL }.CONFIRM_BUTTON`),
                          cancelButtonText: translate.instant(`${ refSWAL }.CANCEL_BUTTON`),
                      }).then(result => {
                if (result.value) {
                    this.executeAprovarDesaprovar();
                }
            });
        }
    }

    executeAprovarDesaprovar() {
        this.service.loading(true);
        this.service.httpPUT(CONSTANTS.ENDPOINTS.patients.treatment.approveDesapprove, { id: this.treatment.id })
            .subscribe(
                data => {
                    this.service.loading(false);
                    if (this.treatment.statusTratamento === LOCAL_CONSTS.AGUARDANDO_APROVACAO) {
                        this.close.emit('close');
                    } else {
                        this.treatment.statusTratamento = LOCAL_CONSTS.AGUARDANDO_APROVACAO;
                        this.treatment.statusTratamentoDescricao =
                            this.LISTAS_LOCAIS.listaStatusTratamento
                                .find(item => item.value === LOCAL_CONSTS.AGUARDANDO_APROVACAO).label;
                        this.formulario.get('statusTratamento').setValue(LOCAL_CONSTS.AGUARDANDO_APROVACAO);
                        this.toggleStateInputs.next(false);
                    }
                },
                err => {
                    this.service.loading(false);
                    this.service.notification.error(
                        this.service.translate.instant('COMMON.ERROR.SEARCH'),
                        err.error.error,
                    );
                },
            );
    }

    deleteTratamento() {
        const refSWAL = 'PATIENTS.FORM_TREATMENT.SWAL';
        const translate = this.service.translate;

        Swal.fire({
                      title: translate.instant(`${ refSWAL }.TITLE`),
                      text: translate.instant(`${ refSWAL }.TEXT_DELETE_TREATMENT`),
                      type: 'question',
                      showCancelButton: true,
                      confirmButtonText: translate.instant(`${ refSWAL }.CONFIRM_BUTTON`),
                      cancelButtonText: translate.instant(`${ refSWAL }.CANCEL_BUTTON`),
                  }).then(result => {
            if (result.value) {
                this.service.loading(true);
                this.service.httpDELETE(CONSTANTS.ENDPOINTS.patients.treatment.removeTreatment + this.treatment.id)
                    .subscribe(
                        data => {
                            this.close.emit('close');

                        },
                        err => {

                            this.service.loading(false);
                            this.service.notification.error(
                                this.service.translate.instant('COMMON.ERROR.SEARCH'),
                                err.error.error,
                            );
                        },
                    );
            }
        });
    }

    async editProcedure(procedure: ContentPatientServicesTableModel, index: number) {
        const modalRef = this._modalService.open(EditProcedureComponent, { size: 'lg' });
        modalRef.componentInstance.procedure = { ...procedure };
        modalRef.componentInstance.professionals = this.profissionais;
        modalRef.componentInstance.statuses = this.LISTAS_LOCAIS.listaStatusTratamentoServico;

        const editedProcedure = await modalRef.result;

        if (editedProcedure) {
            const { profissionalId, profissionalNome, statusTratamentoServicoDescricao, statusTratamentoServico, valor } = editedProcedure;
            procedure.profissionalId = profissionalId;
            procedure.profissionalNome = profissionalNome;
            procedure.statusTratamentoServicoDescricao = statusTratamentoServicoDescricao;
            procedure.statusTratamentoServico = statusTratamentoServico;
            procedure.valor = valor;
            this.treatment.servicos[index].profissionalId = profissionalId;
            this.treatment.servicos[index].profissionalNome = profissionalNome;
            this.treatment.servicos[index].statusTratamentoServicoDescricao = statusTratamentoServicoDescricao;
            this.treatment.servicos[index].statusTratamentoServico = statusTratamentoServico;
            this.treatment.servicos[index].valor = valor;
            this._saveEditProcedure();
        }
    }

    getDescRecebimento(value: string) {
        return this.LISTAS_LOCAIS.listaFormaRecebimento.find(item => item.value === value).label;
    }

    getColorStatusTratamentoServicoDescricao(value: string) {
        return this.LISTAS_LOCAIS.listaStatusTratamentoServico.find(item => item.value === value).color;
    }

    editParcel(parcel: ParcelasModel) {
        this.selectedParcel = { ...parcel };
        this.selectedParcel.dataVencimento = this.service.parseDatePicker(this.selectedParcel.dataVencimento);
        this.showModalParcela = true;

        this.service.modalResult.next(false);

        setTimeout(() => {
            this.service.modalResult
                .pipe(takeWhile(val => val !== null))
                .subscribe(data => {

                    if (data) {


                        const index = this.treatment.parcelas.findIndex(item => item.numeroParcela === parcel.numeroParcela);
                        this.treatment.parcelas[index] = data;
                        this.treatment.parcelas[index].edited = true;
                        this.refreshFinancialTableByPaymentMethod(false);
                        //  this.saveTreatmentLocal();
                        //  this.salvarTratamento();
                        this.showModalParcela = false;
                        this.service.modalResult.next(null);
                    }
                });
            document.querySelector('#modal-parcela').classList.add('md-show');
        }, 500);
    }

    refreshFinancialTableByPaymentMethod(replaceAllFromRefresh = true) {
        const formaPagamento = this.formulario.get('formaPagamento').value;

        switch (formaPagamento) {
            case LOCAL_CONSTS.A_VISTA:
                this.formulario.get('numeroTotalParcelas').setValue(1);
                this.refreshFinancial(replaceAllFromRefresh);
                break;
            case 'NUM_PROCEDIMENTO':
                const numberOfParcels = (classToPlain(
                    this.treatment.servicos.filter(
                        procedure => procedure.statusTratamentoServico !== LOCAL_CONSTS.NAO_AUTORIZADO,
                    )
                ) as []).length;
                this.formulario.get('numeroTotalParcelas').setValue(numberOfParcels);
                this.refreshFinancial(replaceAllFromRefresh, true);
                break;
            default:
                this.refreshFinancial(replaceAllFromRefresh);
                break;
        }
    }

    async updateStatusTratamento($event) {
        if (this.treatment.id) {
            const situation = this.situations.find(s => s.label === $event);
            if (!situation) {
                this.service.notification.info('Nenhum status encontrado com essa descrição.');
                return;
            }
            if (situation.value === 'AGUARDANDO_APROVACAO') {
                this.service.notification.info('Não é permitido alterar o status para ' + situation.label);
                return;
            }
            try {
                const { value } = await Swal.fire({
                                                      title: 'Alteração de status',
                                                      text: 'Tem certeza que deseja alterar o status para ' + situation.label + ' ?',
                                                      type: 'question',
                                                      showCancelButton: true,
                                                      confirmButtonText: 'Sim. Alterar',
                                                      cancelButtonText: 'Cancelar',
                                                  });

                if (!value) {
                    return;
                }
                this.service.loading(true);
                const params = {
                    id: this.treatment.id,
                    statusTratamento: situation.value,
                };
                await this.service.httpPUT(CONSTANTS.ENDPOINTS.patients.treatment.updateStatusTratamento, params)
                    .toPromise();
                this.service.notification.success('Status alterado.');
                this.closeTreatment();
            } catch (e) {
                this.service.notification.error('Ocorreu um erro ao alterar o status.');
            } finally {
                this.service.loading(false);
            }
        }
    }

    changeTreatmentCovenant($event) {
        this.treatment.convenioId = $event;
    }

    private _saveEditProcedure() {
        this.saveTreatmentLocal();
        this.refreshFinancialTableByPaymentMethod(false);
    }

}
