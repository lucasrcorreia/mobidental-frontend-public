import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnamneseComponent } from './anamnese/anamnese.component';
import { SingleAnamneseComponent } from './anamnese/single-anamnese/single-anamnese.component';

const routes: Routes = [
  {
    path: '',
    component: AnamneseComponent
  },
  {
    path: ':id',
    component: SingleAnamneseComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnamneseRoutingModule {
}
