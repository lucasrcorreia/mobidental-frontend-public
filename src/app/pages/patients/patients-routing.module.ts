import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientsComponent } from './patients.component';
import { FormPatientsComponent } from './form-patients/form-patients.component';

const routes: Routes = [
  {
    path: '',
    component: PatientsComponent
  },
  {
    path: 'form',
    component: FormPatientsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientsRoutingModule { }
