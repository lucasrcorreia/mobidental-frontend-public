import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ContentBillTreatmentModel } from "../../../core/models/patients/patients.model";
import { UtilsService } from "../../../services/utils/utils.service";
import { CONSTANTS } from "../../../core/constants/constants";
import { ModalAnimationComponent } from "../../../shared/modal-animation/modal-animation.component";
import { GlobalSpinnerService } from "../../../lib/global-spinner.service";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import {
	dateToNgb,
	ngbToDate,
} from "../../financial/bills-to-receive/bills-to-receive-filters/bills-to-receive-filters.component";
import * as moment from "moment";

@Component({
	selector: "app-modal-edit-parcel-account",
	templateUrl: "./modal-edit-parcel-account.component.html",
	styleUrls: [
		"./modal-edit-parcel-account.component.scss",
		"./../../../../assets/icon/icofont/css/icofont.scss",
	],
})
export class ModalEditParcelAccountComponent implements OnInit {
	@Input() totalParcels: number;
	@Input() status = { text: "", color: "" };
	@Input() modal?: ModalAnimationComponent;

	@Output() onSave = new EventEmitter<void>();
	@Output() onClose = new EventEmitter<void>();

	private _parcel: ContentBillTreatmentModel;
	descricao?: string;

	locale = this.service.translate.instant("COMMON.LOCALE");
	dataVencimentoData?: NgbDateStruct;

	constructor(
		public service: UtilsService,
		private readonly _globalSpinnerService: GlobalSpinnerService
	) {}

	ngOnInit(): void {
		if (this.parcel.dataVencimento) {
			this.dataVencimentoData = dateToNgb(
				moment(this.parcel.dataVencimento, "DD/MM/YYYY").toDate()
			);
		}
	}

	get parcel(): ContentBillTreatmentModel {
		return this._parcel;
	}

	@Input()
	set parcel(value: ContentBillTreatmentModel) {
		this._parcel = value;
		this.descricao = this.descricao || value.descricao;
	}

	toDate(ngb: NgbDateStruct) {
		return ngbToDate(ngb);
	}

	closeModal(parcel: ContentBillTreatmentModel = null) {
		if (this.modal) {
			this.modal.close(parcel);
		}

		this.service.closeModal("modal-edit-parcel-account", parcel);
		this.onClose.emit();
	}

	save() {
		const dataParcel = {
			id: this.parcel.id,
			dataVencimento: moment(ngbToDate(this.dataVencimentoData)).format("DD/MM/YYYY"),
			descricao: this.descricao,
		};

		const loadingToken = this._globalSpinnerService.loadingManager.start();

		this.service.httpPUT(CONSTANTS.ENDPOINTS.patients.financial.changeParcel, dataParcel).subscribe(
			(data) => {
				this.closeModal(this.parcel);
				this.onSave.emit();
				loadingToken.finished();
			},
			(err) => {
				loadingToken.finished();
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			}
		);
	}
}
