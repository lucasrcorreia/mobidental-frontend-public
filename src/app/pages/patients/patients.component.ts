import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { UtilsService } from '../../services/utils/utils.service';
import { ColumnsTableModel } from '../../core/models/table/columns.model';
import { TranslateService } from '@ngx-translate/core';
import { PaginationResponseModel } from '../../core/models/response/paginationResponse.model';
import { CONSTANTS } from '../../core/constants/constants';
import { interval, Subject } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import Swal from 'sweetalert2';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { takeUntil } from 'rxjs/operators';
import { MaskFormatService } from '../../services/utils/mask-format.service';
import { FULL_CELLPHONE_LENGTH } from '../agenda/agenda.component';
import { NotificationsService } from 'angular2-notifications';
import { UserPermissionService } from '../../auth/user-permission.service';

declare let $: any;

export const PATIENT_PERMISSIONS = {
    DELETE: 7,
    EDIT: 6,
};

@Component({
    selector: 'app-patients',
    templateUrl: './patients.component.html',
    styleUrls: [
        './patients.component.scss',
        './../../../assets/icon/icofont/css/icofont.scss'
    ]
})
export class PatientsComponent implements OnInit, OnDestroy {

    readonly PATIENT_PERMISSIONS = PATIENT_PERMISSIONS;
    @ViewChild(DatatableComponent) table: DatatableComponent;

    columns: ColumnsTableModel[] = [
        {
            action: true,
            name: this.service.translate.instant('PATIENTS.TABLE.COL1'),
            prop: 'id',
            sortable: false,
            width: 105,
            fixed: true
        },
        {
            action: false,
            name: this.service.translate.instant('PATIENTS.TABLE.COL2'),
            prop: 'pessoaNome',
            sortable: true,
            width: 350
        },
        {
            action: false,
            name: this.service.translate.instant('PATIENTS.TABLE.COL3'),
            prop: 'codigoProntuario',
            sortable: true,
            width: 60
        },
        {
            action: false,
            name: this.service.translate.instant('PATIENTS.TABLE.COL4'),
            prop: 'pessoaCelular',
            sortable: true,
            width: 130,
            cellphone: true,
        },
        {
            action: false,
            name: this.service.translate.instant('PATIENTS.TABLE.COL5'),
            prop: 'pessoaTelefone',
            sortable: true,
            width: 80
        }
    ];

    rows = {};
    rowsPage = [];
    searchParam = {
        page: 0,
        query: '',
        size: 10,
        sorting: {
            undefined: 'asc'
        }
    };

    page = {
        size: 10,
        totalElements: 0,
        totalPages: 0,
        pageNumber: 0
    };

    time = 0;
    wait = 3;

    subscription: Subscription;
    private destroy$ = new Subject();

    constructor(
        public service: UtilsService,
        public translate: TranslateService,
        private cd: ChangeDetectorRef,
        private readonly _maskFormatService: MaskFormatService,
        private readonly _notificationsService: NotificationsService,
        private readonly _userPermissionService: UserPermissionService,
    ) {
        this.service.toggleMenu.pipe(takeUntil(this.destroy$)).subscribe(data => {
            if (data) {
                this.updateDataTable();
            }
        });
    }

    ngOnInit() {
        this.populateTable();
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    updateDataTable() {
        this.cd.markForCheck();
        setTimeout(() => {
            this.table.recalculate();
            (this.table as any).cd.markForCheck();
        });
    }

    populateTable() {
        this.service.loading(true);
        this.searchParam.page = this.page.pageNumber;
        this.page.size = this.searchParam.size;
        this.service
            .httpPOST(CONSTANTS.ENDPOINTS.patients.general.active, this.searchParam)
            .subscribe(
                data => {
                    const result = data.body as PaginationResponseModel;
                    if (this.page.pageNumber === 0) {
                        this.page.totalElements = result.totalElements;
                        this.page.totalPages = result.totalPages;
                    }

                    this.rows[this.page.pageNumber] = result.content;
                    this.rowsPage = result.content;
                    this.service.loading(false);
                },
                err => {

                    this.service.loading(false);
                    this.service.notification.error(
                        this.service.translate.instant('COMMON.ERROR.SEARCH'),
                        err.error.error
                    );
                }
            );
    }

    setPage(pageInfo) {
        this.page.pageNumber = pageInfo.offset;
        if (this.rows[this.page.pageNumber]) {
            this.rowsPage = this.rows[this.page.pageNumber];
        } else {
            this.populateTable();
        }
    }

    startCronometro() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }

        const source = interval(250);
        this.subscription = source.subscribe(x => {
            this.time++;
            if (this.time >= this.wait) {
                this.stopCronometro();
                this.time = 0;
                this.populateTable();
            }
        });
    }

    stopCronometro() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    new() {
        this.service.navigate(CONSTANTS.ROUTERS.PATIENTS.FORM, {id: -1});
    }

    edit(id: number) {
        this.service.navigate(CONSTANTS.ROUTERS.PATIENTS.FORM, {id});
    }

    hasPermission(actionId: number): boolean {
        return this._userPermissionService.hasPermission(actionId);
    }

    delete(id: number) {
        const translate = this.service.translate;
        const refSWAL = 'PATIENTS.SWAL';

        Swal.fire({
            title: translate.instant(`${refSWAL}.TITLE`),
            text: translate.instant(`${refSWAL}.TEXT`),
            type: 'question',
            showCancelButton: true,
            confirmButtonText: translate.instant(`${refSWAL}.CONFIRM_BUTTON`),
            cancelButtonText: translate.instant(`${refSWAL}.CANCEL_BUTTON`)
        }).then(result => {
            if (result.value) {
                this.service.loading(true);
                this.service
                    .httpDELETE(CONSTANTS.ENDPOINTS.patients.general.delete + id)
                    .subscribe(
                        data => {
                            this.service.loading(false);
                            this.populateTable();
                        },
                        err => {

                            this.service.loading(false);
                            this.service.notification.error(
                                this.service.translate.instant('COMMON.ERROR.SEARCH'),
                                err.error.error
                            );
                        }
                    );
            }
        });
    }

    getCellphoneWithMask(cellphone: string): string {
        return this._maskFormatService.getFormatedCellPhone(cellphone);
    }

    goToPatientWhatsapp(cellphone: string) {
        const primaryCellphone = cellphone.replace(/\D+/g, '');

        if (primaryCellphone.length !== FULL_CELLPHONE_LENGTH) {
            this._notificationsService.alert('O paciente não possui um número de telefone válido! Deve ter 11 dígitos.');
            return;
        }

        const whatsappLink = 'https://api.whatsapp.com/send?phone=55' + primaryCellphone;

        // Usar window.open() pode não funcionar de acordo com browser e/ou configuração do usuário
        $('<a />', {href: whatsappLink, target: '_blank'}).get(0).click();
    }

}
