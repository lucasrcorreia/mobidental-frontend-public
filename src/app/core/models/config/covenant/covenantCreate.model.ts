export class CovenanteCreate {
  convenioNome: string = null;
  isCopiaProcedimentos: number = null;
  convenioId: number = null;
}

export class CovenantModel {
  id: number = null;
  nome: string = null;
  isPadrao = false;
  especialidades: Array<SpecialtiesCovenantModel> = [];
}

export class SpecialtiesCovenantModel {
  id: number = null;
  nome: string = null;
  procedimentos: Array<ProceduresCovenantModel> = [];
}

export interface ProceduresCovenantModel {
  id: number;
  convenioId: number;
  procedimentoId: number;
  especialidadeId: number;
  procedimentoNome: string;
  procedimentoIsAceitaFace;
  procedimentoStatus;
  preco: number;
  procedimentoPorcentagemComissao: number;
  procedimentoValorCustoProtetico: number;
}
