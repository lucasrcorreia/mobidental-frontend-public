import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContaConfigComponent } from './conta-config.component';

const routes: Routes = [
  {
    path: '',
    component: ContaConfigComponent,
    data: {
      id: 8,
      children: true,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContaConfigRoutingModule {}
