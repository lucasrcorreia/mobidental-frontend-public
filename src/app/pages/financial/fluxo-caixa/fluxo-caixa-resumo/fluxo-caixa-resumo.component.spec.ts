import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FluxoCaixaResumoComponent } from './fluxo-caixa-resumo.component';

describe('FluxoCaixaResumoComponent', () => {
  let component: FluxoCaixaResumoComponent;
  let fixture: ComponentFixture<FluxoCaixaResumoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FluxoCaixaResumoComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FluxoCaixaResumoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
