import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UtilsService } from '../../../../services/utils/utils.service';
import { ConvenantModel } from '../../../../core/models/patients/convenio.model';
import { ShortListItem } from '../../../../core/models/forms/common/common.model';
import { HttpClient } from '@angular/common/http';
import { CONSTANTS } from '../../../../core/constants/constants';
import { ProstheticToothModel } from '../../../../core/models/tools/prosthetic/tooth.model';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, flatMap } from 'rxjs/operators';
import { ProceduresCovenantModel } from '../../../../core/models/config/covenant/covenantCreate.model';
import { DetachedTreatment } from './model/detached-treatment';

@Component({
	selector: 'app-detached-treatment',
	templateUrl: './detached-treatment.component.html',
	styleUrls: ['./detached-treatment.component.scss'],
})
export class DetachedTreatmentComponent implements OnInit {

	@Input() patientId?: number;
	detachedTreatment: DetachedTreatment = { statusTratamentoServico: 'REALIZAR' };

	searchProcedureValue = '';
	formatMatches = (value: ProceduresCovenantModel) => value.procedimentoNome;
	readonly procedures = (text$: Observable<string>) => text$.pipe(
			debounceTime(300),
			distinctUntilChanged(),
			flatMap(value => this.fetchProcedures(value)),
	);

	covenantId: ConvenantModel;
	covenants: ConvenantModel[];
	professionals: ShortListItem[];
	teeth: ProstheticToothModel[];
	readonly faces = [
		{
			label: this._service.translate.instant(
					'PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceDistal',
			),
			prop: 'isFaceDistal',
		},
		{
			label: this._service.translate.instant(
					'PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceOclusalIncisal',
			),
			prop: 'isFaceOclusalIncisal',
		},
		{
			label: this._service.translate.instant(
					'PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceLingualPalatina',
			),
			prop: 'isFaceLingualPalatina',
		},
		{
			label: this._service.translate.instant(
					'PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceMesial',
			),
			prop: 'isFaceMesial',
		},
		{
			label: this._service.translate.instant(
					'PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceVestibular',
			),
			prop: 'isFaceVestibular',
		},
	];
	showFaces = false;

	constructor(private readonly _activeModal: NgbActiveModal,
				private readonly _http: HttpClient,
				readonly _service: UtilsService) {}

	ngOnInit() {
		this._fetchCovenants();
		this._fetchProfessionals();
		this._fetchTooths();

		this.detachedTreatment.pacienteId = this.patientId;
	}

	private async _fetchCovenants() {
		const { body } = await this._service.httpGET(CONSTANTS.ENDPOINTS.patients.treatment.convenant).toPromise();
		this.covenants = body as ConvenantModel[];
	}

	private async _fetchProfessionals() {
		const { body } = await this._service.httpGET(CONSTANTS.ENDPOINTS.professional.active).toPromise();
		this.professionals = body as ShortListItem[];
	}

	private async _fetchTooths() {
		const { body } = await this._service.httpGET(CONSTANTS.ENDPOINTS.tools.prosthetic.dropdownTooth).toPromise();
		this.teeth = body as ProstheticToothModel[];
	}

	async fetchProcedures($event) {
		if (!$event || !this.covenantId) {
			return [];
		}
		const { body } = await this._service.httpGET(`api/convenio_especialidade_procedimento/listarProcedimentosByConvenio/?convenioId=${ this.covenantId }&filtroProcedimentoNome=${ $event }`).toPromise();
		return body as any[];
	}

	fillProcedure($event: ProceduresCovenantModel) {
		this.detachedTreatment.convenioEspecialidadeProcedimentoId = $event.id;
		this.detachedTreatment.convenioEspecialidadeProcedimentoNome = $event.procedimentoNome;
		this.detachedTreatment.valor = $event.preco;
		this.showFaces = $event.procedimentoIsAceitaFace;
	}

	save() {
		this._activeModal.close(this.detachedTreatment);
	}

	close() {
		this._activeModal.close();
	}

}
