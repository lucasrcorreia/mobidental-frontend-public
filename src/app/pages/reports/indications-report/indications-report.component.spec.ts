import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndicationsReportComponent } from './indications-report.component';

describe('IndicationsReportComponent', () => {
  let component: IndicationsReportComponent;
  let fixture: ComponentFixture<IndicationsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndicationsReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndicationsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
