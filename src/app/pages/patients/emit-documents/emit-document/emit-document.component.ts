import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Tab } from '../emit-documents.component';
import { DocumentModelService } from '../../../configs/medical-record/document-model/service/document-model.service';
import { UtilsService } from '../../../../services/utils/utils.service';
import { NotificationsService } from 'angular2-notifications';
import { PdfGeneratorService, PDFMake } from '../../../../services/pdf-generator/pdf-generator.service';
import { DocumentModel } from '../../../configs/medical-record/document-model/models/DocumentModel';
import { FormBuilder } from '@angular/forms';
import { CONSTANTS } from '../../../../core/constants/constants';
import { ShortListItem } from '../../../../core/models/forms/common/common.model';
import { PrintConfiguration } from '../../../configs/medical-record/print/model/print-configuration';
import { PatientModel } from '../../../../core/models/patients/patients.model';
import { IssuableDocument } from '../../../configs/medical-record/document-model/models/IssuableDocument';
import { ActivatedRoute } from '@angular/router';
import { INgxSelectOption } from 'ngx-select-ex';
import { IssuableDocumentType } from '../../../configs/medical-record/document-model/models/enum/IssuableDocumentType';
import { CommonPrintService } from '../service/common-print.service';
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'app-emit-document',
	templateUrl: './emit-document.component.html',
	styleUrls: ['./emit-document.component.scss'],
})
export class EmitDocumentComponent implements OnInit {

	@Input() patient: PatientModel;
	@Input() printConfiguration: PrintConfiguration;
	@Output() tabEmitter = new EventEmitter<{ tab: Tab, issuableDocumentType: IssuableDocumentType }>();
	loadingText = false;
	models: DocumentModel[];
	professionals: ShortListItem[];
	issuableDocument: IssuableDocument = { tipoDocumento: IssuableDocumentType.DOCUMENTO };

	constructor(private readonly _documentModelService: DocumentModelService,
				readonly _service: UtilsService,
				private readonly _fb: FormBuilder,
				private readonly _notificationsService: NotificationsService,
				private readonly _pdfGeneratorService: PdfGeneratorService,
				private readonly _commonPrintService: CommonPrintService,
				private readonly _route: ActivatedRoute,
				private readonly _ngbCalendar: NgbCalendar) {
		this.issuableDocument.data = _ngbCalendar.getToday();
	}

	ngOnInit() {
		this.issuableDocument.pacienteId = this.patient.id.toString();
		this.issuableDocument.nomeModelo = 'Documento';
		this._initDropdowns();
	}

	private async _initDropdowns() {
		this.models = await this._documentModelService.listDocumentModelsAsDropdown();
		const professionals = await this._service.httpGET(CONSTANTS.ENDPOINTS.professional.active).toPromise();
		this.professionals = professionals.body as ShortListItem[];
	}

	async getModelText(options: INgxSelectOption[]) {
		try {
			this.loadingText = true;
			const data = options[0].data;
			const model: DocumentModel = await this._documentModelService.findOne(data.id);
			this.issuableDocument.texto = model.texto;
		} catch (e) {
			this._notificationsService.error('Ocorreu um erro ao tentar obter o modelo');
		} finally {
			this.loadingText = false;
		}
	}

	async emit() {
		const fileName = this.issuableDocument.nomeModelo + ' - ' + this.patient.pessoaNome + '.pdf';
		try {
			this._service.loading(true);
			this.issuableDocument.data = this._service.parseDateFromDatePicker(this.issuableDocument.data);
			const pdfMake = await this._buildPdfMakeContent(this.issuableDocument);
			const pdfMakeDefinition = this._pdfGeneratorService.exportPDFMakeAsPDF(pdfMake.pdfMake, pdfMake.html, fileName);
			await this._documentModelService.emit(
					this.issuableDocument,
					await this._commonPrintService.returnFileFromPdfMakeDefinition(pdfMakeDefinition, fileName));
			this._notificationsService.success('Documento emitido com sucesso');
			this.close();
			this._commonPrintService.visualizeIssuableDocument(pdfMakeDefinition);
		} catch (e) {
			this._notificationsService.error('Ocorreu um erro ao emitir o documento.');
		} finally {
			this._service.loading(false);
		}
	}

	private async _buildPdfMakeContent(issuableDocument: IssuableDocument): Promise<{ pdfMake: PDFMake, html: string }> {
		let pdfMake: PDFMake = {};
		let html = issuableDocument.texto;

		pdfMake = this._commonPrintService.getFooter(this.printConfiguration, pdfMake);
		html = await this._commonPrintService.getHeader(this.printConfiguration, html);

		const tagsValues: any = await this._documentModelService.getTagValuesByDocument(issuableDocument);

		return { pdfMake, html: this._replaceAllTagsOnHtml(tagsValues, html) };
	}

	private _replaceAllTagsOnHtml(tagValues: any, html: string): string {
		let text = html;
		const values = tagValues.map(tag => {
			return Object.entries(tag);
		});

		values.forEach(value => {
			const match = new RegExp(value[0][0], 'g');
			text = text.replace(match, value[0][1]);
		});

		return text;
	}

	close() {
		this.tabEmitter.emit({ tab: Tab.LIST, issuableDocumentType: IssuableDocumentType.DOCUMENTO });
	}

}
