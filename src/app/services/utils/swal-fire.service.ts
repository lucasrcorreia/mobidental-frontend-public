import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SwalFireService {

  constructor() { }

  async confirmDelete(message: string) {
    const { value } = await Swal.fire({
      title: 'Confirmação de exclusão',
      text: message,
      type: 'question',
      showCancelButton: true,
      confirmButtonText: 'Sim. Apague',
      cancelButtonText: 'Cancelar',
    });

    return value;
  }
}
