import { Component } from '@angular/core';
import { GlobalSpinnerService } from '../../../lib/global-spinner.service';
import { NotificationsService } from 'angular2-notifications';
import { errorToString } from '../../../lib/helpers/error-to-string';
import { BehaviorSubject, from, Observable, of } from 'rxjs';
import { PermissionAction, PermissionModule, PermissionProfile, PermissionsApiService } from '../../../api/permissions-api.service';
import { distinctUntilChanged, flatMap, map, shareReplay, take, tap } from 'rxjs/operators';
import { debug } from 'util';
import { prop, partition, chain, pipe, map as arrMap } from 'ramda';
import { FormBuilder, FormGroup } from '@angular/forms';
import { observableTakeNext } from '../../../lib/helpers/observable-take-next';
import { Location } from '@angular/common';
import { load } from '@angular/core/src/render3';

const separaIdsAcoesAtivas: (modulos: PermissionModule[]) => [number[], number[]] = pipe(
  chain<PermissionModule, PermissionAction>(module => module.moduloTelaAcao),
  partition(perm => perm.possuiPermissao),
  arrMap(arrMap(prop('moduloTelaAcaoId'))),
) as any;

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss'],
})
export class PermissionsComponent {

  private readonly _profileSubject = new BehaviorSubject<PermissionProfile | undefined>(undefined);

  readonly profile$ = from(this._fetchProfiles());

  readonly permission$: Observable<PermissionModule[]> = this._profileSubject.pipe(
    map(profile => profile && profile.id),
    distinctUntilChanged(),
    flatMap(profileId => profileId ? this._fetchPermissions(profileId) : of([])),
    shareReplay(1),
  );

  constructor(
    private readonly _permissionsApiService: PermissionsApiService,
    private readonly _fb: FormBuilder,
    private readonly _globalSpinnerService: GlobalSpinnerService,
    private readonly _notificationsService: NotificationsService,
    private readonly _location: Location,
  ) {
    this._subscribeToFirstProfileResult();
  }

  get profile() {
    return this._profileSubject.value;
  }

  set profile(p: PermissionProfile | undefined) {
    this._profileSubject.next(p);
  }

  toggleAllFor(module: PermissionModule): void {
    const allActivated = this.allActivated(module);

    for (const action of module.moduloTelaAcao) {
      action.possuiPermissao = !allActivated;
    }
  }

  allActivated(module: PermissionModule): boolean {
    return module.moduloTelaAcao.every(action => action.possuiPermissao);
  }

  async save(): Promise<void> {
    const { profile } = this;

    if (!profile) {
      return;
    }

    const data: PermissionModule[] = await observableTakeNext(this.permission$);

    const [active, nonActive] = separaIdsAcoesAtivas(data);

    const loadingToken = this._globalSpinnerService.loadingManager.start();

    try {
      await this._permissionsApiService.updatePermissions(profile.id, active, nonActive);
      this._notificationsService.success('Configurações salvas com sucesso!');
    } catch (e) {
      this._notificationsService.error('Erro ao salvar as configurações', errorToString(e));
    } finally {
      loadingToken.finished();
    }
  }

  cancel() {
    this._location.back();
  }

  private async _subscribeToFirstProfileResult() {
    const first = await this.profile$.pipe(take(1)).toPromise();
    this._profileSubject.next(first[0]);
  }

  private async _fetchProfiles(): Promise<PermissionProfile[]> {
    const token = this._globalSpinnerService.loadingManager.start();

    try {
      return await this._permissionsApiService.fetchProfiles();
    } catch (e) {
      this._notificationsService.error('Erro ao consultar perfis disponíveis', errorToString(e));
      return [];
    } finally {
      token.finished();
    }
  }

  private async _fetchPermissions(profileId: number): Promise<PermissionModule[]> {
    const token = this._globalSpinnerService.loadingManager.start();

    try {
      return await this._permissionsApiService.fetchPermissionsByProfile(profileId);
    } catch (e) {
      this._notificationsService.error('Erro ao consultar permissões disponíveis', errorToString(e));
      return [];
    } finally {
      token.finished();
    }
  }

}
