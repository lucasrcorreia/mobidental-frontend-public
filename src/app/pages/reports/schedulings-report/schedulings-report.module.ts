import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SchedulingsReportRoutingModule } from './schedulings-report-routing.module';
import { SchedulingsReportComponent } from './schedulings-report.component';
import { SchedulingStatisticsComponent } from './scheduling-statistics/scheduling-statistics.component';
import { SchedulingsReportPageComponent } from './schedulings-report-page/schedulings-report-page.component';
import { NgbDatepickerModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxTranslateModule } from '../../../core/translate/translate.module';
import { NgxSelectModule } from 'ngx-select-ex';
import { InputMaskModule } from 'racoon-mask-raw';
import { FormsModule } from '@angular/forms';
import { ReportsModule } from '../reports.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
	declarations: [
		SchedulingsReportComponent,
		SchedulingStatisticsComponent,
		SchedulingsReportPageComponent,
	],
	imports: [
		CommonModule,
		SchedulingsReportRoutingModule,
		NgbTabsetModule,
		NgxTranslateModule,
		NgxSelectModule,
		InputMaskModule,
		FormsModule,
		ReportsModule,
		NgxChartsModule,
		NgbDatepickerModule,
	],
})
export class SchedulingsReportModule {}
