import { Component, Input, OnInit } from '@angular/core';
import { ContaModel } from '../../../../api/conta-api.service';
import { CurrencyMaskConfig } from 'ngx-currency';

@Component({
  selector: 'app-conta-config-financeiro',
  templateUrl: './conta-config-financeiro.component.html',
  styleUrls: ['./conta-config-financeiro.component.scss'],
})
export class ContaConfigFinanceiroComponent {

  readonly maskOptions: Readonly<Partial<CurrencyMaskConfig>> = {
    prefix: '',
    suffix: ' %',
  };

  @Input() conta: Partial<ContaModel> = {};

}
