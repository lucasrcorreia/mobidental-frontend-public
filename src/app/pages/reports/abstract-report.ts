import { SheetGeneratorService } from "../../services/sheets-generator/sheet-generator.service";
import { MaskFormatService } from "../../services/utils/mask-format.service";
import { ReportPrintType } from "./report-header-actions/report-header-actions.component";
import { ReportDatatableColumn, Type } from "./report-datatable/report-datatable.component";
import { CNPJ_LENGTH, CPF_LENGTH } from "../../lib/input-masks";
import { AsyncTemplate } from "../../lib/helpers/async-template";
import { NotificationsService } from "angular2-notifications";
import { GlobalSpinnerService } from "../../lib/global-spinner.service";
import {
	PdfGeneratorService,
	PDFMake,
	PdfMakePageOrientation,
} from "../../services/pdf-generator/pdf-generator.service";
import {
	CommonsReportBuildService,
	REPORT_STYLES,
} from "../../services/report/commons-report-build.service";
import { UtilsService } from "../../services/utils/utils.service";
import * as moment from "moment";
import { AbstractReportFilters } from "./abstract-report-filters";
import { Inject } from "@angular/core";
import { isEmpty } from "lodash-es";
import { DropdownGenericService } from "../patients/form-patients/services/dropdown-generic.service";

export abstract class AbstractReport extends AbstractReportFilters {
	abstract ownData: any[];
	abstract readonly columns: ReportDatatableColumn[];
	readonly pdfColumns: ReportDatatableColumn[];
	readonly sheetColumns: ReportDatatableColumn[];
	abstract sheetFileName: string;

	reportPageOrientation: PdfMakePageOrientation = "portrait";
	reportTableBodyWidths: number[] = [50, 130, 130, 90, 50, 50];

	constructor(
		@Inject(UtilsService) _service: UtilsService,
		@Inject(DropdownGenericService) _dropdownGenericService: DropdownGenericService,
		@Inject(NotificationsService) _notificationsService: NotificationsService,
		protected readonly _globalSpinnerService: GlobalSpinnerService,
		protected readonly _commonsReportService: CommonsReportBuildService,
		protected readonly _pdfGeneratorService: PdfGeneratorService,
		protected readonly _sheetGeneratorService: SheetGeneratorService,
		protected readonly _maskFormatService: MaskFormatService,
		protected readonly _asyncTemplate: AsyncTemplate
	) {
		super(_service, _dropdownGenericService, _notificationsService);
	}

	async fetchOwnData<R>(fn: () => Promise<R>): Promise<R> {
		return await this._asyncTemplate.wrapUserFeedback("Ocorreu um erro ao listar os dados", fn);
	}

	abstract async buildReportHeader();

	print(type: ReportPrintType): void {
		if (!this._printIsAvaliable) {
			this._notificationsService.info("Não há dados a serem exportados.");
			return;
		}
		const loading = this._globalSpinnerService.loadingManager.start();
		try {
			switch (type) {
				case ReportPrintType.SHEETS:
					this._generateSheetsReport();
					break;
				default:
					this._generatePdfReport();
					break;
			}
		} catch (e) {
			this._notificationsService.error("Ocorreu um erro ao emitir o relatório.");
		} finally {
			loading.finished();
		}
	}

	private get _printIsAvaliable(): boolean {
		return this.ownData && this.ownData.length > 0;
	}

	private async _generatePdfReport() {
		const pdfMake = await this._buildReport();
		const pdfMakeDefinition = await this._pdfGeneratorService.exportPDFMakeAsPDF(
			pdfMake,
			"",
			this.sheetFileName + ".pdf"
		);
		pdfMakeDefinition.open();
	}

	protected async _buildReport(): Promise<PDFMake> {
		const pdfMake: PDFMake = { afterContent: [], pageOrientation: this.reportPageOrientation };
		pdfMake.styles = REPORT_STYLES;
		pdfMake.afterContent.push(await this.buildReportHeader());
		pdfMake.afterContent.push(this._buildReportBody());

		return pdfMake;
	}

	private _buildReportBody() {
		const cols = this.pdfColumns || this.columns;
		return {
			table: {
				widths: this.reportTableBodyWidths,
				body: [
					cols.map((column) => {
						return {
							text: this._service.translate.instant(column.translation),
							style: "tableHeader",
						};
					}),
					...this._buildReportRows(),
				],
			},
		};
	}

	private _buildReportRows() {
		return this.ownData.map((receipt) => {
			return (this.pdfColumns || this.columns).map((column) => {
				const value = receipt[column.prop];
				switch (column.type) {
					case Type.CURRENCY:
						return { text: this._currencyValue(value), style: "tableRow", alignment: "right" };
					case Type.DATE:
						return { text: this._dateValue(value), style: "tableRow", alignment: "center" };
					case Type.CELL:
						return { text: this._cellPhoneValue(value), style: "tableRow", alignment: "center" };
					case Type.PHONE:
						return { text: this._phoneValue(value), style: "tableRow", alignment: "center" };
					case Type.CPF_CNPJ:
						return { text: this._cpfCnpjValue(value), style: "tableRow" };
					default:
						return { text: value || " ", style: "tableRow", alignment: column.pdfAlignment };
				}
			});
		});
	}

	private _generateSheetsReport(): void {
		this._sheetGeneratorService.exportJsonAsExcelFile(this._buildRowsToSheet(), this.sheetFileName);
	}

	private _buildRowsToSheet(): unknown[] {
		return this.ownData.map((receipt) => {
			let row = {};
			(this.sheetColumns || this.columns)
				.map((column) => {
					const value = receipt[column.prop];
					const key = this._service.translate.instant(column.translation);
					switch (column.type) {
						case Type.CURRENCY:
							return { [key]: this._currencyValue(value) };
						case Type.DATE:
							return { [key]: this._dateValue(value) };
						case Type.CELL:
							return { [key]: this._cellPhoneValue(value) };
						case Type.PHONE:
							return { [key]: this._phoneValue(value) };
						case Type.CPF_CNPJ:
							return { [key]: this._cpfCnpjValue(value) };
						default:
							return { [key]: value || " " };
					}
				})
				.forEach((r) => (row = { ...row, ...r }));
			return row;
		});
	}

	private _cpfCnpjValue(value): string {
		return !isEmpty(value)
			? value.length === CPF_LENGTH
				? this._maskFormatService.getFormatedCpf(value)
				: value.length === CNPJ_LENGTH
				? this._maskFormatService.getFormatedCnpj(value)
				: value
			: " ";
	}

	private _dateValue(value: string): string {
		return !isEmpty(value) ? value.split(" ")[0] : " ";
	}

	private _currencyValue(value: number): string {
		return value ? value.toFixed(2) : " ";
	}

	private _cellPhoneValue(value: string): string {
		return !isEmpty(value) ? this._maskFormatService.getFormatedCellPhone(value) : " ";
	}

	private _phoneValue(value: string): string {
		return !isEmpty(value) ? this._maskFormatService.getFormatedPhone(value) : " ";
	}

	_getDateFromFilter(date): Date {
		return moment(this._service.parseDateFromDatePicker(date), "DD/MM/YYYY").toDate();
	}
}
