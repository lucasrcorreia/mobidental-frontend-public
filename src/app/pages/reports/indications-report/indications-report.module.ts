import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndicationsReportComponent } from './indications-report.component';
import { IndicationsReportRoutingModule } from './indications-report-routing.module';
import { ReportsModule } from '../reports.module';
import { NgxTranslateModule } from '../../../core/translate/translate.module';
import { FormsModule } from '@angular/forms';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSelectModule } from 'ngx-select-ex';
import { InputMaskModule } from 'racoon-mask-raw';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
	declarations: [
		IndicationsReportComponent,
	],
	imports: [
		CommonModule,
		IndicationsReportRoutingModule,
		ReportsModule,
		NgxTranslateModule,
		FormsModule,
		NgbDatepickerModule,
		NgxSelectModule,
		InputMaskModule,
		NgxChartsModule,
	],
})
export class IndicationsReportModule {}
