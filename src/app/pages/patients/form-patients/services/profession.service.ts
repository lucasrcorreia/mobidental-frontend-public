import { Inject, Injectable } from '@angular/core';
import { API_CONFIGURATION, ApiConfiguration } from '../../../../api/api-configuration';
import { HttpClient } from '@angular/common/http';
import { DropdownGenericModel } from '../form-patients.component';

const PROFESSION = '/profissao/';

@Injectable({
  providedIn: 'root'
})
export class ProfessionService {

  constructor(
    @Inject(API_CONFIGURATION) private readonly _config: ApiConfiguration,
    private readonly _http: HttpClient,
  ) {
  }

  listAll(): Promise<DropdownGenericModel[]> {
    return this._http
      .get<DropdownGenericModel[]>(`${ this._config.apiBasePath + PROFESSION + 'dropdown'}`)
      .toPromise();
  }

  save(profession: DropdownGenericModel): Promise<DropdownGenericModel> {
    return this._http
      .post<DropdownGenericModel>(`${ this._config.apiBasePath + PROFESSION + 'save'}`, profession)
      .toPromise();
  }

  delete(professionId: number): Promise<void> {
    return this._http
      .delete<void>(`${ this._config.apiBasePath + PROFESSION }` + professionId)
      .toPromise();
  }
}
