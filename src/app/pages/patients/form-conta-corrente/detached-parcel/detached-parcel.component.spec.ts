import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetachedParcelComponent } from './detached-parcel.component';

describe('DetachedParcelComponent', () => {
  let component: DetachedParcelComponent;
  let fixture: ComponentFixture<DetachedParcelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetachedParcelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetachedParcelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
