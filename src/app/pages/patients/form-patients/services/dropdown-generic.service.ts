import { Inject, Injectable } from '@angular/core';
import { API_CONFIGURATION, ApiConfiguration } from '../../../../api/api-configuration';
import { HttpClient } from '@angular/common/http';
import { DropdownGenericModel } from '../form-patients.component';

export const SITUATION_PATH = '/situacaoPaciente/';
export const ORGANIZATION_PATH = '/empresaPaciente/';
export const EXPERTISE_PATH = '/especialidadePaciente/';

@Injectable({
	providedIn: 'root',
})
export class DropdownGenericService {

	constructor(
			@Inject(API_CONFIGURATION) private readonly _config: ApiConfiguration,
			private readonly _http: HttpClient,
	) {
	}

	listAll(path: string): Promise<DropdownGenericModel[]> {
		return this._http
				.get<DropdownGenericModel[]>(`${ this._config.apiBasePath + path + 'dropdown' }`)
				.toPromise();
	}

	save(generic: DropdownGenericModel, path: string): Promise<DropdownGenericModel> {
		if (path === SITUATION_PATH) {
			generic.descricao = generic.nome;
			delete generic.nome;
		}
		return this._http
				.post<DropdownGenericModel>(`${ this._config.apiBasePath + path + 'save' }`, generic)
				.toPromise();
	}

	delete(genericId: number, path: string): Promise<void> {
		return this._http
				.delete<void>(`${ this._config.apiBasePath + path }` + genericId)
				.toPromise();
	}
}
