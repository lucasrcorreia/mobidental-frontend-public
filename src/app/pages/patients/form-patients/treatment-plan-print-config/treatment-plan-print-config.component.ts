import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PlanoTratamentoPrintConfig } from '../form-patients.component';

@Component({
	selector: 'app-treatment-plan-print-config',
	templateUrl: './treatment-plan-print-config.component.html',
	styleUrls: ['./treatment-plan-print-config.component.scss'],
})
export class TreatmentPlanPrintConfigComponent implements OnInit {

	printConfigurations: PlanoTratamentoPrintConfig = { onlyTotalValue: false, showImage: false };

	constructor(private readonly _activeModal: NgbActiveModal) {}

	ngOnInit() {
	}

	print() {
		this._activeModal.close(this.printConfigurations);
	}

	close() {
		this._activeModal.close();
	}

}
