import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UtilsService } from '../../../services/utils/utils.service';
import { TreeviewConfig, TreeviewItem } from 'ngx-treeview';
import { CONSTANTS } from '../../../core/constants/constants';
import { CategoryInterface, CenterCostInterface } from '../../../core/models/config/financial/financial.model';
import Swal from 'sweetalert2';
import { TreeviewService } from '../../../services/treeview/treeview.service';
import { GlobalSpinnerService } from '../../../lib/global-spinner.service';
import { take } from 'rxjs/operators';
import { NotificationsService } from 'angular2-notifications';
import { errorToString } from '../../../lib/helpers/error-to-string';

@Component({
  selector: 'app-financial',
  templateUrl: './financial.component.html',
  styleUrls: [
    './financial.component.scss',
    './../../../../assets/icon/icofont/css/icofont.scss',
  ],
})
export class FinancialComponent implements OnInit {
  typeComp: 'centerCost' | 'category' = 'centerCost';
  showModalDetails = false;

  treeview = {
    config: TreeviewConfig.create({
      hasAllCheckBox: false,
      hasFilter: false,
      hasCollapseExpand: false,
      decoupleChildFromParent: false,
      maxHeight: 10e12,
    }),
    items: [],
  };

  objectParentDescription?: string;

  object: Partial<{
    id: number,
    descricao: string,
    status: boolean,
    excluido: boolean,
    centroCustoPaiId: number,
    categoriaPaiId: number,
  }> = {
    id: null,
    descricao: '',
    status: true,
    excluido: false,
    centroCustoPaiId: null,
    categoriaPaiId: null,
  };

  constructor(
    private route: ActivatedRoute,
    public service: UtilsService,
    public treeviewService: TreeviewService,
    private readonly _globalSpinnerService: GlobalSpinnerService,
    private readonly _notificationsService: NotificationsService,
  ) {
  }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.typeComp = data.type;
    });
    this.getDataTreeView();
  }

  async getDataTreeView() {
    const loadingToken = this._globalSpinnerService.loadingManager.start();

    try {
      const data = await this.service
        .httpGET(
          this.typeComp === 'centerCost'
            ? CONSTANTS.ENDPOINTS.config.financial.centerCost
            : CONSTANTS.ENDPOINTS.config.financial.category,
        ).toPromise();

      this.treeview.items = this.treeviewService.createTreeView(
        data.body as any,
      );
    } catch (e) {

      this._notificationsService.error('Erro ao consultar centros de custo', errorToString(e));
    } finally {
      loadingToken.finished();
    }
  }

  openModalDetails() {
    this.showModalDetails = true;

    setTimeout(() => {
      document
        .querySelector('#modal-config-financial')
        .classList.add('md-show');
    }, 500);
  }

  add() {
    this.object = {
      id: null,
      descricao: '',
      status: true,
      centroCustoPaiId: null,
      categoriaPaiId: null,
      excluido: false,
    };
    this.openModalDetails();
  }

  edit(item: TreeviewItem) {
    this.objectParentDescription = item.value.parent && item.value.parent.descricao;
    this.object = {
      id: item.value.id,
      descricao: item.value.descricao,
      status: item.value.status,
      centroCustoPaiId: item.value.centroCustoPaiId,
      categoriaPaiId: item.value.categoriaPaiId,
      excluido: item.value.excluido,
    };
    this.openModalDetails();
  }

  tituloModal() {
    const descricaoPai = this.objectParentDescription;

    const editando = !!this.object && !!this.object.id;
    const filho = descricaoPai != null;

    if (this.typeComp === 'centerCost') {
      return `${editando ? 'Editando' : 'Novo'} Centro de Custo `
        + `${filho ? `filho de "${descricaoPai}"` : ''}`;
    } else {
      return `${editando ? 'Editando' : 'Nova'} Categoria `
        + `${filho ? `filha de "${descricaoPai}"` : ''}`;
    }
  }

  addChildren(item: TreeviewItem) {
    this.objectParentDescription = item.value.descricao;
    this.object = {
      id: null,
      descricao: '',
      status: true,
      centroCustoPaiId: item.value.id,
      categoriaPaiId: item.value.id,
      excluido: false,
    };
    this.openModalDetails();
  }

  toggleActive(item: TreeviewItem) {
    const refSWAL = 'CONFIG.FINANCIAL.SWAL';
    const translate = this.service.translate;

    Swal.fire({
      title: translate.instant(`${refSWAL}.TITLE_TOGGLE`),
      text: translate.instant(
        `${refSWAL}.${
          item.value.status ? 'TEXT_TOGGLE_DEACTIVE' : 'TEXT_TOGGLE_ACTIVE'
        }`,
      ),
      type: 'question',
      showCancelButton: true,
      confirmButtonText: translate.instant(`${refSWAL}.CONFIRM_BUTTON`),
      cancelButtonText: translate.instant(`${refSWAL}.CANCEL_BUTTON`),
    }).then(result => {
      if (result.value) {
        this.processItemTree(item, 'toggle');
      }
    });
  }

  delete(item: TreeviewItem) {
    const refSWAL = 'CONFIG.FINANCIAL.SWAL';
    const translate = this.service.translate;

    Swal.fire({
      title: translate.instant(`${refSWAL}.TITLE_DELETE`),
      text: translate.instant(`${refSWAL}.TEXT_DELETE`),
      type: 'question',
      showCancelButton: true,
      confirmButtonText: translate.instant(`${refSWAL}.CONFIRM_BUTTON`),
      cancelButtonText: translate.instant(`${refSWAL}.CANCEL_BUTTON`),
    }).then(result => {
      if (result.value) {
        this.processItemTree(item, 'delete');
      }
    });
  }

  async processItemTree(item: TreeviewItem, action: string) {
    let ref = '';
    const ccost: Partial<CenterCostInterface> = {};
    const category: Partial<CategoryInterface> = {};

    if (this.typeComp === 'centerCost') {
      ref = CONSTANTS.ENDPOINTS.config.financial.saveCCost;
      delete ccost.centroCustoChilds;
      ccost.id = item.value.id;
      ccost.descricao = item.value.descricao;
      ccost.status =
        action === 'delete' ? item.value.status : !item.value.status;
      ccost.excluido = action === 'delete' ? true : item.value.excluido;
      ccost.centroCustoPaiId = item.value.centroCustoPaiId;
    } else {
      ref = CONSTANTS.ENDPOINTS.config.financial.saveCat;
      delete category.categoriaChilds;
      category.id = item.value.id;
      category.descricao = item.value.descricao;
      category.status =
        action === 'delete' ? item.value.status : !item.value.status;
      category.excluido = action === 'delete' ? true : item.value.excluido;
      category.categoriaPaiId = item.value.categoriaPaiId;
    }

    const loadingToken = this._globalSpinnerService.loadingManager.start();
    try {
      await this.service
        .httpPOST(ref, this.typeComp === 'centerCost' ? ccost : category)
        .pipe(take(1))
        .toPromise();

      this.getDataTreeView();
    } catch (e) {

      this._notificationsService.error(
        `Erro ao ${action === 'delete' ? 'apagar' : 'alternar status'} de item`,
        errorToString(e),
      );
    } finally {
      loadingToken.finished();
    }
  }

}
