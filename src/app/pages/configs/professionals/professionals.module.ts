import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ProfessionalsRoutingModule } from "./professionals-routing.module";
import { ProfessionalsComponent } from "./professionals.component";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { SimpleNotificationsModule } from "angular2-notifications";
import { SharedModule } from "../../../shared/shared.module";
import { NgSelectModule } from "@ng-select/ng-select";
import { NgxTranslateModule } from "../../../core/translate/translate.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DirectivesModule } from "../../../core/directive/directives.module";
import { FormProfessionalComponent } from "./form-professional/form-professional.component";
import { NgxMaskModule } from "ngx-mask";
import { InputMaskModule } from "racoon-mask-raw";
import { IMaskModule } from "angular-imask";
import { UiSwitchModule } from "ngx-ui-switch";
import { NgbAccordionModule } from "@ng-bootstrap/ng-bootstrap";
import { AppLibModule } from "../../../lib/app-lib.module";
import { ComissionProcedureModalComponent } from "./form-professional/comission-procedure/comission-procedure-modal.component";
import {NgxSelectModule} from 'ngx-select-ex';
import {NgxCurrencyModule} from 'ngx-currency';

@NgModule({
	imports: [
		CommonModule,
		NgxDatatableModule,
		SimpleNotificationsModule,
		SharedModule,
		NgSelectModule,
		NgbAccordionModule,
		NgxTranslateModule,
		NgxMaskModule.forRoot(),
		InputMaskModule,
		FormsModule,
		ReactiveFormsModule,
		DirectivesModule,
		ProfessionalsRoutingModule,
		IMaskModule,
		UiSwitchModule,
		NgxSelectModule,
		AppLibModule,
		NgxCurrencyModule,
	],
	declarations: [
		ProfessionalsComponent,
		FormProfessionalComponent,
		ComissionProcedureModalComponent,
	],
	entryComponents: [ComissionProcedureModalComponent],
})
export class ProfessionalsModule {}
