import {
	ChangeDetectorRef,
	Component,
	ElementRef,
	OnDestroy,
	OnInit,
	ViewChild,
} from "@angular/core";
import { animate, AUTO_STYLE, state, style, transition, trigger } from "@angular/animations";
import { ActivatedRoute } from "@angular/router";
import { UtilsService } from "../../../services/utils/utils.service";
import { CONSTANTS } from "../../../core/constants/constants";
import {
	ContentTreatmentModel,
	ParcelasModel,
	PatientModel,
	ProceduresModel,
	TreatmentModelTable,
} from "../../../core/models/patients/patients.model";
import { FormGroup } from "@angular/forms";
import { FormPatient } from "../../../core/forms/patient";
import { CustomDatepickerI18n, I18n } from "../../../shared/ngb-datepicker/datepicker-i18n";
import {
	NgbDateParserFormatter,
	NgbDatepickerI18n,
	NgbModal,
	NgbTabChangeEvent,
} from "@ng-bootstrap/ng-bootstrap";
import { NgbDateFRParserFormatter } from "../../../shared/ngb-datepicker/ngb-date-fr-parser-formatter";
import { UF_LIST } from "../../../core/constants/uf/uf";
import { ConvenantModel } from "../../../core/models/patients/convenio.model";
import { ColumnsTableModel } from "../../../core/models/table/columns.model";
import {
	SelectItem,
	ShortListItem,
	ShortListItemDesc,
} from "../../../core/models/forms/common/common.model";
import { BehaviorSubject, forkJoin, Subject } from "rxjs";
import Swal from "sweetalert2";
import { LISTAS } from "../lists";
import { LOCAL_CONSTS } from "../constants";
import * as moment from "moment";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { takeUntil } from "rxjs/operators";
import { ImageCroppedEvent } from "ngx-image-cropper";
import { WebcamImage, WebcamInitError, WebcamUtil } from "ngx-webcam";
import { Observable } from "rxjs/Observable";
import { DomSanitizer } from "@angular/platform-browser";
import { ProfessionService } from "./services/profession.service";
import { NotificationsService } from "angular2-notifications";
import {
	PdfGeneratorService,
	PDFMake,
} from "../../../services/pdf-generator/pdf-generator.service";
import {
	CommonsReportBuildService,
	REPORT_IMAGE_HEADER_SIZE,
} from "../../../services/report/commons-report-build.service";
import { PrintConfigurationsService } from "../../configs/medical-record/print/service/print-configurations.service";
import { classToPlain } from "class-transformer";
import { MaskFormatService } from "../../../services/utils/mask-format.service";
import { TreatmentPlanPrintConfigComponent } from "./treatment-plan-print-config/treatment-plan-print-config.component";
import { ContaApiService, ContaModel } from "../../../api/conta-api.service";
import { toothsList } from "../tooths";
import { toDataURL } from "app/services/utils/file-utils.service";
import {
	DropdownGenericService,
	EXPERTISE_PATH,
	ORGANIZATION_PATH,
	SITUATION_PATH,
} from "./services/dropdown-generic.service";
import { SIM_NAO_TRANSLATE } from "../anamnese/models/SimNaoResposta";
import { FULL_CELLPHONE_LENGTH } from "../../agenda/agenda.component";
import { LoginService } from "../../../auth/login.service";
import { UserPermissionService } from "../../../auth/user-permission.service";
import { PATIENT_PERMISSIONS } from "../patients.component";
import { TableColumnsTreatment } from "../form-treatment/tables";
import { CommonPrintService } from "../emit-documents/service/common-print.service";
import * as htmlToPdfmake from "html-to-pdfmake";

declare let $: any;

export interface PlanoTratamentoPrintConfig {
	showImage?: boolean;
	onlyTotalValue?: boolean;
}

export interface DropdownGenericModel {
	id?: number;
	nome?: string;
	descricao?: string;
}

@Component({
	selector: "app-form-patients",
	templateUrl: "./form-patients.component.html",
	styleUrls: [
		"./form-patients.component.scss",
		"./../../../../assets/icon/icofont/css/icofont.scss",
	],
	animations: [
		trigger("fadeInOutTranslate", [
			transition(":enter", [
				style({ opacity: 0 }),
				animate("400ms ease-in-out", style({ opacity: 1 })),
			]),
			transition(":leave", [
				style({ transform: "translate(0)" }),
				animate("400ms ease-in-out", style({ opacity: 0 })),
			]),
		]),
		trigger("notificationBottom", [
			state(
				"an-off, void",
				style({
					overflow: "hidden",
					height: "0px",
				})
			),
			state(
				"an-animate",
				style({
					overflow: "hidden",
					height: AUTO_STYLE,
				})
			),
			transition("an-off <=> an-animate", [animate("400ms ease-in-out")]),
		]),
	],
	providers: [
		I18n,
		{ provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
		{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
	],
})
export class FormPatientsComponent implements OnInit, OnDestroy {
	@ViewChild(DatatableComponent) table: DatatableComponent;
	@ViewChild("selectFoto") selectFoto: ElementRef;

	cardTitle = "";
	patient: PatientModel;
	formPatient: FormGroup;
	marital = [];
	indications: SelectItem[] = [];
	minDate = { year: 1930, month: 1, day: 1 };
	ufList = UF_LIST;
	convenios: ConvenantModel[] = [];
	profissionais: ShortListItem[] = [];
	isEdit = "nope";
	recordsDropdown: ShortListItemDesc[] = [];
	imageChangedEvent;
	showCropper = false;
	currentFile = null;
	fileChoise: any;
	showTreatmentResumeModal = false;
	treatmentModal: any;
	selectedResumeTreatment: any;
	modalTreatmentsTable: any;
	columnsTables = new TableColumnsTreatment(this.service.translate);
	contractHtml: string;
	finishedModalOpening = false;

	showCam = false;
	cropperCam = false;
	private trigger: Subject<void> = new Subject<void>();
	private nextWebcam: Subject<boolean | string> = new Subject<boolean | string>();
	public errors: WebcamInitError[] = [];
	public webcamImage: WebcamImage = null;
	public multipleWebcamsAvailable = false;
	public allowCameraSwitch = true;
	public deviceId: string;
	public videoOptions: MediaTrackConstraints = {
		// width: {ideal: 1024},
		// height: {ideal: 576}
	};

	listaStatusTratamento: SelectItem[] = this.service.translate.instant(
		"PATIENTS.FORM.listaStatusTratamento"
	);
	treatmentSearch = {
		pacienteId: null,
		page: 0,
		size: 50,
		sorting: {
			undefined: "asc",
		},
	};

	rowsPageTreatment = [];
	rowsTreatment = null;
	pagesTreatment = {
		pageNumber: 0,
		size: 100,
		totalElements: 0,
		totalPages: 0,
	};

	printTreatmentPlanParam = {
		id: null,
	};

	private destroy$ = new Subject();
	LISTAS_LOCAIS = new LISTAS(this.service.translate);
	AGUARDANDO_APROVACAO = LOCAL_CONSTS.AGUARDANDO_APROVACAO;
	locale = this.service.translate.instant("COMMON.LOCALE");
	treatmentStep = "table";
	selectedTreatment: ContentTreatmentModel = new ContentTreatmentModel();

	columnsTreatment: ColumnsTableModel[] = [
		{
			action: true,
			name: this.service.translate.instant("PATIENTS.FORM.TABLE_TREATMENT.COL1"),
			prop: "id",
			sortable: false,
			width: 90,
		},
		{
			action: false,
			name: this.service.translate.instant("PATIENTS.FORM.TABLE_TREATMENT.COL2"),
			prop: "posicaoTratamento",
			sortable: false,
			width: 35,
		},
		{
			action: false,
			name: this.service.translate.instant("PATIENTS.FORM.TABLE_TREATMENT.COL3"),
			prop: "descricao",
			sortable: false,
			width: 280,
		},
		{
			action: false,
			name: this.service.translate.instant("PATIENTS.FORM.TABLE_TREATMENT.COL4"),
			prop: "valorComDesconto",
			sortable: true,
			width: 40,
			currency: true,
		},
		{
			action: false,
			name: this.service.translate.instant("PATIENTS.FORM.TABLE_TREATMENT.COL5"),
			prop: "dataAbertura",
			sortable: true,
			width: 60,
		},
		{
			action: false,
			name: this.service.translate.instant("PATIENTS.FORM.TABLE_TREATMENT.COL6"),
			prop: "statusTratamento",
			sortable: true,
			width: 120,
		},
		{
			action: false,
			name: this.service.translate.instant("PATIENTS.FORM.TABLE_TREATMENT.COL7"),
			prop: "convenioNome",
			sortable: false,
			width: 90,
		},
	];

	readonly listFace = [
		{
			label: this.service.translate.instant("PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceDistal"),
			porp: "isFaceDistal",
		},
		{
			label: this.service.translate.instant(
				"PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceOclusalIncisal"
			),
			porp: "isFaceOclusalIncisal",
		},
		{
			label: this.service.translate.instant(
				"PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceLingualPalatina"
			),
			porp: "isFaceLingualPalatina",
		},
		{
			label: this.service.translate.instant("PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceMesial"),
			porp: "isFaceMesial",
		},
		{
			label: this.service.translate.instant("PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceVestibular"),
			porp: "isFaceVestibular",
		},
	];

	readonly searchProfession = new BehaviorSubject<undefined | string>(undefined);
	readonly searchExpertise = new BehaviorSubject<undefined | string>(undefined);
	readonly searchOrganization = new BehaviorSubject<undefined | string>(undefined);
	readonly searchSituation = new BehaviorSubject<undefined | string>(undefined);
	professions: DropdownGenericModel[];
	expertises: DropdownGenericModel[];
	especialidadesPaciente: DropdownGenericModel[];
	situations: DropdownGenericModel[];
	organizations: DropdownGenericModel[];

	readonly EXPERTISE_KEY = "especialidadesPaciente";
	readonly ORGANIZATION_KEY = "empresaId";
	readonly SITUATION_KEY = "situacaoId";
	readonly SIM_NAO_TRANSLATE = SIM_NAO_TRANSLATE;

	answerAlertNotification: string;
	answerAlertNotificationClass: string;

	canEditCodigoProntuario = false;
	userCanSave = true;

	constructor(
		private route: ActivatedRoute,
		public service: UtilsService,
		private cd: ChangeDetectorRef,
		private readonly _sanitizer: DomSanitizer,
		private readonly _professionService: ProfessionService,
		private readonly _dropdownGenericService: DropdownGenericService,
		private readonly _notificationsService: NotificationsService,
		private readonly _pdfGeneratorService: PdfGeneratorService,
		private readonly _commonsReportService: CommonsReportBuildService,
		private readonly _maskFormatService: MaskFormatService,
		private readonly _printConfigurationsService: PrintConfigurationsService,
		private readonly _userPermissionService: UserPermissionService,
		private readonly _modalService: NgbModal,
		private readonly _loginService: LoginService,
		private readonly _contaApiService: ContaApiService,
		private readonly _commonPrintService: CommonPrintService
	) {
		this.answerAlertNotification = "an-off";

		this.marital = this.service.translate.instant("PATIENTS.FORM.MARITAL");
		this.indications = this.service.translate.instant("PATIENTS.FORM.INDICATIONS");
		this.route.params.subscribe((params) => {
			if (params.id !== "-1") {
				this.isEdit = "yes";
				this.getPatient(params.id);
				this.treatmentSearch.pacienteId = params.id;
			} else {
				this._resolveCanEditCodigoProntuario();
				this.isEdit = "nope";
				this.cardTitle = this.service.translate.instant("PATIENTS.FORM.NEW");
				this.formPatient = this.service.createForm(FormPatient, new PatientModel());
				this._userCanSave();
			}
		});

		this.service.toggleMenu.pipe(takeUntil(this.destroy$)).subscribe((data) => {
			if (data) {
				this.updateDataTable();
			}
		});

		this._initProfessionDropdown();
		this._initOrganizationsDropdown();
		this._initSituationsDropdown();
		this._initExpertisesDropdown();
	}

	buildImageCssUrl(): string {
		let url: string = this.currentFile;

		if (!url && this.patient) {
			url = this.patient.urlImagemPerfil;
		}

		if (!url) {
			url =
				this.patient && this.patient.pessoaSexo === "FEMININO"
					? "assets/images/user-profile/girl.svg"
					: "assets/images/user-profile/man.svg";
		}

		return `url(${url})`;
	}

	ngOnInit() {
		this.initializeData();
		document.addEventListener("keydown", ({ key }) => {
			if (key === "Escape") {
				this.closeTreatmentResumeModal();
			}
		});
	}

	ngOnDestroy() {
		this.destroy$.next();
		this.destroy$.complete();
	}

	updateDataTable() {
		this.cd.markForCheck();
		setTimeout(() => {
			this.table.recalculate();
			(this.table as any).cd.markForCheck();
		});
	}

	initializeData() {
		this.service.loading(true);
		const convenios = this.service.httpGET(CONSTANTS.ENDPOINTS.patients.treatment.convenant);
		const profissionais = this.service.httpGET(CONSTANTS.ENDPOINTS.professional.active);
		forkJoin([convenios, profissionais]).subscribe(
			(data) => {
				this.convenios = data[0].body as ConvenantModel[];
				this.profissionais = data[1].body as ShortListItem[];
				this.service.loading(false);
			},
			(err) => {
				this.service.loading(false);
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			}
		);
	}

	getPatient(id: string) {
		this.service.loading(true);
		this.service.httpGET(CONSTANTS.ENDPOINTS.patients.general.findOne + id).subscribe(
			(data) => {
				this.patient = data.body as PatientModel;
				this._resolveCanEditCodigoProntuario();
				this.service.loading(false);
				this.patient.pessoaDataNascimento = this.service.parseDatePicker(
					this.patient.pessoaDataNascimento
				);
				this.patient.pessoaDataNascimentoResponsavel = this.service.parseDatePicker(
					this.patient.pessoaDataNascimentoResponsavel
				);
				this.patient.vencimentoCarterinha = this.service.parseDatePicker(
					this.patient.vencimentoCarterinha
				);

				if (this.patient.urlImagemPerfil) {
					this.patient.urlImagemPerfil = this.patient.urlImagemPerfil + "?decache=" + Math.random();
				}

				this.formPatient = this.service.createForm(FormPatient, this.patient);
				this._userCanSave();
				this.especialidadesPaciente = this.patient.especialidadesPaciente;
			},
			(err) => {
				this.service.loading(false);
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			}
		);
	}

	getCep() {
		const cep = this.formPatient.get("pessoaCep").value as string;
		if (cep.length === 8) {
			this.service.loading(true);
			this.service.httpGET(`${CONSTANTS.ENDPOINTS.zipCode}?cep=${cep}`).subscribe(
				(data) => {
					if (data.body) {
						this.formPatient.patchValue({
							pessoaEndereco: (data.body as any).logradouro,
						});
						this.formPatient.patchValue({
							pessoaBairro: (data.body as any).bairro,
						});
						this.formPatient.patchValue({
							pessoaCidade: (data.body as any).cidade,
						});
						this.formPatient.patchValue({
							pessoaUfCidade: (data.body as any).estado,
						});
					} else {
						this.service.notification.info(
							this.service.translate.instant("PATIENTS.FORM.CEP_INEXISTENTE")
						);
					}
					this.service.loading(false);
				},
				(err) => {
					this.service.loading(false);
				}
			);
		}
	}

	getTreatment() {
		this.service.loading(true);
		this.service
			.httpPOST(CONSTANTS.ENDPOINTS.patients.treatment.treatment, this.treatmentSearch)
			.subscribe(
				(data) => {
					const result = data.body as TreatmentModelTable;
					if (this.pagesTreatment.pageNumber === 0) {
						this.pagesTreatment.totalElements = result.totalElements;
						this.pagesTreatment.totalPages = result.totalPages;
					}

					this.rowsPageTreatment = result.content;
					if (!this.rowsTreatment) {
						this.rowsTreatment = {};
					}
					this.rowsTreatment[this.pagesTreatment.pageNumber] = result.content;
					this.service.loading(false);

					this.rowsPageTreatment.sort((a, b) => {
						if (
							moment(a.dataAbertura, "DD/MM/YYYY").isBefore(moment(b.dataAbertura, "DD/MM/YYYY"))
						) {
							return 1;
						} else if (
							moment(a.dataAbertura, "DD/MM/YYYY").isAfter(moment(b.dataAbertura, "DD/MM/YYYY"))
						) {
							return -1;
						} else {
							return 0;
						}
					});

					this.rowsPageTreatment.map((row, index) => {
						row.posicaoTratamento = `${this.rowsPageTreatment.length - index}/${
							this.rowsPageTreatment.length
						}`;
					});
				},
				(err) => {
					this.service.loading(false);
					this.service.notification.error(
						this.service.translate.instant("COMMON.ERROR.SEARCH"),
						err.error.error
					);
				}
			);
	}

	savePatient() {
		this.service.loading(true);
		const patient = this.formPatient.value as PatientModel;
		patient.especialidadesPaciente = this.especialidadesPaciente;
		delete patient.bills;
		delete patient.treatment;

		if (patient.pessoaDataNascimento && Number.isNaN(patient.pessoaDataNascimento.year)) {
			patient.pessoaDataNascimento = null;
		} else if (patient.pessoaDataNascimento && patient.pessoaDataNascimento.year) {
			patient.pessoaDataNascimento = this.service.parseDateFromDatePicker(
				patient.pessoaDataNascimento
			);
		}

		if (
			patient.pessoaDataNascimentoResponsavel &&
			Number.isNaN(patient.pessoaDataNascimentoResponsavel.year)
		) {
			patient.pessoaDataNascimentoResponsavel = null;
		} else if (
			patient.pessoaDataNascimentoResponsavel &&
			patient.pessoaDataNascimentoResponsavel.year
		) {
			patient.pessoaDataNascimentoResponsavel = this.service.parseDateFromDatePicker(
				patient.pessoaDataNascimentoResponsavel
			);
		}

		if (patient.vencimentoCarterinha && Number.isNaN(patient.vencimentoCarterinha.year)) {
			patient.vencimentoCarterinha = null;
		} else if (patient.vencimentoCarterinha && patient.vencimentoCarterinha.year) {
			patient.vencimentoCarterinha = this.service.parseDateFromDatePicker(
				patient.vencimentoCarterinha
			);
		}

		if (patient.urlImagemPerfil) {
			const posDecache = patient.urlImagemPerfil.indexOf("?decache");
			if (posDecache > -1) {
				patient.urlImagemPerfil = patient.urlImagemPerfil.substr(0, posDecache);
			}
		}

		this.service.httpPOST(CONSTANTS.ENDPOINTS.patients.general.save, patient).subscribe(
			(data) => {
				if (this.currentFile) {
					this.patient = data.body as PatientModel;
					this._resolveCanEditCodigoProntuario();
					this.imageSave();
				} else {
					this.service.back();
				}
			},
			(err) => {
				this.service.loading(false);
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			}
		);
	}

	setPageTreatment(pageInfo) {
		this.pagesTreatment.pageNumber = pageInfo.offset;
		if (this.rowsTreatment[this.pagesTreatment.pageNumber]) {
			this.rowsPageTreatment = this.rowsTreatment[this.pagesTreatment.pageNumber];
		} else {
			this.getTreatment();
		}
	}

	changeTabEvent(event: NgbTabChangeEvent) {
		if (event.nextId === "treatment") {
			this.closeTreatment("close");
			if (!this.rowsTreatment) {
				this.getTreatment();
			}
		}
	}

	getStatus(status) {
		return this.listaStatusTratamento.find((item) => item.value === status).label;
	}

	formTreatment(treatment: ContentTreatmentModel) {
		if (!treatment) {
			const tempTreatment = this.service.getStorage("treatment") as ContentTreatmentModel;
			if (tempTreatment && tempTreatment.pacienteId === this.patient.id) {
				const translate = this.service.translate;
				const refSWAL = "PATIENTS.FORM.SWAL";

				Swal.fire({
					title: translate.instant(`${refSWAL}.TITLE`),
					text: translate.instant(`${refSWAL}.TEXT`),
					type: "question",
					showCancelButton: true,
					confirmButtonText: translate.instant(`${refSWAL}.CONFIRM_BUTTON`),
					cancelButtonText: translate.instant(`${refSWAL}.CANCEL_BUTTON`),
				}).then((result) => {
					if (result.value) {
						this.selectedTreatment = tempTreatment;
						this.isEdit = "partial";
						this.treatmentStep = "form";
					} else {
						this.selectedTreatment = new ContentTreatmentModel();
						this.selectedTreatment.pacienteId = this.patient.id;
						this.isEdit = "nope";
						this.treatmentStep = "form";
					}
				});
			} else {
				this.selectedTreatment = new ContentTreatmentModel();
				this.selectedTreatment.pacienteId = this.patient.id;
				this.isEdit = "nope";
				this.treatmentStep = "form";
			}
		} else {
			this.selectedTreatment = treatment;
			this.selectedTreatment.pacienteId = this.patient.id;
			this.isEdit = "yes";
			this.treatmentStep = "form";
		}
	}

	closeTreatment(event) {
		if (event === "close") {
			this.treatmentStep = "table";
			this.getTreatment();
		} else if (event === "underEdit") {
			this.isEdit = "partial";
		}
	}

	getColorStatus(status: string) {
		return this.LISTAS_LOCAIS.listaStatusTratamento.find((item) => item.value === status).color;
	}

	deleteTreatment(id: number) {
		const refSWAL = "PATIENTS.FORM_TREATMENT.SWAL";
		const translate = this.service.translate;

		Swal.fire({
			title: translate.instant(`${refSWAL}.TITLE`),
			text: translate.instant(`${refSWAL}.TEXT_DELETE_TREATMENT`),
			type: "question",
			showCancelButton: true,
			confirmButtonText: translate.instant(`${refSWAL}.CONFIRM_BUTTON`),
			cancelButtonText: translate.instant(`${refSWAL}.CANCEL_BUTTON`),
		}).then((result) => {
			if (result.value) {
				this.service.loading(true);
				this.service
					.httpDELETE(CONSTANTS.ENDPOINTS.patients.treatment.removeTreatment + id)
					.subscribe(
						(data) => {
							this.getTreatment();
						},
						(err) => {
							this.service.loading(false);
							this.service.notification.error(
								this.service.translate.instant("COMMON.ERROR.SEARCH"),
								err.error.error
							);
						}
					);
			}
		});
	}

	async treatmentPrintOptions(id: number) {
		const modalRef = this._modalService.open(TreatmentPlanPrintConfigComponent, {
			size: "lg",
			centered: true,
		});
		const planoTratamentoPrintConfig: PlanoTratamentoPrintConfig = await modalRef.result;

		if (planoTratamentoPrintConfig) {
			this.printTreatment(id, planoTratamentoPrintConfig);
		}
	}

	async printTreatment(id: number, planoTratamentoPrintConfig: PlanoTratamentoPrintConfig) {
		this.service.loading(true);
		try {
			const { body } = await this.service
				.httpGET(CONSTANTS.ENDPOINTS.patients.treatment.getTreatment + id)
				.toPromise();
			const treatment = body as ContentTreatmentModel;
			const contaModel = this._printConfigurationsService.getContaModel();
			const pdfMake = await FormPatientsComponent.buildReport(
				treatment,
				planoTratamentoPrintConfig,
				this._commonsReportService,
				this._maskFormatService,
				contaModel,
				this._getNecessaryTranslations(),
				this.listFace
			);
			const pdfMakeDefinition = await this._pdfGeneratorService.exportPDFMakeAsPDF(
				pdfMake,
				"",
				`PlanoDeTratamento-${treatment.pacientePessoaNome}.pdf`
			);
			pdfMakeDefinition.open();
		} catch (e) {
			this._notificationsService.error("Ocorreu um erro ao imprimir o Plano de Tratamento");
		} finally {
			this.service.loading(false);
		}
	}

	private _getNecessaryTranslations(): SelectItem[] {
		const {
			listaFormaPagamento,
			listaFormaRecebimento,
			listaStatusTratamentoServico,
		} = this.LISTAS_LOCAIS;
		return [
			...listaStatusTratamentoServico,
			...listaFormaRecebimento,
			...listaFormaPagamento,
			...toothsList,
		];
	}

	static async buildReport(
		treatment: ContentTreatmentModel,
		planoTratamentoPrintConfig: PlanoTratamentoPrintConfig,
		commonsReportBuildService: CommonsReportBuildService,
		maskFormatService: MaskFormatService,
		contaModel: ContaModel,
		traslations: SelectItem[],
		listFace: any[]
	): Promise<PDFMake> {
		const pdfMake: PDFMake = { afterContent: [], pageMargins: [20, 20, 20, 95] };
		const telefone = maskFormatService.getFormatedPhone(contaModel.telefone);
		pdfMake.defaultStyle = {
			fontSize: 10,
		};
		pdfMake.afterContent.push(
			await FormPatientsComponent._buildHeader(treatment, commonsReportBuildService, telefone)
		);
		pdfMake.afterContent.push(await FormPatientsComponent._buildImage(planoTratamentoPrintConfig));
		pdfMake.afterContent.push(
			FormPatientsComponent._buildBody(treatment, planoTratamentoPrintConfig, traslations, listFace)
		);
		pdfMake.afterContent.push(FormPatientsComponent._buildResume(treatment, traslations));
		pdfMake.afterContent.push(
			FormPatientsComponent._buildFooter(treatment, maskFormatService, contaModel)
		);

		return pdfMake;
	}

	private static async _buildHeader(
		treatment: ContentTreatmentModel,
		commonsReportBuildService: CommonsReportBuildService,
		telefone: string
	) {
		const imageForHeader = await commonsReportBuildService.getImageForHeader();
		return [
			{
				columns: [
					{
						width: REPORT_IMAGE_HEADER_SIZE,
						image: imageForHeader,
						fit: [REPORT_IMAGE_HEADER_SIZE, REPORT_IMAGE_HEADER_SIZE],
					},
					{
						width: "*",
						stack: [
							{
								text: [{ text: "Paciente: ", bold: true }, treatment.pacientePessoaNome],
							},
							{
								text: [{ text: "Convênio: ", bold: true }, treatment.convenioNome],
							},
							{
								text: [{ text: "Telefone Clínica: ", bold: true }, telefone],
							},
						],
						margin: [30, 20, 0, 0],
					},
					{
						width: 120,
						text: moment().format("L"),
						alignment: "right",
					},
				],
			},
			{
				text: "Plano de Tratamento",
				alignment: "center",
				bold: true,
				fontSize: 14,
				margin: [0, 10, 0, 0],
			},
		];
	}

	private static async _buildImage(planoTratamentoPrintConfig: PlanoTratamentoPrintConfig) {
		if (planoTratamentoPrintConfig.showImage) {
			const base64 = await toDataURL("assets/images/treatment-plan/odontograma.jpg");
			return {
				width: 550,
				image: base64,
				alignment: "center",
			};
		}

		return {};
	}

	private static _buildBody(
		treatment: ContentTreatmentModel,
		planoTratamentoPrintConfig: PlanoTratamentoPrintConfig,
		traslations: SelectItem[],
		listFace: any[],
		onlyApproved: boolean = true
	) {
		const widthsWithoutValue = [400, 150];
		const widthsWithValue = [310, 130, 100];
		const headersWithoutValue = [
			{ text: `Procedimento${onlyApproved ? "" : " Não Aprovado"}`, bold: true },
			/*{ text: 'Situação', alignment: 'center', bold: true },*/
			{ text: "Dente/Região", alignment: "center", bold: true },
		];
		const headersWithValue = [
			...headersWithoutValue,
			{ text: "Valor", alignment: "right", bold: true },
		];
		return [
			{
				table: {
					widths: planoTratamentoPrintConfig.onlyTotalValue ? widthsWithoutValue : widthsWithValue,
					body: [
						planoTratamentoPrintConfig.onlyTotalValue ? headersWithoutValue : headersWithValue,
						...this._buildProcedureRows(
							treatment.servicos,
							planoTratamentoPrintConfig,
							traslations,
							listFace,
							onlyApproved
						),
					],
				},
				margin: [0, 20, 0, 0],
				layout: "noBorders",
			},
		];
	}

	private static _buildProcedureRows(
		procedures: ProceduresModel[],
		planoTratamentoPrintConfig: PlanoTratamentoPrintConfig,
		traslations: SelectItem[],
		listFace: any[],
		onlyApproved: boolean
	) {
		return procedures
			.filter((procedure) =>
				onlyApproved ? procedure.statusTratamentoServico !== "NAO_AUTORIZADO" : true
			)
			.map((procedure) =>
				this._singleProcedureRow(
					procedure,
					traslations,
					planoTratamentoPrintConfig.onlyTotalValue,
					listFace
				)
			);
	}

	private static _singleProcedureRow(
		procedure: any,
		traslations: SelectItem[],
		onlyTotalValue: boolean,
		listFace: any[]
	) {
		const row = [
			{
				text:
					procedure.convenioEspecialidadeProcedimentoNome ||
					procedure.convenioEspecialidadeProcedimentoProcedimentoNome +
						this._buildFacesDescription(procedure, listFace),
			},
			/*{ text: this._translate(procedure.statusTratamentoServico, traslations), alignment: 'center' },*/
			{ text: this._translate(procedure.emDente.toString(), traslations), alignment: "center" },
		];
		return onlyTotalValue
			? row
			: [...row, { text: "R$ " + procedure.valor.toFixed(2), alignment: "right" }];
	}

	private static _buildFacesDescription(procedure: any, listFace: any[]) {
		const {
			isFaceDistal,
			isFaceLingualPalatina,
			isFaceMesial,
			isFaceOclusalIncisal,
			isFaceVestibular,
		} = procedure;
		if (
			isFaceDistal ||
			isFaceLingualPalatina ||
			isFaceMesial ||
			isFaceOclusalIncisal ||
			isFaceVestibular
		) {
			const faces = [
				{ label: "isFaceDistal", value: isFaceDistal },
				{ label: "isFaceLingualPalatina", value: isFaceLingualPalatina },
				{ label: "isFaceMesial", value: isFaceMesial },
				{ label: "isFaceOclusalIncisal", value: isFaceOclusalIncisal },
				{ label: "isFaceVestibular", value: isFaceVestibular },
			];
			return (
				" (" +
				faces
					.filter((face) => face.value)
					.map(
						(face, index) =>
							(index === 0 ? "" : " ") + listFace.find((item) => item.porp === face.label).label
					) +
				")"
			);
		}
		return "";
	}

	private static _buildResume(
		treatment: ContentTreatmentModel,
		traslations: SelectItem[],
		alignment: string = "right"
	) {
		return [
			{
				columns: [
					{
						width: alignment == "right" ? 500 : 80,
						alignment: alignment,
						stack: [
							{ text: "Sub Total (R$):", bold: true },
							{ text: "Desconto (R$):", bold: true },
							{ text: "Total (R$):", bold: true },
						],
					},
					{
						alignment: alignment,
						stack: [
							{ text: treatment.valor.toFixed(2) },
							{ text: treatment.valorDesconto.toFixed(2) },
							{ text: treatment.valorComDesconto.toFixed(2) },
						],
					},
				],
				margin: [0, 30, 0, 0],
			},
			{
				text: [
					{ text: "Condições de Pagamento: ", bold: true },
					this._translate(treatment.formaPagamento, traslations) +
						", conforme detalhamento abaixo:",
				],
				margin: [0, 30, 0, 10],
			},
			this._buildParcels(treatment.parcelas, traslations),
			{
				text:
					"O tratamento odontológico é fundamental para se ter e manter uma saúde geral. Parabéns por cuidar de sua saúde." +
					" Você merece esse carinho!",
				alignment: "center",
				fontSize: 10,
				margin: [0, 20, 0, 0],
			},
		];
	}

	private static _translate(value: string, translates: SelectItem[]) {
		const found = translates.find((item) => item.value === value);
		return found ? found.label : value;
	}

	private static _buildParcels(parcels: ParcelasModel[], traslations: SelectItem[]) {
		const left = parcels.filter((parcel, index) => index % 2 == 0);
		const right = parcels.filter((parcel, index) => index % 2 != 0);
		return {
			columns: [
				{
					type: "none",
					ol: [this._buildParcelRows(left, traslations)],
				},
				{
					type: "none",
					ol: [this._buildParcelRows(right, traslations)],
				},
			],
		};
	}

	private static _buildParcelRows(parcels: ParcelasModel[], traslations: SelectItem[]) {
		return parcels.map((parcel) => {
			return {
				text: `${parcel.numeroParcela} em ${parcel.dataVencimento} ${this._translate(
					parcel.formaDeRecebimento,
					traslations
				)} R$ ${parcel.valor.toFixed(2)}`,
			};
		});
	}

	private static _buildFooter(
		treatment: ContentTreatmentModel,
		maskFormatService: MaskFormatService,
		contaModel: ContaModel,
		isContract: boolean = false
	) {
		return {
			columns: [
				this._buildFooterColumn([
					`Ciente e de acordo com o ${isContract ? "contrato" : "tratamento"} proposto,`,
					treatment.pacientePessoaNome,
					maskFormatService.getFormatedCpf(treatment.pacientePessoaCpfCnpj),
				]),
				this._buildFooterColumn([" ", contaModel.razaoSocial, " "]),
			],
			margin: [0, 40, 0, 0],
		};
	}

	private static _buildFooterColumn(labels: string[]) {
		return {
			stack: [
				{ text: labels[0], margin: [0, 0, 0, 30] },
				{ canvas: [{ type: "line", x1: 0, y1: -1, x2: 180, y2: -1, lineWidth: 0.5 }] },
				{ text: labels[1], margin: [0, 5, 0, 0] },
				{ text: labels[2], margin: [0, 5, 0, 0] },
			],
			alignment: "center",
			margin: [0, 0, 0, 10],
		};
	}

	navigateClients() {
		this.service.navigate(CONSTANTS.ROUTERS.PATIENTS.LIST, null);
	}

	changeListenerArquivos(event) {
		this.imageChangedEvent = event;
		this.showCropper = true;

		setTimeout(() => {
			document.querySelector("#choiseImage").classList.add("md-show");
		}, 500);
	}

	imageCropped(event: ImageCroppedEvent) {
		this.currentFile = event.base64;
		this.fileChoise = event.file;
	}

	imageLoaded() {
		this.showCropper = true;
	}

	applyImage(type = "galery") {
		if (type === "galery") {
			this.service.closeModal("choiseImage", null);
		} else {
			this.triggerSnapshot();
			this.closeWebCamModal();
		}
	}

	public triggerSnapshot(): void {
		this.trigger.next();
	}

	closeWebCamModal() {
		this.service.closeModal("webCamModal", null);
		this.showCam = false;
	}

	imageSave() {
		this.service.loading(true);

		const formData: FormData = new FormData();
		formData.append("file", this.fileChoise, "tempFile.png");
		formData.append("paciente", `{"id": "${this.patient.id}"}`);

		this.service.httpPOST(CONSTANTS.ENDPOINTS.patients.general.perfilImage, formData).subscribe(
			(data) => {
				this.service.loading(false);
				this.service.back();
			},
			(err) => {
				this.service.loading(false);
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			}
		);
	}

	loadImageFailed(ev) {}

	getFromGalery() {
		this.selectFoto.nativeElement.click();
	}

	getFromCam() {
		WebcamUtil.getAvailableVideoInputs().then((mediaDevices: MediaDeviceInfo[]) => {
			this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
			this.showCam = true;
			setTimeout(() => {
				document.querySelector("#webCamModal").classList.add("md-show");
			}, 500);
		});
	}

	chooseOriginImage() {
		const translate = this.service.translate;
		const refSWAL = "PATIENTS.FORM.SWAL";

		Swal.fire({
			title: translate.instant(`${refSWAL}.TITLE_CHOOSE`),
			html: `<div class="row">
          <div class="col-12 col-sm-12 col-md-6">
            <button id="galery" class="btn btn-primary btn-block" (click)="getFromGalery()">
              <i class="ci icon-image"></i>${translate.instant("PATIENTS.FORM.GALERY")}
            </button>
          </div>
          <div class="col-12 col-sm-12 col-md-6">
            <button id="cam" class="btn btn-primary btn-block" (click)="getFromCam()">
              <i class="ci icon-camera"></i>${translate.instant("PATIENTS.FORM.CAM")}
            </button>
          </div>
        </div>`,
			showCancelButton: false,
			showConfirmButton: false,
			onBeforeOpen: () => {
				const content = Swal.getContent();
				const $ = content.querySelector.bind(content);

				const galery = $("#galery");
				const cam = $("#cam");

				galery.addEventListener("click", () => {
					this.getFromGalery();
					Swal.close();
				});

				cam.addEventListener("click", () => {
					this.getFromCam();
					Swal.close();
				});
			},
		});
	}

	public handleInitError(error: WebcamInitError): void {
		if (error.mediaStreamError && error.mediaStreamError.name === "NotAllowedError") {
			console.warn("Camera access was not allowed by user!");
		}
	}

	public get triggerObservable(): Observable<void> {
		return this.trigger.asObservable();
	}

	public get nextWebcamObservable(): Observable<boolean | string> {
		return this.nextWebcam.asObservable();
	}

	public handleImage(webcamImage: WebcamImage): void {
		this.webcamImage = webcamImage;

		this.showCropper = true;
		this.cropperCam = true;

		// this.webcamImage.imageAsDataUrl
		setTimeout(() => {
			document.querySelector("#choiseImage").classList.add("md-show");
		}, 500);
	}

	public cameraWasSwitched(deviceId: string): void {
		this.deviceId = deviceId;
	}

	private async _initProfessionDropdown() {
		this.professions = await this._professionService.listAll();
	}

	private async _initExpertisesDropdown() {
		const expertises = await this._dropdownGenericService.listAll(EXPERTISE_PATH);
		this.expertises = expertises.map((e) => {
			return { id: e.id, nome: e.nome };
		});
	}

	private async _initSituationsDropdown() {
		this.situations = await this._dropdownGenericService.listAll(SITUATION_PATH);
	}

	private async _initOrganizationsDropdown() {
		this.organizations = await this._dropdownGenericService.listAll(ORGANIZATION_PATH);
	}

	prepareToSaveNewProfession(profession: string | undefined) {
		if (!profession || !profession.trim()) {
			this._notificationsService.alert("A profissão não pode ser vazia!");
			return;
		}

		profession = profession.trim();
		const existingProfession = this.professions.find((p) => p.nome === profession);
		if (existingProfession) {
			this.formPatient.patchValue({
				pessoaProfissaoId: existingProfession.id,
			});

			return;
		}

		this.saveNewProfession({ id: null, nome: profession });
	}

	prepareToSaveNewSituation(situation: string | undefined) {
		const value = this.prepareToSaveNewDropdown(situation, this.situations, this.SITUATION_KEY);
		if (!value) {
			return;
		}

		this.saveNewSituation(value);
	}

	prepareToSaveNewOrganization(organization: string | undefined) {
		const value = this.prepareToSaveNewDropdown(
			organization,
			this.organizations,
			this.ORGANIZATION_KEY
		);
		if (!value) {
			return;
		}

		this.saveNewOrganization(value);
	}

	prepareToSaveNewExpertise(expertise: string | undefined) {
		if (!this.searchExpertise.value) {
			return;
		}
		const splitted = expertise.split("×");
		expertise = splitted.pop();
		const value = this.prepareToSaveNewExpertiseDropdown(expertise);
		if (!value) {
			return;
		}

		this.saveNewExpertise(value);
	}

	prepareToSaveNewExpertiseDropdown(value: string | undefined): DropdownGenericModel | undefined {
		if (!value || !value.trim()) {
			this._notificationsService.alert("A especialidade não pode ser vazia!");
			return;
		}

		value = value.trim();
		const existingValue = this.expertises.find((p) => p.nome === value);
		if (existingValue) {
			this.especialidadesPaciente.push(existingValue);
			return;
		}

		return { id: null, nome: value };
	}

	prepareToSaveNewDropdown(
		value: string | undefined,
		array: DropdownGenericModel[],
		path: string
	): DropdownGenericModel | undefined {
		if (!value || !value.trim()) {
			this._notificationsService.alert("O valor não pode ser vazio!");
			return;
		}

		value = value.trim();
		const existingValue = array.find((p) => (p.nome || p.descricao) === value);
		if (existingValue) {
			this.formPatient.patchValue({
				[path]: existingValue.id,
			});

			return;
		}

		return { id: null, nome: value };
	}

	async saveNewProfession(profession: DropdownGenericModel) {
		try {
			const { id } = await this._professionService.save(profession);
			this.setFormProfessionValue(id);
			this._initProfessionDropdown();
			this._notificationsService.success(`Profissão ${profession.nome} salva!`);
		} catch (e) {
			this._notificationsService.error(
				"Ocorreu um erro ao salvar a profissão. Por favor tente novamente."
			);
		}
	}

	async saveNewSituation(situation: DropdownGenericModel) {
		try {
			const { id } = await this._dropdownGenericService.save(situation, SITUATION_PATH);
			this.setFormValue(this.SITUATION_KEY, id);
			this._initSituationsDropdown();
			this._notificationsService.success(`Situação ${situation.descricao} salva!`);
		} catch (e) {
			this._notificationsService.error(
				"Ocorreu um erro ao salvar a situação. Por favor tente novamente."
			);
		}
	}

	async saveNewOrganization(organization: DropdownGenericModel) {
		try {
			const { id } = await this._dropdownGenericService.save(organization, ORGANIZATION_PATH);
			this.setFormValue(this.ORGANIZATION_KEY, id);
			this._initOrganizationsDropdown();
			this._notificationsService.success(`Organização ${organization.nome} salva!`);
		} catch (e) {
			this._notificationsService.error(
				"Ocorreu um erro ao salvar a organização. Por favor tente novamente."
			);
		}
	}

	async saveNewExpertise(expertise: DropdownGenericModel) {
		try {
			const ex = await this._dropdownGenericService.save(expertise, EXPERTISE_PATH);
			this.especialidadesPaciente.pop();
			this.especialidadesPaciente.push(ex);
			this._initExpertisesDropdown();
			this._notificationsService.success(`Especialidade ${expertise.nome} salva!`);
		} catch (e) {
			this._notificationsService.error(
				"Ocorreu um erro ao salvar a especialidade. Por favor tente novamente."
			);
		}
	}

	async editCurrentProfession() {
		const currentProfession: DropdownGenericModel = this.professions.find(
			(p) => p.id === this.getFormProfessionValue()
		);

		const input = await Swal.fire({
			title: "Modifique o nome da profissão",
			input: "text",
			inputValue: currentProfession.nome,
			showCancelButton: true,
			confirmButtonText: "Salvar",
			cancelButtonText: "Cancelar",
		});

		const value: string = String(input.value || "").trim();

		if (!value) {
			return;
		}

		this.saveNewProfession({ id: currentProfession.id, nome: value });
	}

	async editCurrentSituation() {
		const currentSituation: DropdownGenericModel = this.situations.find(
			(p) => p.id === this.getFormValue(this.SITUATION_KEY)
		);

		const input = await Swal.fire({
			title: "Modifique o nome da situação",
			input: "text",
			inputValue: currentSituation.descricao,
			showCancelButton: true,
			confirmButtonText: "Salvar",
			cancelButtonText: "Cancelar",
		});

		const value: string = String(input.value || "").trim();

		if (!value) {
			return;
		}

		this.saveNewSituation({ id: currentSituation.id, nome: value });
	}

	async editCurrentOrganization() {
		const currentOrganization: DropdownGenericModel = this.organizations.find(
			(p) => p.id === this.getFormValue(this.ORGANIZATION_KEY)
		);

		const input = await Swal.fire({
			title: "Modifique o nome da organização",
			input: "text",
			inputValue: currentOrganization.nome,
			showCancelButton: true,
			confirmButtonText: "Salvar",
			cancelButtonText: "Cancelar",
		});

		const value: string = String(input.value || "").trim();

		if (!value) {
			return;
		}

		this.saveNewOrganization({ id: currentOrganization.id, nome: value });
	}

	async deleteProfession(professionId: number) {
		const confirmation = await Swal.fire({
			title: "Confirmação de exclusão",
			text: "Tem certeza que quer apagar esta profissão?",
			type: "question",
			showCancelButton: true,
			confirmButtonText: "Sim. Apague",
			cancelButtonText: "Cancelar",
		});

		if (!confirmation.value) {
			return;
		}

		try {
			await this._professionService.delete(professionId);
			this._initProfessionDropdown();
			if (professionId === this.getFormProfessionValue()) {
				this.setFormProfessionValue(null);
			}
			this._notificationsService.success("Profissão removida!");
		} catch (e) {
			this._notificationsService.error(
				"Ocorreu um erro ao remover a profissão. Por favor tente novamente."
			);
		}
	}

	async deleteOrganization(organizationId) {
		const confirmation = await Swal.fire({
			title: "Confirmação de exclusão",
			text: "Tem certeza que quer apagar esta empresa?",
			type: "question",
			showCancelButton: true,
			confirmButtonText: "Sim. Apague",
			cancelButtonText: "Cancelar",
		});

		if (!confirmation.value) {
			return;
		}

		try {
			await this._dropdownGenericService.delete(organizationId, ORGANIZATION_PATH);
			this._initOrganizationsDropdown();
			if (organizationId === this.getFormValue(this.ORGANIZATION_KEY)) {
				this.setFormValue(this.ORGANIZATION_KEY, null);
			}
			this._notificationsService.success("Empresa removida!");
		} catch (e) {
			this._notificationsService.error(
				"Ocorreu um erro ao remover a empresa. Por favor tente novamente."
			);
		}
	}

	async deleteExpertise(expertise: DropdownGenericModel) {
		const confirmation = await Swal.fire({
			title: "Confirmação de exclusão",
			text: "Tem certeza que quer apagar esta especialidade?",
			type: "question",
			showCancelButton: true,
			confirmButtonText: "Sim. Apague",
			cancelButtonText: "Cancelar",
		});

		if (!confirmation.value) {
			return;
		}

		try {
			await this._dropdownGenericService.delete(expertise.id, EXPERTISE_PATH);
			this._initExpertisesDropdown();
			this.especialidadesPaciente = this.especialidadesPaciente.filter(
				(ex: DropdownGenericModel) => ex.id !== expertise.id
			);
			this._notificationsService.success("Especialidade removida!");
		} catch (e) {
			this._notificationsService.error(
				"Ocorreu um erro ao remover a especialidade. Por favor tente novamente."
			);
		}
	}

	async deleteSituation(situationId) {
		const confirmation = await Swal.fire({
			title: "Confirmação de exclusão",
			text: "Tem certeza que quer apagar esta situação?",
			type: "question",
			showCancelButton: true,
			confirmButtonText: "Sim. Apague",
			cancelButtonText: "Cancelar",
		});

		if (!confirmation.value) {
			return;
		}

		try {
			await this._dropdownGenericService.delete(situationId, SITUATION_PATH);
			this._initSituationsDropdown();
			if (situationId === this.getFormValue(this.SITUATION_KEY)) {
				this.setFormValue(this.SITUATION_KEY, null);
			}
			this._notificationsService.success("Situação removida!");
		} catch (e) {
			this._notificationsService.error(
				"Ocorreu um erro ao remover a situação. Por favor tente novamente."
			);
		}
	}

	setFormProfessionValue(value: number): void {
		if (this.formPatient) {
			this.formPatient.patchValue({
				pessoaProfissaoId: value,
			});
		}
	}

	setFormValue(path: string, value): void {
		if (this.formPatient) {
			this.formPatient.patchValue({
				[path]: value,
			});
		}
	}

	getFormValue(path: string) {
		const value = this.formPatient && this.formPatient.get(path);
		return value && value.value;
	}

	getFormProfessionValue(): number {
		const pessoaProfissaoId = this.formPatient && this.formPatient.get("pessoaProfissaoId");
		return pessoaProfissaoId && pessoaProfissaoId.value;
	}

	getPatientProfession(): string {
		const patientProfession = this.professions.find((p) => p.id === this.patient.pessoaProfissaoId);
		return patientProfession && patientProfession.nome;
	}

	getPatientMarital(): string {
		const patientMarital = this.marital.find(
			(m: SelectItem) => m.value === this.patient.pessoaEstadoCivil
		);
		return patientMarital && patientMarital.label;
	}

	toggleProfileNotification() {
		this.answerAlertNotification =
			this.answerAlertNotification === "an-off" ? "an-animate" : "an-off";
		this.answerAlertNotificationClass =
			this.answerAlertNotification === "an-animate" ? "active" : "";
	}

	notificationOutsideClick(ele: string) {
		if (ele === "respostasAlerta" && this.answerAlertNotification === "an-animate") {
			this.toggleProfileNotification();
		}
	}

	get showWhatsappIcon() {
		return (
			this.patient && this.patient.id && (this.patient.pessoaCelular || this.patient.pessoaCelular2)
		);
	}

	goToPatientWhatsapp() {
		const primaryCellphone = (this.patient.pessoaCelular || "").replace(/\D+/g, "");
		const secondaryCellPhone = (this.patient.pessoaCelular2 || "").replace(/\D+/g, "");
		if (
			primaryCellphone.length !== FULL_CELLPHONE_LENGTH &&
			secondaryCellPhone.length !== FULL_CELLPHONE_LENGTH
		) {
			this._notificationsService.alert(
				"O paciente não possui um número de telefone válido! Deve ter 11 dígitos."
			);
			return;
		}

		const whatsappLink =
			"https://api.whatsapp.com/send?phone=55" + primaryCellphone || secondaryCellPhone;

		// Usar window.open() pode não funcionar de acordo com browser e/ou configuração do usuário
		$("<a />", { href: whatsappLink, target: "_blank" }).get(0).click();
	}

	private async _resolveCanEditCodigoProntuario(): Promise<void> {
		if (this.patient && this.patient.id) {
			this.canEditCodigoProntuario = this.patient.geraProntuarioAutomatico === "NAO";
		} else {
			const { currentConta } = this._loginService;
			const { geraProntuarioAutomatico } = await this._contaApiService.fetchContaCompleta(
				currentConta.perfilContaModuloContaId
			);
			this.canEditCodigoProntuario = geraProntuarioAutomatico === "NAO";
		}
	}

	private _userCanSave() {
		if (!this._userPermissionService.hasPermission(PATIENT_PERMISSIONS.EDIT)) {
			this.formPatient.disable();
			this.userCanSave = false;
		}
	}

	async newTreatmentsNotApproved(row: any) {
		const confirmation = await Swal.fire({
			title: "Criar um novo orçamento à partir dos procedimentos não aprovados deste?",
			text: "Um novo orçamento será criado com os procedimentos não aprovados deste, confirma?",
			type: "warning",
			showCancelButton: true,
			confirmButtonText: "Sim, crie!!",
			cancelButtonText: "Não, obrigado!",
		});

		if (confirmation.value) {
			this.createFromNotApproved(row);
		}
	}

	createFromNotApproved(row: any) {
		this.service.loading(true);
		this.service
			.httpPOST(CONSTANTS.ENDPOINTS.patients.treatment.createTreatmentFromNotAproved, {
				tratamentoId: row.id,
			})
			.subscribe(
				(res) => {
					this.service.loading(false);
					this.getTreatment();
					this.service.notification.success("Novo tratamento criado!");
				},
				(err) => {
					this.service.loading(false);
					this.service.notification.error(
						this.service.translate.instant("COMMON.ERROR.SEARCH"),
						err.error.error
					);
				}
			);
	}

	treatmentResume(row: any) {
		this.selectedResumeTreatment = row;
		const ref = CONSTANTS.ENDPOINTS.patients.treatment.getTreatment + row.id;
		const treatment = this.service.httpGET(ref);
		this.showTreatmentResumeModal = true;
		this.service.loading(true);
		treatment.subscribe(
			(res) => {
				this.treatmentModal = res.body as ContentTreatmentModel;
				this.applyToothNames(this.treatmentModal.servicos);
				this.applyStatusName(this.treatmentModal.servicos);
				this.getProcedures();
				this.treatmentModal = this.treatmentModal.servicos.filter(
					(procedure) => procedure.statusTratamentoServico !== LOCAL_CONSTS.NAO_AUTORIZADO
				);
				this.service.loading(false);
				this.finishedModalOpening = true;
			},
			(err) => {
				this.service.loading(false);
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			}
		);
	}

	printContract(row: any) {
		const ref = CONSTANTS.ENDPOINTS.patients.treatment.getDefaultContract;
		const treatment = this.service.httpGET(ref);

		this.service.loading(true);
		treatment.subscribe(
			(res) => {
				this.service.loading(false);
				this.createContract(res, row);
			},
			(err) => {
				this.service.loading(false);
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			}
		);
	}

	createContract(res: any, row: any) {
		this.contractHtml = res.body.texto;

		const ref = CONSTANTS.ENDPOINTS.patients.treatment.getContractTagsValue;
		const tratamentoId = row.id;
		const treatment = this.service.httpPOST(ref, { tratamentoId });

		this.service.loading(true);
		treatment.subscribe(
			(res) => {
				this.service.loading(false);
				this.contractPDF(res, row);
			},
			(err) => {
				this.service.loading(false);
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			}
		);
	}

	async contractPDF(res: any, row: any) {
		this.service.loading(true);
		const tagsValue = res.body.tags;
		tagsValue.forEach((el) => {
			const key = Object.keys(el)[0];
			let value = el[key];
			if (key.includes("valor") && !key.includes("extenso")) {
				value = (value as number).toFixed(2).toString();
			}
			const replace = new RegExp(key, "g");
			this.contractHtml = this.contractHtml.replace(replace, value);
		});

		let pdfMake: PDFMake = { afterContent: [], pageMargins: [20, 40, 20, 40] };
		pdfMake.defaultStyle = {
			fontSize: 10,
		};
		let html = this.contractHtml;
		html = await this._commonPrintService.getHeader(
			await this._printConfigurationsService.getConfigurations(),
			html
		);

		let approved;
		let notApproved;
		let installments;

		const traslations = this._getNecessaryTranslations();
		const listFace = this.listFace;
		const planoTratamentoPrintConfig: PlanoTratamentoPrintConfig = {
			onlyTotalValue: true,
			showImage: false,
		};
		const { body } = await this.service
			.httpGET(CONSTANTS.ENDPOINTS.patients.treatment.getTreatment + row.id)
			.toPromise();
		const treatment = body as ContentTreatmentModel;

		if (res.body.servicosAprovados && res.body.servicosAprovados.length > 0) {
			treatment.servicos = res.body.servicosAprovados;
			approved = FormPatientsComponent._buildBody(
				treatment,
				planoTratamentoPrintConfig,
				traslations,
				listFace
			);
		}
		if (res.body.servicosNaoAprovados && res.body.servicosNaoAprovados.length > 0) {
			treatment.servicos = res.body.servicosNaoAprovados;
			notApproved = FormPatientsComponent._buildBody(
				treatment,
				planoTratamentoPrintConfig,
				traslations,
				listFace,
				false
			);
		}
		if (
			(res.body.servicosAprovados && res.body.servicosAprovados.length > 0) ||
			(res.body.servicosNaoAprovados && res.body.servicosNaoAprovados.length > 0)
		) {
			treatment.parcelas = res.body.parcelas;
			installments = FormPatientsComponent._buildParcels(treatment.parcelas, traslations);
		}

		let content = htmlToPdfmake(html, { defaultStyle: pdfMake.defaultStyle });
		for (let i = 0; i < content.length; i++) {
			if (content[i].text && content[i].text == "#procedimentos_aprovados") {
				if (approved) content[i] = approved;
				else content.splice(i, 1);
			} else if (content[i].text && content[i].text == "#procedimentos_nao_aprovados") {
				if (notApproved) content[i] = notApproved;
				else content.splice(i, 1);
			} else if (content[i].text && content[i].text == "#parcelas_pagamento") {
				if (installments) content[i] = installments;
				else content.splice(i, 1);
			}
		}
		pdfMake.afterContent.push(content);

		const pdfMakeDefinition = this._pdfGeneratorService.exportPDFMakeAsPDF(
			pdfMake,
			null,
			`ContratoTratamento-${treatment.pacientePessoaNome}.pdf`
		);
		this.service.loading(false);
		pdfMakeDefinition.open();
	}

	private static _buildSimpleTextHeader(text: string, size: number) {
		return [
			{
				text: text,
				alignment: "center",
				bold: true,
				fontSize: size,
				margin: [0, 10, 0, 0],
			},
		];
	}

	closeTreatmentResumeModal() {
		if (this.showTreatmentResumeModal && this.finishedModalOpening) {
			this.showTreatmentResumeModal = false;
			this.modalTreatmentsTable = null;
			this.finishedModalOpening = false;
		}
	}

	applyToothNames(list: ProceduresModel[]) {
		list.map((item) => {
			const value = item.emDente.value ? item.emDente.value : item.emDente;
			const tooth = toothsList.find((each) => each.value === value);
			item.emDenteDescricao = tooth.label;
		});
	}

	applyStatusName(list: ProceduresModel[]) {
		list.map((item) => {
			const status = this.LISTAS_LOCAIS.listaStatusTratamentoServico.find(
				(each) => each.value === item.statusTratamentoServico
			);
			if (status) {
				item.statusTratamentoServicoDescricao = status.label;
			}
		});
	}

	getProcedures() {
		if (this.treatmentModal.servicos) {
			this.modalTreatmentsTable = classToPlain(
				this.treatmentModal.servicos.filter(
					(procedure) => procedure.statusTratamentoServico !== LOCAL_CONSTS.NAO_AUTORIZADO
				)
			) as ProceduresModel[];

			this.modalTreatmentsTable.map((procedure) => {
				let desc = "(";

				if (procedure.isFaceDistal) {
					desc += `${this.service.translate.instant(
						"PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceDistal"
					)}, `;
				}

				if (procedure.isFaceOclusalIncisal) {
					desc += `${this.service.translate.instant(
						"PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceOclusalIncisal"
					)}, `;
				}

				if (procedure.isFaceLingualPalatina) {
					desc += `${this.service.translate.instant(
						"PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceLingualPalatina"
					)}, `;
				}

				if (procedure.isFaceMesial) {
					desc += `${this.service.translate.instant(
						"PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceMesial"
					)}, `;
				}

				if (procedure.isFaceVestibular) {
					desc += `${this.service.translate.instant(
						"PATIENTS.MODAL_PROCEDURE.LIST_FACE.isFaceVestibular"
					)} `;
				}

				if (desc.trim().substr(desc.trim().length - 1, 1) === ",") {
					desc = desc.trim().substr(0, desc.trim().length - 1);
				}

				if (desc !== "(") {
					procedure.convenioEspecialidadeProcedimentoNome =
						procedure.convenioEspecialidadeProcedimentoNome + " " + desc + ")";
				}
			});
		}
	}
}
