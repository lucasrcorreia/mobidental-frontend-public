import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleAnamneseComponent } from './single-anamnese.component';

describe('SingleAnamneseComponent', () => {
  let component: SingleAnamneseComponent;
  let fixture: ComponentFixture<SingleAnamneseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleAnamneseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleAnamneseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
