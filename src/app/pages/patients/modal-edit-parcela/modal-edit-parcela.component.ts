import { Component, Input, OnInit } from '@angular/core';
import { UtilsService } from '../../../services/utils/utils.service';
import { LISTAS } from '../lists';
import { ParcelasModel } from '../../../core/models/patients/patients.model';

@Component({
	selector: 'app-modal-edit-parcela',
	templateUrl: './modal-edit-parcela.component.html',
	styleUrls: ['./modal-edit-parcela.component.scss',
		'./../../../../assets/icon/icofont/css/icofont.scss']
})

export class ModalEditParcelaComponent implements OnInit {

	@Input() parcela: ParcelasModel;
	LISTAS_LOCAIS = new LISTAS(this.service.translate);

	constructor(
			public service: UtilsService
	) { }

	ngOnInit() {
	}

	save() {
		this.parcela.dataVencimento = this.service.parseDateFromDatePicker(this.parcela.dataVencimento);
		this.service.closeModal('modal-parcela', this.parcela);
	}

}
