import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../../services/utils/utils.service';
import { ColumnsTableModel } from '../../../core/models/table/columns.model';
import { CONSTANTS } from '../../../core/constants/constants';
import { PaginationResponseModel } from '../../../core/models/response/paginationResponse.model';
import {
  CovenanteCreate,
  CovenantModel,
  ProceduresCovenantModel,
  SpecialtiesCovenantModel,
} from '../../../core/models/config/covenant/covenantCreate.model';
import { ConveniosApiService } from '../../../api/convenios-api.service';
import Swal from 'sweetalert2';
import { NotificationsService } from 'angular2-notifications';
import { errorToString } from '../../../lib/helpers/error-to-string';
import { GlobalSpinnerService } from '../../../lib/global-spinner.service';
import { load } from '@angular/core/src/render3';

@Component({
  selector: 'app-covenant',
  templateUrl: './covenant.component.html',
  styleUrls: [
    './covenant.component.scss',
    './../../../../assets/icon/icofont/css/icofont.scss',
  ],
})
export class CovenantComponent implements OnInit {
  showModalCreate = false;
  columns: ColumnsTableModel[] = [
    {
      action: true,
      name: this.service.translate.instant('CONFIG.COVENANT.TABLE.COL1'),
      prop: 'id',
      sortable: false,
      width: 120,
    },
    {
      action: false,
      name: this.service.translate.instant('CONFIG.COVENANT.TABLE.COL2'),
      prop: 'nome',
      sortable: true,
      width: 350,
    },
    {
      action: false,
      name: this.service.translate.instant('CONFIG.COVENANT.TABLE.COL3'),
      prop: 'isPadrao',
      sortable: true,
      width: 100,
    },
  ];

  rows = {};
  rowsPage = [];
  searchParam = {
    page: 0,
    nome: '',
    size: 100,
    sorting: {
      undefined: 'asc',
    },
  };

  page = {
    size: 100,
    totalElements: 0,
    totalPages: 0,
    pageNumber: 0,
  };

  idDefault = -1;

  constructor(
    public service: UtilsService,
    private readonly _conveniosApiService: ConveniosApiService,
    private readonly _notificationsService: NotificationsService,
    private readonly _globalSpinnerService: GlobalSpinnerService,
  ) {
  }

  ngOnInit() {
    this.populateTable();
  }

  populateTable() {
    const loadingToken = this._globalSpinnerService.loadingManager.start();

    this.searchParam.page = this.page.pageNumber;

    this.service
      .httpPOST(CONSTANTS.ENDPOINTS.config.covenant.table, this.searchParam)
      .subscribe(
        data => {
          const result = data.body as PaginationResponseModel;
          if (this.page.pageNumber === 0) {
            this.page.totalElements = result.totalElements;
            this.page.totalPages = result.totalPages;
          }

          this.rows[this.page.pageNumber] = result.content;
          this.rowsPage = result.content;
        },
        err => {

          this.service.notification.error(
            this.service.translate.instant('COMMON.ERROR.SEARCH'),
            err.error.error,
          );
        },
        () => {
          loadingToken.finished();
        },
      );
  }

  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    if (this.rows[this.page.pageNumber]) {
      this.rowsPage = this.rows[this.page.pageNumber];
    } else {
      this.populateTable();
    }
  }

  formCovenant(covenant) {
    this.service.loading(true);
    if (!covenant) {
      this.service
        .httpGET(CONSTANTS.ENDPOINTS.config.covenant.findDefault)
        .subscribe(
          data => {
            this.service.loading(false);
            this.showModalCreate = true;
            this.idDefault = (data.body as any).id;
            setTimeout(() => {
              document
                .querySelector('#modal-create-covenant')
                .classList.add('md-show');
            }, 500);
          },
          err => {

            this.service.loading(false);
            this.service.notification.error(
              this.service.translate.instant('COMMON.ERROR.SEARCH'),
              err.error.error,
            );
          },
        );
    } else {
      this.service
        .httpGET(
          CONSTANTS.ENDPOINTS.config.covenant.getSpecialties + covenant.id,
        )
        .subscribe(
          data => {
            const covenantEdit = new CovenantModel();
            covenantEdit.id = covenant.id;
            covenantEdit.nome = covenant.nome;
            covenantEdit.isPadrao = covenant.isPadrao;
            covenantEdit.especialidades = data.body as SpecialtiesCovenantModel[];
            covenantEdit.especialidades.map(espec => {
              espec.procedimentos = [];
            });

            const ref =
              CONSTANTS.ENDPOINTS.config.covenant.getProcedures.replace(
                '#specialtyId',
                covenantEdit.especialidades[0].id.toString(),
              ) + covenant.id;

            this.service.httpGET(ref).subscribe(
              dataProc => {
                covenantEdit.especialidades[0].procedimentos = dataProc.body as ProceduresCovenantModel[];
                this.service.setStorage(
                  CONSTANTS.STORAGE.COVENANT,
                  covenantEdit,
                );

                this.service.loading(false);

                this.service.navigate(CONSTANTS.ROUTERS.CONFIG.COVENANT.FORM, {
                  editing: true,
                });
              },
              err => {

                this.service.loading(false);
                this.service.notification.error(
                  this.service.translate.instant('COMMON.ERROR.SEARCH'),
                  err.error.error,
                );
              },
            );
          },
          err => {

            this.service.loading(false);
            this.service.notification.error(
              this.service.translate.instant('COMMON.ERROR.SEARCH'),
              err.error.error,
            );
          },
        );
    }
  }

  async deleteConvenio(id: number) {
    const { value } = await Swal.fire({
      title: 'Apagar convênio',
      text: 'Tem certeza que deseja apagar esse convênio?',
      type: 'question',
      showCancelButton: true,
      confirmButtonText: 'Sim, apagar',
      cancelButtonText: 'Não, cancelar',
    });

    if (!value) {
      return;
    }

    const loadingToken = this._globalSpinnerService.loadingManager.start();

    try {
      await this._conveniosApiService.apagarConvenio(id);
      this._notificationsService.success('Convênio apagado com sucesso');
      this.populateTable();
    } catch (e) {
      this._notificationsService.error('Erro ao apagar convênio', errorToString(e));
    } finally {
      loadingToken.finished();
    }
  }

  processResultModalCreate(event: CovenanteCreate) {
    if (event) {
      this.service.loading(true);
      const ref = CONSTANTS.ENDPOINTS.config.covenant.create
        .replace('#name', event.convenioNome)
        .replace('#copy', event.isCopiaProcedimentos.toString())
        .replace('#id', event.convenioId.toString());

      this.service.httpGET(ref).subscribe(
        data => {
          this.service.loading(false);

          this.service.setStorage(CONSTANTS.STORAGE.COVENANT, data.body);
          this.service.navigate(CONSTANTS.ROUTERS.CONFIG.COVENANT.FORM, {
            editing: false,
          });
        },
        err => {

          this.service.loading(false);
          this.service.notification.error(
            this.service.translate.instant('COMMON.ERROR.SEARCH'),
            err.error.error,
          );
        },
      );
    }
    this.showModalCreate = false;
  }
}
