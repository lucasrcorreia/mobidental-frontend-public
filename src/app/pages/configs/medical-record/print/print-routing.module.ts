import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrintConfigurationComponent } from "./print-configuration/print-configuration.component";

const routes: Routes = [
  {
    path: '',
    component: PrintConfigurationComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrintRoutingModule {
}
