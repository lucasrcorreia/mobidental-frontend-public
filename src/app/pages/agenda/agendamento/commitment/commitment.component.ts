import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ShortListItem } from '../../../../core/models/forms/common/common.model';
import { PatientModel } from '../../../../core/models/patients/patients.model';
import { UtilsService } from '../../../../services/utils/utils.service';
import { CommitmentService } from './service/commitment.service';
import { Commitment } from './model/commitment';
import { Recorrencia, RECORRENCIA_TRANSLATE } from './model/enum/recorrencia';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject } from 'rxjs';
import * as moment from 'moment';

const FINAL_TIME_STRUCT: NgbTimeStruct = { hour: 23, minute: 59, second: 59 };
const INITIAL_TIME_STRUCT: NgbTimeStruct = { hour: 0, minute: 0, second: 0 };

@Component({
	selector: 'app-commitment',
	templateUrl: './commitment.component.html',
	styleUrls: ['./commitment.component.scss'],
})
export class CommitmentComponent implements OnInit {

	@Input() professionals: ShortListItem[] = [];
	@Input() patient: PatientModel;
	@Input() commitment: Commitment;

	@Output() close = new EventEmitter<void>();
	@Output() eventsChange = new EventEmitter<void>();

	private allDaySubject = new BehaviorSubject<boolean>(false);
	allDayObservable = this.allDaySubject.asObservable();

	repeat = false;
	finalDefaulTime: NgbTimeStruct;
	initialDefaulTime: NgbTimeStruct;
	private readonly _recorrencias = Object.values(Recorrencia);
	recorrenciaList: { id: Recorrencia, name: string }[];

	constructor(public readonly _service: UtilsService,
				private readonly _commitmentService: CommitmentService) { }

	ngOnInit() {
		this._mergeAttributes();
		this.commitment.isDiaInteiro = false;
		this.recorrenciaList = this._recorrencias.map(rec => { return { id: rec, name: RECORRENCIA_TRANSLATE[rec] }});
	}

	private _mergeAttributes(): void {
		this.commitment.profissionalId = this.commitment['profissionalId'];
		if (this.commitment.id) {
			this.commitment.descricao = this.commitment['nomePaciente'];
			this.commitment.dataInicial = this.commitment['dataHoraInicialEvento'].replace('T', ' ');
			this.commitment.dataFinal = this.commitment['dataHoraFinalEvento'].replace('T', ' ');
		} else {
			this.commitment.dataInicial = this._formatteDateForMomentCanParse();
			this.commitment.dataFinal = moment(this.commitment.dataInicial).add(4, 'hours').toDate();
		}
	}

	private _formatteDateForMomentCanParse(): Date {
		const splitted = this.commitment['dataHoraInicialEvento'].split(' ');
		const momentString = splitted[0].split('/').reverse().join('-') + ' ' + splitted[1];
		return moment(momentString).toDate();
	}

	dateTime(dateTime: Date): string {
		return dateTime ? moment(dateTime).format('L HH:mm') : '';
	}

	allDayChange($event) {
		this.finalDefaulTime = $event ? FINAL_TIME_STRUCT : null;
		this.initialDefaulTime = $event ? INITIAL_TIME_STRUCT : null;

		setTimeout(() => this.allDaySubject.next($event), 500);
	}

	doOpenSelect() {
		CommitmentComponent._setMinHeightToBody('500px');
		this._service.doOpenSelect();
	}

	doCloseSelect() {
		CommitmentComponent._setMinHeightToBody('300px');
	}

	private static _setMinHeightToBody(px: string) {
		const select = document.getElementsByClassName('card-body')[1];
		(select as HTMLElement).style.minHeight = px;
	}

	async save() {
		const success = await this._commitmentService.saveOrUpdate(this.commitment);
		if (success) {
			this.close.emit();
			this.eventsChange.emit();
		}
	}

	async remove() {
		const success = await this._commitmentService.removeDialog(this.commitment);
		if (success) {
			this.close.emit();
			this.eventsChange.emit();
		}
	}

}
