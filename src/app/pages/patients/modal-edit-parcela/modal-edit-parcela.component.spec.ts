import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEditParcelaComponent } from './modal-edit-parcela.component';

describe('ModalEditParcelaComponent', () => {
  let component: ModalEditParcelaComponent;
  let fixture: ComponentFixture<ModalEditParcelaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEditParcelaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEditParcelaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
