import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAddProcedureComponent } from './modal-add-procedure.component';

describe('ModalAddProcedureComponent', () => {
  let component: ModalAddProcedureComponent;
  let fixture: ComponentFixture<ModalAddProcedureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAddProcedureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAddProcedureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
