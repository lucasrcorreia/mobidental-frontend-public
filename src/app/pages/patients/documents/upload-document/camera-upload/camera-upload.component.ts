import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsService } from 'angular2-notifications';
import { WebcamImage, WebcamUtil } from 'ngx-webcam';
import { Observable, Subject } from 'rxjs';

export interface WebCamFile {
	file: WebcamImage;
	description: string;
	note?: string;
	thumb?: { thumb: string, caption: string, src: string };
}

@Component({
	selector: 'app-camera-upload',
	templateUrl: './camera-upload.component.html',
	styleUrls: ['./camera-upload.component.scss'],
})
export class CameraUploadComponent implements OnInit {

	webcamImage: WebcamImage = null;

	description: string;
	note: string;

	multipleWebcamsAvailable = false;
	readonly videoOptions: MediaTrackConstraints = {
		// width: {ideal: 1024},
		// height: {ideal: 576}
	};
	trigger: Subject<void> = new Subject<void>();

	constructor(public activeModal: NgbActiveModal,
				private readonly _notificationsService: NotificationsService) {
	}

	ngOnInit(): void {
		WebcamUtil.getAvailableVideoInputs()
				.then((mediaDevices: MediaDeviceInfo[]) => {
					this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
				});
	}

	handleImage(webcamImage: WebcamImage): void {
		this.webcamImage = webcamImage;
	}

	triggerSnapshot(): void {
		this.trigger.next();
	}

	get triggerObservable(): Observable<void> {
		return this.trigger.asObservable();
	}

	tryAgain() {
		this.webcamImage = null;
	}

	save() {
		const webcamFile: WebCamFile = {
			description: this.description,
			note: this.note,
			file: this.webcamImage,
		};

		this.activeModal.close(webcamFile);
	}

}
