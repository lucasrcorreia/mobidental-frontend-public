import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'patients-report',
    loadChildren: './patients-report/patients-report.module#PatientsReportModule',
    data: {
      id: 20,
      children: true,
    },
  },
  {
    path: 'age-range-report',
    loadChildren: './age-range-report/age-range-report.module#AgeRangeReportModule',
    data: {
      id: 26,
      children: true,
    },
  },
  {
    path: 'birthday-report',
    loadChildren: './birthday-report/birthday-report.module#BirthdayReportModule',
    data: {
      id: 25,
      children: true,
    },
  },
  {
    path: 'receipts-report',
    loadChildren: './receipts-report/receipts-report.module#ReceiptsReportModule',
    data: {
      id: 21,
      children: true,
    },
  },
  {
    path: 'treatments-report',
    loadChildren: './treatments-report/treatments-report.module#TreatmentsReportModule',
    data: {
      id: 22,
      children: true,
    },
  },
  {
    path: 'indications-report',
    loadChildren: './indications-report/indications-report.module#IndicationsReportModule',
    data: {
      id: 23,
      children: true,
    },
  },
  {
    path: 'schedulings-report',
    loadChildren: './schedulings-report/schedulings-report.module#SchedulingsReportModule',
    data: {
      id: 24,
      children: true,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportsRoutingModule {
}
