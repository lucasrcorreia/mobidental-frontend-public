import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSelectModule } from 'ngx-select-ex';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SellReportComponent } from './sell-report.component';
import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';
import { ReportsModule } from '../../reports/reports.module';

@NgModule({
  declarations: [SellReportComponent],
    imports: [
        CommonModule,
        NgxSelectModule,
        FormsModule,
        TranslateModule,
        NgbAccordionModule,
        ReportsModule
    ],
  exports: [SellReportComponent]
})
export class SellReportModule {
}
