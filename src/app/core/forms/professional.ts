import { FormsModel } from '../models/forms/forms.model';
import { Validators } from '@angular/forms';

export const FormProfessional: FormsModel[] = [
  {
    field: 'profissionalNome',
    validators: [Validators.required]
  },
  {
    field: 'usuarioEmail',
    validators: [Validators.required, Validators.email]
  },
  {
    field: 'perfilContaModuloId',
    validators: [Validators.required]
  },
];
