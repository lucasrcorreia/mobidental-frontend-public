import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AgeRangeReportComponent } from "./age-range-report.component";

const routes: Routes = [
	{
		path: "",
		component: AgeRangeReportComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class AgeRangeReportRoutingModule {}
