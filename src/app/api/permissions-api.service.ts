import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_CONFIGURATION, ApiConfiguration } from './api-configuration';

export interface PermissionProfile {
  id: number;
  descricao: string;
  isDentista: boolean;
}

export interface PermissionAction {
  moduloTelaAcaoAcao: string;
  moduloTelaAcaoId: number;
  possuiPermissao: boolean;
}

export interface PermissionModule {
  moduloTelaAcao: PermissionAction[];
  moduloTelaDescricao: string;
  moduloTelaId: number;
}

@Injectable({
  providedIn: 'root',
})
export class PermissionsApiService {

  constructor(
    private readonly _http: HttpClient,
    @Inject(API_CONFIGURATION) private readonly _config: Readonly<ApiConfiguration>,
  ) {
  }

  fetchProfiles(): Promise<PermissionProfile[]> {
    return this._http
      .get<PermissionProfile[]>(`${this._config.apiBasePath}/perfilContaModulo/dropdown`)
      .toPromise();
  }

  fetchPermissionsByProfile(profileId: number) {
    return this._http
      .get<PermissionModule[]>(`${this._config.apiBasePath}/perfilContaModulo/${profileId}/telas`)
      .toPromise();
  }

  updatePermissions(
    profileId: number,
    activatedPermissionsIds: number[],
    deativatedPermissionsIds: number[],
  ) {
    return this._http.put<void>(
      `${this._config.apiBasePath}/perfilContaModulo/${profileId}/atualizar/acessos`,
      undefined, {
        params: {
          moduloTelaAcaoSelecionados: activatedPermissionsIds.join(','),
          moduloTelaAcaoNaoSelecionados: deativatedPermissionsIds.join(','),
        },
      },
    ).toPromise();
  }
}
