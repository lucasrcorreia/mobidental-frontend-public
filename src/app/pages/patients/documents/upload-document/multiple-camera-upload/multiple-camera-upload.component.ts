import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { WebcamImage, WebcamUtil } from 'ngx-webcam';
import { Observable, Subject } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsService } from 'angular2-notifications';
import { WebCamFile } from '../camera-upload/camera-upload.component';
import { UtilsService } from '../../../../../services/utils/utils.service';
import Swal from 'sweetalert2';
import { Lightbox } from 'ngx-lightbox';

@Component({
	selector: 'app-multiple-camera-upload',
	templateUrl: './multiple-camera-upload.component.html',
	styleUrls: ['./multiple-camera-upload.component.scss'],
})
export class MultipleCameraUploadComponent implements OnInit {

	@ViewChild('webCamContainer') webCamContainer: ElementRef;

	webcamFiles: WebCamFile[] = [];

	camHeight: number;
	currentMediaDeviceId: string;
	mediaDevices: MediaDeviceInfo[];
	readonly videoOptions: MediaTrackConstraints = {
		// width: {ideal: 1024},
		// height: {ideal: 576}
	};
	trigger: Subject<void> = new Subject<void>();
	nextWebcam: Subject<boolean | string> = new Subject<boolean | string>();

	constructor(private readonly _activeModal: NgbActiveModal,
				public readonly _service: UtilsService,
				private readonly _notificationsService: NotificationsService,
				private readonly _lightbox: Lightbox) {
	}

	ngOnInit(): void {
		WebcamUtil.getAvailableVideoInputs()
				.then((mediaDevices: MediaDeviceInfo[]) => {
					this.mediaDevices = mediaDevices;
					this.currentMediaDeviceId = mediaDevices && mediaDevices.length > 0 ? mediaDevices[0].deviceId : null;
				});
		this.resizeCamHeight();
	}

	resizeCamHeight() {
		// faz modal-footer ficar visível até resolução de altura = 550px
		this.camHeight = window.innerHeight / 2;
	}

	handleImage(webcamImage: WebcamImage): void {
		const webcamFile: WebCamFile = {
			description: 'Captura ' + (this.webcamFiles.length + 1),
			file: webcamImage,
		};
		webcamFile.thumb = {
			src: 'data:image/jpg;base64,' + webcamFile.file.imageAsBase64,
			caption: webcamFile.description,
			thumb: webcamFile.file.imageAsDataUrl,
		};
		this.webcamFiles.push(webcamFile);
	}

	triggerSnapshot(): void {
		this.trigger.next();
	}

	get triggerObservable(): Observable<void> {
		return this.trigger.asObservable();
	}

	get nextWebcamObservable(): Observable<boolean | string> {
		return this.nextWebcam.asObservable();
	}

	changeCamera() {
		this.nextWebcam.next(this.currentMediaDeviceId);
	}

	removeCapture(webCamFile: WebCamFile, index: number) {
		this.webcamFiles = this.webcamFiles.filter(file => file !== webCamFile);
	}

	saveAll() {
		this._activeModal.close(this.webcamFiles);
	}

	async close() {
		const { value } = await Swal.fire({
			title: 'Confirmar ação',
			text: 'Deseja realmente sair? Suas imagens não foram salvas e serão perdidas.',
			type: 'question',
			showCancelButton: true,
			confirmButtonText: 'Sim, sair',
			cancelButtonText: 'Cancelar',
		});

		if (!value) {
			return;
		}

		this._activeModal.close(false);
	}

	openLightbox(index: number): void {
		const thumbs = this.webcamFiles.map(w => w.thumb);
		this._lightbox.open(thumbs, index, {
			wrapAround: true,
			centerVertically: true,
			showImageNumberLabel: true,
			albumLabel: 'Imagem %1 de %2',
		});
	}

}
