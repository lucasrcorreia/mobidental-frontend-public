import { Component, Input, OnDestroy } from '@angular/core';
import { ModalRef } from '../../../shared/modal-animation/modal-ref';
import { getTipoMovimentoInfo, MovimentoEstoque, TipoMovimento } from '../../../api/model/movimento-estoque';
import { Produto } from '../../../api/model/produto';
import { ProdutosApiService } from '../../../api/produtos-api.service';
import { BehaviorSubject, combineLatest, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, flatMap, map, shareReplay, take } from 'rxjs/operators';
import { DestinoDropdown } from '../../../api/model/destino-dropdown';
import clone from 'ramda/es/clone';
import values from 'ramda/es/values';
import { AsyncTemplate } from '../../../lib/helpers/async-template';
import { NotificationsService } from 'angular2-notifications';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-movimentacao',
  templateUrl: './modal-movimentacao.component.html',
  styleUrls: ['./modal-movimentacao.component.scss'],
})
export class ModalMovimentacaoComponent implements OnDestroy {
  private _sub = new Subscription();

  @Input() modal?: ModalRef<Partial<MovimentoEstoque>>;
  @Input() produto?: Produto;
  @Input() data: Partial<MovimentoEstoque> = {};

  readonly TipoMovimentacao = TipoMovimento;
  readonly tiposMovimentacao = values(TipoMovimento);
  readonly getTipoMovimentoInfo = getTipoMovimentoInfo;

  destinoId?: number;

  readonly searchSubject = new BehaviorSubject<undefined | string>(undefined);
  readonly forceSearch = new BehaviorSubject<undefined>(undefined);
  private readonly resultadosSubject = new BehaviorSubject<DestinoDropdown[]>([]);

  private todosResultado$ = this.forceSearch.pipe(
    flatMap(() => this._asyncTemplate.wrapUserFeedback(
      'Erro ao consultar destinos de movimentação',
      () => this._produtosApi.listarDestinos(),
    )),
    shareReplay(1),
  );

  readonly resultado$ = combineLatest([
    this.todosResultado$,
    this.searchSubject.pipe(
      debounceTime(300),
      distinctUntilChanged(),
    ),
  ]).pipe(
    map(([ds, txt]) =>
      !txt || !txt.trim()
        ? ds
        : ds.filter(d => d.descricao.toLowerCase().indexOf(txt.trim().toLowerCase()) > -1),
    ),
  );

  constructor(
    private readonly _produtosApi: ProdutosApiService,
    private readonly _notificationsService: NotificationsService,
    private readonly _asyncTemplate: AsyncTemplate,
  ) {
    this._sub.add(this.todosResultado$.subscribe(this.resultadosSubject));
  }

  ngOnDestroy(): void {
    this._sub.unsubscribe();
  }

  cancelar() {
    if (this.modal) {
      this.modal.close();
    }
  }

  async salvar() {
    const dados: Partial<MovimentoEstoque> = clone(this.data);

    await this._asyncTemplate.wrapUserFeedback(
      'Erro ao salvar movimentação',
      async () => {
        if (dados.tipoMovimento === TipoMovimento.SAIDA) {
          dados.destinoId =
            this.destinoId ||
            (await this.salvarDestino(this.searchSubject.value)).id;
        }

        if (this.produto) {
          dados.produtoId = this.produto.id;
        }

        await this._produtosApi.salvarMovimentacao(this._validarDados(dados));

        if (this.modal) {
          this.modal.close(dados);
        }

        this._notificationsService.success(
          'Movimentação adicionada com sucesso',
        );
      },
    );
  }

  async salvarDestino(
    str: string | undefined,
  ): Promise<DestinoDropdown> {
    if (!str || !str.trim()) {
      throw { message: 'O destino não pode estar em branco' };
    }

    return this._produtosApi.salvarDestino(str);
  }

  async onSaveNew(
    str: string | undefined,
  ) {
    const resultados = this.resultadosSubject.value;

    const destino = resultados.find(res => res.descricao === str.trim())
      || await this._asyncTemplate.wrapUserFeedback(
        'Erro ao salvar novo destino',
        async () => {
          const r = await this.salvarDestino(str.trim());

          this._notificationsService.success('Destino salvo com sucesso!');

          this.forceSearch.next(undefined);

          await this._esperarProximaConsulta();

          return r;
        },
      );

    this.searchSubject.next('');
    this.destinoId = destino.id;

  }

  async apagarDestino(id: number) {
    const confirmation = await Swal.fire({
      title: 'Confirmação de exclusão',
      text: 'Tem certeza que quer apagar o destino de movimentação?',
      type: 'question',
      showCancelButton: true,
      confirmButtonText: 'Sim. Apague',
      cancelButtonText: 'Cancelar',
    });

    if (!confirmation.value) {
      return;
    }

    await this._asyncTemplate.wrapUserFeedback(
      'Erro ao apagar destino',
      () => this._produtosApi.apagarDestinoMovimentacao(id),
    );

    this.forceSearch.next(undefined);
    await this._esperarProximaConsulta();
    this.destinoId = undefined;
  }

  private _esperarProximaConsulta() {
    return this.todosResultado$.pipe(take(2)).toPromise();
  }

  async altereONome(destinoId: number) {
    const destino: DestinoDropdown = this.resultadosSubject.value.find(it => it.id === destinoId);

    const input = await Swal.fire({
      title: 'Modifique o nome do destino',
      input: 'text',
      inputValue: destino.descricao,
      showCancelButton: true,
      confirmButtonText: 'Salvar',
      cancelButtonText: 'Cancelar',
    });

    const value: string = String(input.value || '').trim();

    if (!value) {
      return;
    }

    const destinoSalvo = await this._asyncTemplate.wrapUserFeedback(
      'Erro ao salvar destino',
      () => this._produtosApi.atualizarDestino(destino.id, {
        id: destino.id,
        descricao: value,
      }),
    );

    this.forceSearch.next(undefined);
    await this._esperarProximaConsulta()
    this.destinoId = destinoSalvo.id;
  }

  private _validarDados(data: Partial<MovimentoEstoque>): MovimentoEstoque {
    let errorMsg: string | undefined;

    if (data.quantidadeMovimentada == null) {
      errorMsg = 'Quantidade movimentada não preenchida';
    }

    if (data.quantidadeMovimentada === 0) {
      errorMsg = 'Quantidade movimentada não pode ser zero';
    }

    if (data.tipoMovimento == null) {
      errorMsg = 'Tipo de movimentação não preenchida';
    }

    if (errorMsg) {
      throw { message: errorMsg };
    }

    return data as MovimentoEstoque;
  }
}
