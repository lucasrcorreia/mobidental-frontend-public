import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal, NgbDateParserFormatter, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { AutoCompletePatientList, PatientModel } from '../../../../core/models/patients/patients.model';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { UtilsService } from '../../../../services/utils/utils.service';
import { CONSTANTS } from '../../../../core/constants/constants';
import { DetachedParcel } from './models/detached-parcel';
import { CustomDatepickerI18n, I18n } from '../../../../shared/ngb-datepicker/datepicker-i18n';
import { NgbDateFRParserFormatter } from '../../../../shared/ngb-datepicker/ngb-date-fr-parser-formatter';
import { ShortListItem } from '../../../../core/models/forms/common/common.model';
import { NotificationsService } from 'angular2-notifications';
import { GlobalSpinnerService } from '../../../../lib/global-spinner.service';
import { descricoesPagamentos, FormaPagamento } from '../../../../api/model/forma-pagamento';
import { getTipoParcelaInfo, TipoParcela } from '../../../../api/model/tipo-parcela';

const DEFAULT_OCCURRENCY: string = 'MENSALMENTE';
const OCCURRENCYS: { label: string, value: string }[] = [
    { label: 'Semanalmente', value: 'SEMANALMENTE' },
    { label: 'Quinzenalmente', value: 'QUINZENALMENTE' },
    { label: 'Mensalmente', value: DEFAULT_OCCURRENCY }
];

export type DetachedParcelOrigin = 'contas-a-receber' | 'conta-corrente';

@Component({
    selector: 'app-detached-parcel',
    templateUrl: './detached-parcel.component.html',
    styleUrls: ['./detached-parcel.component.scss'],
    providers: [
        I18n,
        { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
        { provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
    ],
})
export class DetachedParcelComponent implements OnInit {

    readonly defaultOrigin = 'conta-corrente';

    @Input() patient?: PatientModel;
    @Input() origin?: DetachedParcelOrigin = this.defaultOrigin;

    readonly occurrencys = OCCURRENCYS;
    readonly detachedParcelTypes: { label: string, value: string }[];
    readonly paymentMethods: { label: string, value: string }[];
    repeatDeteachedParcel = false;
    detachedParcel: DetachedParcel = { tipoRecorrencia: DEFAULT_OCCURRENCY };
    covenants: ShortListItem[];

    typeaheadValue = '';
    formatMatches = (value: AutoCompletePatientList) => value.pessoaNome;
    search = (text$: Observable<string>) =>
        text$.pipe(
            debounceTime(200),
            distinctUntilChanged(),
            switchMap(term =>
                term.length < 3
                    ? []
                    : this._fetchPatients(term),
            ),
        );

    constructor(
        public readonly _activeModal: NgbActiveModal,
        public readonly _service: UtilsService,
        private readonly _spinner: GlobalSpinnerService,
        private readonly _notifications: NotificationsService
    ) {
        this.detachedParcelTypes = Object.values(TipoParcela)
            .filter(type => type != TipoParcela.PARCELA_TRATAMENTO)
            .map(type => {
                return { label: getTipoParcelaInfo(type).descricao, value: type };
            });
        this.paymentMethods = Object.values(FormaPagamento).map(method => {
            return { label: descricoesPagamentos[method], value: method };
        });
    }

    ngOnInit() {
        this._initPatientInformationsIfExists();
        this._initCovenantDropdown();
    }

    get isDefaultOrigin(): boolean {
        return this.origin === this.defaultOrigin;
    }

    private _initPatientInformationsIfExists() {
        if (this.patient) {
            this.detachedParcel.pacienteId = this.patient.id;
            this.typeaheadValue = this.patient.pessoaNome;
        }
    }


    private async _initCovenantDropdown() {
        const covenants = await this._service.httpGET(CONSTANTS.ENDPOINTS.patients.treatment.convenant).toPromise();
        this.covenants = covenants.body as ShortListItem[];
    }

    private _fetchPatients(term: string) {
        return this._service
            .httpGET(CONSTANTS.ENDPOINTS.patients.general.autoComplete + term)
            .pipe(map(data => data.body));
    }

    selectPatient(patient) {
        this.detachedParcel.pacienteId = patient.item.id;
    }

    async save() {
        if (!this.repeatDeteachedParcel) {
            this.detachedParcel.quantidadeOcorrencia = null;
            this.detachedParcel.tipoRecorrencia = null;
        }

        const loadingToken = this._spinner.loadingManager.start();
        const detachedParcel = { ...this.detachedParcel };
        detachedParcel.quantidadeOcorrencia = detachedParcel.quantidadeOcorrencia ? +detachedParcel.quantidadeOcorrencia : null;
        detachedParcel.dataVencimento = this._service.parseDateFromDatePicker(detachedParcel.dataVencimento);

        try {
            await this._service.httpPOST(CONSTANTS.ENDPOINTS.patients.financial.newDetachedParcelToReceived, detachedParcel)
                .toPromise();
            this._notifications.success('Parcela Avulsa cadastrada com sucesso');

            this._activeModal.close(true);
        } catch (e) {
            this._notifications.error('Ocorreu um erro ao cadastrar a Parcela Avulsa');
        } finally {
            loadingToken.finished();
        }

    }

}
