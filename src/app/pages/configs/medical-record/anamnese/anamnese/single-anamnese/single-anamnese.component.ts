import { Component, OnInit } from '@angular/core';
import { AnamneseService } from '../../service/anamnese.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AnamneseModel } from '../../models/AnamneseModel';
import { TIPO_ALERTA_PERGUNTA_ANAMNESE_TRANSLATE } from '../../models/enums/TipoAlertaPerguntaAnamnese';
import { TIPO_PERGUNTA_ANAMNESE_TRANSLATE } from '../../models/enums/TipoPerguntaAnamnese';
import { AtivoInativo } from '../../models/enums/AtivoInativo';
import { DragulaService } from 'ng2-dragula';
import { CreateQuestionComponent } from '../create-question/create-question.component';
import { AnamnesePerguntaModel } from '../../models/AnamnesePerguntaModel';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UtilsService } from '../../../../../../services/utils/utils.service';
import { NotificationsService } from 'angular2-notifications';
import { AnamneseContext } from '../strategy/AnamneseContext';
import { AnamneseEdition } from '../strategy/classes/AnamneseEdition';
import { AnamneseCreation } from '../strategy/classes/AnamneseCreation';
import { AnamneseStrategy } from '../strategy/AnamneseStrategy';

const IMPOSSIBLE_ID = 'novo';

@Component({
  selector: 'app-single-anamnese',
  templateUrl: './single-anamnese.component.html',
  styleUrls: ['./single-anamnese.component.scss'],
  providers: [DragulaService],
})
export class SingleAnamneseComponent implements OnInit {

  readonly questionAlert = TIPO_ALERTA_PERGUNTA_ANAMNESE_TRANSLATE;
  readonly questionType = TIPO_PERGUNTA_ANAMNESE_TRANSLATE;
  readonly status = AtivoInativo;

  anamnese: AnamneseModel = {
    nome: '',
    perguntas: []
  };

  constructor(private readonly _route: ActivatedRoute,
              private readonly _service: UtilsService,
              private readonly _notificationsService: NotificationsService,
              private readonly _anamneseService: AnamneseService,
              private readonly _dragulaService: DragulaService,
              private readonly _modalService: NgbModal,
              private readonly _router: Router) {
    this._dragulaService.createGroup('ANAMNESES', {
      revertOnSpill: true
    });
  }

  ngOnInit() {
    this._initAnamnese();
  }

  async newQuestion(questionModel?: AnamnesePerguntaModel) {
    const strategy: AnamneseStrategy = questionModel ? new AnamneseEdition({...questionModel}) : new AnamneseCreation();
    const context: AnamneseContext = new AnamneseContext(strategy);

    const modalRef = this._modalService.open(CreateQuestionComponent, {size: 'lg'});
    modalRef.componentInstance.question = context.getQuestion();
    modalRef.componentInstance.title = context.getTitle();

    const question: AnamnesePerguntaModel = await modalRef.result;

    if (question) {
      context.workWithQuestion(this.anamnese, question, this.anamnese.perguntas.indexOf(questionModel));
      this._notificationsService.success(`Pergunta ${context.getOperation()} com sucesso.`);
    }

  }

  async save() {
    try {
      this._service.loading(true);
      const {id} = await this._anamneseService.save(this.anamnese);
      this._notificationsService.success('Modelo salvo.');

      this._router.navigate(['..'], {relativeTo: this._route});
    } catch (e) {
      this._notificationsService.error('Ocorreu um erro ao salvar o Modelo.');
    } finally {
      this._service.loading(false);
    }
  }

  onDrag(questions: AnamnesePerguntaModel[]) {
    questions.forEach((question: AnamnesePerguntaModel, index: number) => {
      question.posicao = index;
    });
  }

  onStatusChange(question: AnamnesePerguntaModel, $event: boolean) {
    question.status = $event ? AtivoInativo.ATIVO : AtivoInativo.INATIVO;
  }

  private async _initAnamnese() {
    const anamneseId = this._route.snapshot.params.id;
    if (anamneseId !== IMPOSSIBLE_ID) {
      this.anamnese = await this._anamneseService.getOne(anamneseId);
      this.anamnese.perguntas = this.anamnese.perguntas.sort((n1, n2) => n1.posicao - n2.posicao);
    }
  }
}
