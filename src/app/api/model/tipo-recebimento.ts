export enum TipoRecebimento {
  A_PAGAR = 'A_PAGAR',
  PAGO = 'PAGO',
  ATRASADO = 'ATRASADO',
}

export const descricoesRecebimentos: { [k in TipoRecebimento]: string } = {
  A_PAGAR: 'A pagar',
  ATRASADO: 'Atrasado',
  PAGO: 'Recebido',
};
