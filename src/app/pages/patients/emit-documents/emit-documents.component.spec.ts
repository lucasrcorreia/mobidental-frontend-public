import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmitDocumentsComponent } from './emit-documents.component';

describe('PrintComponent', () => {
  let component: EmitDocumentsComponent;
  let fixture: ComponentFixture<EmitDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmitDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmitDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
