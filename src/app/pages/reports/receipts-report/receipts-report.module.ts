import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReceiptsReportRoutingModule } from './receipts-report-routing.module';
import { ReceiptsReportComponent } from './receipts-report.component';
import { NgxTranslateModule } from '../../../core/translate/translate.module';
import { FormsModule } from '@angular/forms';
import { NgbDatepickerModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { InputMaskModule } from 'racoon-mask-raw';
import { ReportsModule } from '../reports.module';
import { NgxSelectModule } from 'ngx-select-ex';

@NgModule({
	declarations: [
		ReceiptsReportComponent,
	],
	imports: [
		CommonModule,
		ReceiptsReportRoutingModule,
		NgxTranslateModule,
		FormsModule,
		NgbDatepickerModule,
		InputMaskModule,
		ReportsModule,
		NgbTypeaheadModule,
		NgxSelectModule,
	],
})
export class ReceiptsReportModule {}
