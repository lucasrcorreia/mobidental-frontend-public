export enum TipoAlertaPerguntaAnamnese {
  ALERTA_SIM = 'ALERTA_SIM',
  ALERTA_NAO = 'ALERTA_NAO',
  SEM_ALERTA = 'SEM_ALERTA',
}

export const TIPO_ALERTA_PERGUNTA_ANAMNESE_TRANSLATE: { [key in TipoAlertaPerguntaAnamnese]: string } = {
  ALERTA_SIM: 'Alerta se sim',
  ALERTA_NAO: 'Alerta se não',
  SEM_ALERTA: 'Sem alerta',
};
