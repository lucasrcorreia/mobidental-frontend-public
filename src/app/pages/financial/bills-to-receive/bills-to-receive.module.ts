import { SharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BillsToReceiveRoutingModule } from './bills-to-receive-routing.module';
import { BillsToReceiveComponent } from './bills-to-receive.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxCurrencyModule } from 'ngx-currency';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxTranslateModule } from '../../../core/translate/translate.module';
import { PipesModule } from '../../../core/pipes/pipes.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { DirectivesModule } from '../../../core/directive/directives.module';
import { BillsToReceiveFiltersComponent } from './bills-to-receive-filters/bills-to-receive-filters.component';
import { ContasModule } from '../../../contas/contas.module';
import { PatientsModule } from '../../patients/patients.module';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  declarations: [
    BillsToReceiveComponent,
    BillsToReceiveFiltersComponent,
  ],
  imports: [
    CommonModule,
    NgSelectModule,
    NgxTranslateModule,
    NgxCurrencyModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    SimpleNotificationsModule,
    SharedModule,
    PipesModule,
    DirectivesModule,
    BillsToReceiveRoutingModule,
    ContasModule,
    PatientsModule,
    ComponentsModule,
  ],
})
export class BillsToReceiveModule {}
