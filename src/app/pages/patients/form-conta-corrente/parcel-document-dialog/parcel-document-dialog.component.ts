import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { CPF_LENGTH } from "../../../../lib/input-masks";
import { MaskFormatService } from "../../../../services/utils/mask-format.service";
import { ContaApiService, ContaModel } from "../../../../api/conta-api.service";
import { LoginService } from "../../../../auth/login.service";
import { PatientModel } from "../../../../core/models/patients/patients.model";

export enum ParcelDocumentType {
	BOOKLET,
	PROMISSORY,
}

export interface ParcelDocumentModel {
	name?: string;
	cpf?: string;
	address?: string;
	neighborhood?: string;
	zipCode?: string;
	clinicName?: string;
	clinicAddress?: string;
	clinicCpfCnpj?: string;
	clinicCity?: string;
}

@Component({
	selector: 'app-parcel-document-dialog',
	templateUrl: './parcel-document-dialog.component.html',
	styleUrls: ['./parcel-document-dialog.component.scss']
})
export class ParcelDocumentDialogComponent implements OnInit {

	@Input() parcelDocumentType: ParcelDocumentType;
	@Input() patientModel?: PatientModel;

	title: string;
	confirmText: string;
	readonly CNPJ_LENGTH = 18;

	parcelDocument: ParcelDocumentModel = {};

	constructor(private readonly _activeModal: NgbActiveModal,
				private readonly _maskFormatService: MaskFormatService,
				private readonly _contaApiService: ContaApiService,
				private readonly _loginService: LoginService) {
		this._initConta();
	}

	ngOnInit() {
		switch (this.parcelDocumentType) {
			case ParcelDocumentType.BOOKLET:
				this.title = 'Emitir Carnês';
				this.confirmText = 'Imprimir Carnês';
				break;
			case ParcelDocumentType.PROMISSORY:
				this.title = 'Emitir Promissórias';
				this.confirmText = 'Imprimir Promissórias';
				break;
			default:
				break;
		}
		this._initPatient();
	}

	async _initConta() {
		const { currentConta } = this._loginService;
		if (currentConta) {
			const contaModel = await this._contaApiService.fetchContaCompleta(currentConta.perfilContaModuloContaId);
			this.parcelDocument.clinicCpfCnpj = contaModel.cpfCnpj;
			this.parcelDocument.clinicName = contaModel.razaoSocial;
			this.parcelDocument.clinicAddress = contaModel.endereco +
					(contaModel.numero ? ' N-' + contaModel.numero : contaModel.complemento) +
					' ' + contaModel.bairro + ' ' + contaModel.cidade + ' - ' + contaModel.estado + ', ' + 'CEP: ' + contaModel.cep;
			this.parcelDocument.clinicCity = contaModel.cidade;
		}
	}

	async _initPatient() {
		if (this.patientModel) {
			this.parcelDocument.name = this.patientModel.pessoaNome;
			this.parcelDocument.cpf = this.patientModel.pessoaCpfCnpj;
			this.parcelDocument.neighborhood = this.patientModel.pessoaBairro;
			this.parcelDocument.address = this.patientModel.pessoaEndereco + (this.patientModel.pessoaNumeroEndereco ? ', N-' + this.patientModel.pessoaNumeroEndereco : '');
			this.parcelDocument.zipCode = this.patientModel.pessoaCep;
		}
	}

	clinicCpfCnpjMask() {
		if (!this.parcelDocument.clinicCpfCnpj) {
			return;
		}
		const cpfCnpj = this.parcelDocument.clinicCpfCnpj.replace(/\./g, '').replace(/-/g, '').replace(/\//g, '');
		this.parcelDocument.clinicCpfCnpj = cpfCnpj.length <= CPF_LENGTH ? this._maskFormatService.getFormatedCpf(cpfCnpj) :
				this._maskFormatService.getFormatedCnpj(cpfCnpj);
	}


	save() {
		this._activeModal.close(this.parcelDocument);
	}

	close() {
		this._activeModal.close();
	}

}
