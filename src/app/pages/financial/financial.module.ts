import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { FinancialRoutingModule } from "./financial-routing.module";
import { FluxoCaixaModule } from "./fluxo-caixa/fluxo-caixa.module";
import { SellReportModule } from "./sell-report/sell-report.module";
import { NgbTooltipModule } from "@ng-bootstrap/ng-bootstrap";

@NgModule({
	declarations: [],
	imports: [
		CommonModule,
		FinancialRoutingModule,
		FluxoCaixaModule,
		SellReportModule,
		NgbTooltipModule,
	],
})
export class FinancialModule {}
