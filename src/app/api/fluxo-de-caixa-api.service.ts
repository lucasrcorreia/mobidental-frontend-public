import { Inject, Injectable } from '@angular/core';
import { API_CONFIGURATION, ApiConfiguration } from './api-configuration';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import { TipoParcela } from './model/tipo-parcela';
import { FormaPagamento } from './model/forma-pagamento';
import { ListaFluxoCaixaReport } from '../pages/financial/fluxo-caixa/models/lista-fluxo-caixa-report';

export interface FluxoDeCaixaEntrada {
	cliente: string;
	observacao: string;
	tipoParcela: TipoParcela;
	dataVencimento: string;
	valor: number;
	formaDeRecebimento: FormaPagamento;
	usuario: string;
	id: number;
	dataEmissao: string;
	dataRecebimento: string;
	descricao: string;
}

export interface FluxoDeCaixaSaida {
	observacao: string;
	dataPagamento: string;
	dataVencimento: string;
	valor: number;
	formaDeRecebimento: FormaPagamento;
	usuario: string;
	id: number;
	dataCompetencia: string;
	numeroDocumento: unknown;
	dataEmissao: string;
	descricao: string;
}

export interface FluxoDeCaixaTotais {
	totalEntradas: number;
	totalSaidas: number;
	saidas: {
		totalDinheiro: number;
		totalCartaoCredito: number;
		totalCartaoDebito: number;
		totalCheque: number;
		totalBoleto: number;
		totalDebitoAutomatico: number;
		totalTransferenciaBancaria: number;
		permuta: number;
	},
	entradas: {
		totalDinheiro: number;
		totalCartaoCredito: number;
		totalCartaoDebito: number;
		totalCheque: number;
		totalBoleto: number;
		totalDebitoAutomatico: number;
		totalTransferenciaBancaria: number;
		permuta: number;
	}
}

export interface FluxoDeCaixa {
	entradas: FluxoDeCaixaEntrada[];
	saidas: FluxoDeCaixaSaida[];
	totais: FluxoDeCaixaTotais;
}

@Injectable({
	providedIn: 'root',
})
export class FluxoDeCaixaApiService {

	constructor(
			@Inject(API_CONFIGURATION) private readonly _config: ApiConfiguration,
			private readonly _http: HttpClient,
	) {
	}

	listarFluxoDeCaixa(dataInicio: Date, dataFim: Date): Promise<FluxoDeCaixa> {
		return this._http
				.post<FluxoDeCaixa>(`${ this._config.apiBasePath }/fluxoCaixa/obterFluxoDeCaixa`, {
					dataInicio: dataInicio ? moment(dataInicio).format('DD/MM/YYYY') : undefined,
					dataFim: dataFim ? moment(dataFim).format('DD/MM/YYYY') : undefined,
				})
				.toPromise();
	}

	getListaFluxoCaixaReport(body: { entradas: FluxoDeCaixaEntrada[], saidas: FluxoDeCaixaSaida[] }): Promise<ListaFluxoCaixaReport[]> {
		return this._http
				.post<ListaFluxoCaixaReport[]>(`${ this._config.apiBasePath + '/fluxoCaixa/buildListaFluxoDeCaixaReport' }`, body)
				.toPromise();
	}
}
