export const memoizePromiseSuccess = <T, R>(
  fn: (arg: T) => Promise<R>,
): ((arg: T) => Promise<R>) => {
  const map = new Map<T, Promise<R>>();
  const tries = new Map<T, number>();

  return (arg) => {
    if (!map.has(arg) && (tries.get(arg) || 0) < 5) {
      const prom = fn(arg);

      tries.set(arg, (tries.get(arg) || 0) + 1);

      map.set(arg, prom);

      prom.catch(() => {
        map.delete(arg);
      });
    }

    return map.get(arg);
  };
};
