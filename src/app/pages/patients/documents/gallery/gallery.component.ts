import {
	Component,
	OnInit,
	OnDestroy,
	ViewChild,
	ElementRef,
	ContentChildren,
	QueryList,
} from "@angular/core";
import * as PhotoSwipe from "photoswipe";
import * as PhotoSwipeDefaultUi from "photoswipe/dist/photoswipe-ui-default";
import { GalleryItemDirective, AppGalleryPhotoswipeItem } from "./gallery-item.directive";
import { DocumentsCommonService } from "../documents-common.service";

const ZOOM_TRANSITION_TIME = 200;

@Component({
	selector: "app-gallery",
	templateUrl: "./gallery.component.html",
	styleUrls: ["./gallery.component.scss"],
})
export class GalleryComponent implements OnInit, OnDestroy {
	@ViewChild("pswpContainer") pspwContainerRef: ElementRef;
	@ContentChildren(GalleryItemDirective) images: QueryList<GalleryItemDirective>;

	private _pspwInstancePromise?: Promise<PhotoSwipe<PhotoSwipeDefaultUi.Options>>;
	private _pspwInstance?: PhotoSwipe<PhotoSwipeDefaultUi.Options>;

	private _internalZoom?: number;

	constructor(private readonly _docService: DocumentsCommonService) { }

	open(index?: number) {
		this._initInstance(index).then((instance) => instance.init());
	}

	ngOnInit(): void { }

	zoomIn() {
		const zoomFactor = 1.5;
		setTimeout(() => this._changeZoom((curr) => Math.min(curr * zoomFactor, zoomFactor ** 4)));
	}

	zoomOut() {
		const zoomFactor = 0.75;
		setTimeout(() => this._changeZoom((curr) => curr * zoomFactor));
	}

	private _changeZoom(zoomOrModifier: number | ((val: number) => number)) {
		const { _pspwInstance: inst, _internalZoom } = this;

		if (!inst) return;

		const base = inst.getZoomLevel();

		const next = Math.max(
			typeof zoomOrModifier === "number" ? zoomOrModifier : zoomOrModifier(base),
			inst.currItem.initialZoomLevel
		);

		this._internalZoom = next;

		inst.zoomTo(
			next,
			{ x: inst.viewportSize.x / 2, y: inst.viewportSize.y / 2 },
			ZOOM_TRANSITION_TIME
		);
	}

	ngOnDestroy(): void {
		if (this._pspwInstance != undefined) {
			this._pspwInstance.close();
		}

		this._pspwInstance = undefined;
	}

	downloadCurrent() {
		if (!this._pspwInstance) return;

		const currItem: AppGalleryPhotoswipeItem = this._pspwInstance
			.currItem as AppGalleryPhotoswipeItem;

		this._docService.download(currItem.document);
	}

	private async _initInstance(index?: number): Promise<PhotoSwipe<PhotoSwipeDefaultUi.Options>> {
		if (this._pspwInstance == null) {
			if (this._pspwInstancePromise == null) {
				this._pspwInstancePromise = this._buildInstance(index);
			}

			this._pspwInstance = await this._pspwInstancePromise;
			this._pspwInstancePromise = undefined;
		}

		return this._pspwInstance;
	}

	private async _buildInstance(index?: number): Promise<PhotoSwipe<PhotoSwipeDefaultUi.Options>> {
		const imageItems: PhotoSwipe.Item[] = await Promise.all(
			this.images.toArray().map((item) => item.toPhotoSwipeItem())
		);

		const inst = new PhotoSwipe(
			this.pspwContainerRef.nativeElement,
			PhotoSwipeDefaultUi,
			imageItems,
			{
				bgOpacity: 0.85,
				index,
				loop: false,
				history: false,
				closeOnScroll: false,
				allowPanToNext: false,
				isClickableElement: (el: HTMLElement) => {
					return el.tagName === "BUTTON";
				},
				tapToClose: false,
				clickToCloseNonZoomable: false,
			}
		);

		inst.listen("preventDragEvent", ({ target }, isDown, preventObj) => {
			if (target instanceof HTMLElement && target.tagName === "BUTTON") throw Error("not a error");
		});
		inst.listen("destroy", () => {
			if (this._pspwInstance === inst) this._pspwInstance = undefined;
		});

		this._pspwInstance = inst;
		return inst;
	}
}
