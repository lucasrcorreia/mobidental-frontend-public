import {
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	ViewChild,
} from "@angular/core";
import { FormGroup } from "@angular/forms";
import { UtilsService } from "../../../../services/utils/utils.service";
import { ProstheticServiceObjectModel } from "../../../../core/models/tools/prosthetic/prostheticService.model";
import { FormServiceProsthetic } from "../../../../core/forms/prosthetic";
import { ShortListItem } from "../../../../core/models/forms/common/common.model";
import { CONSTANTS } from "../../../../core/constants/constants";
import { forkJoin, Observable, of } from "rxjs";
import { ProstheticToothModel } from "../../../../core/models/tools/prosthetic/tooth.model";
import {
	AutoCompletePatientList,
	PatientModel,
} from "../../../../core/models/patients/patients.model";
import {
	catchError,
	debounceTime,
	distinctUntilChanged,
	map,
	switchMap,
	tap,
} from "rxjs/operators";
import { CustomDatepickerI18n, I18n } from "../../../../shared/ngb-datepicker/datepicker-i18n";
import { NgbDateParserFormatter, NgbDatepickerI18n } from "@ng-bootstrap/ng-bootstrap";
import { NgbDateFRParserFormatter } from "../../../../shared/ngb-datepicker/ngb-date-fr-parser-formatter";
import { classToPlain } from "class-transformer";
import { GlobalSpinnerService } from "../../../../lib/global-spinner.service";
import { NotificationsService } from "angular2-notifications";
import Swal from "sweetalert2";
import { AsyncTemplate } from "../../../../../app/lib/helpers/async-template";

const STATUS_ABERTO = "ABERTO";

@Component({
	selector: "app-form-service-prosthetic",
	templateUrl: "./form-service.component.html",
	styleUrls: [
		"./form-service.component.scss",
		"./../../../../../assets/icon/icofont/css/icofont.scss",
	],
	providers: [
		I18n,
		{ provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
		{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
	],
})
export class FormServiceComponent implements OnInit, OnChanges {
	@Input() modalId: string;
	@Input() prosthetic: ProstheticServiceObjectModel;

	@Output() resultModal = new EventEmitter<boolean>();

	@ViewChild("patientSearch") patientSearch: ElementRef;

	formProsthetic: FormGroup;
	listProstheticService: Array<ShortListItem>;
	listProsthetic: Array<ShortListItem>;
	listProfessional: Array<ShortListItem>;
	listTooth: Array<ProstheticToothModel>;

	searchingProstheticTxt?: string;

	searching = false;
	searchFailed = false;
	typeaheadValue = "";

	formatMatches = (value: AutoCompletePatientList) => value.pessoaNome;
	search = (text$: Observable<string>) =>
		text$.pipe(
			debounceTime(200),
			distinctUntilChanged(),
			tap(() => (this.searching = true)),
			switchMap((term) =>
				term.length < 3
					? []
					: this.findPatients(term).pipe(
							tap((data: AutoCompletePatientList[]) => {
								this.searchFailed = false;
							}),
							catchError(() => {
								this.searchFailed = true;
								return of([]);
							})
					  )
			),
			tap(() => (this.searching = false))
		);

	constructor(
		public service: UtilsService,
		private readonly _globalSpinnerService: GlobalSpinnerService,
		private readonly _asyncTemplate: AsyncTemplate,
		private readonly _notificationsService: NotificationsService
	) {}

	ngOnInit() {}

	findPatients(value: string) {
		return this.service
			.httpGET(CONSTANTS.ENDPOINTS.patients.general.autoComplete + value)
			.pipe(map((data) => data.body));
	}

	onSelectPatient(patient) {
		this.formProsthetic.patchValue({
			pacienteId: patient.item.id,
		});
	}

	initDropdowns() {
		this.service.loading(true);

		const dropdownServices = this.service.httpGET(
			CONSTANTS.ENDPOINTS.tools.prosthetic.dropdownServices
		);
		const dropdownProsthetic = this.service.httpGET(
			CONSTANTS.ENDPOINTS.tools.prosthetic.dropdownProsthetic
		);
		const dropdownProfessional = this.service.httpGET(CONSTANTS.ENDPOINTS.professional.active);
		const dropdownTooth = this.service.httpGET(CONSTANTS.ENDPOINTS.tools.prosthetic.dropdownTooth);

		forkJoin([dropdownServices, dropdownProsthetic, dropdownProfessional, dropdownTooth]).subscribe(
			(list) => {
				this.listProstheticService = list[0].body as Array<ShortListItem>;
				this.listProsthetic = list[1].body as Array<ShortListItem>;
				this.listProfessional = list[2].body as Array<ShortListItem>;
				this.listTooth = list[3].body as Array<ProstheticToothModel>;

				if (this.prosthetic.dataPrevistaEntrega) {
					this.prosthetic.dataPrevistaEntrega = this.service.parseDatePicker(
						this.prosthetic.dataPrevistaEntrega
					);
				}

				if (this.prosthetic.pacienteId) {
					this.service
						.httpGET(CONSTANTS.ENDPOINTS.patients.general.findOne + this.prosthetic.pacienteId)
						.subscribe(
							(patient) => {
								this.typeaheadValue = (patient.body as PatientModel).pessoaNome;
								const tooths = this.prosthetic.dentes;
								this.prosthetic.dentes = null;
								this.formProsthetic = this.service.createForm(
									FormServiceProsthetic,
									this.prosthetic
								);
								this.formProsthetic.get("dentes").setValue(tooths);

								this.service.loading(false);
							},
							(err) => {
								this.service.loading(false);
								this.service.notification.error(
									this.service.translate.instant("COMMON.ERROR.SEARCH"),
									err.error.error
								);
							}
						);
				} else {
					const tooths = this.prosthetic.dentes;
					this.prosthetic.dentes = null;
					this.formProsthetic = this.service.createForm(FormServiceProsthetic, this.prosthetic);
					this.formProsthetic.get("dentes").setValue(tooths);

					this.service.loading(false);
				}
			},
			(err) => {
				this.service.loading(false);
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			}
		);
	}

	ngOnChanges() {
		this.initDropdowns();
	}

	closeModal() {
		this.resultModal.emit(false);
		this.service.closeModal(this.modalId, null);
	}

	async addService(name: string) {
		await this._asyncTemplate.wrapUserFeedback("Ocorreu um erro ao cadastrar o novo serviço", () =>
			this._updateService(null, name)
		);

		this._notificationsService.success("Serviço criado com sucesso!");

		await this._reloadServices();

		this._patchServiceForName(name);
	}

	async alterarNomeService() {
		const currentId: number | undefined | null = this.formProsthetic.get("itemServicoProteticoId")
			.value;

		if (!currentId) {
			this._notificationsService.info("Escolha um serviço já cadastrado para abrir opções");
			return;
		}

		const currentValue = this.listProstheticService.find((service) => service.id === currentId)!;

		const input = await Swal.fire({
			title: "Modifique o nome do serviço",
			input: "text",
			inputValue: currentValue.nome,
			showCancelButton: true,
			confirmButtonText: "Salvar",
			cancelButtonText: "Cancelar",
		});

		const normalized = String(input.value || "").trim();
		if (!normalized) return;

		await this._asyncTemplate.wrapUserFeedback("Ocorreu um erro ao atualizar o serviço", () =>
			this._updateService(currentId, normalized)
		);

		this._notificationsService.success("Serviço alterado com sucesso!");

		await this._reloadServices();

		this._patchServiceForName(normalized);
	}

	private async _updateService(id: number | null, nome: string) {
		return this.service
			.httpPOST(CONSTANTS.ENDPOINTS.tools.prosthetic.saveDropdown, { id, nome })
			.toPromise();
	}

	private async _reloadServices() {
		const innerLoading = this._globalSpinnerService.loadingManager.start();

		try {
			const data = await this.service
				.httpGET(CONSTANTS.ENDPOINTS.tools.prosthetic.dropdownServices)
				.toPromise();

			this.listProstheticService = data.body as ShortListItem[];
		} catch (err) {
			this.service.notification.error(
				this.service.translate.instant("COMMON.ERROR.SEARCH"),
				err.error.error
			);
		} finally {
			innerLoading.finished();
		}
	}

	private _patchServiceForName(name: string) {
		const newValue = this.listProstheticService.find((it) => it.nome === name);

		this.formProsthetic.patchValue({
			itemServicoProteticoId: newValue.id,
		});
	}

	private _loading() {
		return this._globalSpinnerService.loadingManager.start();
	}

	async deleteItemDrop(id: number) {
		const confirmation = await Swal.fire({
			title: "Apagar serviço",
			text: "Tem certeza que quer apagar esse serviço?",
			type: "question",
			showCancelButton: true,
			confirmButtonText: "Sim, apague!",
			cancelButtonText: "Não, cancele",
		});

		if (!confirmation.value) return;

		const loadingToken = this._loading();

		this.service
			.httpDELETE(CONSTANTS.ENDPOINTS.tools.prosthetic.deleteDropdown + id.toString())
			.subscribe(
				(data) => {
					const innerLoading = this._loading();

					const { value } = this.formProsthetic.get("itemServicoProteticoId");
					if (value && value === id) {
						this.formProsthetic.patchValue({
							itemServicoProteticoId: null,
						});
					}

					this.service.httpGET(CONSTANTS.ENDPOINTS.tools.prosthetic.dropdownServices).subscribe(
						(dataGet) => {
							this.listProstheticService = dataGet.body as Array<ShortListItem>;
						},
						(errGet) => {
							this.service.notification.error(
								this.service.translate.instant("COMMON.ERROR.SEARCH"),
								errGet.error.error
							);
						},
						() => innerLoading.finished()
					);
				},
				(err) => {
					this.service.notification.error(
						this.service.translate.instant("COMMON.ERROR.SEARCH"),
						err.error.error
					);
				},
				() => loadingToken.finished()
			);
	}

	save() {
		if (this.formProsthetic.invalid) {
			this._showFormErrors();
			return;
		}
		this.service.loading(true);
		this.prosthetic = classToPlain(this.formProsthetic.value) as ProstheticServiceObjectModel;

		this.prosthetic.dataPrevistaEntrega = this.service.parseDateFromDatePicker(
			this.prosthetic.dataPrevistaEntrega
		);

		if (!this.prosthetic.statusServicoProtetico) {
			this.prosthetic.statusServicoProtetico = STATUS_ABERTO;
		}

		this.service.httpPOST(CONSTANTS.ENDPOINTS.tools.prosthetic.save, this.prosthetic).subscribe(
			(data) => {
				this.service.loading(false);
				this.resultModal.emit(true);
				this.service.closeModal(this.modalId, null);
			},
			(err) => {
				this.service.loading(false);
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			}
		);
	}

	private _showFormErrors() {
		for (const field in this.formProsthetic.controls) {
			const control = this.formProsthetic.get(field);
			control.markAsTouched({ onlySelf: true });
		}

		if (this.formProsthetic.get("pacienteId").invalid) {
			this.patientSearch.nativeElement.value = "";
		}
	}

	applyCssError(field: string, form: FormGroup) {
		return {
			"has-error": this.formFieldIsInvalid(field, form),
		};
	}

	formFieldIsInvalid(field: string, form: FormGroup): boolean {
		return form.get(field).invalid && form.get(field).touched;
	}
}
