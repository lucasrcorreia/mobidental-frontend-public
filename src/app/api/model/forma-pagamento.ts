export enum FormaPagamento {
  BOLETO = 'BOLETO',
  CARTAO_CREDITO = 'CARTAO_CREDITO',
  CARTAO_DEBITO = 'CARTAO_DEBITO',
  CHEQUE = 'CHEQUE',
  DINHEIRO = 'DINHEIRO',
  DEBITO_AUTOMATICO = 'DEBITO_AUTOMATICO',
  TRANSFERENCIA_BANCARIA = 'TRANSFERENCIA_BANCARIA',
  PERMUTA = 'PERMUTA',
}

export const descricoesPagamentos: { [k in FormaPagamento]: string } = {
  BOLETO: 'Boleto',
  CARTAO_CREDITO: 'Cartão de crédito',
  CARTAO_DEBITO: 'Cartão de débito',
  CHEQUE: 'Cheque',
  DEBITO_AUTOMATICO: 'Débito automático',
  DINHEIRO: 'Dinheiro',
  PERMUTA: 'Permuta',
  TRANSFERENCIA_BANCARIA: 'Transferência bancária',
};
