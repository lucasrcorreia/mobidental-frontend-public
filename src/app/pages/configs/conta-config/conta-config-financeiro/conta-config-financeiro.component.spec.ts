import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContaConfigFinanceiroComponent } from './conta-config-financeiro.component';

describe('ContaConfigFinanceiroComponent', () => {
  let component: ContaConfigFinanceiroComponent;
  let fixture: ComponentFixture<ContaConfigFinanceiroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContaConfigFinanceiroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContaConfigFinanceiroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
