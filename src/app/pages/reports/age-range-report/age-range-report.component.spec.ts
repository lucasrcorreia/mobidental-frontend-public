import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgeRangeReportComponent } from './age-range-report.component';

describe('TreatmentsReportComponent', () => {
  let component: AgeRangeReportComponent;
  let fixture: ComponentFixture<AgeRangeReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgeRangeReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgeRangeReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
