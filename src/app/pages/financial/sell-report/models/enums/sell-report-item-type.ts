export enum SellReportItemType {
  ENTRADA = "ENTRADA",
  SAIDA = "SAIDA",
  VENDA = "VENDA",
}

export const SellReportItemTypeDescription: { [k in SellReportItemType]: string } = {
  ENTRADA: 'Entrada',
  SAIDA: 'Saída',
  VENDA: 'Venda',
};
