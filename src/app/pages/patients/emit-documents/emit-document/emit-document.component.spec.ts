import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmitDocumentComponent } from './emit-document.component';

describe('EmitDocumentComponent', () => {
  let component: EmitDocumentComponent;
  let fixture: ComponentFixture<EmitDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmitDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmitDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
