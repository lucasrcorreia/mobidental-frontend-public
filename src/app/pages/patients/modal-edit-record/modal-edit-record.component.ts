import { Component, OnInit, Input } from '@angular/core';
import { UtilsService } from '../../../services/utils/utils.service';
import { ContentPatientServicesTableModel } from '../../../core/models/patients/procedures/procedures.model';
import { LISTAS } from '../lists';
import { CONSTANTS } from '../../../core/constants/constants';
import { LOCAL_CONSTS } from '../constants';
import * as moment from 'moment';
import { ShortListItem } from '../../../core/models/forms/common/common.model';

@Component({
  selector: 'app-modal-edit-record',
  templateUrl: './modal-edit-record.component.html',
  styleUrls: [
    './modal-edit-record.component.scss',
    './../../../../assets/icon/icofont/css/icofont.scss'
  ]
})
export class ModalEditRecordComponent implements OnInit {
  @Input() procedure: ContentPatientServicesTableModel;

  LISTAS_LOCAIS = new LISTAS(this.service.translate);
  listaStatusTratamentoServico = [];

  professionals: ShortListItem[] = [];

  constructor(public service: UtilsService) {}

  ngOnInit() {
    this._initProfessionalDropdowns();

    setTimeout(() => {
      this.listaStatusTratamentoServico = this.LISTAS_LOCAIS.listaStatusTratamentoServico.filter(
        item => item.value !== LOCAL_CONSTS.NAO_AUTORIZADO
      );
    });
  }

  private async _initProfessionalDropdowns() {
    const professionals = await this.service.httpGET(CONSTANTS.ENDPOINTS.professional.active).toPromise();
    this.professionals = professionals.body as ShortListItem[];
  }

  closeModal() {
    if (this.procedure.dataFinalizacao) {
      this.procedure.dataFinalizacao = this.service.parseDateFromDatePicker(
        this.procedure.dataFinalizacao,
        'DD/MM/YYYY'
      );
    }
    this.service.closeModal('modal-edit-record', null);
  }

  changeStatus() {
    if (this.procedure.statusTratamentoServico === LOCAL_CONSTS.FINALIZADO) {
      this.procedure.dataFinalizacao = this.service.parseDatePicker(
        moment().format('YYYY-MM-DD'),
        'YYYY-MM-DD'
      );
    } else {
      this.procedure.dataFinalizacao = null;
    }

    this.procedure.statusTratamentoServicoDescricao = this.LISTAS_LOCAIS.listaStatusTratamentoServico.find(
      item => item.value === this.procedure.statusTratamentoServico
    ).label;
  }

  save() {
    this.service.loading(true);

    if (this.procedure.dataFinalizacao && this.procedure.dataFinalizacao.year) {
      this.procedure.dataFinalizacao = this.service.parseDateFromDatePicker(
        this.procedure.dataFinalizacao
      );
    }

    this.service
      .httpPOST(
        CONSTANTS.ENDPOINTS.patients.records.saveProcedure,
        this.procedure
      )
      .subscribe(
        data => {
          this.service.loading(false);
          this.service.closeModal('modal-edit-record', this.procedure);
          this.service.notification.success('Tratamento editado com sucesso');
        },
        err => {

          this.service.loading(false);
          this.service.notification.error(
            this.service.translate.instant('COMMON.ERROR.SEARCH'),
            err.error.error
          );
        }
      );
  }
}
