import { NgModule } from '@angular/core';
import { PerfilConfigComponent } from './perfil-config.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: PerfilConfigComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PerfilConfigRoutingModule {}
