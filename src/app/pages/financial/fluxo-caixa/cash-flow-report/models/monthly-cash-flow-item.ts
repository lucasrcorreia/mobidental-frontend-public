export interface MonthlyCashFlowItem {
  mes?: string;
  saldoPrevistoMesAnterior?: number;
  saldoRealizadoMesAnterior?: number;
  recebimentoPrevisto?: number;
  recebimentoRealizado?: number;
  saidaPrevisto?: number;
  saidaRealizado?: number;
  lucroPrejuizoPrevisto?: number;
  lucroPrejuizoRealizado?: number;
}
