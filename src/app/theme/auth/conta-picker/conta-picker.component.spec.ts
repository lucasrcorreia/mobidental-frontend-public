import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContaPickerComponent } from './conta-picker.component';

describe('ContaPickerComponent', () => {
  let component: ContaPickerComponent;
  let fixture: ComponentFixture<ContaPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContaPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContaPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
