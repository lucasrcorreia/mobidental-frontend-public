import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Tab } from '../emit-documents.component';
import { UtilsService } from '../../../../services/utils/utils.service';
import { NotificationsService } from 'angular2-notifications';
import { ShortListItem } from '../../../../core/models/forms/common/common.model';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PdfGeneratorService, PDFMake } from '../../../../services/pdf-generator/pdf-generator.service';
import { MedicineService } from './services/medicine.service';
import { CONSTANTS } from '../../../../core/constants/constants';
import { Medicine } from './models/medicine';
import { Uso, USO_TRANSLATE } from './models/enums/uso';
import { PatientModel } from '../../../../core/models/patients/patients.model';
import { PRESCRIPTION_TYPE_TRANSLATE, PrescriptionType } from './models/enums/prescription-type';
import { CommonPrintService } from '../service/common-print.service';
import { PrintConfiguration } from '../../../configs/medical-record/print/model/print-configuration';
import { PrescriptionMedicine } from './models/prescription';
import { EmQuantidadeMedicamento, QUANTIDADE_TRANSLATE } from './models/enums/em-quantidade-medicamento ';
import { EmUnidadeMedidaMedicamento, MEDIDA_TRANSLATE } from './models/enums/em-unidade-medida-medicamento ';
import { IssuableDocumentType } from '../../../configs/medical-record/document-model/models/enum/IssuableDocumentType';
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { ProfessionalModel } from '../../../../core/models/config/professional/professional.model';
import { MaskFormatService } from '../../../../services/utils/mask-format.service';
import { PrintConfigurationsService } from '../../../configs/medical-record/print/service/print-configurations.service';
import { PrescriptionService } from './services/prescription.service';
import * as moment from 'moment';

const SIMPLE_PRESCRIPTION_TITLE = 'RECEITUÁRIO SIMPLES';
const ESPECIAL_PRESCRIPTION_TITLE = 'RECEITUÁRIO CONTROLE ESPECIAL';
const UNIDADE_MEDIDA = 'unidadeMedida';
const MEDICAMENTO_ID = 'medicamentoId';
const POSOLOGIA = 'posologia';
const QUANTIDADE = 'quantidade';

const ESPECIAL_FOOTER_SIGNATURE = {
  columns: [
    {
      stack: [
        { text: moment().format('L'), alignment: 'center' },
        { canvas: [{ type: 'line', x1: 0, y1: -1, x2: 80, y2: -1, lineWidth: 0.5 }] },
        { text: 'Data', margin: [0, 5, 0, 0] },
      ],
      alignment: 'center',
      margin: [0, 0, 0, 10],
    },
    {
      stack: [
        { text: ' ', alignment: 'center' },
        { canvas: [{ type: 'line', x1: 0, y1: -1, x2: 180, y2: -1, lineWidth: 0.5 }] },
        { text: 'Assinatura do Emitente', margin: [0, 5, 0, 0] },
      ],
      alignment: 'center',
    },
  ],
}

const ESPECIAL_FOOTER_INFOS = {
  columns: [
    {
      table: {
        widths: [250],
        body: [
          [{
            stack: [
              { text: 'Identificação do Comprador', bold: true, margin: [0, 5, 0, 5] },
              'Nome: ',
              { canvas: [{ type: 'line', x1: 35, y1: -1, x2: 250, y2: -1, lineWidth: 0.5 }] },
              {
                columns: [
                  { text: 'Ident: ' },
                  { text: 'Org.Emissor: ' },
                ],
              },
              { canvas: [{ type: 'line', x1: 35, y1: -1, x2: 125, y2: -1, lineWidth: 0.5 }] },
              { canvas: [{ type: 'line', x1: 195, y1: -1, x2: 250, y2: -1, lineWidth: 0.5 }] },
              'End: ',
              { canvas: [{ type: 'line', x1: 25, y1: -1, x2: 250, y2: -1, lineWidth: 0.5 }] },
              {
                columns: [
                  { text: 'Cidade: ' },
                  { text: 'Uf: ' },
                ],
              },
              { canvas: [{ type: 'line', x1: 40, y1: -1, x2: 125, y2: -1, lineWidth: 0.5 }] },
              { canvas: [{ type: 'line', x1: 140, y1: -1, x2: 250, y2: -1, lineWidth: 0.5 }] },
              'Telefone: ',
              { canvas: [{ type: 'line', x1: 50, y1: -1, x2: 250, y2: -1, lineWidth: 0.5 }] },
            ],
          }],
        ],
      },
      margin: [20, 0, 0, 0],
    },
    {
      table: {
        heights: [95],
        widths: [250],
        body: [
          [{
            stack: [
              { text: 'Identificação do Comprador', bold: true, margin: [0, 5, 0, 30] },
              {
                columns: [
                  { text: '' },
                  { text: '/     /', alignment: 'center' },
                ],
              },
              { canvas: [{ type: 'line', x1: 0, y1: -1, x2: 125, y2: -1, lineWidth: 0.5 }] },
              { canvas: [{ type: 'line', x1: 155, y1: -1, x2: 230, y2: -1, lineWidth: 0.5 }] },
              { text: 'Assinatura do Farmacêutico', margin: [0, 10, 0, 0], fontSize: 10 },
            ],
          }],
        ],
      },
      margin: [17, 0, 0, 0],
    },
  ],
}

@Component({
  selector: 'app-emit-prescription',
  templateUrl: './emit-prescription.component.html',
  styleUrls: ['./emit-prescription.component.scss'],
})
export class EmitPrescriptionComponent implements OnInit {

  @Input() patient: PatientModel;
  @Output() tabEmitter = new EventEmitter<{ tab: Tab, issuableDocumentType: IssuableDocumentType }>();
  @Input() printConfiguration: PrintConfiguration;

  readonly usos = Object.values(Uso);
  readonly usoTranslate = USO_TRANSLATE;
  readonly prescriptionTypes = Object.values(PrescriptionType);
  readonly prescriptionTypesTranslate = PRESCRIPTION_TYPE_TRANSLATE;
  readonly usoContinuo = EmQuantidadeMedicamento.USO_CONTINUO;
  readonly medicineAmountTranslate = QUANTIDADE_TRANSLATE;
  readonly medicineMeasureTranslate = MEDIDA_TRANSLATE;

  professionals: ShortListItem[];
  newMedicine: Medicine;
  medicines: Medicine[];
  medicineMeasure = [];
  medicineAmount = [];
  prescriptionForm: FormGroup;

  // Guarda o index do medicamento que ele está buscando para substituir quando criar um novo
  currentMedicineIndex: number;
  // Medicamento que será sendo buscado
  searchingMedicine: string;

  constructor(readonly _service: UtilsService,
              private readonly _fb: FormBuilder,
              private readonly _notificationsService: NotificationsService,
              private readonly _pdfGeneratorService: PdfGeneratorService,
              private readonly _medicineService: MedicineService,
              private readonly _commonPrintService: CommonPrintService,
              private readonly _ngbCalendar: NgbCalendar,
              private readonly _maskFormatService: MaskFormatService,
              private readonly _printConfigurationsService: PrintConfigurationsService,
              private readonly _prescriptionService: PrescriptionService) {
    this.medicineAmount = Object.values(EmQuantidadeMedicamento).map(amount => {
      return { value: amount, name: this.medicineAmountTranslate[amount] }
    });
    this.medicineAmount.unshift({ value: 0, name: 'Nenhum' });
    this.medicineMeasure = Object.values(EmUnidadeMedidaMedicamento).map(measure => {
      return { value: measure, name: this.medicineMeasureTranslate[measure] }
    });
  }

  ngOnInit() {
    this._initDropdowns();
  }

  private async _initDropdowns() {
    this.medicines = await this._medicineService.medicineDropdown();
    const professionals = await this._service.httpGET(CONSTANTS.ENDPOINTS.professional.active).toPromise();
    this.professionals = professionals.body as ShortListItem[];
    this._initForm();
  }

  private _initForm() {
    this.prescriptionForm = this._fb.group({
      id: [null],
      tipoReceita: [PrescriptionType.RECEITUARIO_SIMPLES, Validators.required],
      data: [this._ngbCalendar.getToday(), Validators.required],
      profissionalId: [null, Validators.required],
      pacienteId: [this.patient.id, Validators.required],
      medicamentos: this._fb.array([this._initPrescriptionMedicine({}, true)]),
    });
  }

  private _initPrescriptionMedicine(medicine: Medicine, required = false) {
    return this._fb.group({
      id: [null],
      receitaId: [null],
      medicamentoId: [medicine.id, required ? Validators.required : null],
      posologia: [medicine.posologia, required ? Validators.required : null],
      quantidade: [0],
      unidadeMedida: [null],
    });
  }

  async addNewMedicine() {
    try {
      this._service.loading(true);
      const newMedicine = await this._medicineService.save(this.newMedicine);
      this.newMedicine = null;
      this.medicines.push(newMedicine);
      this.medicineFormArray.setControl(this.currentMedicineIndex, this._initPrescriptionMedicine(newMedicine));
      this._addEmptyMedicineOnArrayIfNecessary();
      this._service.loading(false);
    } catch (e) {
      this._notificationsService.error('Ocorreu um erro ao criar o Medicamento.');
    }
  }

  fillPosologia($event: Medicine, index: number) {
    this.medicineFormArray.at(index).patchValue({ posologia: $event ? $event.posologia : '' });
    this._setRequiredOnMedicine(index);
    this._addEmptyMedicineOnArrayIfNecessary();
  }

  private _addEmptyMedicineOnArrayIfNecessary() {
    if (this.medicineFormArray.at(this.medicineFormArray.length - 1).get(MEDICAMENTO_ID).value) {
      this.medicineFormArray.push(this._initPrescriptionMedicine({}));
    }
  }

  private _setRequiredOnMedicine(index: number) {
    this.medicineFormArray.at(index).get(MEDICAMENTO_ID).setValidators([Validators.required]);
    this.medicineFormArray.at(index).get(POSOLOGIA).setValidators([Validators.required]);
    this.medicineFormArray.at(index).get(POSOLOGIA).updateValueAndValidity();
    this.medicineFormArray.at(index).get(MEDICAMENTO_ID).updateValueAndValidity();
  }

  setMedidaRequiredIfNecessary($event, index: number) {
    if ($event && $event !== this.usoContinuo) {
      this.medicineFormArray.at(index).get(UNIDADE_MEDIDA).setValidators([Validators.required]);
    } else {
      this.medicineFormArray.at(index).get(UNIDADE_MEDIDA).clearValidators();
    }
    this.prescriptionForm.updateValueAndValidity();
  }

  initNewMedicine(nome: string, index: number) {
    this.newMedicine = { nome: nome, tipoUso: Uso.USO_INTERNO };
    this.currentMedicineIndex = index;
    setTimeout(() => document.getElementById('new_posologia').focus());
  }

  prepareToSaveNewMedicine(medicine: string | undefined, index: number) {
    if (!medicine || !medicine.trim()) {
      this._notificationsService.alert('O nome do medicamento não pode ser vazio!');
      return;
    }

    medicine = medicine.trim();
    const existingMedicine = this.medicines.find(m => m.nome === medicine);
    if (existingMedicine) {
      this.medicineFormArray.at(index).patchValue({
        medicamentoId: existingMedicine.id,
      });

      return;
    }

    this.initNewMedicine(medicine, index);
  }

  deleteMedicine(index: number) {
    this.medicineFormArray.removeAt(index);
    const medicinesLength = this.medicineFormArray.length;
    if (medicinesLength === 1) {
      this._setRequiredOnMedicine(0);
    } else if (medicinesLength === 0) {
      this.medicineFormArray.push(this._initPrescriptionMedicine({}));
    }
  }

  getArrayControlValue(field: string, index: number) {
    return this.medicineFormArray.at(index).get(field).value;
  }

  get medicineFormArray(): FormArray {
    return this.prescriptionForm.get('medicamentos') as FormArray;
  }

  async emit() {
    try {
      this._service.loading(true);
      this._treatPrescriptionFormBeforeEmit();
      const professional = await this._service.httpGET(CONSTANTS.ENDPOINTS.config.professional.findOne + this.prescriptionForm.get('profissionalId').value).toPromise();
      const { pdfMake, html } = await this._buildPdfMakeContent(professional.body as ProfessionalModel);
      const fileName = 'Receituário - ' + this.patient.pessoaNome + '.pdf';
      const pdfMakeDefinition = await this._pdfGeneratorService.exportPDFMakeAsPDF(pdfMake, html, fileName);
      await this._prescriptionService.save(this.prescriptionForm.value, await this._commonPrintService.returnFileFromPdfMakeDefinition(pdfMakeDefinition, fileName));
      this._service.loading(false);
      this._notificationsService.success('Receituário emitido com sucesso');
      this.close();
      this._commonPrintService.visualizeIssuableDocument(pdfMakeDefinition);
    } catch (e) {
      this._notificationsService.error('Ocorreu um erro ao emitir o Receituário');
    } finally {
      this._service.loading(false);
    }
  }

  private _treatPrescriptionFormBeforeEmit() {
    // remove último medicamento q é sempre o vazio
    if (!this.medicineFormArray.at(this.medicineFormArray.length - 1).get(MEDICAMENTO_ID).value) {
      this.medicineFormArray.removeAt(this.medicineFormArray.length - 1);
    }
    this.prescriptionForm.patchValue({ data: this._service.parseDateFromDatePicker(this.prescriptionForm.get('data').value) });
    this.medicineFormArray.controls.forEach(control => {
      // se for 0 é 'nenhum', então vai nulo
      if (control.get(QUANTIDADE).value === 0) {
        control.get(QUANTIDADE).setValue(null);
      }
    })
  }

  private async _buildPdfMakeContent(professional: ProfessionalModel): Promise<{ pdfMake: PDFMake, html: string }> {
    const prescriptionType: PrescriptionType = this.prescriptionForm.get('tipoReceita').value;
    let pdfMake: PDFMake = { afterContent: [], footer: [] };
    let html = '';
    html += await this._buildCommmonHeader(prescriptionType);

    if (prescriptionType === PrescriptionType.RECEITUARIO_ESPECIAL) {
      this._buildEspecialFooter(pdfMake, professional);
    } else {
      this._buildSimpleFooter(pdfMake, html, professional);
      html += `<p><b>Paciente: </b> ${ this.patient.pessoaNome }</p>`;
    }

    pdfMake.afterContent.push(this._buildMedicinesContent(this.medicineFormArray.value));

    pdfMake.styles = {
      table: {
        headerRows: 1,
      },
      header: {
        alignment: 'center',
      },
      ...pdfMake.styles,
    }

    return { pdfMake, html };
  }

  private _buildEspecialFooter(pdfMake: PDFMake, professional: ProfessionalModel) {
    pdfMake.pageMargins = [20, 20, 20, 150];
    pdfMake.afterContent.push(this._buildEspecialHeader(professional));
    pdfMake.afterContent.push({
      text: [
        { text: 'Paciente: ', bold: true },
        `${ this.patient.pessoaNome }`,
      ],
      margin: [0, 15, 0, 15],
    });
    pdfMake.footer.push(ESPECIAL_FOOTER_SIGNATURE);
    pdfMake.footer.push(ESPECIAL_FOOTER_INFOS);
  }

  private _buildSimpleFooter(pdfMake: PDFMake, html: string, professional: ProfessionalModel) {
    pdfMake.pageMargins = [20, 20, 20, 155];
    const { profissionalNome, profissionalCRO } = professional;
    const extraFooter = [
      {
        stack: [
          {
            text: this._printConfigurationsService.getContaModel().cidade + moment().format(', dddd DD MMMM YYYY'),
            margin: [0, 0, 0, 10],
          },
          { canvas: [{ type: 'line', x1: -300, y1: -1, x2: 0, y2: -1, lineWidth: 1 }] },
          { text: `Dr(a). ${ profissionalNome } CRO - ${ profissionalCRO || '' }`, margin: [0, 5, 0, 20] },
        ],
        alignment: 'center',
      },
    ];
    pdfMake = this._commonPrintService.getFooter(this.printConfiguration, pdfMake, extraFooter);
  }

  private _buildMedicinesContent(prescriptionMedicines: PrescriptionMedicine[]) {
    const medicineContent = [];
    prescriptionMedicines.forEach(prescriptionMedicine => {
      const medicine = this.medicines.find(m => m.id === prescriptionMedicine.medicamentoId);
      medicineContent.push({ text: `${ this.usoTranslate[medicine.tipoUso] }`, color: 'gray', margin: [0, 10, 0, 10] });
      const content = {
        columns: [
          { text: `${ medicine.nome }`, bold: true },
          { text: `${ prescriptionMedicine.quantidade ? '- - - - - - - - - - - - - - - - - - - - - - - - - - - - -' : ' ' }` },
          {
            text: `${ this.medicineAmountTranslate[prescriptionMedicine.quantidade] || '' } ${ this.medicineMeasureTranslate[prescriptionMedicine.unidadeMedida] || '' }`,
            alignment: 'center'
          },
        ],
      };
      medicineContent.push(content);
      medicineContent.push({ text: `${ prescriptionMedicine.posologia }`, margin: [0, 5, 0, 0] });
    });

    return medicineContent;
  }

  private _buildEspecialHeader(professional: ProfessionalModel) {
    const { rodapeLinha1 } = this._printConfigurationsService.getRodapePlaceholder();
    const { profissionalNome, profissionalCRO, profissionalTelefone, profissionalCelular } = professional;
    let foneText = 'Fone: '
    if (profissionalTelefone || profissionalCelular) {
      foneText = profissionalTelefone ? 'Fone: ' + this._maskFormatService.getFormatedPhone(profissionalTelefone || '') :
        'Celular: ' + this._maskFormatService.getFormatedCellPhone(profissionalCelular || '');
    }

    return {
      columns: [
        {
          style: 'table',
          table: {
            widths: [250],
            body: [
              [{ text: 'IDENTIFICAÇÃO DO EMITENTE', bold: true, alignment: 'center' }],
              [{
                text: `${ profissionalNome }\nCRO - ${ profissionalCRO || '' }\n${ rodapeLinha1 }\n${ foneText }`,
                fontSize: 10,
                alignment: 'center'
              }],
            ],
          },
        },
        {
          table: {
            heights: [65],
            widths: [200],
            body: [
              ['\n\n1. Via da Farmácia\n2. Via do Paciente\n'],
            ],
          },
          margin: [65, 0, 0, 0],
        },
      ],
    }
  }

  private async _buildCommmonHeader(prescriptionType: PrescriptionType): Promise<string> {
    let header = await this._commonPrintService.getHeader(this.printConfiguration, '');
    const title = prescriptionType === PrescriptionType.RECEITUARIO_SIMPLES ? SIMPLE_PRESCRIPTION_TITLE : ESPECIAL_PRESCRIPTION_TITLE;
    header += `<h4 style="text-decoration: underline;
						text-align: center;
						margin-bottom: 20px"><b>${ title }</b></h4>`;

    return header;
  }

  close() {
    this.tabEmitter.emit({ tab: Tab.LIST, issuableDocumentType: IssuableDocumentType.RECEITUARIO });
  }

}
