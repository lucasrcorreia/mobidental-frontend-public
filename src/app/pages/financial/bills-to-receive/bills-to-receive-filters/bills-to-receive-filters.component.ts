import { Component, EventEmitter, Input, OnInit, Output, OnDestroy } from "@angular/core";
import { BehaviorSubject, Subject, Observable, Subscription } from "rxjs";
import * as moment from "moment";
import { descricoesPagamentos, FormaPagamento } from "../../../../api/model/forma-pagamento";
import { descricoesLancamentos, TipoLancamento } from "../../../../api/model/tipo-lancamento";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { ConvenantModel } from "../../../../core/models/patients/convenio.model";
import { CONSTANTS } from "../../../../core/constants/constants";
import { UtilsService } from "../../../../services/utils/utils.service";
import { debounceTime, distinctUntilChanged, withLatestFrom } from "rxjs/operators";
import { DetachedParcelDialogService } from "../../../patients/form-conta-corrente/detached-parcel/detached-parcel-dialog.service";
import { PatientModel } from "../../../../core/models/patients/patients.model";
import equals from "ramda/es/equals";

export interface BillsToReceiveFilters {
	startDate: NgbDateStruct;
	endDate: NgbDateStruct;
	lancamento?: TipoLancamento[];
	pagamento?: FormaPagamento[];
	idConvenio?: number;
	descricao?: string;
	nomePaciente?: string;
}

export enum FormatoPeriodo {
	HOJE = "HOJE",
	ESSA_SEMANA = "ESSA_SEMANA",
	ESSE_MES = "ESSE_MES",
	MES_PASSADO = "MES_PASSADO",
	ESPECIFICO = "ESPECIFICO",
}

const descricoesPeriodo: { [k in FormatoPeriodo]: string } = {
	ESPECIFICO: "Específico",
	ESSA_SEMANA: "Essa semana",
	ESSE_MES: "Esse mês",
	HOJE: "Hoje",
	MES_PASSADO: "Mês passado",
};

export const ngbToDate = (ngb: NgbDateStruct): Date =>
	moment({ ...ngb, month: ngb.month - 1 }).toDate();

export const dateToNgb = (date: Date): NgbDateStruct => ({
	year: date.getFullYear(),
	month: date.getMonth() + 1,
	day: date.getDate(),
});

const resolveStartAndEndDate = (period: FormatoPeriodo): [Date, Date] | undefined => {
	const endOfToday = moment().endOf("day").toDate();

	switch (period) {
		case FormatoPeriodo.ESSE_MES: {
			return [moment().startOf("month").toDate(), moment().endOf("month").toDate()];
		}

		case FormatoPeriodo.ESSA_SEMANA: {
			const lastWeek = moment().subtract(1, "week").startOf("day");
			return [lastWeek.toDate(), endOfToday];
		}

		case FormatoPeriodo.MES_PASSADO: {
			const lastMonth = moment().startOf("month").subtract(1, "month");
			return [lastMonth.toDate(), lastMonth.endOf("month").toDate()];
		}

		case FormatoPeriodo.HOJE: {
			return [moment().startOf("day").toDate(), endOfToday];
		}

		case FormatoPeriodo.ESPECIFICO:
			return undefined;
	}
};

@Component({
	selector: "app-bills-to-receive-filters",
	templateUrl: "./bills-to-receive-filters.component.html",
	styleUrls: ["./bills-to-receive-filters.component.scss"],
})
export class BillsToReceiveFiltersComponent implements OnInit, OnDestroy {
	private readonly subs = new Subscription();
	readonly FormatoPeriodo = FormatoPeriodo;
	covenants: ConvenantModel[];

	readonly periodosOptions = Object.values(FormatoPeriodo).map((periodo: FormatoPeriodo) => ({
		value: periodo,
		label: descricoesPeriodo[periodo],
	}));

	readonly lancamentosOptions = Object.values(TipoLancamento).map((lancamento: TipoLancamento) => ({
		value: lancamento,
		label: descricoesLancamentos[lancamento],
	}));

	readonly pagamentoOptions = Object.values(FormaPagamento).map((pagamento: FormaPagamento) => ({
		value: pagamento,
		label: descricoesPagamentos[pagamento],
	}));

	periodo?: FormatoPeriodo = FormatoPeriodo.ESSE_MES;

	readonly filterActivated = new BehaviorSubject<undefined>(undefined);
	readonly filtersState: BehaviorSubject<BillsToReceiveFilters>;

	@Output() readonly filtersChange: Observable<BillsToReceiveFilters>;
	@Output() readonly generateReport = new EventEmitter<void>();

	descricao = new BehaviorSubject("");
	private readonly _descricao$ = this.descricao
		.pipe(debounceTime(800), distinctUntilChanged())
		.subscribe((descricao) => this.patchFilters({ descricao }));

	nomePaciente = new BehaviorSubject("");
	private readonly _nomePaciente$ = this.nomePaciente
		.pipe(debounceTime(800), distinctUntilChanged())
		.subscribe((nomePaciente) => this.patchFilters({ nomePaciente }));

	constructor(
		private readonly _service: UtilsService,
		private readonly _detachedParcelDialogService: DetachedParcelDialogService
	) {
		const [startDate, endDate] = resolveStartAndEndDate(this.periodo).map(dateToNgb);
		this.filtersState = new BehaviorSubject({ startDate, endDate });
		this.filtersChange = this.filterActivated.pipe(
			withLatestFrom(this.filtersState, (_, filters) => filters),
			distinctUntilChanged<BillsToReceiveFilters>(equals)
		);

		this.subs.add(this._nomePaciente$).add(this._descricao$);
	}

	ngOnInit(): void {
		this._fetchCovenants();
	}

	ngOnDestroy(): void {
		this.subs.unsubscribe();
	}

	private async _fetchCovenants() {
		const { body } = await this._service
			.httpGET(CONSTANTS.ENDPOINTS.patients.treatment.convenant)
			.toPromise();
		this.covenants = body as ConvenantModel[];
	}

	@Input()
	set filters(filters: BillsToReceiveFilters) {
		this.filtersState.next(filters);
	}

	get filters() {
		return this.filtersState.value;
	}

	patchFilters(partials: Partial<BillsToReceiveFilters>) {
		this.filters = { ...this.filters, ...partials };
	}

	updateFiltersFromPeriod(period: FormatoPeriodo) {
		const [
			startDate = dateToNgb(moment().startOf("month").toDate()),
			endDate = dateToNgb(moment().endOf("month").toDate()),
		] = resolveStartAndEndDate(period).map(dateToNgb) || [];

		this.patchFilters({ endDate, startDate });
	}

	triggerFilter() {
		this.filterActivated.next(undefined);
	}

	async detachedParcel() {
		const did = await this._detachedParcelDialogService.open(
			new PatientModel(),
			"contas-a-receber"
		);
		if (did) {
			this.patchFilters({});
		}
	}
}
