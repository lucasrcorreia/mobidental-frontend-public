export enum EmUnidadeMedidaMedicamento {
	AMPOLA = 'AMPOLA',
	CAIXA = 'CAIXA',
	COMPRIMIDOS = 'COMPRIMIDOS',
	FRASCOS = 'FRASCOS',
	PACOTES = 'PACOTES',
	TUBO = 'TUBO',
	CAPSULAS = 'CAPSULAS',
}

export const MEDIDA_TRANSLATE: { [key in EmUnidadeMedidaMedicamento]: string } = {
	AMPOLA: 'Ampola(s)',
	CAIXA: 'Caixa(s)',
	COMPRIMIDOS: 'Comprimido(s)',
	FRASCOS: 'Frasco(s)',
	PACOTES: 'Pacotes(s)',
	TUBO: 'Tubo(s)',
	CAPSULAS: 'Cápsula(s)',
};
