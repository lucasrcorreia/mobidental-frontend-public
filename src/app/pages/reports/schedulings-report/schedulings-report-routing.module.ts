import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SchedulingsReportComponent } from './schedulings-report.component';

const routes: Routes = [
	{
		path: '',
		component: SchedulingsReportComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class SchedulingsReportRoutingModule {}
