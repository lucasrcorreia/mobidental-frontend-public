import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {forEach, isEmpty} from 'lodash-es';
import {AbstractControl, FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {DocumentsCommonService} from '../documents-common.service';

const PREVIEW_KEY = 'preview';
const FILE_KEY = 'file';
const DESCRIPTION_KEY = 'description';

const EXTENSION_REGEX = /[^\\]*\.(\w+)$/;
const EXTENSION_POSITION = 1;

export interface UploadFile {
  description: string;
  note: string;
  file: File;
  preview: SafeResourceUrl;
}

@Component({
  selector: 'app-upload-document',
  templateUrl: './upload-document.component.html',
  styleUrls: ['./upload-document.component.scss']
})
export class UploadDocumentComponent implements OnInit {

  readonly acceptableFileExtensions = 'application/pdf, image/*, .doc, .docx, .odt';

  filesForm: FormGroup;

  constructor(public activeModal: NgbActiveModal,
              private readonly _formBuilder: FormBuilder,
              private readonly _sanitizer: DomSanitizer,
              private readonly _documentsCommonService: DocumentsCommonService) {
  }

  ngOnInit() {
    this._initForm();
  }

  private _initForm() {
    this.filesForm = this._formBuilder.group({
      files: this._formBuilder.array([this._initRows()])
    });
  }

  private _initRows(file?: File) {
    return this._formBuilder.group({
      description: [file && file.name || ''],
      note: [''],
      file: [file || '', Validators.required],
      preview: [file && this._createPreview(file) || ''],
    });
  }

  get formArray(): FormArray {
    return this.filesForm.get('files') as FormArray;
  }

  get existFiles(): boolean {
    return this._getFormAt(0).get(FILE_KEY).value;
  }

  addNewFile(file?: File): void {
    this.formArray.push(this._initRows(file));
  }

  deleteFile(index: number): void {
    this.formArray.removeAt(index);

    if (isEmpty(this.formArray.controls)) {
      this._initForm();
    }
  }

  private _getFormAt(index: number): AbstractControl {
    return this.formArray.at(index);
  }

  onFileChanged($event: any) {
    const files: FileList = $event;
    this._presentCurrentInformations(files.item(0));

    forEach(files, (file, index) => {
      if (index !== 0) {
        this.addNewFile(file);
        this._createPreview(file);
      }
    });
  }

  private _presentCurrentInformations(file: File) {
    if (!this._getFormAt(0).get(DESCRIPTION_KEY).value) {
      this._getFormAt(0).get(DESCRIPTION_KEY).setValue(file.name);
    }
    this._getFormAt(0).get(FILE_KEY).setValue(file);
    this._getFormAt(0).get(PREVIEW_KEY).setValue(this._createPreview(file));
  }

  private _createPreview(file: File): SafeResourceUrl {
    const extensionFromFileName = file.name.match(EXTENSION_REGEX)[EXTENSION_POSITION];

    if (this._documentsCommonService.nonImageExtensions.includes(extensionFromFileName)) {
      return this._documentsCommonService.getCorrectUrlFromExtension(extensionFromFileName);
    } else {
      const url: string = URL.createObjectURL(file);
      return this._sanitizer.bypassSecurityTrustResourceUrl(url);
    }
  }

  getPreview(index: number): SafeResourceUrl {
    return this._getFormAt(index).get(PREVIEW_KEY).value;
  }

  save() {
    this.activeModal.close(this.formArray.value as UploadFile[]);
  }

}
