import { Component, OnInit, Input, Output } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { combineLatest } from 'rxjs/observable/combineLatest';
import {
  flatMap,
  map,
  debounceTime,
  startWith,
  shareReplay,
  tap
} from 'rxjs/operators';
import { Produto } from '../../../../api/model/produto';
import { ProdutosApiService } from '../../../../api/produtos-api.service';
import { RespostaPaginada } from '../../../../api/model/resposta-paginada';
import { AsyncTemplate } from '../../../../lib/helpers/async-template';
import {
  getInfoUnidadeMedida,
  UnidadeMedida
} from '../../../../api/model/unidade-medida';

export interface NgxSorting {
  dir: 'asc' | 'desc';
  prop: string;
}

export enum AbaEstoque {
  TODOS = 'TODOS',
  ABAIXO_MINIMO = 'ABAIXO_MINIMO',
  EM_FALTA = 'EM_FALTA'
}

@Component({
  selector: 'app-produtos-list-table',
  templateUrl: './produtos-list-table.component.html',
  styleUrls: ['./produtos-list-table.component.scss']
})
export class ProdutosListTableComponent implements OnInit {
  @Output() readonly edit = new Subject<Produto>();
  @Output() readonly delete = new Subject<Produto>();
  @Output() readonly novaMovimentacao = new Subject<Produto>();

  @Input() esconderLancamentos = false;

  private _abaAtualSubject = new BehaviorSubject<AbaEstoque>(AbaEstoque.TODOS);

  private readonly _querySubject = new BehaviorSubject<string | undefined>(
    undefined
  );

  private readonly _pageSubject = new BehaviorSubject<number>(0);
  private readonly _reloadSubject = new Subject<undefined>();
  private readonly _sortSubject = new BehaviorSubject<
    [string, 'asc' | 'desc'][]
  >([]);

  readonly pageNumber$ = this._pageSubject.asObservable();

  readonly pagina$: Observable<RespostaPaginada<Produto>> = combineLatest([
    this._querySubject.pipe(debounceTime(300)),
    this._pageSubject,
    this._sortSubject,
    this._abaAtualSubject.pipe(map(pag => pag || AbaEstoque.TODOS)),
    this._reloadSubject.pipe(startWith(undefined))
  ]).pipe(
    flatMap(async ([query, page, sorting, pagina]) => {
      const result = await this._asyncTemplate.wrapUserFeedback(
        'Erro ao consultar produtos',
        () =>
          this._produtosApi.listarProdutos({
            page,
            sorting,
            situacaoEstoque: pagina,
            query
          })
      );

      if (result.totalPages > 0 && result.totalPages < page + 1) {
        // Passou da última página
        setTimeout(() => {
          this._pageSubject.next(result.totalPages - 1);
        });
      }

      return result;
    }),
    shareReplay(1)
  );

  constructor(
    private readonly _produtosApi: ProdutosApiService,
    private readonly _asyncTemplate: AsyncTemplate
  ) {}

  ngOnInit() {}

  @Input()
  set query(q: string) {
    this._querySubject.next(q);
  }

  @Input()
  set aba(aba: AbaEstoque) {
    this._abaAtualSubject.next(aba);
  }

  reload() {
    this._reloadSubject.next();
  }

  updateSortings(sorts: NgxSorting[]) {
    this._sortSubject.next(
      sorts.map(({ dir, prop }) => [prop, dir] as [string, 'asc' | 'desc'])
    );
  }

  updatePage(page: number) {
    this._pageSubject.next(page);
  }

  unidadeMedidaInfo(un: UnidadeMedida) {
    return getInfoUnidadeMedida(un);
  }
}
