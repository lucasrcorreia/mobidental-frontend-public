import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateReturnAlertComponent } from './create-return-alert.component';

describe('CreateAlertComponent', () => {
  let component: CreateReturnAlertComponent;
  let fixture: ComponentFixture<CreateReturnAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateReturnAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateReturnAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
