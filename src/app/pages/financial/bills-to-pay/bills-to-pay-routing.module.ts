import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BillsToPayComponent } from './bills-to-pay.component';

const routes: Routes = [
  {
    path: '',
    component: BillsToPayComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillsToPayRoutingModule {}
