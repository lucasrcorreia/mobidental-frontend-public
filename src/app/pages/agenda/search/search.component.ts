import {
  Component,
  ElementRef,
  Input,
  OnInit,
  QueryList,
  ViewChildren
} from '@angular/core';
import { classToPlain } from 'class-transformer';
import * as moment from 'moment';
import { fromEvent } from 'rxjs';
import { debounceTime, tap } from 'rxjs/operators';
import { EventSearchTableModel } from '../../../core/models/agenda/agenda.model';
import { CONSTANTS } from '../../../core/constants/constants';
import { ShortListItem } from '../../../core/models/forms/common/common.model';
import { ColumnsTableModel } from '../../../core/models/table/columns.model';
import { UtilsService } from '../../../services/utils/utils.service';
import { LISTS } from './../lists';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: [
    './search.component.scss',
    './../../../../assets/icon/icofont/css/icofont.scss'
  ]
})
export class SearchComponent implements OnInit {
  @ViewChildren('patient') patient: QueryList<ElementRef>;
  @Input() professionals: ShortListItem[] = [];

  searchParam = {
    dataInicial: this.service.parseDatePicker(moment().format('DD/MM/YYYY')),
    pacientePessoaNome: null,
    page: 0,
    profissionalId: null,
    size: 25,
    sorting: {
      null: 'asc'
    }
  };

  localLists = new LISTS(this.service.translate);

  page = {
    size: 10,
    totalElements: 0,
    totalPages: 0,
    pageNumber: 0
  };

  rows = {};
  rowsPage: EventSearchTableModel[] = [];

  columns: ColumnsTableModel[] = [
    {
      action: false,
      name: this.service.translate.instant('AGENDA.TABLE.COL2'),
      prop: 'dataHoraInicialEvento',
      sortable: true,
      width: 150
    },
    {
      action: false,
      name: this.service.translate.instant('AGENDA.TABLE.COL3'),
      prop: 'pacientePessoaNome',
      sortable: true,
      width: 200
    },
    {
      action: false,
      name: this.service.translate.instant('AGENDA.TABLE.COL4'),
      prop: 'celular',
      sortable: true,
      width: 100
    },
    {
      action: false,
      name: this.service.translate.instant('AGENDA.TABLE.COL5'),
      prop: 'profissionalNome',
      sortable: true,
      width: 80
    },
    {
      action: false,
      name: this.service.translate.instant('AGENDA.TABLE.COL6'),
      prop: 'tipoAtendimentoDescricao',
      sortable: true,
      width: 80
    },
    {
      action: false,
      name: this.service.translate.instant('AGENDA.TABLE.COL7'),
      prop: 'emStatusAgendamento',
      sortable: true,
      width: 80
    }
  ];

  constructor(public service: UtilsService) { }

  ngOnInit() {
    setTimeout(() => {
      fromEvent(this.patient.first.nativeElement, 'keyup')
        .pipe(
          debounceTime(350),
          tap(item => this.findEvents())
        )
        .subscribe();
    });

    this.findEvents();
  }

  findEvents() {
    this.service.loading(true);

    const params = classToPlain(this.searchParam) as any;
    params.dataInicial = this.service.parseDateFromDatePicker(
      this.searchParam.dataInicial
    );



    this.service
      .httpPOST(CONSTANTS.ENDPOINTS.agenda.searchByParam, params)
      .subscribe(
        data => {
          const result = data.body as any;

          if (this.page.pageNumber === 0) {
            this.page.totalElements = result.totalElements;
            this.page.totalPages = result.totalPages;
          }

          this.rows[this.page.pageNumber] = result.content;
          this.rowsPage = result.content;


          this.service.loading(false);
        },
        err => {

          this.service.loading(false);
          this.service.notification.error(
            this.service.translate.instant('COMMON.ERROR.SEARCH'),
            err.error.error
          );
        }
      );
  }

  setPage(pageInfo) {
    this.page.pageNumber = pageInfo.offset;
    if (this.rows[this.page.pageNumber]) {
      this.rowsPage = this.rows[this.page.pageNumber];
    } else {
      this.findEvents();
    }
  }

  getText(value: string) {
    const result = this.localLists.listStatusAgenda.find(
      item => item.value === value
    );

    return result ? result.label : '-';
  }
}
