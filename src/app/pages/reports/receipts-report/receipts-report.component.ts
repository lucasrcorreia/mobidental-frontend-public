import { Component, Inject, OnInit } from '@angular/core';
import { NgbDateParserFormatter, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { CONSTANTS } from '../../../core/constants/constants';
import { ReceiptsReportFilters, ReportsService } from '../reports.service';
import { ReportDatatableColumn, Type } from '../report-datatable/report-datatable.component';
import { CustomDatepickerI18n, I18n } from '../../../shared/ngb-datepicker/datepicker-i18n';
import { NgbDateFRParserFormatter } from '../../../shared/ngb-datepicker/ngb-date-fr-parser-formatter';
import { AutoCompletePatientList } from '../../../core/models/patients/patients.model';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { AbstractReport } from '../abstract-report';
import { UtilsService } from '../../../services/utils/utils.service';
import { SheetGeneratorService } from '../../../services/sheets-generator/sheet-generator.service';
import { MaskFormatService } from '../../../services/utils/mask-format.service';
import { AsyncTemplate } from '../../../lib/helpers/async-template';
import { NotificationsService } from 'angular2-notifications';
import { GlobalSpinnerService } from '../../../lib/global-spinner.service';
import { PdfGeneratorService, PdfMakePageOrientation } from '../../../services/pdf-generator/pdf-generator.service';
import { CommonsReportBuildService } from '../../../services/report/commons-report-build.service';
import { DropdownGenericService } from '../../patients/form-patients/services/dropdown-generic.service';
import { AbstractReportFilters } from '../abstract-report-filters';
import * as moment from 'moment';

const TABLE_COLUMNS: ReportDatatableColumn[] = [
	{ prop: 'dataEmissao', translation: 'REPORTS.TABLE.DATE', type: Type.DATE },
	{ prop: 'nomePaciente', translation: 'REPORTS.TABLE.PATIENT' },
	{ prop: 'nomeEmitente', translation: 'REPORTS.TABLE.EMITTER' },
	{ prop: 'cpfCnpjEmitente', translation: 'REPORTS.TABLE.DATA', type: Type.CPF_CNPJ },
	{ prop: 'referente', translation: 'REPORTS.TABLE.REFER' },
	{ prop: 'valor', translation: 'REPORTS.TABLE.VALUE', type: Type.CURRENCY, cellClassName: 'last-column', headerClassName: 'last-header' },
];

const SHEET_COLUMNS: ReportDatatableColumn[] = [
	{ prop: 'data', translation: 'REPORTS.TABLE.DATE', type: Type.DATE },
	{ prop: 'dataEmissao', translation: 'REPORTS.TABLE.EMISSION_DATE', type: Type.DATE },
	{ prop: 'numeroRecibo', translation: 'REPORTS.TABLE.RECEIPT_NUMBER' },
	{ prop: 'cpfCnpjEmitente', translation: 'REPORTS.TABLE.EMITTER_CPF_CNPJ', type: Type.CPF_CNPJ },
	{ prop: 'nomeEmitente', translation: 'REPORTS.TABLE.EMITTER_NAME' },
	{ prop: 'nomePaciente', translation: 'REPORTS.TABLE.PATIENT_NAME' },
	{ prop: 'cpfCnpjPaciente', translation: 'REPORTS.TABLE.PATIENT_CPF_CNPJ', type: Type.CPF_CNPJ },
	{ prop: 'cpfRespPagamento', translation: 'REPORTS.TABLE.CPF_PAYMENT', type: Type.CPF_CNPJ },
	{ prop: 'nomeRespPagamento', translation: 'REPORTS.TABLE.PAYMENT_NAME' },
	{ prop: 'referente', translation: 'REPORTS.TABLE.REFER' },
	{ prop: 'status', translation: 'REPORTS.TABLE.STATUS' },
	{ prop: 'motivoCancelamento', translation: 'REPORTS.TABLE.CANCEL_REASON' },
	{ prop: 'valor', translation: 'REPORTS.TABLE.VALUE', type: Type.CURRENCY },
];

@Component({
	selector: 'app-receipts-report',
	templateUrl: './receipts-report.component.html',
	styleUrls: ['./receipts-report.component.scss'],
	providers: [
		I18n,
		{ provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
		{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
	],
})
export class ReceiptsReportComponent extends AbstractReport implements OnInit {

	filters: ReceiptsReportFilters = { pacienteId: null, profissionalId: null };
	ownData: any[];
	readonly columns = TABLE_COLUMNS;
	readonly sheetColumns = SHEET_COLUMNS;

	sheetFileName = 'relatorio_comprovantes';
	sheetTitle = this._service.translate.instant('REPORTS.RECEPITS.TITLE');

	reportPageOrientation: PdfMakePageOrientation = 'landscape';
	reportTableBodyWidths = [100, 150, 150, 95, 150, 100];

	currentPatient: AutoCompletePatientList;
	typeaheadValue = '';
	formatMatches = (value: AutoCompletePatientList) => value.pessoaNome;
	observableSearch = (text$: Observable<string>) =>
			text$.pipe(
					debounceTime(200),
					distinctUntilChanged(),
					switchMap(term =>
							term.length < 3 ? [] : this._fetchPatients(term),
					),
			);

	constructor(private readonly _reportsService: ReportsService,
				@Inject(UtilsService)  _service: UtilsService,
				@Inject(DropdownGenericService) _dropdownGenericService: DropdownGenericService,
				@Inject(NotificationsService) _notificationsService: NotificationsService,
				@Inject(GlobalSpinnerService) _globalSpinnerService: GlobalSpinnerService,
				@Inject(CommonsReportBuildService) _commonsReportService: CommonsReportBuildService,
				@Inject(PdfGeneratorService) _pdfGeneratorService: PdfGeneratorService,
				@Inject(SheetGeneratorService) _sheetGeneratorService: SheetGeneratorService,
				@Inject(MaskFormatService) _maskFormatService: MaskFormatService,
				@Inject(AsyncTemplate) _asyncTemplate: AsyncTemplate) {
		super(_service, _dropdownGenericService, _notificationsService, _globalSpinnerService, _commonsReportService, _pdfGeneratorService, _sheetGeneratorService, _maskFormatService, _asyncTemplate);
	}

	ngOnInit() {
		this.filters.dataFim = AbstractReportFilters.momentFromNgbDateStruct(moment());
		this.filters.dataInicio = AbstractReportFilters.momentFromNgbDateStruct(moment().subtract(30, 'days'));
		this.search();
		this.initProfessionalDropdown();
	}

	private async _fetchPatients(term: string) {
		const data = await this._service.httpGET(CONSTANTS.ENDPOINTS.patients.general.autoComplete + term).toPromise();
		return data.body;
	}

	async search() {
		if (!this.requiredFiltersArePresent) {
			return;
		}
		this._behaviorFilters$.next({
			...this.filters,
			patientName: this.currentPatient ? this.currentPatient.pessoaNome : null,
			professionalName: this.professionalName,
		});
		this.ownData = await this.fetchOwnData(() => this._reportsService.findAllReceipts({ ...this.filters }));
	}

	selectPatient(patient): void {
		this.currentPatient = patient.item;
		this.filters.pacienteId = this.currentPatient.id;
	}

	async buildReportHeader() {
		const behaviorFiltersValue = this._behaviorFilters$.value;
		const dates: [Date, Date] = [this._getDateFromFilter(behaviorFiltersValue.dataInicio), this._getDateFromFilter(behaviorFiltersValue.dataFim)];
		return [
			await this._commonsReportService.buildFinancialReportHeader(this.sheetTitle),
			{ ...CommonsReportBuildService.buildPeriodo(dates) },
			{ ...CommonsReportBuildService.buildPatient(behaviorFiltersValue.patientName) },
			{
				...CommonsReportBuildService.buildProfessional(behaviorFiltersValue.professionalName),
				margin: [0, 0, 0, 10],
			},
		]
	}

}
