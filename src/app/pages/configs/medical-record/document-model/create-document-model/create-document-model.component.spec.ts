import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDocumentModelComponent } from './create-document-model.component';

describe('CreateDocumentModelComponent', () => {
  let component: CreateDocumentModelComponent;
  let fixture: ComponentFixture<CreateDocumentModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDocumentModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDocumentModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
