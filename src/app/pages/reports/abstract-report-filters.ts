import { UtilsService } from '../../services/utils/utils.service';
import { SelectItem, SelectItemStatus, ShortListItem } from '../../core/models/forms/common/common.model';
import { LISTAS } from '../patients/lists';
import { CONSTANTS } from '../../core/constants/constants';
import * as moment from 'moment';
import { ReceiptsReportFilters } from './reports.service';
import { BehaviorSubject } from 'rxjs';
import { isEmpty } from 'lodash-es';
import { LISTS } from '../agenda/lists';
import { DropdownGenericService, EXPERTISE_PATH, ORGANIZATION_PATH, SITUATION_PATH } from '../patients/form-patients/services/dropdown-generic.service';
import { DropdownGenericModel } from '../patients/form-patients/form-patients.component';
import { NotificationsService } from 'angular2-notifications';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

export enum Period {
	THIS_MONTH,
	LAST_MONTH,
	PERIOD,
}

const MONTHS: { label: string, value: Period }[] = [
	{ value: Period.THIS_MONTH, label: 'Esse mês' },
	{ value: Period.LAST_MONTH, label: 'Último mês' },
	{ value: Period.PERIOD, label: 'Período' },
];

export abstract class AbstractReportFilters {

	abstract filters: ReceiptsReportFilters;
	readonly _behaviorFilters$ = new BehaviorSubject<ReceiptsReportFilters>({});

	covenants: ShortListItem[];
	professionals: ShortListItem[];
	situations: SelectItemStatus[] = new LISTAS(this._service.translate).listaStatusTratamento;
	statusAgenda: SelectItemStatus[] = new LISTS(this._service.translate).listStatusAgenda;
	expertises: DropdownGenericModel[];
	patientSituations: DropdownGenericModel[];
	organizations: DropdownGenericModel[];
	indications: SelectItem[] = this._service.translate.instant('PATIENTS.FORM.INDICATIONS');
	currentPeriod: Period = Period.THIS_MONTH;
	readonly Period = Period;
	readonly periods = MONTHS;

	constructor(readonly _service: UtilsService,
				private readonly _dropdownGenericService: DropdownGenericService,
				protected readonly _notificationsService: NotificationsService) {}

	onChandePeriod() {
		let now = moment().date(1);
		const lastMonth = moment().subtract(1, 'month').date(1);
		const nextMonth = moment().add(1, 'month').date(1).subtract(1, 'day');
		switch (this.currentPeriod) {
			case Period.LAST_MONTH:
				now = now.subtract(1, 'day');
				this.filters.dataFim = { day: now.date(), month: now.month() + 1, year: now.year() };
				this.filters.dataInicio = { day: lastMonth.date(), month: lastMonth.month() + 1, year: lastMonth.year() };
				this.search();
				break;
			case Period.THIS_MONTH:
				this.filters.dataInicio = { day: now.date(), month: now.month() + 1, year: now.year() };
				this.filters.dataFim = { day: nextMonth.date(), month: nextMonth.month() + 1, year: nextMonth.year() };
				this.search();
				break;
			default:
				break;
		}
	}

	get requiredFiltersArePresent(): boolean {
		if (!this.filters || !this.filters.dataInicio || !this.filters.dataFim) {
			this._notificationsService.info('Por favor informe um período');
			return false;
		}
		return true;
	}

	abstract async search();

	async initCovenantDropdown() {
		const covenants = await this._service.httpGET(CONSTANTS.ENDPOINTS.patients.treatment.convenant).toPromise();
		this.covenants = covenants.body as ShortListItem[];
	}

	async initProfessionalDropdown() {
		const professionals = await this._service.httpGET(CONSTANTS.ENDPOINTS.professional.active).toPromise();
		this.professionals = professionals.body as ShortListItem[];
	}

	async initExpertisesDropdown() {
		const expertises = await this._dropdownGenericService.listAll(EXPERTISE_PATH);
		this.expertises = expertises.map(e => {return { id: e.id, nome: e.nome }});
	}

	async initSituationsDropdown() {
		this.patientSituations = await this._dropdownGenericService.listAll(SITUATION_PATH);
	}

	async initOrganizationsDropdown() {
		this.organizations = await this._dropdownGenericService.listAll(ORGANIZATION_PATH);
	}

	get covenantName(): string {
		return this.filters.convenioId ? (this.covenants.find(p => p.id === this.filters.convenioId)).nome : null;
	}

	get professionalName(): string {
		return this.filters.profissionalId ? (this.professionals.find(p => p.id === this.filters.profissionalId)).nome : null;
	}

	get organizationName(): string {
		return this.filters.empresaId ? (this.organizations.find(p => p.id === this.filters.empresaId)).nome : null;
	}

	listaStatusName(list = this.filters.statusTratamento, refer: (SelectItem | SelectItemStatus)[] = this.situations): string[] {
		const values = isEmpty(list) ? [] : [list.toString()];
		return isEmpty(values) ? null : refer.filter(s => values.includes(s.value)).map(s => s.label);
	}

	get listPatientStatusName(): string[] {
		const situations = !this.filters.situacaoPaciente ? [] : [+this.filters.situacaoPaciente.toString()];
		return isEmpty(situations) ? null : this.patientSituations.filter(s => situations.includes(s.id)).map(s => s.descricao);
	}

	get listaEspecialidadesName(): string[] {
		const expertises = isEmpty(this.filters.especialidades) ? [] : this.filters.especialidades;
		return isEmpty(expertises) ? null : this.expertises.filter(s => expertises.includes(s.id)).map(s => s.nome);
	}

	static momentFromNgbDateStruct(moment): NgbDateStruct {
		const day = moment.date();
		const month = moment.month() + 1;
		const year = moment.year();

		return { month, day, year };
	}

}
