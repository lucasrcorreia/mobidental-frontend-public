import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientsReportComponent } from './patients-report.component';

const routes: Routes = [
	{
		path: '',
		component: PatientsReportComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class PatientsReportRoutingModule {}
