import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { UserRegistrationComponent } from './user-registration.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Registration',
      status: false
    },
    component: UserRegistrationComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrationRoutingModule { }



