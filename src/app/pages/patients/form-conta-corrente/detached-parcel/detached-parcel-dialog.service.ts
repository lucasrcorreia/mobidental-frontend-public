import { Injectable } from '@angular/core';
import { DetachedParcelComponent, DetachedParcelOrigin } from './detached-parcel.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PatientModel } from '../../../../core/models/patients/patients.model';

@Injectable({
  providedIn: 'root'
})
export class DetachedParcelDialogService {

  constructor(private readonly _modalService: NgbModal) { }

  open(patient: PatientModel, origin: DetachedParcelOrigin = 'conta-corrente'): Promise<any> {
    const modalRef = this._modalService.open(DetachedParcelComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.patient = patient;
    modalRef.componentInstance.origin = origin;

    return modalRef.result;
  }
}
