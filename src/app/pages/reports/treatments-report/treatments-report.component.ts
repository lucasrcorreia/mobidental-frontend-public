import { Component, Inject, OnInit } from "@angular/core";
import { NgbDateParserFormatter, NgbDatepickerI18n } from "@ng-bootstrap/ng-bootstrap";
import { NotificationsService } from "angular2-notifications";
import { isEmpty } from "lodash-es";
import * as moment from "moment";
import { BasicoDentista, ProfessionalsApiService } from "../../../api/professionals.api.service";
import { SelectItemStatus, ShortListItem } from "../../../core/models/forms/common/common.model";
import { GlobalSpinnerService } from "../../../lib/global-spinner.service";
import { AsyncTemplate } from "../../../lib/helpers/async-template";
import { PdfGeneratorService } from "../../../services/pdf-generator/pdf-generator.service";
import { CommonsReportBuildService } from "../../../services/report/commons-report-build.service";
import { SheetGeneratorService } from "../../../services/sheets-generator/sheet-generator.service";
import { MaskFormatService } from "../../../services/utils/mask-format.service";
import { UtilsService } from "../../../services/utils/utils.service";
import { CustomDatepickerI18n, I18n } from "../../../shared/ngb-datepicker/datepicker-i18n";
import { NgbDateFRParserFormatter } from "../../../shared/ngb-datepicker/ngb-date-fr-parser-formatter";
import { DropdownGenericService } from "../../patients/form-patients/services/dropdown-generic.service";
import { LISTAS } from "../../patients/lists";
import { AbstractReport } from "../abstract-report";
import { AbstractReportFilters } from "../abstract-report-filters";
import { ReportDatatableColumn, Type } from "../report-datatable/report-datatable.component";
import { ReceiptsReportFilters, ReportsService } from "../reports.service";

const TABLE_COLUMNS: ReportDatatableColumn[] = [
	{ prop: "pacientePessoaNome", translation: "REPORTS.TABLE.PATIENT" },
	{ prop: "dataAbertura", translation: "REPORTS.TABLE.OPENING_DATE", type: Type.DATE },
	{ prop: "dataAprovacao", translation: "REPORTS.TABLE.APROVAL_DATE", type: Type.DATE },
	{ prop: "statusTratamentoDescricao", translation: "REPORTS.TABLE.SITUATION" },
	{ prop: "convenioNome", translation: "REPORTS.TABLE.COVENANT" },
	{ prop: "pacientePessoaTelefone", translation: "REPORTS.TABLE.PHONE", type: Type.PHONE },
	{
		prop: "valor",
		translation: "REPORTS.TABLE.VALUE",
		type: Type.CURRENCY,
		cellClassName: "last-column",
		headerClassName: "last-header",
	},
];

const SHEET_COLUMNS: ReportDatatableColumn[] = [
	{ prop: "pacientePessoaNome", translation: "REPORTS.TABLE.PATIENT_NAME" },
	{ prop: "descricao", translation: "REPORTS.TABLE.DESCRIPTION" },
	{ prop: "convenioNome", translation: "REPORTS.TABLE.COVENANT" },
	{ prop: "profissionalNome", translation: "REPORTS.TABLE.PROFESSIONAL_NAME" },
	{ prop: "dataAbertura", translation: "REPORTS.TABLE.OPENING_DATE", type: Type.DATE },
	{ prop: "operadorVendedorNome", translation: "REPORTS.TABLE.SELLER_NAME" },
	{ prop: "dataAprovacao", translation: "REPORTS.TABLE.APROVAL_DATE", type: Type.DATE },
	{ prop: "pacientePessoaCelular", translation: "REPORTS.TABLE.CELL", type: Type.CELL },
	{ prop: "pacientePessoaTelefone", translation: "REPORTS.TABLE.PHONE", type: Type.PHONE },
	{ prop: "pacientePessoaEmail", translation: "REPORTS.TABLE.EMAIL" },
	{ prop: "statusTratamentoDescricao", translation: "REPORTS.TABLE.TREATMENT_STATUS" },
	{ prop: "formaDeRecebimento", translation: "REPORTS.TABLE.RECEIPT_WAY" },
	{ prop: "formaPagamento", translation: "REPORTS.TABLE.PAYMENT_WAY" },
	{ prop: "valor", translation: "REPORTS.TABLE.VALUE", type: Type.CURRENCY },
	{
		prop: "valorComDesconto",
		translation: "REPORTS.TABLE.VALUE_WITH_DISCOUNT",
		type: Type.CURRENCY,
	},
	{ prop: "valorDesconto", translation: "REPORTS.TABLE.DISCOUNT_VALUE", type: Type.CURRENCY },
	{ prop: "valorPercentualDesconto", translation: "REPORTS.TABLE.DISCOUNT_PERCENT" },
];

@Component({
	selector: "app-age-range-report",
	templateUrl: "./treatments-report.component.html",
	styleUrls: ["./treatments-report.component.scss"],
	providers: [
		I18n,
		{ provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
		{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
	],
})
export class TreatmentsReportComponent extends AbstractReport implements OnInit {
	filters: ReceiptsReportFilters = {
		convenioId: null,
		profissionalId: null,
		listaStatusTratamento: [],
	};
	covenants: ShortListItem[];
	situations: SelectItemStatus[] = new LISTAS(this._service.translate).listaStatusTratamento;
	dentistas: BasicoDentista[];

	ownData: any[];
	totalValue: number;

	sheetFileName = "relatorio_tratamentos";
	sheetTitle = this._service.translate.instant("REPORTS.TREATMENTS.TITLE");

	readonly columns = TABLE_COLUMNS;
	readonly sheetColumns = SHEET_COLUMNS;

	readonly pdfColumns = [
		...TABLE_COLUMNS.slice(0, TABLE_COLUMNS.length - 1),
		{ prop: "profissionalNome", translation: "REPORTS.TABLE.PROFESSIONAL_NAME" },
		...TABLE_COLUMNS.slice(TABLE_COLUMNS.length - 1),
	];
	reportTableBodyWidths: number[] = [140, 50, 50, 130, 110, 60, 140, 50];

	constructor(
		private readonly _reportsService: ReportsService,
		private readonly _professionalsApiService: ProfessionalsApiService,
		@Inject(UtilsService) _service: UtilsService,
		@Inject(DropdownGenericService) _dropdownGenericService: DropdownGenericService,
		@Inject(NotificationsService) _notificationsService: NotificationsService,
		@Inject(GlobalSpinnerService) _globalSpinnerService: GlobalSpinnerService,
		@Inject(CommonsReportBuildService) _commonsReportService: CommonsReportBuildService,
		@Inject(PdfGeneratorService) _pdfGeneratorService: PdfGeneratorService,
		@Inject(SheetGeneratorService) _sheetGeneratorService: SheetGeneratorService,
		@Inject(MaskFormatService) _maskFormatService: MaskFormatService,
		@Inject(AsyncTemplate) _asyncTemplate: AsyncTemplate
	) {
		super(
			_service,
			_dropdownGenericService,
			_notificationsService,
			_globalSpinnerService,
			_commonsReportService,
			_pdfGeneratorService,
			_sheetGeneratorService,
			_maskFormatService,
			_asyncTemplate
		);
	}

	ngOnInit() {
		this.filters.dataFim = AbstractReportFilters.momentFromNgbDateStruct(moment());
		this.filters.dataInicio = AbstractReportFilters.momentFromNgbDateStruct(
			moment().subtract(30, "days")
		);
		this.search();
		this.initCovenantDropdown();
		this._procureDentistas();
	}

	async search() {
		const filters = { ...this.filters };

		filters.listaStatusTratamento = isEmpty(filters.listaStatusTratamento)
			? []
			: [filters.listaStatusTratamento.toString()];

		this._behaviorFilters$.next({
			...filters,
			covenantName: this.covenantName,
			listaStatusTratamentoName: this.listaStatusName(),
		});

		this.ownData = await this.fetchOwnData(() => this._reportsService.findAllTreatments(filters));
		this.totalValue = this.ownData.reduce((sum, row) => sum + Number(row.valor || 0), 0);
	}

	private async _procureDentistas() {
		this.dentistas = await this._asyncTemplate.wrapUserFeedback(
			"Erro ao consultar professionais",
			() => this._professionalsApiService.listaDentistasAtivos()
		);
	}

	protected async _buildReport() {
		const sup = await super._buildReport();
		sup.pageOrientation = "landscape";
		return sup;
	}

	async buildReportHeader() {
		const behaviorFiltersValue = this._behaviorFilters$.value;
		const dates: [Date, Date] = [
			this._getDateFromFilter(behaviorFiltersValue.dataInicio),
			this._getDateFromFilter(behaviorFiltersValue.dataFim),
		];

		const professional =
			this.filters.profissionalId &&
			(this.dentistas || []).find((den) => den.id === this.filters.profissionalId);

		return [
			await this._commonsReportService.buildFinancialReportHeader(this.sheetTitle),
			{ ...CommonsReportBuildService.buildPeriodo(dates) },
			{
				...CommonsReportBuildService.buildProfessional(
					professional && professional.nome,
					"Profissional: "
				),
			},
			{
				...CommonsReportBuildService.buildSituation(behaviorFiltersValue.listaStatusTratamentoName),
			},
			{
				...CommonsReportBuildService.buildCovenant(behaviorFiltersValue.covenantName),
				margin: [0, 0, 0, 10],
			},
		];
	}
}
