import { TestBed } from '@angular/core/testing';

import { PermissionsApiService } from './permissions-api.service';

describe('PermissionsApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PermissionsApiService = TestBed.get(PermissionsApiService);
    expect(service).toBeTruthy();
  });
});
