import { Inject, Injectable } from '@angular/core';
import { API_CONFIGURATION, ApiConfiguration } from './api-configuration';
import { HttpClient } from '@angular/common/http';
import { DocumentModel } from './model/image';
import { isEmpty } from 'lodash-es';

const SAVE_EDIT_PATH = '/arquivoPaciente/salvar/arquivo/paciente';

@Injectable({
  providedIn: 'root'
})
export class DocumentsService {

  constructor(
          @Inject(API_CONFIGURATION) private readonly _config: ApiConfiguration,
          private readonly _http: HttpClient,
  ) {
  }

  list(patientId: number): Promise<DocumentModel[]> {
    const body = { id: patientId };

    return this._http
            .post<DocumentModel[]>(`${ this._config.apiBasePath }/arquivoPaciente/all`, body)
            .toPromise();
  }

  upload(document: DocumentModel, file?: File): Promise<DocumentModel> {
    const formData = this._buildFormData(document, file);

    return this._http
            .post<DocumentModel>(`${ this._config.apiBasePath + SAVE_EDIT_PATH }`, formData)
            .toPromise();
  }

  async download(documentModel: DocumentModel): Promise<void> {
    const data = await this._downloadResquest(documentModel);
    const blob = new Blob([data]);
    const htmlElement = document.createElement('a');

    // Cria url de download
    const url = window.URL.createObjectURL(blob);

    document.body.appendChild(htmlElement);
    htmlElement.href = url;
    htmlElement.download = (documentModel.descricao || documentModel.id) + '.' + documentModel.extensao;
    htmlElement.click();

    // Exclui url e elemento de download após download
    document.body.removeChild(htmlElement);
    window.URL.revokeObjectURL(url);
  }

  private _downloadResquest(document: DocumentModel): Promise<ArrayBuffer> {
    return this._http
            .get<ArrayBuffer>(`${ this._config.apiBasePath + '/arquivoPaciente/download/' }` + document.id,
                    { responseType: 'blob' as 'json' }).toPromise();
  }

  delete(documentId: number): Promise<void> {
    return this._http
            .delete<void>(`${ this._config.apiBasePath + '/arquivoPaciente/' }` + documentId)
            .toPromise();
  }

  private _buildFormData(document: DocumentModel, file?: File): FormData {
    const formData = new FormData();
    document.data = null;

    // Back-end não aceita "" ou null
    document.observacao = isEmpty(document.observacao) ? ' ' : document.observacao;

    const { id, descricao, observacao, data, pacienteId } = document;

    formData.append('file', file);
    formData.append('dados', JSON.stringify({ id, descricao, observacao, data, pacienteId }));

    return formData;
  }

}
