import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_CONFIGURATION, ApiConfiguration } from './api-configuration';
import { RespostaPaginada } from './model/resposta-paginada';
import { Produto } from './model/produto';
import { MovimentoEstoque } from './model/movimento-estoque';
import { DestinoDropdown } from './model/destino-dropdown';

export enum SituacaoEstoque {
  TODOS = 'TODOS',
  EM_FALTA = 'EM_FALTA',
  ABAIXO_MINIMO = 'ABAIXO_MINIMO'
}

export interface ProdutosListingParams {
  query?: string;
  page?: number;
  pageSize?: number;
  sorting?: [string?, ('asc' | 'desc')?][];
  situacaoEstoque: SituacaoEstoque;
}

export interface MovimentacoesListingParams {
  page?: number;
  size?: number;
}

export interface RespostaMovimentacoes {
  produtoId: number;
  produtoNome: string;
  movimentacoes: RespostaPaginada<MovimentoEstoque>;
}

@Injectable({
  providedIn: 'root',
})
export class ProdutosApiService {
  constructor(
    private readonly _http: HttpClient,
    @Inject(API_CONFIGURATION)
    private readonly _config: Readonly<ApiConfiguration>,
  ) {
  }

  listarProdutos(
    {
      query,
      page,
      pageSize = 10,
      situacaoEstoque,
      sorting,
    }: ProdutosListingParams,
  ) {
    if (!sorting || sorting.length === 0) {
      sorting = [['undefined', 'asc']];
    }

    return this._http
      .post<RespostaPaginada<Produto>>(
        `${this._config.apiBasePath}/produto/all`,
        {
          query,
          page,
          situacaoEstoque,
          size: pageSize,
          sorting: sorting.reduce(
            (obj, [prop, dir]) => ({ ...obj, [prop]: dir }),
            {},
          ),
        },
      )
      .toPromise();
  }

  salvarProduto(prod: Produto): Promise<unknown> {
    return this._http
      .post(`${this._config.apiBasePath}/produto/save`, prod)
      .toPromise();
  }

  apagarProduto(idProd: number): Promise<void> {
    return this._http
      .delete<void>(`${this._config.apiBasePath}/produto/${idProd}`)
      .toPromise();
  }

  listarMovimentacoes(
    produtoId: number,
    { page, size }: MovimentacoesListingParams,
  ): Promise<RespostaMovimentacoes> {
    return this._http
      .post<RespostaMovimentacoes>(
        `${this._config.apiBasePath}/movimentoEstoque/getMovimentacaoProduto`,
        { size, page, produtoId, sorting: { undefined: 'asc' } },
      )
      .toPromise();
  }

  listarDestinos(): Promise<DestinoDropdown[]> {
    return this._http
      .get<DestinoDropdown[]>(
        `${this._config.apiBasePath}/destinoEstoque/dropdown`,
      )
      .toPromise();
  }

  salvarDestino(descricao: string): Promise<DestinoDropdown> {
    return this._http
      .post<DestinoDropdown>(
        `${this._config.apiBasePath}/destinoEstoque/save`,
        {
          descricao,
        },
      )
      .toPromise();
  }

  atualizarDestino(idDestino: number, destino: Partial<DestinoDropdown>): Promise<DestinoDropdown> {
    return this._http
      .post<DestinoDropdown>(
        `${this._config.apiBasePath}/destinoEstoque/save`,
        {
          ...destino,
          id: idDestino,
        },
      )
      .toPromise();
  }

  salvarMovimentacao(movimentacao: MovimentoEstoque): Promise<void> {
    return this._http
      .post<void>(
        `${this._config.apiBasePath}/movimentoEstoque/save`,
        movimentacao,
      )
      .toPromise();
  }

  apagarDestinoMovimentacao(id: number): Promise<unknown> {
    return this._http.delete(`${this._config.apiBasePath}/destinoEstoque/${id}`).toPromise();
  }
}
