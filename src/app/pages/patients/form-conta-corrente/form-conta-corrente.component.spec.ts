import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormContaCorrenteComponent } from './form-conta-corrente.component';

describe('FormContaCorrenteComponent', () => {
  let component: FormContaCorrenteComponent;
  let fixture: ComponentFixture<FormContaCorrenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormContaCorrenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormContaCorrenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
