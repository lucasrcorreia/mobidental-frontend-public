import { Component, Input, OnInit } from '@angular/core';
import { DocumentModel } from '../../../../api/model/image';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { isEmpty } from 'lodash-es';

@Component({
  selector: 'app-edit-document',
  templateUrl: './edit-document.component.html',
  styleUrls: ['./edit-document.component.scss']
})
export class EditDocumentComponent implements OnInit {

  @Input() document: DocumentModel;

  constructor(public activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  edit() {
    this.activeModal.close(this.document);
  }

  get requiredFieldsAreFilleds(): boolean {
    return this.document &&
            this.document.id &&
            !isEmpty(this.document.descricao) &&
            this.document.descricao !== ' ';
  }

}
