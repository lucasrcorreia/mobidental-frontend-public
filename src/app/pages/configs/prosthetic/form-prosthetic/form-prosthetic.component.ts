import { Component, OnInit } from '@angular/core';
import { ProstheticModel } from '../../../../core/models/config/prosthetic/prosthetic.model';
import { FormGroup } from '@angular/forms';
import { UF_LIST } from '../../../../core/constants/uf/uf';
import { UtilsService } from '../../../../services/utils/utils.service';
import { FormProsthetic } from '../../../../core/forms/prosthetic';
import { CONSTANTS } from '../../../../core/constants/constants';
import { ActivatedRoute, Router } from "@angular/router";

const IMPOSSIBLE_ID = 'novo';

@Component({
	selector: 'app-form-prosthetic',
	templateUrl: './form-prosthetic.component.html',
	styleUrls: ['./form-prosthetic.component.scss']
})
export class FormProstheticComponent implements OnInit {

	formProsthetic: FormGroup;
	ufList = UF_LIST;
	prosthetic: ProstheticModel;

	constructor(public service: UtilsService,
				private readonly _route: ActivatedRoute,
				private readonly _router: Router) { }

	ngOnInit() {
		this._initProsthetic();
	}

	private _initForm() {
		this.formProsthetic = this.service.createForm(FormProsthetic, this.prosthetic);
	}

	goBackToProstheticPage() {
		this._router.navigate(['..'], { relativeTo: this._route });
	}

	private async _initProsthetic() {
		const prostheticId = this._route.snapshot.params.id;
		if (prostheticId === IMPOSSIBLE_ID) {
			this.prosthetic = new ProstheticModel();
		} else {
			try {
				const data = await this.service.httpGET(CONSTANTS.ENDPOINTS.config.prosthetic.findOne + prostheticId).toPromise();
				this.prosthetic = data.body as ProstheticModel;
			} catch (e) {

				this.service.notification.error(this.service.translate.instant('COMMON.ERROR.SEARCH'), e.error.error);
			}
		}

		this._initForm();
	}

	async save() {
		try {
			this.service.loading(true);
			await this.service.httpPOST(CONSTANTS.ENDPOINTS.config.prosthetic.save, this.formProsthetic.value).toPromise();
			const prostheticId = this._route.snapshot.params.id;
			this.service.notification.success(prostheticId === IMPOSSIBLE_ID ? 'Protético criado.' : 'Protético editado.');
			this.goBackToProstheticPage()
		} catch (e) {

			this.service.notification.error(this.service.translate.instant('COMMON.ERROR.SEARCH'), e.error.error);
		} finally {
			this.service.loading(false);
		}
	}
}
