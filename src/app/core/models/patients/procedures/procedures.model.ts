export interface PatientServicesTableModel {
  content: ContentPatientServicesTableModel[];
  totalElements: number;
  totalPages: number;
}

export interface ContentPatientServicesTableModel {
  contaId: number;
  convenioEspecialidadeProcedimentoId: number;
  convenioEspecialidadeProcedimentoProcedimentoId: number;
  convenioEspecialidadeProcedimentoProcedimentoNome: string;
  data: string;
  dataFinalizacao: any;
  diagnostico: string;
  emDente: string;
  emDenteDescricao: string;
  id: number;
  isFaceDistal: boolean;
  isFaceLingualPalatina: boolean;
  isFaceMesial: boolean;
  isFaceOclusalIncisal: boolean;
  isFaceVestibular: boolean;
  observacao: string;
  porcentagemComissao: number;
  profissionalId: number;
  profissionalNome: string;
  statusComissao: string;
  statusTratamentoServico: string;
  statusTratamentoServicoDescricao: string;
  tratamentoConvenioId: number;
  tratamentoDescricao: string;
  tratamentoId: number;
  tratamentoPacienteId: number;
  valor: number;
  valorComDesconto: number;
  valorProtetico: number;
  pacienteId: number;
}

export interface ProcedureSpecialties {
  especialidadeId: number;
  id: number;
  preco: number;
  procedimentoId: number;
  procedimentoIsAceitaFace: boolean;
  procedimentoNome: string;
  procedimentoPorcentagemComissao: number;
  procedimentoStatus: boolean;
  procedimentoValorCustoProtetico: number;
}
