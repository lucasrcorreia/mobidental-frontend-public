export enum SimNaoResposta {
  SIM = 'SIM',
  NAO = 'NAO',
}

export const SIM_NAO_TRANSLATE: { [key in SimNaoResposta]: string } = {
  SIM: 'Sim',
  NAO: 'Não',
};
