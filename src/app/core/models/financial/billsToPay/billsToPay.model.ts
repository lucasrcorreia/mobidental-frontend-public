export interface BillsToPayTableModel {
  concialacaoId: number;
  contaPagarDataHoraCadastro: string;
  contaPagarDescricao: string;
  contaPagarId: number;
  contaPagarNumeroTotalParcelas: number;
  contaPagarTipoCusto: string;
  contaPagarValor: number;
  dataCompetencia: string;
  dataPagamento: any;
  dataVencimento: string;
  formaDeRecebimento: string;
  id: number;
  itemsRateio: RatioTableBillsToPayModel[];
  numeroParcela: number;
  valor: number;
  observacao: string;
}

export interface RatioTableBillsToPayModel {
  id: number;
  categoriaId: number;
  categoria: string;
  centroCustoId: number;
  centroCusto: string;
  valor: number;
}

export class ParcelAccountBillsToPayModel {
  id: number = null;
  numeroParcela: number = null;
  dataCompetencia: any = null;
  dataVencimento: any = null;
  dataPagamento: any = null;
  formaDeRecebimento: string = null;
  valor: number = null;
  observacao: string = null;
  contaPagarId: number = null;
}

export class RatioAccountBillsToPayModel {
  id: number = null;
  categoriaId: number = null;
  categoria: string = null;
  centroCustoId: number = null;
  centroCusto: string = null;
  contaPagarId: number = null;
  valor: number = null;
}

export class AuxPropertyBillsToPay {
  dataVencimento: any = null;
  formaDeRecebimento: string = null;
}

export class AccountBillsToPayModel {
  id: number = null;
  numeroParcela: number = null;
  numeroTotalParcelas: number = null;
  descricao: string = null;
  dataCompetencia: any = null;
  dataVencimento: any = null;
  dataPagamento: any = null;
  formaDeRecebimento: string = null;
  valor: number = null;
  desconto: number = null;
  juros: number = null;
  valorTotal: number = null;
  observacao: string = null;
  conciliacaoId: string = null;
  categoriaId: number = null;
  categoria: string = null;
  centroCustoId: number = null;
  numeroDocumento: string = null;
  tipoCusto: string = null;
  recorrenciaId: string = null;
  servicoProteticoId: number = null;
}
