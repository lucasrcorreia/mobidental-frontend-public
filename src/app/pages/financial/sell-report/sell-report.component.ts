import { Component, OnInit } from "@angular/core";
import { UtilsService } from "../../../services/utils/utils.service";
import {
	CASH_FLOW_MONTHS,
	CashFlowFilter,
	numberToBRLCurrency,
} from "../fluxo-caixa/cash-flow-report/cash-flow-report.component";
import * as moment from "moment";
import { NotificationsService } from "angular2-notifications";
import { GlobalSpinnerService } from "../../../lib/global-spinner.service";
import { CashFlowService } from "../fluxo-caixa/cash-flow-report/service/cash-flow.service";
import { descricoesPagamentos } from "../../../api/model/forma-pagamento";
import { SellReport } from "./models/sell-report";
import {
	SellReportItemType,
	SellReportItemTypeDescription,
} from "./models/enums/sell-report-item-type";
import { SellReportItemChildren } from "./models/sell-report-item-children";
import { flatMapDeep, isEmpty } from "lodash-es";
import { ReportPrintType } from "../../reports/report-header-actions/report-header-actions.component";
import { SheetGeneratorService } from "../../../services/sheets-generator/sheet-generator.service";
import { SellReportItem } from "./models/sell-report-item";
import {
	PdfGeneratorService,
	PDFMake,
} from "../../../services/pdf-generator/pdf-generator.service";
import {
	BLUE_COLOR,
	CommonsReportBuildService,
	GREEN_COLOR,
	GREY_COLOR,
	RED_COLOR,
} from "../../../services/report/commons-report-build.service";
import { NgbPanelChangeEvent } from "@ng-bootstrap/ng-bootstrap";

const REPORT_FILE_NAME = "relatório-de-vendas";

@Component({
	selector: "app-sell-report",
	templateUrl: "./sell-report.component.html",
	styleUrls: ["./sell-report.component.scss"],
})
export class SellReportComponent implements OnInit {
	readonly months = CASH_FLOW_MONTHS;
	readonly years: number[] = [];
	readonly moment = moment();
	readonly descricoesPagamentos = descricoesPagamentos;
	readonly SellReportItemTypeDescription = SellReportItemTypeDescription;
	readonly numberToBRLCurrency = numberToBRLCurrency;
	readonly prefixTranslate = "FINANCIAL.SELL_REPORT.";

	filter: CashFlowFilter = { ano: moment().year(), mes: this.months[moment().month()].value };

	sellReport: SellReport;

	constructor(
		readonly _service: UtilsService,
		private readonly _notificationsService: NotificationsService,
		private readonly _cashFlowService: CashFlowService,
		private readonly _globalSpinnerService: GlobalSpinnerService,
		private readonly _sheetGeneratorService: SheetGeneratorService,
		private readonly _commonsReportService: CommonsReportBuildService,
		private readonly _pdfGeneratorService: PdfGeneratorService
	) {}

	ngOnInit() {
		this._fetchSellReport();
		this._generateYearsList();
	}

	async _fetchSellReport() {
		const loading = this._globalSpinnerService.loadingManager.start();
		try {
			this.sellReport = await this._cashFlowService.fetchSellReport(this.filter);
			this.fillExtraFields();
		} catch (e) {
			this._notificationsService.error("Ocorreu um erro ao listar os dados.");
		} finally {
			loading.finished();
		}
	}

	print(type: ReportPrintType): void {
		const loading = this._globalSpinnerService.loadingManager.start();
		try {
			switch (type) {
				case ReportPrintType.SHEETS:
					this._generateSheetsReport();
					break;
				default:
					this._generatePdfReport();
					break;
			}
		} catch (e) {
			this._notificationsService.error("Ocorreu um erro ao emitir o relatório.");
		} finally {
			loading.finished();
		}
	}

	private _generateSheetsReport() {
		this._sheetGeneratorService.exportJsonAsExcelFile(this._buildRowsToSheet(), REPORT_FILE_NAME);
	}

	private _buildRowsToSheet(): unknown[] {
		const items: SellReportItem[] = this.sellReport.items.map((item) => item);
		const childrens: SellReportItemChildren[] = flatMapDeep(
			items
				.map((item) => {
					return { data: item.data, childrens: item.childrens };
				})
				.map((item) => {
					item.childrens.forEach((children) => (children.data = item.data));
					return item.childrens;
				})
		);

		return childrens.map((children) => {
			return {
				Data: children.data,
				Descrição: children.descricao,
				Nome: children.nomePaciente,
				Valor: numberToBRLCurrency(children.valor),
				Tipo: this.SellReportItemTypeDescription[children.emTipoItemContaCorrente],
			};
		});
	}

	private async _generatePdfReport() {
		const pdfMake = await this._buildReport();
		const pdfMakeDefinition = await this._pdfGeneratorService.exportPDFMakeAsPDF(
			pdfMake,
			"",
			REPORT_FILE_NAME + ".pdf"
		);
		pdfMakeDefinition.open();
	}

	private async _buildReport(): Promise<PDFMake> {
		const pdfMake: PDFMake = { afterContent: [], pageMargins: [20, 20, 20, 20] };
		pdfMake.styles = {
			title: {
				fontSize: 22,
				bold: true,
				alignment: "center",
				margin: [10, 40, 0, 0],
			},
			subTitle: {
				fontSize: 9,
				alignment: "right",
			},
			tableHeader: {
				bold: true,
				fontSize: 10,
				alignment: "right",
			},
			tableLabel: {
				bold: true,
				fontSize: 10,
				color: GREY_COLOR,
			},
			resumeHeader: {
				bold: true,
				fontSize: 11,
			},
			resumeRow: {
				fontSize: 10,
			},
		};
		pdfMake.afterContent.push(await this._buildReportHeader());
		pdfMake.afterContent.push(this._buildReportBody());
		pdfMake.afterContent.push(this._buildReportResume());

		return pdfMake;
	}

	private async _buildReportHeader() {
		return [
			{ ...(await this._commonsReportService.buildFinancialReportHeader("Relatório de Vendas")) },
			{
				text: [
					{ text: "Mês: ", bold: true },
					CASH_FLOW_MONTHS.find((month) => month.value === this.filter.mes).label,
				],
				fontSize: 9,
				margin: [0, 10, 0, 0],
			},
			{
				text: [{ text: "Ano: ", bold: true }, this.filter.ano],
				fontSize: 9,
				margin: [0, 0, 0, 10],
			},
		];
	}

	private _buildReportBody() {
		return this.sellReport.items.map((item) => {
			return {
				margin: [0, 0, 0, 15],
				table: {
					widths: [100, 135, 135, 150],
					body: [
						[
							{
								text: item.dayOfMonth + "/" + item.dayOfMonth + " - " + item.dayOfWeek,
								style: "tableHeader",
								alignment: "left",
							},
							{
								text: "Vendas: " + numberToBRLCurrency(item.totalVenda),
								style: "tableHeader",
								color: BLUE_COLOR,
							},
							{
								text: "Entradas: " + numberToBRLCurrency(item.totalEntrada),
								style: "tableHeader",
								color: GREEN_COLOR,
							},
							{
								text: "Saídas: " + numberToBRLCurrency(item.totalSaida),
								style: "tableHeader",
								color: RED_COLOR,
								alignment: "center",
							},
						],
						...this._buildReportRows(item.childrens),
					],
				},
				layout: "lightHorizontalLines",
			};
		});
	}

	private _buildReportRows(childrens: SellReportItemChildren[]) {
		return childrens.map((children) => {
			const colorByType = this.getTableTextColor(children);
			return [
				{
					colSpan: 4,
					table: {
						widths: [200, 200, 120],
						body: [
							[
								{
									text: [
										{ text: "Paciente: ", style: "tableLabel" },
										{ text: children.nomePaciente, fontSize: 10 },
									],
								},
								{
									text: [
										{ text: "Descrição: ", style: "tableLabel" },
										{ text: children.descricao, fontSize: 10 },
									],
								},
								{
									rowSpan: 2,
									text: numberToBRLCurrency(children.valor) || " ",
									fontSize: 10,
									color: colorByType,
									alignment: "right",
								},
							],
							[
								{
									text: [
										{ text: "Tipo: ", style: "tableLabel" },
										{
											text: this.SellReportItemTypeDescription[children.emTipoItemContaCorrente],
											color: colorByType,
											fontSize: 10,
										},
									],
								},
								{
									text: [
										{ text: "Forma Pagamento: ", style: "tableLabel" },
										{
											text: this.descricoesPagamentos[children.emFormaDeRecebimento],
											fontSize: 10,
										},
									],
								},
								"",
							],
						],
					},
					layout: "noBorders",
				},
			];
		});
	}

	private _buildReportResume() {
		return {
			columns: [
				{
					width: 460,
					alignment: "right",
					stack: [
						{ text: "Vendas:", style: "resumeHeader", color: BLUE_COLOR },
						{ text: "Entradas:", style: "resumeHeader", color: GREEN_COLOR },
						{ text: "Saídas:", style: "resumeHeader", color: RED_COLOR },
					],
				},
				{
					alignment: "right",
					stack: [
						{
							text: numberToBRLCurrency(this.sellReport.totalVenda) || " - ",
							style: "resumeRow",
							color: BLUE_COLOR,
						},
						{
							text: numberToBRLCurrency(this.sellReport.totalEntrada) || " - ",
							style: "resumeRow",
							color: GREEN_COLOR,
						},
						{
							text: numberToBRLCurrency(this.sellReport.totalSaida) || " - ",
							style: "resumeRow",
							color: RED_COLOR,
						},
					],
				},
			],
			margin: [0, 10, 0, 0],
		};
	}

	private fillExtraFields() {
		if (this.sellReport && !isEmpty(this.sellReport.items)) {
			this.sellReport.items.forEach((item) => {
				item.monthAbbreviation = this._monthAbbreviation(item.data);
				item.dayOfWeek = this._dayOfWeek(item.data);
				item.dayOfMonth = this._dayOfMonth(item.data);
			});
		}
	}

	private _dayOfWeek(date) {
		return moment(this._ddmmyyyyToMoment(date)).format("dddd");
	}

	private _dayOfMonth(date) {
		return moment(this._ddmmyyyyToMoment(date)).format("DD");
	}

	private _monthAbbreviation(date) {
		return moment(this._ddmmyyyyToMoment(date)).format("MMM");
	}

	private _ddmmyyyyToMoment(date: string) {
		return date.split("/").reverse().join("-");
	}

	private _generateYearsList(): void {
		let minYear = 2010;
		const currentYear = moment().year();
		for (; minYear <= currentYear; minYear++) {
			this.years.push(minYear);
		}
	}

	getBadgeColor(children: SellReportItemChildren): string {
		switch (children.emTipoItemContaCorrente) {
			case SellReportItemType.ENTRADA:
				return "badge-success";
			case SellReportItemType.SAIDA:
				return "badge-danger";
			case SellReportItemType.VENDA:
				return "badge-primary";
			default:
				return "";
		}
	}

	getTextColor(children: SellReportItemChildren): string {
		switch (children.emTipoItemContaCorrente) {
			case SellReportItemType.ENTRADA:
				return "text-green";
			case SellReportItemType.SAIDA:
				return "text-crimson";
			case SellReportItemType.VENDA:
				return "txt-primary";
			default:
				return "";
		}
	}

	getTableTextColor(children: SellReportItemChildren): string {
		switch (children.emTipoItemContaCorrente) {
			case SellReportItemType.ENTRADA:
				return GREEN_COLOR;
			case SellReportItemType.SAIDA:
				return RED_COLOR;
			case SellReportItemType.VENDA:
				return BLUE_COLOR;
			default:
				return "";
		}
	}

	rotateArrowIcon(event: NgbPanelChangeEvent) {
		const icon = document.getElementById(event.panelId);
		if (icon) {
			icon.style.transform = event.nextState ? "rotate(90deg)" : "none";
		}
	}
}
