import { Component, Input, OnInit } from '@angular/core';
import { FluxoDeCaixaTotais } from '../../../../api/fluxo-de-caixa-api.service';
import { AutoSizedDatatableService } from '../../../../lib/auto-sized-datatable/auto-sized-datatable.service';

@Component({
  selector: 'app-fluxo-caixa-resumo',
  templateUrl: './fluxo-caixa-resumo.component.html',
  styleUrls: ['./fluxo-caixa-resumo.component.scss'],
})
export class FluxoCaixaResumoComponent implements OnInit {

  @Input() totais?: FluxoDeCaixaTotais;

  collapsed = false;

  constructor(
    private readonly _autoSizedDatatableService: AutoSizedDatatableService,
  ) {}

  ngOnInit() {
  }

  toggleCollapse() {
    this.collapsed = !this.collapsed;
    setTimeout(() => this._autoSizedDatatableService.recalcule());
  }

}
