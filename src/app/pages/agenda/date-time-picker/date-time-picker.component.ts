import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { NgbDateParserFormatter, NgbDatepicker, NgbDatepickerI18n, NgbDateStruct, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { UtilsService } from '../../../services/utils/utils.service';
import { CustomDatepickerI18n, I18n } from '../../../shared/ngb-datepicker/datepicker-i18n';
import { NgbDateFRParserFormatter } from '../../../shared/ngb-datepicker/ngb-date-fr-parser-formatter';

@Component({
  selector: 'app-date-time-picker',
  templateUrl: './date-time-picker.component.html',
  styleUrls: ['./date-time-picker.component.scss'],
  providers: [
    I18n,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
    { provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter }
  ]
})
export class DateTimePickerComponent implements OnInit, OnChanges {
  @ViewChild('dp') dp: NgbDatepicker;

  @Output() selectDate = new EventEmitter<string>(false);
  @Input() dateTime;
  date: NgbDateStruct;
  time: NgbTimeStruct;

  constructor(
    private service: UtilsService
  ) { }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.dateTime) {
      const arrayDateTime = this.dateTime.split(' ');


      this.date = this.service.parseDatePicker(arrayDateTime[0]);
      this.time = {
        hour: parseFloat(arrayDateTime[1].split(':')[0]),
        minute: parseFloat(arrayDateTime[1].split(':')[1]),
        second: 0,
      };

      this.dp.navigateTo(this.date);
    }
  }

  applyDate() {
    const date = this.service.parseDateFromDatePicker(this.date) + ' ' +
      this._formatTimeFields(this.time.hour) + ':' +
      this._formatTimeFields(this.time.minute) + ':' +
      this._formatTimeFields(this.time.second);
    this.selectDate.emit(date);
  }

  private _formatTimeFields(field: number): string {
    return ('0' + field).slice(-2);
  }
}
