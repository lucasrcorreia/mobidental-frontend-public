import { Component } from '@angular/core';
import { ContaApiService } from '../../../api/conta-api.service';
import { Conta } from '../../../api/model/conta';
import { imasks } from '../../../lib/input-masks';
import { NotificationsService } from 'angular2-notifications';
import { Router } from '@angular/router';
import { LoginService } from '../../../auth/login.service';
import { CONSTANTS } from '../../../core/constants/constants';
import { LoginApiService } from '../../../api/login-api.service';
import { GlobalSpinnerService } from '../../../lib/global-spinner.service';
import { Location } from '@angular/common';
import { UsuarioPerfilContaModuloSelecionado } from '../../../core/models/user/user.model';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.scss'],
})
export class UserRegistrationComponent {

  readonly cpfCnpjMask = imasks.cpfCnpj();
  readonly phoneNumberMask = imasks.phoneNumber();

  email?: string;
  senha?: string;
  nomeResponsavel?: string;
  nomeClinica?: string;
  cpfCnpj?: string;
  celular?: string;
  telefone?: string;

  constructor(
    private readonly _contaApiService: ContaApiService,
    private readonly _notificationsService: NotificationsService,
    private readonly _loginApiService: LoginApiService,
    private readonly _loginService: LoginService,
    private readonly _router: Router,
    private readonly _globalSpinner: GlobalSpinnerService,
  ) {
  }

  async save() {
    const loadingToken = this._globalSpinner.loadingManager.start();

    try {
      const contaToBeSaved = this._buildConta();
      const newUser = await this._contaApiService.novaConta(contaToBeSaved);

      this._loginService.logAsUser(newUser);

      let contaModuloId = newUser.usuarioPerfilContaModuloSelecionado
        && newUser.usuarioPerfilContaModuloSelecionado.id;

      if (contaModuloId == null) {
        const contas = await this._loginApiService.encontraContas();
        const contaAtual = contas.find(
          // TODO: Escolher conta por id ou outra chave única
          conta => conta.razaoSocial === contaToBeSaved.razaoSocial,
        ) || contas[0];

        contaModuloId = contaAtual.usuarioPerfilContaModuloId;
      }

      await this._loginService.escolherConta(contaModuloId);

      window.location.replace('/' + CONSTANTS.ROUTERS.DASHBOARD);
    } catch (e) {
      const message = (e && e.error && typeof e.error.message === 'string')
        ? e.error.message
        : 'Erro desconhecido do sistema';

      this._notificationsService.error('Falha ao criar conta', message);
    } finally {
      loadingToken.finished();
    }
  }

  private _buildConta(): Conta {
    return {
      email: this.email,
      senha: this.senha,
      nomeDoResponsavel: this.nomeResponsavel,
      razaoSocial: this.nomeClinica,
      cpfCnpj: this.cpfCnpj,
      celular: this.celular,
      telefone: this.telefone,
    };
  }

}
