import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrintConfigurationComponent } from './print-configuration/print-configuration.component';
import { NgxTranslateModule } from "../../../../core/translate/translate.module";
import { RouterModule } from "@angular/router";
import { PrintRoutingModule } from "./print-routing.module";
import { FormsModule } from "@angular/forms";
import { UiSwitchModule } from "ngx-ui-switch";

@NgModule({
	declarations: [PrintConfigurationComponent],
	imports: [
		CommonModule,
		NgxTranslateModule,
		RouterModule,
		PrintRoutingModule,
		FormsModule,
		UiSwitchModule,
	]
})
export class PrintModule {}
