import { TranslateService } from "@ngx-translate/core";
import { Injectable } from "@angular/core";
import { ColumnsTableModel } from "../../../core/models/table/columns.model";

@Injectable()
export class TableColumnsTreatment {
	tableProcedures: ColumnsTableModel[];
	tableFinancial: ColumnsTableModel[];
	tableProceduresModal: ColumnsTableModel[];

	constructor(translate: TranslateService) {
		this.tableProcedures = [
			{
				action: true,
				name: translate.instant("PATIENTS.FORM_TREATMENT.TABLE_PROCEDURES.COL1"),
				prop: " id",
				sortable: false,
				width: 150,
			},
			{
				action: false,
				name: translate.instant("PATIENTS.FORM_TREATMENT.TABLE_PROCEDURES.COL2"),
				prop: "convenioEspecialidadeProcedimentoNome",
				sortable: true,
				width: 300,
			},
			{
				action: false,
				name: translate.instant("PATIENTS.FORM_TREATMENT.TABLE_PROCEDURES.COL3"),
				prop: "emDenteDescricao",
				sortable: true,
				width: 100,
			},
			{
				action: false,
				name: translate.instant("PATIENTS.FORM_TREATMENT.TABLE_PROCEDURES.COL4"),
				prop: "profissionalNome",
				sortable: true,
				width: 200,
			},
			{
				action: false,
				name: translate.instant("PATIENTS.FORM_TREATMENT.TABLE_PROCEDURES.COL5"),
				prop: "valor",
				sortable: true,
				width: 100,
				currency: true,
			},
			{
				action: false,
				name: translate.instant("PATIENTS.FORM_TREATMENT.TABLE_PROCEDURES.COL6"),
				prop: "statusTratamentoServicoDescricao",
				sortable: true,
				width: 120,
			},
		];

		this.tableFinancial = [
			{
				action: true,
				name: translate.instant("PATIENTS.FORM_TREATMENT.TABLE_FINANCIAL.COL1"),
				prop: "id",
				sortable: false,
				width: 55,
			},
			{
				action: false,
				name: translate.instant("PATIENTS.FORM_TREATMENT.TABLE_FINANCIAL.COL2"),
				prop: "dataVencimento",
				sortable: true,
				width: 120,
			},
			{
				action: false,
				name: translate.instant("PATIENTS.FORM_TREATMENT.TABLE_FINANCIAL.COL3"),
				prop: "numeroParcela",
				sortable: true,
				width: 100,
			},
			{
				action: false,
				name: translate.instant("PATIENTS.FORM_TREATMENT.TABLE_FINANCIAL.COL4"),
				prop: "valor",
				sortable: true,
				width: 100,
				currency: true,
			},
			{
				action: false,
				name: translate.instant("PATIENTS.FORM_TREATMENT.TABLE_FINANCIAL.COL5"),
				prop: "descricao",
				sortable: true,
				width: 200,
			},
			{
				action: false,
				name: translate.instant("PATIENTS.FORM_TREATMENT.TABLE_FINANCIAL.COL6"),
				prop: "formaDeRecebimento",
				sortable: true,
				width: 200,
			},
		];

		this.tableProceduresModal = [
			{
				action: false,
				name: "",
				prop: "",
				sortable: false,
				width: 40,
				grow: 0,
				fixed: true,
			},
			{
				action: false,
				name: translate.instant("PATIENTS.FORM_TREATMENT.TABLE_PROCEDURES.COL2"),
				prop: "convenioEspecialidadeProcedimentoNome",
				sortable: true,
				width: 300,
				grow: 0,
			},
			{
				action: false,
				name: translate.instant("PATIENTS.FORM_TREATMENT.TABLE_PROCEDURES.COL3"),
				prop: "emDenteDescricao",
				sortable: true,
				width: 100,
				grow: 0,
			},
			{
				action: false,
				name: translate.instant("PATIENTS.FORM_TREATMENT.TABLE_PROCEDURES.COL5"),
				prop: "valor",
				sortable: true,
				width: 100,
				currency: true,
				grow: 0,
			},
		];
	}
}
