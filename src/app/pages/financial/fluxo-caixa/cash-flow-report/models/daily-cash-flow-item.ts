export interface DailyCashFlowItem {
    dia: number;
    data: string,
    totalEntrada: number;
    totalSaida: number;
    saldo: number;

    /* Campos criados no front-end, não conhecidos pelo back-end */
    badgeClassName?: string;
}
