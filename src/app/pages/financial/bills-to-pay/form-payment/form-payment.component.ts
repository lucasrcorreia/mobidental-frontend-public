import {
  Component,
  OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter,
} from '@angular/core';
import { AccountBillsToPayModel } from '../../../../core/models/financial/billsToPay/billsToPay.model';
import { FormGroup } from '@angular/forms';
import { UtilsService } from '../../../../services/utils/utils.service';
import { FormBillsToPay } from '../../../../core/forms/billsToPay';
import { LISTAS } from '../../lists';
import * as moment from 'moment';
import { CONSTANTS } from '../../../../core/constants/constants';
import { classToPlain } from 'class-transformer';

@Component({
  selector: 'app-form-payment',
  templateUrl: './form-payment.component.html',
  styleUrls: [
    './form-payment.component.scss',
    './../../../../../assets/icon/icofont/css/icofont.scss',
  ],
})
export class FormPaymentComponent implements OnInit, OnChanges {
  @Input() account: AccountBillsToPayModel;
  @Output() resultModal = new EventEmitter<any>();
  formAccount: FormGroup;
  localLists = new LISTAS(this.service.translate);
  locale = this.service.translate.instant('COMMON.LOCALE');

  constructor(public service: UtilsService) { }

  ngOnChanges() {
    const temp = classToPlain(this.account) as AccountBillsToPayModel;

    if (!temp.dataPagamento) {
      temp.dataPagamento = this.service.parseDatePicker(
        moment().format('DD/MM/YYYY'),
      );

      temp.juros = 0;
      temp.desconto = 0;
    } else {
      temp.dataPagamento = this.service.parseDatePicker(temp.dataPagamento);
    }

    this.formAccount = this.service.createForm(
      FormBillsToPay,
      temp,
    );



    this.formAccount.get('descricao').disable();
    this.formAccount.get('valor').disable();
    this.formAccount.get('dataVencimento').disable();
  }

  ngOnInit() { }

  pay() {
    this.service.loading(true);

    this.account.dataPagamento = this.service.parseDateFromDatePicker(
      this.formAccount.get('dataPagamento').value,
    );

    this.account.observacao = this.formAccount.get('observacao').value;
    this.account.formaDeRecebimento = this.formAccount.get(
      'formaDeRecebimento',
    ).value;

    const { valor, juros, desconto } = this.formAccount.getRawValue();

    const valorTotal = (Number(valor) || 0)
      + (Number(juros) || 0)
      - (Number(desconto) || 0);

    this.formAccount.get('valorTotal').setValue(valorTotal);

    const temp = classToPlain(this.formAccount.getRawValue()) as AccountBillsToPayModel;
    temp.dataPagamento = this.service.parseDateFromDatePicker(temp.dataPagamento);

    this.service
      .httpPOST(CONSTANTS.ENDPOINTS.financial.billsToPay.pay, temp)
      .subscribe(
        data => {
          this.resultModal.emit(this.account);
          this.service.loading(false);
          this.service.closeModal('modal-account-payment', null);
        },
        err => {

          this.service.loading(false);
          this.service.notification.error(
            this.service.translate.instant('COMMON.ERROR.SEARCH'),
            err.error.error,
          );
        },
      );
  }

  closeModal(id: string) {
    this.resultModal.emit(null);
    this.service.closeModal(id, null);
  }
}
