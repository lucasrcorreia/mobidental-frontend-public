import { Component, OnInit, OnDestroy } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { UtilsService } from "../../../../../services/utils/utils.service";
import { CONSTANTS } from "../../../../../core/constants/constants";
import {
	CovenantModel,
	ProceduresCovenantModel,
} from "../../../../../core/models/config/covenant/covenantCreate.model";
import { Subscription, Observable } from "rxjs";
import { debounceTime, distinctUntilChanged, flatMap, map } from "rxjs/operators";
import prop from "ramda/es/prop";
import {
	ComissionProcedure,
	SavedComissionProcedure,
} from "../../../../../api/professionals.api.service";
import omit from "ramda/es/omit";
import { AsyncTemplate } from "../../../../../lib/helpers/async-template";

@Component({
	selector: "app-comission-procedure-modal",
	templateUrl: "./comission-procedure-modal.component.html",
})
export class ComissionProcedureModalComponent implements OnInit {
	proc: Partial<SavedComissionProcedure> = {};

	covenants: ReadonlyArray<Readonly<CovenantModel>>;

	formatMatches = (value: ProceduresCovenantModel) => value.procedimentoNome;
	readonly procedures = (text$: Observable<string>) =>
		text$.pipe(
			debounceTime(300),
			distinctUntilChanged(),
			flatMap((value) => this._fetchProcedures(value))
		);

	constructor(
		private readonly _activeModal: NgbActiveModal,
		private readonly _asyncTemplate: AsyncTemplate,
		readonly _inutilsService: UtilsService
	) {}

	ngOnInit(): void {
		this._fetchCovenants().catch(() => this.close());
	}

	clearProcedure(clearText = false) {
		this.proc.convenioEspecialidadeProcedimentoId = undefined;
		if (clearText) {
			this.proc.convenioEspecialidadeProcedimentoProcedimentoNome = "";
		}
	}

	fillProcedure(model: ProceduresCovenantModel) {
		this.proc.convenioEspecialidadeProcedimentoId = model.id;
	}

	save() {
		const comissionProc: ComissionProcedure = this.proc as ComissionProcedure;
		this._activeModal.close(
			omit(
				[
					"convenioEspecialidadeProcedimentoProcedimentoNome",
					"profissionalNome",
					"convenioNome",
					"convenioEspecialidadeProcedimentoConvenioEspecialidadeEspecialidadeNome",
				],
				comissionProc
			)
		);
	}

	close() {
		this._activeModal.close();
	}

	get searchProcedureValue() {
		return this.proc.convenioEspecialidadeProcedimentoProcedimentoNome;
	}

	private async _fetchCovenants() {
		this.covenants = (await this._asyncTemplate.wrapUserFeedback(
			"Erro ao consultar convênios",
			() =>
				this._inutilsService
					.httpGET(CONSTANTS.ENDPOINTS.patients.treatment.convenant)
					.toPromise()
					.then(prop("body"))
		)) as any;
	}

	private _fetchProcedures(text: string) {
		return !text || !this.proc.convenioId
			? []
			: this._inutilsService
					.httpGET(
						`api/convenio_especialidade_procedimento/listarProcedimentosByConvenio/?convenioId=${this.proc.convenioId}&filtroProcedimentoNome=${text}`
					)
					.pipe(map(prop("body")));
	}
}
