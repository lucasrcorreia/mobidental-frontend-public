import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProdutosListComponent } from './produtos-list/produtos-list.component';
import { MovimentacoesProdutoComponent } from './movimentacoes-produto/movimentacoes-produto.component';

export const routes: Routes = [
  { path: '', redirectTo: 'list' },
  {
    path: 'list',
    component: ProdutosListComponent
  },
  {
    path: 'list',
    component: ProdutosListComponent
  },
  {
    path: 'movimentacoes/:idProduto',
    component: MovimentacoesProdutoComponent
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)]
})
export class ProdutosRoutingModule {}
