import { TestBed } from '@angular/core/testing';

import { CommonPrintService } from './common-print.service';

describe('CommonPrintService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommonPrintService = TestBed.get(CommonPrintService);
    expect(service).toBeTruthy();
  });
});
