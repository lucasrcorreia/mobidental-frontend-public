import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { UtilsService } from "../../../services/utils/utils.service";
import { AgendaModel, ServicesListModel } from "../../../core/models/agenda/agenda.model";
import { ConvenantModel } from "../../../core/models/patients/convenio.model";
import { ShortListItem } from "../../../core/models/forms/common/common.model";
import { PatientModel } from "../../../core/models/patients/patients.model";
import { ModalAnimationComponent } from "../../../shared/modal-animation/modal-animation.component";
import { COMMITMENT, SCHEDULING } from "../agenda.component";

@Component({
	selector: "app-agendamento",
	templateUrl: "./agendamento.component.html",
	styleUrls: ["./agendamento.component.scss", "./../../../../assets/icon/icofont/css/icofont.scss"],
})
export class AgendamentoComponent implements OnInit {
	@ViewChild("schedulingComponent") schedulingComponent?;

	@Input() convenantsList: ConvenantModel[] = [];
	@Input() professionals: ShortListItem[] = [];
	@Input() patient: PatientModel;
	@Input() servicesList: ServicesListModel[] = [];
	@Input() eventObj: AgendaModel;
	@Input() modal?: ModalAnimationComponent;
	@Input() source = "scheduling-page";

	@Output() eventsChange = new EventEmitter<boolean>(false);

	readonly scheduling = SCHEDULING;
	readonly commitment = COMMITMENT;
	modelType = this.scheduling;

	constructor(public service: UtilsService) { }

	ngOnInit() {
		console.log("evento", this.eventObj);
		if (this.eventObj.emTipoAgendamento === this.commitment) {
			this.modelType = this.commitment;
			if (this.eventObj.id) {
				document.getElementById("card-modal").style.minHeight = "360px";
			}
		}
		this.changeModalWidth(this.modelType);
	}

	changeModalWidth($event) {
		const modal = document.getElementsByClassName("md-modal-lg")[0];
		(modal as HTMLElement).style.maxWidth = $event === COMMITMENT ? "550px" : "900px";
	}

	close() {
		if (this.modal) {
			this.modal.close();
		}
	}
}
