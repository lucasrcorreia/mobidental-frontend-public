import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormCovenantComponent } from './form-covenant.component';

describe('FormCovenantComponent', () => {
  let component: FormCovenantComponent;
  let fixture: ComponentFixture<FormCovenantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormCovenantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCovenantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
