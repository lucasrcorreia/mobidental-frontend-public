import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContaPickerComponent } from './conta-picker/conta-picker.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Authentication',
      status: false,
    },
    children: [
      {
        path: 'login',
        loadChildren: './login/login.module#LoginModule',
      },
      {
        path: 'conta',
        component: ContaPickerComponent,
      },
      {
        path: 'registration',
        loadChildren: './registration/registration.module#RegistrationModule',
      },
      {
        path: 'forgot',
        loadChildren: './forgot/forgot.module#ForgotModule',
      },
      {
        path: 'lock-screen',
        loadChildren: './lock-screen/lock-screen.module#LockScreenModule',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
