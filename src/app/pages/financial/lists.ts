import {
  SelectItem,
  SelectItemStatus
} from '../../core/models/forms/common/common.model';
import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';

@Injectable()
export class LISTAS {
  listSearchRange: SelectItem[];
  listTypeBill: SelectItem[];
  listTypeBillReceive: SelectItem[];
  listTypePayment: SelectItem[];

  constructor(translate: TranslateService) {
    this.listSearchRange = [
      {
        label: translate.instant('FINANCIAL.LIST_SEARCH.RANGE.TODAY'),
        value: 'TODAY'
      },
      {
        label: translate.instant('FINANCIAL.LIST_SEARCH.RANGE.WEEK'),
        value: 'WEEK'
      },
      {
        label: translate.instant('FINANCIAL.LIST_SEARCH.RANGE.MONTH'),
        value: 'MONTH'
      },
      {
        label: translate.instant('FINANCIAL.LIST_SEARCH.RANGE.PREV_MONTH'),
        value: 'PREV_MONTH'
      },
      {
        label: translate.instant('FINANCIAL.LIST_SEARCH.RANGE.BY_RANGE'),
        value: 'BY_RANGE'
      }
    ];

    this.listTypeBill = [
      {
        label: translate.instant('FINANCIAL.LIST_SEARCH.TYPE_BILL.TO_PAY'),
        value: 'A_PAGAR'
      },
      {
        label: translate.instant('FINANCIAL.LIST_SEARCH.TYPE_BILL.PAID'),
        value: 'PAGO'
      },
      {
        label: translate.instant('FINANCIAL.LIST_SEARCH.TYPE_BILL.LAZY'),
        value: 'ATRASADO'
      }
    ];

    this.listTypeBillReceive = [
      {
        label: translate.instant('FINANCIAL.LIST_SEARCH.TYPE_BILL.TO_RECEIVE'),
        value: 'A_RECEBER'
      },
      {
        label: translate.instant('FINANCIAL.LIST_SEARCH.TYPE_BILL.RECEIVED'),
        value: 'RECEBIDO'
      },
      {
        label: translate.instant('FINANCIAL.LIST_SEARCH.TYPE_BILL.LAZY'),
        value: 'ATRASADO'
      }
    ];

    this.listTypePayment = [
      {
        label: translate.instant('FINANCIAL.LIST_SEARCH.TYPE_PAYMENT.BOLETO'),
        value: 'BOLETO'
      },
      {
        label: translate.instant(
          'FINANCIAL.LIST_SEARCH.TYPE_PAYMENT.CARTAO_CREDITO'
        ),
        value: 'CARTAO_CREDITO'
      },
      {
        label: translate.instant(
          'FINANCIAL.LIST_SEARCH.TYPE_PAYMENT.CARTAO_DEBITO'
        ),
        value: 'CARTAO_DEBITO'
      },
      {
        label: translate.instant('FINANCIAL.LIST_SEARCH.TYPE_PAYMENT.CHEQUE'),
        value: 'CHEQUE'
      },
      {
        label: translate.instant('FINANCIAL.LIST_SEARCH.TYPE_PAYMENT.DINHEIRO'),
        value: 'DINHEIRO'
      },
      {
        label: translate.instant(
          'FINANCIAL.LIST_SEARCH.TYPE_PAYMENT.DEBITO_AUTOMATICO'
        ),
        value: 'DEBITO_AUTOMATICO'
      },
      {
        label: translate.instant(
          'FINANCIAL.LIST_SEARCH.TYPE_PAYMENT.TRANSFERENCIA_BANCARIA'
        ),
        value: 'TRANSFERENCIA_BANCARIA'
      },
      {
        label: translate.instant(
          'FINANCIAL.LIST_SEARCH.TYPE_PAYMENT.PERMUTA'
        ),
        value: 'PERMUTA'
      }
    ];
  }
}
