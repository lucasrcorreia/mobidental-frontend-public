import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { UtilsService } from '../../../services/utils/utils.service';
import { TreeviewComponent, TreeviewConfig, TreeviewItem } from 'ngx-treeview';
import { TreeviewService } from '../../../services/treeview/treeview.service';

@Component({
  selector: 'app-modal-ccost-category',
  templateUrl: './modal-ccost-category.component.html',
  styleUrls: [
    './modal-ccost-category.component.scss',
    './../../../../assets/icon/icofont/css/icofont.scss'
  ]
})
export class ModalCcostCategoryComponent implements OnInit, OnChanges {
  @ViewChild(TreeviewComponent) treeviewComponent: TreeviewComponent;

  @Input() items: any[];
  @Input() typeComp: string;
  @Input() isAccount = false;
  @Output() resultModal = new EventEmitter<any>();

  values: any[];

  treeview = {
    config: TreeviewConfig.create({
      hasAllCheckBox: false,
      hasFilter: false,
      hasCollapseExpand: false,
      decoupleChildFromParent: false,
      maxHeight: window.innerHeight / 2
    }),
    items: []
  };

  constructor(
    public service: UtilsService,
    public treeviewService: TreeviewService
  ) {}

  ngOnInit() {}

  ngOnChanges() {
    this.treeview.items = Array.from(this.items);
  }

  clickItem(item: TreeviewItem) {
    if (this.isAccount) {
      if (!item.children || item.children.length === 0) {
        this.treeview.items = [];
        this.service.closeModal('modal-ccost-cat-account', false);
        this.resultModal.emit(item.value);
      }
    } else {
      item.checked = !item.checked;
      this.selectChildren(item, item.checked);
    }
  }

  checkAll(checked: boolean) {
    this.treeview.items.map(item => {
      this.selectChildren(item, checked);
    });
  }

  selectChildren(i: TreeviewItem, checked: boolean) {
    i.collapsed = false;

    if (i.children) {
      this.selectInsideChildren(i, checked);

      i.checked = checked;
    } else {
      i.checked = checked;
    }

    this.treeviewComponent.raiseSelectedChange();
  }

  selectInsideChildren(item: TreeviewItem, checked: boolean) {
    item.children.forEach(i => {
      i.checked = checked;

      if (i.children) {
        this.selectInsideChildren(i, checked);
      }
    });
  }

  applyFilter() {
    this.resultModal.emit(this.values);
    this.service.closeModal('modal-ccost-cat', null);
  }

  onSelectedChange(event) {
    this.values = event;
  }
}
