import { Injectable } from '@angular/core';
import { DocumentModel } from '../../../api/model/image';
import { EditDocumentComponent } from './edit-document/edit-document.component';
import { DeleteDocumentComponent } from './delete-document/delete-document.component';
import { DocumentsService } from '../../../api/documents.service';
import { UtilsService } from '../../../services/utils/utils.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsService } from 'angular2-notifications';
import { isEmpty } from 'lodash-es';
import { BehaviorSubject } from 'rxjs';

enum AvaliableExtension {
  GIF = 'GIF',
  PNG = 'PNG',
  JPEG = 'JPEG',
  SVG = 'SVG',
  PDF = 'PDF',
  DOC = 'DOC',
  ODT = 'ODT',
  DOCX = 'DOCX',
}

const ordinalExtensionDescription: { [key in AvaliableExtension]: string } = {
  GIF: 'gif',
  PNG: 'png',
  JPEG: 'jpeg',
  SVG: 'svg',
  PDF: 'pdf',
  DOC: 'doc',
  ODT: 'odt',
  DOCX: 'docx',
};

const PDF_SVG_PATH = 'assets/images/documents/pdf.svg';
const DOC_SVG_PATH = 'assets/images/documents/doc.svg';

@Injectable({
  providedIn: 'root'
})
export class DocumentsCommonService {

  readonly imagesOrdinalExtensions: string[] = [
    ordinalExtensionDescription.GIF,
    ordinalExtensionDescription.JPEG,
    ordinalExtensionDescription.PNG,
    ordinalExtensionDescription.SVG
  ];
  readonly docsOrdinalExtensions: string[] = [
    ordinalExtensionDescription.DOCX,
    ordinalExtensionDescription.DOC,
    ordinalExtensionDescription.ODT
  ];

  readonly nonImageExtensions: string[] = this.docsOrdinalExtensions.concat(ordinalExtensionDescription.PDF);

  private _dataSource = new BehaviorSubject<boolean>(false);
  $data = this._dataSource.asObservable();

  constructor(
          private readonly _documentsService: DocumentsService,
          private readonly _loadingService: UtilsService,
          private readonly _modalService: NgbModal,
          private readonly _notificationsService: NotificationsService,) {
  }

  async edit(document: DocumentModel) {
    const modalRef = this._modalService.open(EditDocumentComponent, { size: 'lg' });
    modalRef.componentInstance.document = document;

    const userResponse = await modalRef.result;

    if (userResponse && userResponse.id) {
      try {
        this._loadingService.loading(true);
        await this._documentsService.upload(document);
        this._notificationsService.success('Documento alterado com sucesso!');
      } catch (error) {
        this._notificationsService.error('Ocorreu um erro ao editar o documento. Por favor tente novamente.');
      } finally {
        this._loadingService.loading(false);
        this.refreshData();
      }
    }
  }

  download(document: DocumentModel) {
    try {
      this._notificationsService.info('Fazendo download do documento...');
      this._documentsService.download(document);
    } catch (error) {
      this._notificationsService.info('Ocorreu um erro ao tentar fazer o download.');
    }
  }

  async delete(document: DocumentModel) {
    const modalRef = this._modalService.open(DeleteDocumentComponent);
    modalRef.componentInstance.document = document;

    const userResponse: boolean = await modalRef.result;

    if (userResponse) {
      try {
        this._loadingService.loading(true);
        await this._documentsService.delete(document.id);
        this._notificationsService.success('Documento excluído com sucesso!');
      } catch (error) {
        this._notificationsService.error('Ocorreu um erro ao excluir o documento. Por favor tente novamente.');
      } finally {
        this._loadingService.loading(false);
        this.refreshData();
      }
    }
  }

  getCorrectUrlFromDocument(document: DocumentModel): DocumentModel {
    const extension = document.extensao;

    if (this.imagesOrdinalExtensions.includes(extension)) {
      return document;
    }

    if (this.docsOrdinalExtensions.includes(extension)) {
      document.urlAnexo = DOC_SVG_PATH;
      document.urlAnexoThumb = DOC_SVG_PATH;
    } else if (extension.includes(ordinalExtensionDescription.PDF)) {
      document.urlAnexo = PDF_SVG_PATH;
      document.urlAnexoThumb = PDF_SVG_PATH;
    }

    return document;
  }

  getCorrectUrlFromExtension(extension: string): string {
    return this.docsOrdinalExtensions.includes(extension) ? DOC_SVG_PATH : PDF_SVG_PATH;
  }

  getLabel(label: string) {
    return !isEmpty(label) && label !== ' ' ? label : 'Não disponível';
  }

  refreshData() {
    this._dataSource.next(true);
  }

}
