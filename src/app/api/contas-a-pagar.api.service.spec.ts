import { TestBed } from '@angular/core/testing';

import { ContasAPagar.ApiService } from './contas-a-pagar.api.service';

describe('ContasAPagar.ApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContasAPagar.ApiService = TestBed.get(ContasAPagar.ApiService);
    expect(service).toBeTruthy();
  });
});
