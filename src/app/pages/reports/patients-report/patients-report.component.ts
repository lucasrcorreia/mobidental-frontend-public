import { Component, Inject, OnInit } from '@angular/core';
import { AbstractReport } from '../abstract-report';
import { BirthDate, ReceiptsReportFilters, ReportsService } from '../reports.service';
import { UtilsService } from '../../../services/utils/utils.service';
import { NotificationsService } from 'angular2-notifications';
import { GlobalSpinnerService } from '../../../lib/global-spinner.service';
import { CommonsReportBuildService } from '../../../services/report/commons-report-build.service';
import { PdfGeneratorService, PdfMakePageOrientation } from '../../../services/pdf-generator/pdf-generator.service';
import { SheetGeneratorService } from '../../../services/sheets-generator/sheet-generator.service';
import { MaskFormatService } from '../../../services/utils/mask-format.service';
import { AsyncTemplate } from '../../../lib/helpers/async-template';
import { ReportDatatableColumn, Type } from '../report-datatable/report-datatable.component';
import { DropdownGenericService } from '../../patients/form-patients/services/dropdown-generic.service';
import { CustomDatepickerI18n, I18n } from '../../../shared/ngb-datepicker/datepicker-i18n';
import { NgbDateParserFormatter, NgbDatepickerI18n, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateFRParserFormatter } from '../../../shared/ngb-datepicker/ngb-date-fr-parser-formatter';
import { isEmpty } from 'lodash-es';
import { padNumber } from 'app/shared/ngb-datepicker/ngb-date-fr-parser-formatter';
import * as moment from 'moment';
import { AbstractReportFilters } from '../abstract-report-filters';

const TABLE_COLUMNS: ReportDatatableColumn[] = [
	{ prop: 'nomePaciente', translation: 'REPORTS.TABLE.NAME' },
	{ prop: 'dataCadastro', translation: 'REPORTS.TABLE.REGISTER_DATE', type: Type.DATE },
	{ prop: 'celular', translation: 'REPORTS.TABLE.CELL', type: Type.CELL },
	{ prop: 'telefone', translation: 'REPORTS.TABLE.PHONE', type: Type.PHONE },
	{ prop: 'sexo', translation: 'REPORTS.TABLE.GENDER', pdfAlignment: 'center' },
];

const PDF_COLUMNS: ReportDatatableColumn[] = [
	{ prop: 'nomePaciente', translation: 'REPORTS.TABLE.NAME' },
	{ prop: 'dataCadastro', translation: 'REPORTS.TABLE.REGISTER_DATE', type: Type.DATE },
	{ prop: 'celular', translation: 'REPORTS.TABLE.CELL', type: Type.CELL },
	{ prop: 'telefone', translation: 'REPORTS.TABLE.PHONE', type: Type.PHONE },
	{ prop: 'sexo', translation: 'REPORTS.TABLE.GENDER', pdfAlignment: 'center' },
	{ prop: 'indicacao', translation: 'REPORTS.TABLE.INDICATION' },
	{ prop: 'situacaoPaciente', translation: 'REPORTS.TABLE.SITUATION' },
	{ prop: 'nomeProfissional', translation: 'REPORTS.TABLE.RESPONSIBLE_PROFESSIONAL' },
	{ prop: 'nomeEmpresa', translation: 'REPORTS.TABLE.ORGANIZATION_NAME' },
];

const SHEET_COLUMNS: ReportDatatableColumn[] = [
	{ prop: 'nomePaciente', translation: 'REPORTS.TABLE.PATIENT_NAME' },
	{ prop: 'telefone', translation: 'REPORTS.TABLE.PHONE', type: Type.PHONE },
	{ prop: 'convenio', translation: 'REPORTS.TABLE.COVENANT' },
	{ prop: 'situacaoPaciente', translation: 'REPORTS.TABLE.PATIENT_SITUATION' },
	{ prop: 'indicacao', translation: 'REPORTS.TABLE.INDICATION' },
	{ prop: 'descricaoIndicacao', translation: 'REPORTS.TABLE.INDICATION_DESCRIPTION' },
	{ prop: 'nomeEmpresa', translation: 'REPORTS.TABLE.ORGANIZATION_NAME' },
	{ prop: 'nomeProfissional', translation: 'REPORTS.TABLE.PROFESSIONAL_NAME' },
	{ prop: 'especialidades', translation: 'REPORTS.TABLE.EXPERTISES' },
	{ prop: 'celular', translation: 'REPORTS.TABLE.CELL', type: Type.CELL },
	{ prop: 'dataNascimento', translation: 'REPORTS.TABLE.BIRTH_DATE', type: Type.DATE },
	{ prop: 'sexo', translation: 'REPORTS.TABLE.GENDER' },
	{ prop: 'dataCadastro', translation: 'REPORTS.TABLE.REGISTER_DATE', type: Type.DATE },
	{ prop: 'email', translation: 'REPORTS.TABLE.EMAIL' },
];

@Component({
	selector: 'app-patients-report',
	templateUrl: './patients-report.component.html',
	styleUrls: ['./patients-report.component.scss'],
	providers: [
		I18n,
		{ provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
		{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
	],
})
export class PatientsReportComponent extends AbstractReport implements OnInit {

	filters: ReceiptsReportFilters = {};
	ownData: any[];
	sheetFileName = 'relatorio_pacientes';
	readonly columns = TABLE_COLUMNS;
	readonly sheetColumns = SHEET_COLUMNS;
	readonly pdfColumns = PDF_COLUMNS;
	reportPageOrientation: PdfMakePageOrientation = 'landscape';

	birthFinalDateModel: NgbDateStruct;
	birthInitialDateModel: NgbDateStruct;

	sheetTitle = this._service.translate.instant('REPORTS.PATIENTS.TITLE');
	reportTableBodyWidths: number[] = [150, 50, 60, 60, 60, 80, 90, 90, 80];

	constructor(private readonly _reportsService: ReportsService,
				@Inject(UtilsService)  _service: UtilsService,
				@Inject(DropdownGenericService) _dropdownGenericService: DropdownGenericService,
				@Inject(NotificationsService) _notificationsService: NotificationsService,
				@Inject(GlobalSpinnerService) _globalSpinnerService: GlobalSpinnerService,
				@Inject(CommonsReportBuildService) _commonsReportService: CommonsReportBuildService,
				@Inject(PdfGeneratorService) _pdfGeneratorService: PdfGeneratorService,
				@Inject(SheetGeneratorService) _sheetGeneratorService: SheetGeneratorService,
				@Inject(MaskFormatService) _maskFormatService: MaskFormatService,
				@Inject(AsyncTemplate) _asyncTemplate: AsyncTemplate) {
		super(_service, _dropdownGenericService, _notificationsService, _globalSpinnerService, _commonsReportService, _pdfGeneratorService, _sheetGeneratorService, _maskFormatService, _asyncTemplate);
	}

	ngOnInit() {
		this.filters.dataFim = AbstractReportFilters.momentFromNgbDateStruct(moment());
		this.filters.dataInicio = AbstractReportFilters.momentFromNgbDateStruct(moment().subtract(30, 'days'));
		this.search();
		this.initOrganizationsDropdown();
		this.initSituationsDropdown();
		this.initExpertisesDropdown();
		this.initProfessionalDropdown();
	}

	getBirthDateDisplay(ngbDate: NgbDateStruct): string {
		return !ngbDate ? 'dd/mm' : padNumber(ngbDate.day) + '/' + padNumber(ngbDate.month);
	}

	clearBirthDates() {
		this.birthFinalDateModel = null;
		this.birthInitialDateModel = null;
	}

	async search() {
		if (!this.requiredFiltersArePresent) {
			return;
		}
		const filters = { ...this.filters };
		filters.statusTratamento = isEmpty(filters.statusTratamento) ? [] : [filters.statusTratamento.toString()];
		filters.especialidades = isEmpty(filters.especialidades) ? [] : filters.especialidades;
		filters.situacaoPaciente = !filters.situacaoPaciente ? [] : [+filters.situacaoPaciente.toString()];
		filters.indicacoes = isEmpty(filters.indicacoes) ? [] : [filters.indicacoes.toString()];
		filters.nascimentoInicio = PatientsReportComponent._buildBirthDate(this.birthInitialDateModel);
		filters.nascimentoFim = PatientsReportComponent._buildBirthDate(this.birthFinalDateModel);
		this._behaviorFilters$.next({
			...filters,
			indicacoesName: this.listaStatusName(this.filters.indicacoes, this.indications),
			statusTratamentoName: this.listaStatusName(),
			situacaoPacienteName: this.listPatientStatusName,
			professionalName: this.professionalName,
			especialidadesName: this.listaEspecialidadesName,
			empresaName: this.organizationName,
		});
		this.ownData = await this.fetchOwnData(() => this._reportsService.findAllPatients(filters));
	}

	async buildReportHeader() {
		const behaviorFiltersValue = this._behaviorFilters$.value;
		const dates: [Date, Date] = [this._getDateFromFilter(behaviorFiltersValue.dataInicio), this._getDateFromFilter(behaviorFiltersValue.dataFim)];
		return [
			await this._commonsReportService.buildFinancialReportHeader(this.sheetTitle),
			{ ...CommonsReportBuildService.buildPeriodo(dates, 'Cadastrados em: ') },
			{ ...CommonsReportBuildService.buildBirthDates([behaviorFiltersValue.nascimentoInicio, behaviorFiltersValue.nascimentoFim]) },
			{ ...CommonsReportBuildService.buildGeneric('Indicação: ', behaviorFiltersValue.indicacoesName) },
			{ ...CommonsReportBuildService.buildGeneric('Situação do Tratamento: ', behaviorFiltersValue.statusTratamentoName) },
			{ ...CommonsReportBuildService.buildGeneric('Status Atual: ', behaviorFiltersValue.situacaoPacienteName) },
			{ ...CommonsReportBuildService.buildGeneric('Dentista Responsável: ', behaviorFiltersValue.professionalName) },
			{ ...CommonsReportBuildService.buildGeneric('Especialidade(s): ', behaviorFiltersValue.especialidadesName) },
			{
				...CommonsReportBuildService.buildGeneric('Empresa: ', behaviorFiltersValue.empresaName),
				margin: [0, 0, 0, 10],
			},
		]
	}

	private static _buildBirthDate(ngbDate: NgbDateStruct): BirthDate {
		return ngbDate ? { dia: ngbDate.day, mes: ngbDate.month } : null;
	}

}
