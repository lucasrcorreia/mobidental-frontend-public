import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormProstheticComponent } from './form-prosthetic.component';

describe('FormProstheticComponent', () => {
  let component: FormProstheticComponent;
  let fixture: ComponentFixture<FormProstheticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormProstheticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormProstheticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
