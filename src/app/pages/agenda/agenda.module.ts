import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AgendaRoutingModule } from './agenda-routing.module';
import { AgendaComponent } from './agenda.component';
import { SharedModule } from '../../shared/shared.module';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NgxTranslateModule } from './../../core/translate/translate.module';
import { NgxSelectModule } from 'ngx-select-ex';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgendamentoComponent } from './agendamento/agendamento.component';
import { DirectivesModule } from '../../core/directive/directives.module';
import { DateTimePickerComponent } from './date-time-picker/date-time-picker.component';
import { NgxMaskModule } from 'ngx-mask';
import { SearchComponent } from './search/search.component';
import { PrintOptionsComponent } from './print-options/print-options.component';
import { InputMaskModule } from 'racoon-mask-raw';
import { NgSelectModule } from '@ng-select/ng-select';
import { HistoryPatientSchedulingComponent } from './agendamento/scheduling/history-patient-scheduling/history-patient-scheduling.component';
import { CommitmentComponent } from './agendamento/commitment/commitment.component';
import { UiSwitchModule } from 'ngx-ui-switch';
import { SchedulingComponent } from './agendamento/scheduling/scheduling.component';
import { ReturnAlertComponent } from './return-alert/return-alert.component';
import { CreateReturnAlertComponent } from './return-alert/create-alert/create-return-alert.component';

@NgModule({
	declarations: [
		AgendaComponent,
		AgendamentoComponent,
		DateTimePickerComponent,
		SearchComponent,
		PrintOptionsComponent,
		HistoryPatientSchedulingComponent,
		CommitmentComponent,
		SchedulingComponent,
		ReturnAlertComponent,
		CreateReturnAlertComponent,
	],
	exports: [
		PrintOptionsComponent,
	],
	imports: [
		CommonModule,
		AgendaRoutingModule,
		SimpleNotificationsModule,
		SharedModule,
		NgxSelectModule,
		NgSelectModule,
		FormsModule,
		ReactiveFormsModule,
		NgxTranslateModule,
		DirectivesModule,
		NgxDatatableModule,
		InputMaskModule,
		NgxMaskModule.forRoot(),
		UiSwitchModule,
	],
	entryComponents: [CreateReturnAlertComponent],
})
export class AgendaModule {}
