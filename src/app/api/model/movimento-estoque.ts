export enum TipoMovimento {
  BALANCO = 'BALANCO',
  ENTRADA = 'ENTRADA',
  SAIDA = 'SAIDA'
}

export interface MovimentoEstoque {
  id?: number;
  data?: string;
  tipoMovimento: TipoMovimento;
  produtoId: number;
  produtoNome: string;
  quantidadeMovimentada: number;
  saldo?: number;
  destinoId: number;
  destinoDescricao?: string;
  observacao?: string;
  usuarioNome?: string;
}

export interface TipoMovimentoInfo {
  descricao: string;
}

const infos: { [k in TipoMovimento]: TipoMovimentoInfo } = {
  BALANCO: { descricao: 'Balanço' },
  SAIDA: { descricao: 'Saída' },
  ENTRADA: { descricao: 'Entrada' }
};

export const getTipoMovimentoInfo = (tipo: TipoMovimento) => infos[tipo];
