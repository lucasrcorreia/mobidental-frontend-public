import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContaConfigAgendaComponent } from './conta-config-agenda.component';

describe('ContaConfigAgendaComponent', () => {
  let component: ContaConfigAgendaComponent;
  let fixture: ComponentFixture<ContaConfigAgendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContaConfigAgendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContaConfigAgendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
