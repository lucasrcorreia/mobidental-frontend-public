import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreatmentPlanPrintConfigComponent } from './treatment-plan-print-config.component';

describe('TreatmentPlanPrintConfigComponent', () => {
  let component: TreatmentPlanPrintConfigComponent;
  let fixture: ComponentFixture<TreatmentPlanPrintConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreatmentPlanPrintConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreatmentPlanPrintConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
