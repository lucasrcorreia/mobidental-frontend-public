import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdutosListTableComponent } from './produtos-list-table.component';

describe('ProdutosListTableComponent', () => {
  let component: ProdutosListTableComponent;
  let fixture: ComponentFixture<ProdutosListTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdutosListTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdutosListTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
