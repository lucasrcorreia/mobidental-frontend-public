import { DailyCashFlowItem } from './daily-cash-flow-item';

export interface DailyCashFlow {
    items?: DailyCashFlowItem[];
    totalEntrada?: number;
    totalSaida?: number;
    saldo?: number;

    /* Campos criados no front-end, não conhecidos pelo back-end */
    badgeClassName?: string;
}
