import { FormsModel } from '../models/forms/forms.model';
import { Validators } from '@angular/forms';

export const FormBillsToPay: FormsModel[] = [
  {
    field: 'descricao',
    validators: [Validators.required],
  },
  {
    field: 'numeroTotalParcelas',
    validators: [Validators.required],
  },
  {
    field: 'valor',
    validators: [Validators.required],
  },
  {
    field: 'dataCompetencia',
    validators: [Validators.required],
  },
  {
    field: 'dataVencimento',
    validators: [Validators.required],
  },
  {
    field: 'categoriaId',
    validators: [Validators.required],
  },
];
