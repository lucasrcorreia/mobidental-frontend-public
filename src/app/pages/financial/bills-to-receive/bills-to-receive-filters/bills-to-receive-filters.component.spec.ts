import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillsToReceiveFiltersComponent } from './bills-to-receive-filters.component';

describe('BillsToReceiveFiltersComponent', () => {
  let component: BillsToReceiveFiltersComponent;
  let fixture: ComponentFixture<BillsToReceiveFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillsToReceiveFiltersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillsToReceiveFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
