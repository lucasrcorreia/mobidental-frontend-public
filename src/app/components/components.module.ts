import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TotalizersComponent } from './totalizers/totalizers.component';
import { NgxTranslateModule } from '../core/translate/translate.module';
import { PipesModule } from '../core/pipes/pipes.module';

@NgModule({
  declarations: [TotalizersComponent],
  imports: [
    CommonModule,
    NgxTranslateModule,
    PipesModule
  ],
  exports: [
    TotalizersComponent
  ]
})
export class ComponentsModule { }
