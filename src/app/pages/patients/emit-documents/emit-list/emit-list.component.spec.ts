import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmitListComponent } from './emit-list.component';

describe('EmitListComponent', () => {
  let component: EmitListComponent;
  let fixture: ComponentFixture<EmitListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmitListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmitListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
