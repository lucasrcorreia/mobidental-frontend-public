const PROXY_CONFIG = [{
  context: [
    '/api',
    '/open',
  ],
  target: 'https://homolog.mobidental.com.br',
  secure: false,
  changeOrigin: true,
}]

module.exports = PROXY_CONFIG;
