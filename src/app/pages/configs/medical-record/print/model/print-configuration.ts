import { LogoPosition } from './enum/logo-position';

export interface PrintConfiguration {
	id?: number;
	mostraCabecalho?: boolean;
	posicaoLogomarcaCabelho?: LogoPosition;
	mostraRodape?: boolean;
	rodapeLinha1?: string;
	rodapeLinha2?: string;
	rodapeLinha3?: string;
	urlImagem?: string;
}
