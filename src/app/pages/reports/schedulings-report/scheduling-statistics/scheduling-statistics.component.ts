import { Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AbstractReportFilters } from '../../abstract-report-filters';
import { Periodocity, ReceiptsReportFilters, ReportsService } from '../../reports.service';
import { UtilsService } from '../../../../services/utils/utils.service';
import * as moment from 'moment';
import { NgxChartView } from '../../indications-report/indications-report.component';
import { AsyncTemplate } from '../../../../lib/helpers/async-template';
import { SchedulingsStatisticsItemsModel, SchedulingsStatisticsModel } from './schedulings-statistics-model';
import { DropdownGenericService } from '../../../patients/form-patients/services/dropdown-generic.service';
import { NotificationsService } from 'angular2-notifications';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

export interface NgxChartGroupView {
	name: string;
	series: NgxChartView[];
}

const PERIODICITY: { label: string, value: Periodocity }[] = [
	{ label: 'Mensal', value: Periodocity.MENSAL },
	{ label: 'Anual', value: Periodocity.ANUAL },
]

const MONTHS: { label: string, value: number }[] = [
	{ label: 'Janeiro', value: 1 },
	{ label: 'Fevereiro', value: 2 },
	{ label: 'Março', value: 3 },
	{ label: 'Abril', value: 4 },
	{ label: 'Maio', value: 5 },
	{ label: 'Junho', value: 6 },
	{ label: 'Julho', value: 7 },
	{ label: 'Agosto', value: 8 },
	{ label: 'Setembro', value: 9 },
	{ label: 'Outubro', value: 10 },
	{ label: 'Novembro', value: 11 },
	{ label: 'Dezembro', value: 12 },
]

@Component({
	selector: 'app-scheduling-statistics',
	templateUrl: './scheduling-statistics.component.html',
	styleUrls: ['./scheduling-statistics.component.scss'],
})
export class SchedulingStatisticsComponent extends AbstractReportFilters implements OnInit, OnDestroy {

	@ViewChild('barVertical2DComponent') barVertical2DComponent?: ElementRef;
	@ViewChild('pieGridComponent') pieGridComponent?: ElementRef;

	filters: ReceiptsReportFilters = { profissionalId: null, convenioId: null, tipoPeriodo: Periodocity.ANUAL };
	readonly months = MONTHS;
	readonly years: number[] = [];
	readonly periodicitys = PERIODICITY;
	readonly Periodicity = Periodocity;

	readonly green = '#008000';
	readonly crimson = '#dc143c';
	readonly attendance = this._service.translate.instant('REPORTS.SCHEDULINGS.STATISTICS.ATTENDANCE');
	readonly miss = this._service.translate.instant('REPORTS.SCHEDULINGS.STATISTICS.MISS');

	barResults: NgxChartGroupView[] = [];
	pieResults: NgxChartView[] = [
		{ value: 0, name: this.attendance },
		{ value: 0, name: this.miss },
	];
	commonColorScheme = {
		domain: [this.green, this.crimson],
	};

	pieChartView: [number, number];
	barChartView: [number, number];

	private readonly _destroy$ = new Subject();

	constructor(private readonly _asyncTemplate: AsyncTemplate,
				private readonly _reportsService: ReportsService,
				@Inject(NotificationsService) _notificationsService: NotificationsService,
				@Inject(UtilsService) _service: UtilsService,
				@Inject(DropdownGenericService) _dropdownGenericService: DropdownGenericService) {
		super(_service, _dropdownGenericService, _notificationsService);
		this._service.toggleMenu.pipe(takeUntil(this._destroy$)).subscribe(data => {
			if (data) {
				setTimeout(() => this.resizeCharts());
			}
		});
	}

	ngOnInit() {
		this.filters.ano = moment().year();

		this._generateYearsList();
		this.initProfessionalDropdown();
		this.initCovenantDropdown();

		this.resizeCharts();
		this.search();
	}

	ngOnDestroy() {
		this._destroy$.next();
		this._destroy$.complete();
	}

	resizeCharts(): void {
		this.pieChartView = [this.pieGridComponent.nativeElement.offsetWidth, 200];
		this.barChartView = [this.barVertical2DComponent.nativeElement.offsetWidth, 350];
	}

	onChangePeriod($event: Periodocity) {
		if ($event === Periodocity.ANUAL) {
			this.filters.mes = null;
		} else {
			this.filters.mes = moment().month() + 1;
		}
	}

	async search() {
		const data: SchedulingsStatisticsModel = await this._asyncTemplate.wrapUserFeedback(
				'Ocorreu um erro ao listar os dados',
				() => this._reportsService.fetchSchegulindStatistics(this.filters),
		);
		if (data) {
			this._buildPieResults(data);
			this._buildBarResults(data.itens);
		}
	}

	private _buildPieResults(data: SchedulingsStatisticsModel): void {
		this.pieResults = [
			{ value: data.totalComparecimento || 0, name: this.attendance },
			{ value: data.totalFalta || 0, name: this.miss },
		];
	}

	private _buildBarResults(items: SchedulingsStatisticsItemsModel[]): void {
		this.barResults = items.map((item: SchedulingsStatisticsItemsModel) => {
			return {
				name: item.diaMes,
				series: [
					{ name: this.attendance, value: item.presentes },
					{ name: this.miss, value: item.faltantes },
				],
			};
		});
	}

	private _generateYearsList(): void {
		let minYear = 2010;
		const currentYear = moment().year();
		for (; minYear <= currentYear; minYear++) {
			this.years.push(minYear);
		}
	}

}
