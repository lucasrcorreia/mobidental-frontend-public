import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_CONFIGURATION, ApiConfiguration } from './api-configuration';

export enum TipoExclusaoContaAPagar {
  SOMENTE_ESTE_LANCAMENTO = 'SOMENTE_ESTE_LANCAMENTO',
  ESTE_E_PROXIMOS_LANCAMENTOS = 'ESTE_E_PROXIMOS_LANCAMENTOS',
  TODOS = 'TODOS',
}

export interface RemoverContaAPagarDados {
  id: number;
  recorrenciaId: string;
  emTipoExclusaoContaPagar: TipoExclusaoContaAPagar;
}

export interface CategoriaConta {
  id?: number;
  descricao: string;
}

@Injectable({
  providedIn: 'root',
})
export class ContasAPagarApiService {

  constructor(
    private readonly _http: HttpClient,
    @Inject(API_CONFIGURATION) private readonly _config: Readonly<ApiConfiguration>,
  ) {
  }

  removerContaAPagar(dados: RemoverContaAPagarDados): Promise<void> {
    return this._http
      .put<void>(`${this._config.apiBasePath}/contaPagar/removerLancamento`, dados)
      .toPromise();
  }

  consultarTodasCategorias(): Promise<CategoriaConta[]> {
    return this._http
      .get<CategoriaConta[]>(`${this._config.apiBasePath}/categoria/dropdown/ativas`)
      .toPromise();
  }

  salvarCategoria(categoria: CategoriaConta): Promise<CategoriaConta> {
    return this._http
      .post<CategoriaConta>(`${this._config.apiBasePath}/categoria/save`, categoria)
      .toPromise();
  }

  apagarCategoria(categoriaId: number): Promise<void> {
    return this._http
      .delete<void>(`${this._config.apiBasePath}/categoria/${categoriaId}`)
      .toPromise();
  }

}
