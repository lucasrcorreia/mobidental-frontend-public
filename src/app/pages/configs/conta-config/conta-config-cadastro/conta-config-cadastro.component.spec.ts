import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContaConfigCadastroComponent } from './conta-config-cadastro.component';

describe('ContaConfigCadastroComponent', () => {
  let component: ContaConfigCadastroComponent;
  let fixture: ComponentFixture<ContaConfigCadastroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContaConfigCadastroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContaConfigCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
