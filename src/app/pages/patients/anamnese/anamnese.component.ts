import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { AnamneseRespostaModel } from './models/AnamneseRespostaModel';
import { NotificationsService } from 'angular2-notifications';
import { UtilsService } from '../../../services/utils/utils.service';
import { SIM_NAO_TRANSLATE, SimNaoResposta } from './models/SimNaoResposta';
import { PdfGeneratorService } from '../../../services/pdf-generator/pdf-generator.service';
import { PatientModel } from '../../../core/models/patients/patients.model';
import { ProfessionService } from '../form-patients/services/profession.service';
import { MaskFormatService } from '../../../services/utils/mask-format.service';
import { PacienteApiService } from '../../../api/paciente.api.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { capitalize } from 'lodash-es';
import { TipoPerguntaAnamnese } from '../../configs/medical-record/anamnese/models/enums/TipoPerguntaAnamnese';
import { AnamnesePerguntaModel } from '../../configs/medical-record/anamnese/models/AnamnesePerguntaModel';
import { AnamneseService } from '../../configs/medical-record/anamnese/service/anamnese.service';
import { AtivoInativo } from '../../configs/medical-record/anamnese/models/enums/AtivoInativo';
import { AnamneseModel } from '../../configs/medical-record/anamnese/models/AnamneseModel';

interface QuestionAndAnswer {
	answer?: AnamneseRespostaModel;
	question?: AnamnesePerguntaModel;
}

const DEFAULT_ANSWER = (anamneseId, questionId, patientId): AnamneseRespostaModel => {
	return {
		id: null,
		resposta: '',
		respostaSimNao: null,
		perguntaModeloId: anamneseId,
		perguntaId: questionId,
		pacienteId: patientId,
	};
};

@Component({
	selector: 'app-anamnese',
	templateUrl: './anamnese.component.html',
	styleUrls: ['./anamnese.component.scss'],
})
export class AnamneseComponent implements OnInit {

	@Input() patient: PatientModel;
	@Input() profession: string;
	@Input() marital: string;

	readonly tipoPerguntaAnamnese = TipoPerguntaAnamnese;
	readonly yesOrNotTranslated = SIM_NAO_TRANSLATE;
	readonly yesOrNot = SimNaoResposta;
	readonly questionTypesToAlert = [TipoPerguntaAnamnese.SIM_NAO, TipoPerguntaAnamnese.SIM_NAO_TEXTO];
	readonly questionTexts = [TipoPerguntaAnamnese.SIM_NAO_TEXTO, TipoPerguntaAnamnese.TEXTO];
	printEmptyAnamnese = false;
	firstPrint = true;

	anamneses: AnamneseModel[];
	currentAnamnese: AnamneseModel;
	answers: AnamneseRespostaModel[];
	anamneseForm: FormGroup;
	loadingForm = true;
	patientProfileImage: SafeResourceUrl;

	constructor(private readonly _anamneseService: AnamneseService,
				private readonly _formBuilder: FormBuilder,
				private readonly _notificationsService: NotificationsService,
				readonly _service: UtilsService,
				private readonly _pdfMakeService: PdfGeneratorService,
				private readonly _professionService: ProfessionService,
				private readonly _maskFormatService: MaskFormatService,
				private readonly _pacienteApiService: PacienteApiService,
				private readonly _sanitizer: DomSanitizer) {
	}

	get formArrayControls() {
		return (this.anamneseForm.get('questions') as FormArray).controls;
	}

	get pessoaSexo() {
		return capitalize(this.patient.pessoaSexo);
	}

	getQuestionFromControl(control: AbstractControl): AnamnesePerguntaModel {
		return control.get('question').value as AnamnesePerguntaModel;
	}

	ngOnInit() {
		this._initAnamneses();
		this._getPatientProfileImage();
	}

	async save() {
		const answers = this.formArrayControls.map(answer => {
			return answer.get('answer').value;
		});

		try {
			this._service.loading(true);

			await this._anamneseService.saveAnswer(answers);
			this._notificationsService.success('Respostas salvas.');
			await this._anamneseService.updateDefaultPatientAnamnese(this.currentAnamnese.id, this.patient.id);
			this._initAnamneses(this.currentAnamnese.id);
		} catch (e) {
			this._notificationsService.error('Ocorreu um erro ao fazer o envio da(s) resposta(s)');
		} finally {
			this._service.loading(false);
		}
	}

	async exportAnamneseAsPDF(empty = false) {
		if (!empty) {
			this.save();
		}
		this.printEmptyAnamnese = empty;
		await this._pdfMakeService.exportElementAsPDF('htmlToPdf', this.currentAnamnese.nome + ' - ' + this.patient.pessoaNome);
		this.firstPrint = false;
	}

	getFormatedCellPhone(cellphone: string): string {
		return this._maskFormatService.getFormatedCellPhone(cellphone);
	}

	getFormatedPhone(phone: string): string {
		return this._maskFormatService.getFormatedPhone(phone);
	}

	getFormatedCep(cep: string): string {
		return this._maskFormatService.getFormatedCep(cep);
	}

	getFormatedCpf(cpf: string): string {
		return this._maskFormatService.getFormatedCpf(cpf);
	}

	getFormatedBirthDate(birthDate): string {
		if (!birthDate) {
			return '';
		}

		const { day, month, year } = birthDate;

		if (!day || !month || !year) {
			return '';
		}

		return ('0' + day).slice(-2) + '/' + ('0' + month).slice(-2) + '/' + year;
	}

	getYesOrNotOptionsForPrint(answer: AnamneseRespostaModel): string {
		const spaces = '&nbsp;&nbsp;';
		if (!this.printEmptyAnamnese) {
			if (answer.respostaSimNao === SimNaoResposta.SIM) {
				return `( x )${ spaces }Sim${ spaces }(  )${ spaces }Não`;
			} else {
				return `(  )${ spaces }Sim${ spaces }( x )${ spaces }Não`;
			}
		} else {
			return `(  )${ spaces }Sim${ spaces }(  )${ spaces }Não`;
		}
	}

	async _initForm() {
		this.loadingForm = true;
		const anamnese = await this._anamneseService.getOne(this.currentAnamnese.id);
		anamnese.perguntas = anamnese.perguntas.sort((n1, n2) => n1.posicao - n2.posicao);
		this.answers = await this._anamneseService.listAnswerByAnamneseAndPatient(this.currentAnamnese.id, this.patient.id);
		const questions = this._buildAnamneseQuestionsAndAnswers(anamnese.perguntas.filter(q => q.status === AtivoInativo.ATIVO));
		this.anamneseForm = this._formBuilder.group({
			questions: this._formBuilder.array(this._initRows(questions)),
		});
		this.loadingForm = false;
	}

	doSelectionChanges($event) {
		this.currentAnamnese = $event[0].data;
		this._initForm();
	}

	private async _getPatientProfileImage() {
		const data = await this._pacienteApiService.downloadProfileImage(this.patient.id);
		const blob: Blob = new Blob([data]);
		const url: string = URL.createObjectURL(blob);
		this.patientProfileImage = this._sanitizer.bypassSecurityTrustResourceUrl(url);
	}

	private async _initAnamneses(anamneseId: number = this.patient.modeloId) {
		this.anamneses = await this._anamneseService.listAll();
		if (!this.anamneses || this.anamneses.length === 0) { return; }
		this.currentAnamnese = this.anamneses.find(anamnese => anamnese.id === anamneseId);
		if (!this.currentAnamnese) { this.currentAnamnese = this.anamneses[0]; }
		this._initForm();
	}

	private _buildAnamneseQuestionsAndAnswers(questions: AnamnesePerguntaModel[]): QuestionAndAnswer[] {
		return questions.map(question => {
			const answer = this.answers.find(a => a.perguntaId === question.id) ||
					DEFAULT_ANSWER(this.currentAnamnese.id, question.id, this.patient.id);
			return { question, answer };
		});
	}

	private _initRows(questionsAndAnswers: QuestionAndAnswer[]) {
		return questionsAndAnswers.map(qEa => {
			return this._formBuilder.group({
				question: qEa.question,
				answer: this._formBuilder.group(qEa.answer),
			});
		});
	}

}
