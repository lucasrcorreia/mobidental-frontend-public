import { Component, Input, OnInit } from "@angular/core";
import { ColumnMode } from "@swimlane/ngx-datatable";
import { MaskFormatService } from "../../../services/utils/mask-format.service";
import { CNPJ_LENGTH, CPF_LENGTH } from "../../../lib/input-masks";

export enum Type {
	DATE,
	CURRENCY,
	CPF_CNPJ,
	CELL,
	PHONE,
	ICON,
	TOOLTIP,
}

export interface ReportDatatableColumn {
	prop?: string;
	translation: string;
	type?: Type;
	iconClass?: string;
	function?: Function;
	cellClassName?: string;
	headerClassName?: string;
	pdfAlignment?: string;
	tooltipText?: string;
}

@Component({
	selector: "app-report-datatable",
	templateUrl: "./report-datatable.component.html",
	styleUrls: ["./report-datatable.component.scss"],
})
export class ReportDatatableComponent implements OnInit {
	@Input() columns: ReportDatatableColumn[];
	@Input() rows: any[];
	@Input() totalValue?: number;

	readonly ColumnType = Type;
	readonly ColumnMode = ColumnMode;

	constructor(private readonly _maskFormatService: MaskFormatService) {}

	ngOnInit() {}

	messages() {
		const { totalValue, rows } = this;
		const totalMessage =
			totalValue == null || !rows || !rows.length
				? ""
				: ` - Valor total: ${totalValue.toLocaleString("pt-BR", {
						style: "currency",
						currency: "BRL",
				  })}`;

		return {
			emptyMessage: "Nenhum registro encontrado para os filtros selecionados...",
			totalMessage: "registro(s)" + totalMessage,
			selectedMessage: "selecionado",
		};
	}

	date(date: string): string {
		if (!date) {
			return "";
		}
		return date.split(" ")[0];
	}

	currency(value: number): string {
		return value ? value.toFixed(2) : "";
	}

	cell(value: string): string {
		if (!value) {
			return "";
		}

		return this._maskFormatService.getFormatedCellPhone(value);
	}

	phone(value: string): string {
		if (!value) {
			return "";
		}

		return this._maskFormatService.getFormatedPhone(value);
	}

	cpfCnpj(value: string): string {
		if (!value) {
			return "";
		}
		if (value.includes(".") || value.includes("-") || value.includes("/")) {
			return value;
		}

		return value.length === CPF_LENGTH
			? this._maskFormatService.getFormatedCpf(value)
			: value.length === CNPJ_LENGTH
			? this._maskFormatService.getFormatedCnpj(value)
			: value;
	}
}
