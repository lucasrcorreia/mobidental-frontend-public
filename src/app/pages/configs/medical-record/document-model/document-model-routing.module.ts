import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DocumentModelComponent } from "./document-model/document-model.component";
import { CreateDocumentModelComponent } from "./create-document-model/create-document-model.component";
import { ContractDocumentModelComponent } from "./contract-document-model/contract-document-model.component";

const routes: Routes = [
	{
		path: "",
		component: DocumentModelComponent,
	},
	{
		path: ":id",
		component: CreateDocumentModelComponent,
	},
	{
		path: "contract/:id",
		component: ContractDocumentModelComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class DocumentModelRoutingModule {}
