export enum UnidadeMedida {
  QUILOGRAMA = 'QUILOGRAMA',
  UNIDADE = 'UNIDADE',
  CAIXA = 'CAIXA',
  METRO_LINEAR = 'METRO_LINEAR',
  METRO_QUADRADO = 'METRO_QUADRADO',
  METRO_CUBICO = 'METRO_CUBICO',
  PECA = 'PECA',
  LITRO = 'LITRO',
  PACOTE = 'PACOTE'
}

export interface UnidadeMedidaInfo {
  descricao: string;
}

const detalhesUnidades: { [k in UnidadeMedida]: UnidadeMedidaInfo } = {
  QUILOGRAMA: { descricao: 'Quilograma' },
  UNIDADE: { descricao: 'Unidade' },
  CAIXA: { descricao: 'Caixa' },
  METRO_LINEAR: { descricao: 'Metro linear' },
  METRO_QUADRADO: { descricao: 'Metro quadrado' },
  METRO_CUBICO: { descricao: 'Metro cubico' },
  PECA: { descricao: 'Peça' },
  LITRO: { descricao: 'Litro' },
  PACOTE: { descricao: 'Pacote' }
};

export const getInfoUnidadeMedida = (un: UnidadeMedida): UnidadeMedidaInfo =>
  detalhesUnidades[un];
