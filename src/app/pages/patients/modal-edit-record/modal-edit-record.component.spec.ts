import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEditRecordComponent } from './modal-edit-record.component';

describe('ModalEditRecordComponent', () => {
  let component: ModalEditRecordComponent;
  let fixture: ComponentFixture<ModalEditRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEditRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEditRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
