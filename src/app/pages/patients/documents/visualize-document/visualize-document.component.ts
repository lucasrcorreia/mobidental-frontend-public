import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { DocumentModel } from '../../../../api/model/image';
import { NgbActiveModal, NgbCarousel, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { DocumentsCommonService } from '../documents-common.service';

const CAROUSEL_FORMAT_KEY = 'ngb-slide-';
const CAROUSEL_MATCH_ID = /\d+/i;

@Component({
  selector: 'app-visualize-document',
  templateUrl: './visualize-document.component.html',
  styleUrls: ['./visualize-document.component.scss'],
  providers: [NgbCarouselConfig]
})
export class VisualizeDocumentComponent implements OnInit, AfterViewInit {

  @Input() documents: DocumentModel[];
  @ViewChild('carousel') carousel: NgbCarousel;

  firstCarouselId: RegExpMatchArray;

  currentDocument: DocumentModel;

  constructor(public activeModal: NgbActiveModal,
              private readonly _documentsCommonService: DocumentsCommonService) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.firstCarouselId = this.carousel.slides.first.id.match(CAROUSEL_MATCH_ID);
    });
  }

  async edit(document: DocumentModel) {
    await this._documentsCommonService.edit(document);
    this.activeModal.close(true);
  }

  download(document: DocumentModel) {
    this._documentsCommonService.download(document);
  }

  async delete(document: DocumentModel) {
    await this._documentsCommonService.delete(document);
    this.carousel.next();
    this._documentsCommonService.refreshData();
  }

  getLabel(label: string) {
    return this._documentsCommonService.getLabel(label);
  }

  onSlide($event): void {
    const eventId = $event.current;
    const currentDocumentId = eventId.match(CAROUSEL_MATCH_ID) - + this.firstCarouselId;
    this.currentDocument = this.documents[currentDocumentId];
  }

  get getActiveIdBasedOnCurrentDocument(): string {
    if (!this.currentDocument || !this.firstCarouselId) {
      return;
    }

    const ids: number[] = this.documents.map((document: DocumentModel) => {
      return document.id;
    });

    return CAROUSEL_FORMAT_KEY + (ids.indexOf(this.currentDocument.id) + + this.firstCarouselId);
  }

}
