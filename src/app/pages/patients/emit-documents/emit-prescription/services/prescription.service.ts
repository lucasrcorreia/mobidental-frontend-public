import { Inject, Injectable } from '@angular/core';
import { API_CONFIGURATION, ApiConfiguration } from '../../../../../api/api-configuration';
import { HttpClient } from '@angular/common/http';
import { Prescription } from '../models/prescription';

const BASE_PATH = 'receita'

@Injectable({
	providedIn: 'root',
})
export class PrescriptionService {

	constructor(
			@Inject(API_CONFIGURATION) private readonly _config: ApiConfiguration,
			private readonly _http: HttpClient,
	) {
	}

	listAllIssuablePrescriptionByPatient(patientId: number): Promise<Prescription[]> {
		return this._http
				.post<Prescription[]>(`${ this._config.apiBasePath }/${ BASE_PATH }/findAllReceita`, { pacienteId: patientId })
				.toPromise();
	}

	save(prescription: Prescription, file: File): Promise<Prescription> {
		const formData = this._buildFormData(prescription, file);
		return this._http
				.post<Prescription>(`${ this._config.apiBasePath }/${ BASE_PATH }/salvarReceitaComAnexo`, formData)
				.toPromise();
	}

	getPdfFromPrescription(id: number): Promise<ArrayBuffer> {
		return this._http
				.get<ArrayBuffer>(`${ this._config.apiBasePath }/${ BASE_PATH }/download/` + id, { responseType: 'blob' as 'json' })
				.toPromise();
	}

	delete(id: number): Promise<void> {
		return this._http
				.delete<void>(`${ this._config.apiBasePath }/${ BASE_PATH }/${ id }`)
				.toPromise();
	}

	private _buildFormData(prescription: Prescription, file: File): FormData {
		const formData = new FormData();
		formData.append('file', file);
		formData.append('dados', JSON.stringify(prescription));

		return formData;
	}
}
