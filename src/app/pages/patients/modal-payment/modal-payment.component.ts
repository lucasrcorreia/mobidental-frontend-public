import { Component, Input, OnChanges } from '@angular/core';
import { ContentBillTreatmentModel } from '../../../core/models/patients/patients.model';
import { UtilsService } from '../../../services/utils/utils.service';
import { PaymentClientModel } from '../../../core/models/patients/payment/payment.model';
import { LISTAS } from '../lists';
import { CONSTANTS } from '../../../core/constants/constants';
import { classToPlain } from 'class-transformer';
import { LOCAL_CONSTS } from '../constants';
import { ModalAnimationComponent } from '../../../shared/modal-animation/modal-animation.component';
import { GlobalSpinnerService } from '../../../lib/global-spinner.service';
import { NotificationsService } from 'angular2-notifications';

@Component({
               selector: 'app-modal-payment',
               templateUrl: './modal-payment.component.html',
               styleUrls: [
                   './modal-payment.component.scss',
                   './../../../../assets/icon/icofont/css/icofont.scss',
               ],
           })
export class ModalPaymentComponent implements OnChanges {
    @Input() parcel: ContentBillTreatmentModel;
    @Input() payment: PaymentClientModel;
    @Input() totalParcels: number;
    @Input() status = { text: '', color: '' };

    @Input() modal?: ModalAnimationComponent;

    private _dataRecebimento?: unknown;

    locale = this.service.translate.instant('COMMON.LOCALE');
    LISTAS_LOCAIS = new LISTAS(this.service.translate);
    cheque = LOCAL_CONSTS.CHEQUE;

    //Criado pois o usuário conseguia clicar 2x (rapidamente) no botão e salvar 2x;
    disableSave = false;

    constructor(
        public service: UtilsService,
        private readonly _globalSpinnerService: GlobalSpinnerService,
        private readonly _notificationsService: NotificationsService,
    ) {
    }

    ngOnChanges() {
        this.payment.valorRecebido = this.payment.saldoAPagar;
        this._dataRecebimento = this.service.parseDatePicker(this.payment.dataRecebimento);
    }

    set dataRecebimento(d: unknown) {
        this._dataRecebimento = d;
        this.payment.dataRecebimento = this.service.parseDateFromDatePicker(
            d,
        );
    }

    get dataRecebimento(): unknown {
        return this._dataRecebimento;
    }

    async pay() {
        const loadingToken = this._globalSpinnerService.loadingManager.start();

        try {
            await this.service
                .httpPOST(CONSTANTS.ENDPOINTS.patients.financial.payParcel, this.payment).toPromise();
            const returnData = classToPlain(this.payment) as any;
            returnData.valorRecebido = parseFloat(
                this.payment.valorRecebido.toString(),
            );

            this.service.closeModal('modal-parcel', returnData);
            this._notificationsService.success('Parcela paga com sucesso');
        } catch (e) {
            this.service.notification.error(this.service.translate.instant('COMMON.ERROR.SEARCH'), e.error.error);
        } finally {
            loadingToken.finished();
            this.disableSave = false;
        }
    }

    closeModal() {
        if (this.parcel.dataVencimento && this.parcel.dataVencimento) {
            this.parcel.dataVencimento = this.service.parseDateFromDatePicker(
                this.parcel.dataVencimento,
                'DD/MM/YYYY',
            );
        }

        if (this.modal) {
            this.modal.close();
        }

        this.service.closeModal('modal-parcel', null);
    }
}
