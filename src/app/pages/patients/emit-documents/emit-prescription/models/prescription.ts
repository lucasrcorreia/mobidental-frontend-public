import { PrescriptionType } from './enums/prescription-type';

export interface PrescriptionMedicine {
	id?: number;
	medicamentoId?: number;
	posologia?: string;
	quantidade?: string;
	unidadeMedida?: string;
	receitaId?: number;
}

export interface Prescription {
	id?: number;
	data?: string;
	tipoReceita?: PrescriptionType;
	profissionalId?: number;
	pacienteId?: number;
	medicamentos?: PrescriptionMedicine[];
}
