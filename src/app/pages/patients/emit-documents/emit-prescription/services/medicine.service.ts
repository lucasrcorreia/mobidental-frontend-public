import { Inject, Injectable } from '@angular/core';
import { API_CONFIGURATION, ApiConfiguration } from '../../../../../api/api-configuration';
import { HttpClient } from '@angular/common/http';
import { Medicine } from '../models/medicine';

const BASE_PATH = 'medicamento';

@Injectable({
	providedIn: 'root',
})
export class MedicineService {

	constructor(
			@Inject(API_CONFIGURATION) private readonly _config: ApiConfiguration,
			private readonly _http: HttpClient,
	) {
	}

	medicineDropdown(): Promise<Medicine[]> {
		return this._http
				.get<Medicine[]>(`${ this._config.apiBasePath }/${ BASE_PATH }/listarMedicamentos`)
				.toPromise();
	}

	save(medicine: Medicine): Promise<Medicine> {
		return this._http
				.post<Medicine>(`${ this._config.apiBasePath }/${ BASE_PATH }/save`, medicine)
				.toPromise();
	}
}
