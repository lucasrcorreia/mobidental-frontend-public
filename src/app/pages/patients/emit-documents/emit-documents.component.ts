import { Component, Input, OnInit } from '@angular/core';
import { PatientModel } from '../../../core/models/patients/patients.model';
import { PrintConfigurationsService } from '../../configs/medical-record/print/service/print-configurations.service';
import { PrintConfiguration } from '../../configs/medical-record/print/model/print-configuration';
import { IssuableDocumentType } from '../../configs/medical-record/document-model/models/enum/IssuableDocumentType';

export enum Tab {
	LIST,
	DOCUMENT,
	PRESCRIPTION,
}

@Component({
	selector: 'app-emit-documents',
	templateUrl: './emit-documents.component.html',
	styleUrls: ['./emit-documents.component.scss'],
})
export class EmitDocumentsComponent implements OnInit {

	@Input() patient: PatientModel;
	printConfiguration: PrintConfiguration;
	currentIssuableDocumentType: IssuableDocumentType = IssuableDocumentType.DOCUMENTO;
	readonly tabs = Tab;
	tab: Tab = Tab.LIST;

	constructor(private readonly _printConfigurationsService: PrintConfigurationsService) { }

	ngOnInit() {
		this._initPrintConfigurations();
	}

	private async _initPrintConfigurations() {
		this.printConfiguration = await this._printConfigurationsService.getConfigurations();
	}

	changeTab($event: { tab: Tab, issuableDocumentType: IssuableDocumentType }) {
		this.currentIssuableDocumentType = $event.issuableDocumentType
		this.tab = $event.tab;
	}

}
