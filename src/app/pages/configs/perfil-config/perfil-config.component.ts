import { Component, OnInit } from '@angular/core';
import { LoginApiService, MudancasUsuario } from '../../../api/login-api.service';
import { NotificationsService } from 'angular2-notifications';
import { errorToString } from '../../../lib/helpers/error-to-string';
import { GlobalSpinnerService } from '../../../lib/global-spinner.service';

@Component({
  selector: 'app-perfil-config',
  templateUrl: './perfil-config.component.html',
  styleUrls: ['./perfil-config.component.scss'],
})
export class PerfilConfigComponent {

  email?: string;
  nome?: string;
  senha?: string;
  confirmacaoSenha?: string;

  constructor(
    private readonly _loginApiService: LoginApiService,
    private readonly _notificationsService: NotificationsService,
    private readonly _globalSpinnerService: GlobalSpinnerService,
  ) {
    this._fetchUsuario();
  }

  get senhasConfirmadas(): boolean {
    return !this.senha || this.senha === this.confirmacaoSenha;
  }

  async salvar() {
    const mudancas: MudancasUsuario = {
      nome: this.nome,

      ...this.senha && {
        senha: this.senha,
        confirmacaoSenha: this.confirmacaoSenha,
      },
    };

    const loadingToken = this._globalSpinnerService.loadingManager.start();

    try {
      await this._loginApiService.atualizarUsuario(mudancas);
      this._notificationsService.success('Perfil salvo com sucesso');
    } catch (e) {
      this._notificationsService.error('Erro ao salvar perfil', errorToString(e));
    } finally {
      loadingToken.finished();
    }
  }

  private async _fetchUsuario() {
    const loadingToken = this._globalSpinnerService.loadingManager.start();

    try {
      const info = await this._loginApiService.fetchUsuarioInfo();
      this.nome = info.nome;
      this.email = info.email;
    } catch (e) {
      this._notificationsService.error('Erro ao carregar perfil', errorToString(e));
    } finally {
      loadingToken.finished();
    }
  }

}
