import { Component, Input, OnInit } from '@angular/core';
import { ReturnAlert } from '../model/return-alert';
import { NgbActiveModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { GlobalSpinnerService } from '../../../../lib/global-spinner.service';
import { NotificationsService } from 'angular2-notifications';
import { ReturnAlertService } from '../service/return-alert.service';
import { AutoCompletePatientList, PatientModel } from '../../../../core/models/patients/patients.model';
import { ShortListItem } from '../../../../core/models/forms/common/common.model';
import { UtilsService } from '../../../../services/utils/utils.service';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { CONSTANTS } from '../../../../core/constants/constants';
import Swal from 'sweetalert2';

@Component({
               selector: 'app-create-alert',
               templateUrl: './create-return-alert.component.html',
               styleUrls: ['./create-return-alert.component.scss'],
           })
export class CreateReturnAlertComponent implements OnInit {

    @Input() returnAlert: ReturnAlert;
    @Input() professionals: ShortListItem[] = [];

    minDate: NgbDateStruct;
    ngbDatePickerModel: NgbDateStruct;
    patients: PatientModel[];
    reasons: ShortListItem[];
    readonly months = [
        '1 mês',
        '2 meses',
        '3 meses',
        '4 meses',
        '5 meses',
        '6 meses',
        '12 meses',
    ];

    searchingReason: string;
    typeaheadValue = '';
    formatMatches = (value: AutoCompletePatientList) => value.pessoaNome;
    search = (text$: Observable<string>) =>
        text$.pipe(
            debounceTime(200),
            distinctUntilChanged(),
            switchMap(term =>
                          term.length < 3
                              ? []
                              : this._fetchPatients(term),
            ),
        );

    constructor(
        private readonly _activeModal: NgbActiveModal,
        private readonly _notificationsService: NotificationsService,
        private readonly _globalSpinnerService: GlobalSpinnerService,
        private readonly _returnAlertService: ReturnAlertService,
        public readonly service: UtilsService
    ) {
    }

    async ngOnInit() {
        this.minDate = this.service.parseDatePicker(moment().format('L'));
        this.professionals = this.professionals.filter(p => p.id);
        await this._initReasons();
        this._fillReturnAlertInformations();
    }

    private _fetchPatients(term: string) {
        return this.service
            .httpGET(CONSTANTS.ENDPOINTS.patients.general.autoComplete + term)
            .pipe(map(data => data.body));
    }

    private async _initReasons() {
        this.reasons = await this._returnAlertService.getMotivoAlertaRetornoDropdown();
    }

    private _fillReturnAlertInformations() {
        if (this.returnAlert.id) {
            this.typeaheadValue = this.returnAlert.pacientePessoaNome;
            this.ngbDatePickerModel = this.service.parseDatePicker(this.returnAlert.dataRetorno);
            this.changeDataRetorno('editDate');
        }
    }

    async save() {
        const loading = this._globalSpinnerService.loadingManager.start();
        try {
            const operation = this.returnAlert.id ? 'editado' : 'cadastrado';
            await this._returnAlertService.save(this.returnAlert);
            this._notificationsService.success('Alerta de Retorno ' + operation);
            this._activeModal.close(true);
        } catch (e) {
            this._notificationsService.error('Ocorreu um erro ao salvar o Alerta de Retorno.');
        } finally {
            loading.finished();
        }
    }

    prepareToSaveNewReason() {
        if (!this.searchingReason || !this.searchingReason.trim()) {
            this._notificationsService.alert('O motivo não pode ser vazio!');
            return;
        }

        this.searchingReason = this.searchingReason.trim();
        const existingReason = this.reasons.find(m => m.nome === this.searchingReason);
        if (existingReason) {
            this.returnAlert.motivoId = existingReason.id;
            return;
        }

        this.saveNewReason();
    }

    async saveNewReason(reasonToSave: ShortListItem = { id: null, nome: this.searchingReason }) {
        const loading = this._globalSpinnerService.loadingManager.start();
        try {
            const reason = await this._returnAlertService.saveMotivoAlertaRetornoDropdown(reasonToSave);
            this._notificationsService.success(`Motivo ${ reasonToSave.id ? 'editado' : 'criado' }`);
            this.returnAlert.motivoId = reason.id;
            this._initReasons();
        } catch (e) {
            this._notificationsService.error(`Ocorreu um erro ao ${ reasonToSave.id ? 'editar' : 'criar' } o motivo`);
        } finally {
            loading.finished();
        }
    }

    async editCurrentReason() {
        const currentReason: ShortListItem = this.reasons.find(p => p.id === this.returnAlert.motivoId);

        const input = await Swal.fire({
                                          title: 'Modifique o motivo',
                                          input: 'text',
                                          inputValue: currentReason.nome,
                                          showCancelButton: true,
                                          confirmButtonText: 'Salvar',
                                          cancelButtonText: 'Cancelar',
                                      });

        const value: string = String(input.value || '').trim();

        if (!value) {
            return;
        }

        this.saveNewReason({ id: currentReason.id, nome: value });
    }

    async removeReason(id: number) {
        const loading = this._globalSpinnerService.loadingManager.start();
        try {
            await this._returnAlertService.removeMotivoAlertaRetornoDropdown(id);
            this.reasons = this.reasons.filter(reason => reason.id !== id);
            if (this.returnAlert.motivoId === id) {
                this.returnAlert.motivoId = null;
            }
        } catch (e) {
            this._notificationsService.error('Ocorreu um erro ao remover o motivo.');
        } finally {
            loading.finished();
        }
    }

    selectPatient(patient) {
        this.returnAlert.pacienteId = patient.item.id;
    }

    selectMonth($event) {
        const range = $event.match(/(\d+)/);
        const newDate = moment().add(range[0], 'months');
        this.ngbDatePickerModel = { day: newDate.date(), month: newDate.month() + 1, year: newDate.year() };
        this.changeDataRetorno();
    }

    changeDataRetorno(elementId: string = 'dataRetorno') {
        this.returnAlert.dataRetorno = this.service.parseDateFromDatePicker(this.ngbDatePickerModel);
        setTimeout(() => {
            ((document.getElementById(elementId)) as HTMLInputElement).value = this.returnAlert.dataRetorno;
        });
    }

    close() {
        this._activeModal.close();
    }

}
