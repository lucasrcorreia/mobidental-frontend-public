import { Component, OnInit } from "@angular/core";
import {
	ContractDataApiService,
	ModeloContrato,
	TipoModeloContrato,
} from "../../../../../api/contract-data-api.service";
import { AsyncTemplate } from "../../../../../lib/helpers/async-template";
import { NotificationsService } from "angular2-notifications";
import { ActivatedRoute, Router } from "@angular/router";
import { Location } from "@angular/common";

@Component({
	selector: "app-contract-document-model",
	templateUrl: "./contract-document-model.component.html",
	styleUrls: ["./contract-document-model.component.scss"],
})
export class ContractDocumentModelComponent implements OnInit {
	tags?: string[];
	contrato?: ModeloContrato;
	nome?: string;
	texto?: string;
	id?: number;

	constructor(
		private _contractDataApiService: ContractDataApiService,
		private _notificationsService: NotificationsService,
		private _route: ActivatedRoute,
		private _location: Location,
		private _asyncTemplate: AsyncTemplate
	) {}

	ngOnInit() {
		this.id = Number(this._route.snapshot.params.id);
		this._fetchTags();
		this._fetchModel();
	}

	navigateBack() {
		this._location.back();
	}

	containsTag(tag: string) {
		const { texto } = this;
		if (!texto) return false;
		const fullTag = "#" + tag.replace(/^#/, "");
		const index = texto.indexOf(fullTag);
		return index > -1 && /\W/.test(texto.charAt(index + fullTag.length));
	}

	async save() {
		await this._asyncTemplate.wrapUserFeedback("Erro ao salvar modelo de contrato", () =>
			this._contractDataApiService.saveContractModel({
				...this.contrato,
				nome: this.nome,
				texto: this.texto,
			})
		);

		this.navigateBack();
		await this._notificationsService.success("Modelo salvo com sucesso!");
	}

	private async _fetchTags() {
		this.tags = (
			await this._asyncTemplate.wrapUserFeedback("Erro ao buscar tags", () =>
				this._contractDataApiService.listContractTags()
			)
		).map(({ tag }) => tag.replace(/^#/, ""));
	}

	private async _fetchModel() {
		this.contrato = await this._asyncTemplate.wrapUserFeedback(
			"Erro ao consultar modelo de contrato",
			async () => {
				try {
					return this._contractDataApiService.fetchContractModel(this.id);
				} catch {
					return {
						id: null,
						dataCriacao: null,
						tipoModeloContrato: TipoModeloContrato.MODELO_PADRAO,
						nome: "Contrato padrão",
						texto: "",
					};
				}
			}
		);
		this.texto = this.contrato.texto;
		this.nome = this.contrato.nome;
	}
}
