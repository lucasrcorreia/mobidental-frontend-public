import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { UtilsService } from '../../../services/utils/utils.service';
import { ProstheticModel } from '../../../core/models/config/prosthetic/prosthetic.model';
import { ColumnsTableModel } from '../../../core/models/table/columns.model';
import { CONSTANTS } from '../../../core/constants/constants';
import Swal from 'sweetalert2';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';


const telefoneRegex = /(\d{2})(\d)?(\d{4})(\d{4})/;

@Component({
  selector: 'app-prosthetic',
  templateUrl: './prosthetic.component.html',
  styleUrls: [
    './prosthetic.component.scss',
    './../../../../assets/icon/icofont/css/icofont.scss',
  ],
})
export class ProstheticComponent implements OnInit, OnDestroy {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  columns: ColumnsTableModel[] = [
    {
      action: true,
      name: this.service.translate.instant('CONFIG.PROSTHETIC.TABLE.COL1'),
      prop: 'id',
      sortable: false,
      width: 150,
    },
    {
      action: false,
      name: this.service.translate.instant('CONFIG.PROSTHETIC.TABLE.COL2'),
      prop: 'nome',
      sortable: true,
      width: 350,
    },
    {
      action: false,
      name: this.service.translate.instant('CONFIG.PROSTHETIC.TABLE.COL3'),
      prop: 'telefone',
      sortable: true,
      width: 200,
    },
    {
      action: false,
      name: this.service.translate.instant('CONFIG.PROSTHETIC.TABLE.COL4'),
      prop: 'celular',
      sortable: true,
      width: 200,
    },
    {
      action: false,
      name: this.service.translate.instant('CONFIG.PROSTHETIC.TABLE.COL5'),
      prop: 'email',
      sortable: true,
      width: 150,
    },
  ];

  searchParam = {
    nome: '',
  };

  rowsPage = [];

  showModalCreate = false;
  private destroy$ = new Subject();

  constructor(
    public service: UtilsService,
    private cd: ChangeDetectorRef,
  ) {
    this.service.toggleMenu.pipe(takeUntil(this.destroy$)).subscribe(data => {
      if (data) {
        this.updateDataTable();
      }
    });
  }

  ngOnInit() {
    this.populateTable();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  updateDataTable() {


    this.cd.markForCheck();
    setTimeout(() => {
      this.table.recalculate();
      (this.table as any).cd.markForCheck();
    });
  }

  populateTable() {
    this.service.loading(true);
    this.service
      .httpPOST(CONSTANTS.ENDPOINTS.config.prosthetic.table, this.searchParam)
      .subscribe(
        data => {
          this.rowsPage = data.body as Array<ProstheticModel>;
          this.service.loading(false);
        },
        err => {

          this.service.loading(false);
          this.service.notification.error(
            this.service.translate.instant('COMMON.ERROR.SEARCH'),
            err.error.error,
          );
        },
      );
  }

  formatTelephone(tel: unknown): string {
    const match = typeof tel === 'string' ? tel.match(telefoneRegex) : null;

    if (!match) {
      return tel == null ? '' : String(tel);
    }

    return `(${match[1]}) ${match[2] ? match[2] + ' ' : ''}${match[3]}-${match[4]}`;
  }

  async deleteProfessional(prosthetic: ProstheticModel) {
    const { value } = await Swal.fire({
      title: 'Confirmação de exclusão',
      text: 'Tem certeza que quer apagar este Protético?',
      type: 'question',
      showCancelButton: true,
      confirmButtonText: 'Sim. Apague',
      cancelButtonText: 'Cancelar',
    });

    if (!value) {
      return;
    }

    try {
      this.service.loading(true);
      await this.service.httpDELETE(CONSTANTS.ENDPOINTS.config.prosthetic.delete + prosthetic.id).toPromise();
      this.service.notification.success('Protético deletado.');
      this.populateTable();
    } catch (e) {
      this.service.notification.error('Ocorreu um erro ao tetar deletar o protético.')
    } finally {
      this.service.loading(false);
    }
  }

  processResultModalCreate(event) {
    if (event) {
      this.service.notification.success(
        this.service.translate.instant('CONFIG.PROSTHETIC.TOAST.SUCCESS.TITLE'),
        this.service.translate.instant('CONFIG.PROSTHETIC.TOAST.SUCCESS.SUBTITLE'),
      );

      this.rowsPage = [];
      this.populateTable();
    }

    this.showModalCreate = false;
  }

  delete(id: number) {
    const translate = this.service.translate;
    const refSWAL = 'CONFIG.PROSTHETIC.SWAL';

    Swal.fire({
      title: translate.instant(`${refSWAL}.TITLE_DELETE`),
      text: translate.instant(`${refSWAL}.TEXT_DELETE`),
      type: 'question',
      showCancelButton: true,
      confirmButtonText: translate.instant(`${refSWAL}.CONFIRM_BUTTON`),
      cancelButtonText: translate.instant(`${refSWAL}.CANCEL_BUTTON`),
    }).then(result => {
      if (result.value) {
        this.service.loading(true);
        this.service
          .httpDELETE(
            CONSTANTS.ENDPOINTS.config.prosthetic.delete + id,
          )
          .subscribe(
            data => {
              this.service.notification.success(
                this.service.translate.instant('CONFIG.PROSTHETIC.TOAST.DELETE.TITLE'),
                this.service.translate.instant('CONFIG.PROSTHETIC.TOAST.DELETE.SUBTITLE'),
              );
              this.service.loading(false);
              this.rowsPage = [];
              this.populateTable();
            },
            err => {

              this.service.loading(false);
              this.service.notification.error(
                this.service.translate.instant('COMMON.ERROR.SEARCH'),
                err.error.error,
              );
            },
          );
      }
    });
  }

}
