import { Recorrencia } from './enum/recorrencia';

export interface Commitment {
	id?: number;
	recorrenciaId?: string;
	profissionalId?: number,
	descricao?: string,
	dataInicial?: Date,
	dataFinal?: Date,
	tipoRecorrencia?: Recorrencia,
	isDiaInteiro?: boolean,
}
