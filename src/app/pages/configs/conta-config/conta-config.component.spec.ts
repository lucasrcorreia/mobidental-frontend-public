import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContaConfigComponent } from './conta-config.component';

describe('ContaConfigComponent', () => {
  let component: ContaConfigComponent;
  let fixture: ComponentFixture<ContaConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContaConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContaConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
