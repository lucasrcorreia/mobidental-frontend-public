import { FormsModel } from '../models/forms/forms.model';
import { Validators } from '@angular/forms';

export const FormCovenantCreate: FormsModel[] = [
  {
    field: 'convenioNome',
    validators: [Validators.required]
  }
];
