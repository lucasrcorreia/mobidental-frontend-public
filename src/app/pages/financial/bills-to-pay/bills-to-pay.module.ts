import { PipesModule } from './../../../core/pipes/pipes.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BillsToPayRoutingModule } from './bills-to-pay-routing.module';
import { BillsToPayComponent } from './bills-to-pay.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxTranslateModule } from '../../../core/translate/translate.module';
import { NgxSelectModule } from 'ngx-select-ex';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { InputMaskModule } from 'racoon-mask-raw';
import { ModalCcostCategoryModule } from '../modal-ccost-category/modal-ccost-category.module';
import { NgxMaskModule } from 'ngx-mask';
import { DirectivesModule } from '../../../core/directive/directives.module';
import { CONSTANTS } from '../../../core/constants/constants';
import { CURRENCY_MASK_CONFIG, CurrencyMaskConfig } from 'ngx-currency/src/currency-mask.config';
import { NgxCurrencyModule } from 'ngx-currency';
import { FormPaymentComponent } from './form-payment/form-payment.component';
import { AccountBillToPayModule } from './account/account.module';
import { ComponentsModule } from '../../../components/components.module';

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: 'right',
  allowNegative: true,
  allowZero: true,
  decimal: CONSTANTS.COMMONS_VALUES.DECIMAL,
  precision: 2,
  prefix: CONSTANTS.COMMONS_VALUES.CURRENCY_SYMBOL + ' ',
  suffix: '',
  thousands: CONSTANTS.COMMONS_VALUES.THOUSANDS,
  nullable: false
};
@NgModule({
  declarations: [BillsToPayComponent, FormPaymentComponent],
  imports: [
    CommonModule,
    NgxDatatableModule,
    SimpleNotificationsModule,
    SharedModule,
    NgxSelectModule,
    NgSelectModule,
    NgxTranslateModule,
    NgxCurrencyModule,
    FormsModule,
    ReactiveFormsModule,
    InputMaskModule,
    ModalCcostCategoryModule,
    NgxDatatableModule,
    PipesModule,
    DirectivesModule,
    NgSelectModule,
    AccountBillToPayModule,
    NgxMaskModule.forRoot(),
    BillsToPayRoutingModule,
    ComponentsModule,
  ],
  providers: [
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig }
  ]
})
export class BillsToPayModule { }
