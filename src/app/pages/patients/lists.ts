import {
  SelectItem,
  SelectItemStatus
} from '../../core/models/forms/common/common.model';
import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';

@Injectable()
export class LISTAS {
  listaStatusTratamento: SelectItemStatus[];
  listaStatusTratamentoServico: SelectItemStatus[];
  listaFormaPagamento: SelectItem[];
  listaTipoDesconto: SelectItem[];
  listaFormaRecebimento: SelectItem[];

  constructor(translate: TranslateService) {
    this.listaStatusTratamentoServico = [
      {
        label: translate.instant(
          'PATIENTS.MODAL_PROCEDURE.STATUS_TREATMENT.REALIZAR'
        ),
        value: 'REALIZAR',
        color: '#c9ac09'
      },
      {
        label: translate.instant(
          'PATIENTS.MODAL_PROCEDURE.STATUS_TREATMENT.FINALIZADO'
        ),
        value: 'FINALIZADO',
        color: '#058359'
      },
      {
        label: translate.instant(
          'PATIENTS.MODAL_PROCEDURE.STATUS_TREATMENT.INICIADO'
        ),
        value: 'INICIADO',
        color: '#4099ff'
      },
      {
        label: translate.instant(
          'PATIENTS.MODAL_PROCEDURE.STATUS_TREATMENT.OBSERVADO'
        ),
        value: 'OBSERVADO',
        color: '#3f3f3f'
      },
      {
        label: translate.instant(
          'PATIENTS.MODAL_PROCEDURE.STATUS_TREATMENT.NAO_AUTORIZADO'
        ),
        value: 'NAO_AUTORIZADO',
        color: '#FF5370'
      }
    ];

    this.listaStatusTratamento = [
      {
        label: translate.instant(
          'PATIENTS.FORM_TREATMENT.LISTA_STATUS_TRATAMENTO.AGUARDANDO_APROVACAO'
        ),
        value: 'AGUARDANDO_APROVACAO',
        color: '#c9ac09'
      },
      {
        label: translate.instant(
          'PATIENTS.FORM_TREATMENT.LISTA_STATUS_TRATAMENTO.EM_TRATAMENTO'
        ),
        value: 'EM_TRATAMENTO',
        color: '#4099ff'
      },
      {
        label: translate.instant(
          'PATIENTS.FORM_TREATMENT.LISTA_STATUS_TRATAMENTO.FINALIZADO'
        ),
        value: 'FINALIZADO',
        color: '#058359'
      }
    ];

    this.listaFormaPagamento = [
      {
        label: translate.instant(
          'PATIENTS.FORM_TREATMENT.LISTA_FORMA_PAGAMENTO.A_VISTA'
        ),
        value: 'A_VISTA'
      },
      {
        label: translate.instant(
          'PATIENTS.FORM_TREATMENT.LISTA_FORMA_PAGAMENTO.A_PRAZO'
        ),
        value: 'A_PRAZO'
      },
      {
        label: translate.instant(
          'PATIENTS.FORM_TREATMENT.LISTA_FORMA_PAGAMENTO.NUM_PROCEDIMENTO'
        ),
        value: 'NUM_PROCEDIMENTO'
      }
    ];

    this.listaTipoDesconto = [
      {
        label: translate.instant(
          'PATIENTS.FORM_TREATMENT.LISTA_TIPO_DESCONTO.MOEDA'
        ),
        value: 'MOEDA'
      },
      {
        label: translate.instant(
          'PATIENTS.FORM_TREATMENT.LISTA_TIPO_DESCONTO.PORCENTAGEM'
        ),
        value: 'PORCENTAGEM'
      }
    ];

    this.listaFormaRecebimento = [
      {
        label: translate.instant('FINANCIAL.LIST_SEARCH.TYPE_PAYMENT.BOLETO'),
        value: 'BOLETO'
      },
      {
        label: translate.instant(
          'FINANCIAL.LIST_SEARCH.TYPE_PAYMENT.CARTAO_CREDITO'
        ),
        value: 'CARTAO_CREDITO'
      },
      {
        label: translate.instant(
          'FINANCIAL.LIST_SEARCH.TYPE_PAYMENT.CARTAO_DEBITO'
        ),
        value: 'CARTAO_DEBITO'
      },
      {
        label: translate.instant('FINANCIAL.LIST_SEARCH.TYPE_PAYMENT.CHEQUE'),
        value: 'CHEQUE'
      },
      {
        label: translate.instant('FINANCIAL.LIST_SEARCH.TYPE_PAYMENT.DINHEIRO'),
        value: 'DINHEIRO'
      },
      {
        label: translate.instant(
          'FINANCIAL.LIST_SEARCH.TYPE_PAYMENT.DEBITO_AUTOMATICO'
        ),
        value: 'DEBITO_AUTOMATICO'
      },
      {
        label: translate.instant(
          'FINANCIAL.LIST_SEARCH.TYPE_PAYMENT.TRANSFERENCIA_BANCARIA'
        ),
        value: 'TRANSFERENCIA_BANCARIA'
      },
      {
        label: translate.instant(
          'FINANCIAL.LIST_SEARCH.TYPE_PAYMENT.PERMUTA'
        ),
        value: 'PERMUTA'
      }
    ];
  }
}
