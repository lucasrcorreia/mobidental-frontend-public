import { TestBed } from '@angular/core/testing';

import { Invites.ApiService } from './invites.api.service';

describe('Invites.ApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Invites.ApiService = TestBed.get(Invites.ApiService);
    expect(service).toBeTruthy();
  });
});
