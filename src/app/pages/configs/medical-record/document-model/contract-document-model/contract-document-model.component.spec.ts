import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ContractDocumentModelComponent } from "./contract-document-model.component";

describe("ContractDocumentModelComponent", () => {
	let component: ContractDocumentModelComponent;
	let fixture: ComponentFixture<ContractDocumentModelComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ContractDocumentModelComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ContractDocumentModelComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
