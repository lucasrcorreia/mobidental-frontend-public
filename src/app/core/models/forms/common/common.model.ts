export interface SelectItem {
  label: string;
  value: string;
}

export interface SelectItemStatus {
  label: string;
  value: string;
  color: string;
}

export interface ShortListItem {
  id: number;
  nome: string;
}

export interface ShortListItemDesc {
  id: number;
  descricao: string;
}
