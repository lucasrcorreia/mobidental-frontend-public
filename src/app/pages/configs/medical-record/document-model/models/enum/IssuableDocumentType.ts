export enum IssuableDocumentType{
	DOCUMENTO = 'DOCUMENTO',
	RECEITUARIO = 'RECEITUARIO',
}

export const ISSUABLE_DOCUMENT_TYPE_TRANSLATE: { [key in IssuableDocumentType]: string } = {
	DOCUMENTO: 'Documento',
	RECEITUARIO: 'Receituário',
};

