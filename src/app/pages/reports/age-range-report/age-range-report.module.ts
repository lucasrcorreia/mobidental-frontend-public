import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AgeRangeReportRoutingModule } from "./age-range-report-routing.module";
import { AgeRangeReportComponent } from "./age-range-report.component";
import { NgxTranslateModule } from "../../../core/translate/translate.module";
import { ReportsModule } from "../reports.module";
import { NgxChartsModule } from "@swimlane/ngx-charts";

@NgModule({
	declarations: [AgeRangeReportComponent],
	imports: [
		CommonModule,
		AgeRangeReportRoutingModule,
		NgxTranslateModule,
		ReportsModule,
		NgxChartsModule,
	],
})
export class AgeRangeReportModule {}
