import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsModule } from '../../../reports/reports.module';
import { NgxTranslateModule } from '../../../../core/translate/translate.module';
import { FormsModule } from '@angular/forms';
import { NgbButtonsModule, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSelectModule } from 'ngx-select-ex';
import { InputMaskModule } from 'racoon-mask-raw';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { CashFlowReportComponent } from "./cash-flow-report.component";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { AppLibModule } from "../../../../lib/app-lib.module";
import { DailyCashFlowComponent } from './daily-cash-flow/daily-cash-flow.component';
import { MonthlyCashFlowComponent } from './monthly-cash-flow/monthly-cash-flow.component';
import { SharedModule } from '../../../../shared/shared.module';

@NgModule({
  declarations: [
    CashFlowReportComponent,
    DailyCashFlowComponent,
    MonthlyCashFlowComponent,
  ],
              imports: [
                  CommonModule,
                  ReportsModule,
                  NgxTranslateModule,
                  FormsModule,
                  NgbDatepickerModule,
                  NgxSelectModule,
                  InputMaskModule,
                  NgxChartsModule,
                  NgxDatatableModule,
                  AppLibModule,
                  NgbButtonsModule,
                  SharedModule,
              ],
  exports: [
    CashFlowReportComponent
  ]
})
export class CashFlowReportModule {
}
