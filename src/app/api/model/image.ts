export interface DocumentModel {
  id?: number;
  descricao: string;
  observacao: string;
  data?: string;
  pacienteId: number;
  urlAnexo?: string;
  urlAnexoThumb?: string;
  extensao?: string;
}
