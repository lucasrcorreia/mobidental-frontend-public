import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../../services/guard/auth.guard';

const routes: Routes = [
  {
    path: 'anamnese',
    loadChildren: './anamnese/anamnese.module#AnamneseModule',
    data: {
      id: 17,
      children: true,
    },
    canActivate: [AuthGuard],
  },
  {
    path: 'document-model',
    loadChildren: './document-model/document-model.module#DocumentModelModule',
    data: {
      id: 17,
      children: true,
    },
    canActivate: [AuthGuard],
  },
  {
    path: 'print',
    loadChildren: './print/print.module#PrintModule',
    data: {
      id: 17,
      children: true,
    },
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MedicalRecordRoutingModule {}
