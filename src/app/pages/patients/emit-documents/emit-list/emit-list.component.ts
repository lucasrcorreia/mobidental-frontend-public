import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { BASE_PATH_ISSUABLE, DocumentModelService } from '../../../configs/medical-record/document-model/service/document-model.service';
import { UtilsService } from '../../../../services/utils/utils.service';
import { NotificationsService } from 'angular2-notifications';
import { Tab } from '../emit-documents.component';
import { IssuableDocument } from '../../../configs/medical-record/document-model/models/IssuableDocument';
import { PatientModel } from '../../../../core/models/patients/patients.model';
import { ISSUABLE_DOCUMENT_TYPE_TRANSLATE, IssuableDocumentType } from '../../../configs/medical-record/document-model/models/enum/IssuableDocumentType';
import Swal from 'sweetalert2';
import { Action } from '../../../../services/pdf-generator/pdf-generator.service';
import { VisualizeIssuableDocumentComponent } from '../visualize-issuable-document/visualize-issuable-document.component';
import { NgbModal, NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';
import { convertBlobToBase64 } from '../../../../services/utils/file-utils.service';
import { PrescriptionService } from '../emit-prescription/services/prescription.service';
import { MIME_TYPE_FOR_PDF } from '../service/common-print.service';
import { Prescription } from '../emit-prescription/models/prescription';
import { PRESCRIPTION_TYPE_TRANSLATE } from '../emit-prescription/models/enums/prescription-type';

@Component({
	selector: 'app-emit-list',
	templateUrl: './emit-list.component.html',
	styleUrls: ['./emit-list.component.scss'],
})
export class EmitListComponent implements OnInit {

	@ViewChild('ngbTabset') ngbTabset: NgbTabset;

	@Input() patient: PatientModel;
	@Input() currentIssuableDocumentType: IssuableDocumentType;
	@Output() tabEmitter = new EventEmitter<{ tab: Tab, issuableDocumentType: IssuableDocumentType }>();
	readonly tab = Tab;
	readonly action = Action;
	readonly issuableDocumentType = IssuableDocumentType;
	readonly issuableDocumentTypeTranslate = ISSUABLE_DOCUMENT_TYPE_TRANSLATE;
	readonly prescriptionTypeTranslate = PRESCRIPTION_TYPE_TRANSLATE;
	issuableDocuments: IssuableDocument[];
	prescriptions: Prescription[];

	constructor(private readonly _documentModelService: DocumentModelService,
				private readonly _prescriptionService: PrescriptionService,
				readonly _service: UtilsService,
				private readonly _modalService: NgbModal,
				private readonly _sanitizer: DomSanitizer,
				private readonly _notificationsService: NotificationsService) { }

	ngOnInit() {
		this._initDocuments();
	}

	private async _initDocuments() {
		if (this.currentIssuableDocumentType == IssuableDocumentType.DOCUMENTO) {
			this.issuableDocuments = null;
			this.issuableDocuments = await this._documentModelService.listAllIssuableDocumentByPatient(this.patient.id);
		} else {
			this.prescriptions = null;
			this.prescriptions = await this._prescriptionService.listAllIssuablePrescriptionByPatient(this.patient.id);
		}

		this.ngbTabset.select(this.currentIssuableDocumentType);
	}

	changeDatatableFilter($event) {
		if (this.currentIssuableDocumentType !== $event.nextId) {
			this.currentIssuableDocumentType = $event.nextId;
			this._initDocuments();
		}
	}

	async delete(id: number, type: IssuableDocumentType) {
		const model = type === IssuableDocumentType.DOCUMENTO ? 'Documento' : 'Receituário';
		const { value } = await Swal.fire({
			title: 'Confirmação de exclusão',
			text: `Tem certeza que quer apagar este ${ model }?`,
			type: 'question',
			showCancelButton: true,
			confirmButtonText: 'Sim. Apague',
			cancelButtonText: 'Cancelar',
		});

		if (!value) {
			return;
		}

		try {
			this._service.loading(true);
			type === IssuableDocumentType.DOCUMENTO ? this._documentModelService.delete(id, BASE_PATH_ISSUABLE) : this._prescriptionService.delete(id);
			this._notificationsService.success(`${ model } excluído`);
			this._initDocuments();
		} catch (e) {
			this._notificationsService.error(`Ocorreu um erro ao excluir do ${ model }`);
		} finally {
			this._service.loading(false);
		}
	}

	async getPdfFromIssuableDocument(issuableDocument: IssuableDocument, action: Action, type: IssuableDocumentType) {
		const data = type === IssuableDocumentType.DOCUMENTO ? await this._documentModelService.getPdfFromDocument(issuableDocument.id) :
				await this._prescriptionService.getPdfFromPrescription(issuableDocument.id);
		const fileName = type === IssuableDocumentType.DOCUMENTO ? issuableDocument.nomeModelo + ' - ' + this.patient.pessoaNome + '.pdf' :
				'Receituário' + ' - ' + this.patient.pessoaNome + '.pdf';
		switch (action) {
			case Action.OPEN:
				this._visualize(data);
				break;
			case Action.DOWNLOAD:
				this._download(data, fileName);
				break;
			default:
				break;
		}
	}

	private async _visualize(arrayBuffer: ArrayBuffer) {
		const blob: Blob = new Blob([arrayBuffer], MIME_TYPE_FOR_PDF);
		let base64Url = await convertBlobToBase64(blob);
		const modalRef = this._modalService.open(VisualizeIssuableDocumentComponent, { size: 'xl' as 'lg' });
		modalRef.componentInstance.pdfUrl = this._sanitizer.bypassSecurityTrustResourceUrl(base64Url.toString());
	}

	private _download(arrayBuffer: ArrayBuffer, fileName) {
		const blob = new Blob([arrayBuffer]);
		const htmlElement = document.createElement('a');

		// Cria url de download
		const url = window.URL.createObjectURL(blob);

		document.body.appendChild(htmlElement);
		htmlElement.href = url;
		htmlElement.download = fileName;
		htmlElement.click();

		// Exclui url e elemento de download após download
		document.body.removeChild(htmlElement);
		window.URL.revokeObjectURL(url);
	}

	emit(tab: Tab, issuableDocumentType: IssuableDocumentType) {
		this.tabEmitter.emit({ tab: tab, issuableDocumentType: issuableDocumentType });
	}

}
