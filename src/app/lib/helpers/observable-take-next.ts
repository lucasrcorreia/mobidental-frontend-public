import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

export const observableTakeNext = <T>(obs: Observable<T>): Promise<T> =>
  obs.pipe(take(1)).toPromise();
