import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FluxoCaixaListagemComponent } from './fluxo-caixa-listagem.component';

describe('FluxoCaixaListagemComponent', () => {
  let component: FluxoCaixaListagemComponent;
  let fixture: ComponentFixture<FluxoCaixaListagemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FluxoCaixaListagemComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FluxoCaixaListagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
