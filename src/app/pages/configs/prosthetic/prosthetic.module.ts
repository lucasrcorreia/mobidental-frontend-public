import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProstheticRoutingModule } from './prosthetic-routing.module';
import { ProstheticComponent } from './prosthetic.component';
import { DirectivesModule } from '../../../core/directive/directives.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InputMaskModule } from 'racoon-mask-raw';
import { NgxMaskModule } from 'ngx-mask';
import { NgxTranslateModule } from '../../../core/translate/translate.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from '../../../shared/shared.module';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormProstheticComponent } from './form-prosthetic/form-prosthetic.component';

@NgModule({
  declarations: [ProstheticComponent, FormProstheticComponent],
  imports: [
    CommonModule,
    NgxDatatableModule,
    SimpleNotificationsModule,
    SharedModule,
    NgSelectModule,
    NgxTranslateModule,
    NgxMaskModule.forRoot(),
    InputMaskModule,
    FormsModule,
    ReactiveFormsModule,
    DirectivesModule,
    ProstheticRoutingModule
  ]
})
export class ProstheticModule { }
