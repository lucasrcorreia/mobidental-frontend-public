import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientsReportRoutingModule } from './patients-report-routing.module';
import { PatientsReportComponent } from './patients-report.component';
import { NgxSelectModule } from 'ngx-select-ex';
import { FormsModule } from '@angular/forms';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { InputMaskModule } from 'racoon-mask-raw';
import { ReportsModule } from '../reports.module';
import { NgxTranslateModule } from '../../../core/translate/translate.module';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
	declarations: [
		PatientsReportComponent,
	],
	imports: [
		CommonModule,
		PatientsReportRoutingModule,
		NgxSelectModule,
		FormsModule,
		NgbDatepickerModule,
		InputMaskModule,
		ReportsModule,
		NgxTranslateModule,
		NgSelectModule,
	],
})
export class PatientsReportModule {}
