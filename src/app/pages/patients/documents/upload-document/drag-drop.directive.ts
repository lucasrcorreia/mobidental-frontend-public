import { Directive, EventEmitter, HostBinding, HostListener, Output } from '@angular/core';

@Directive({
  selector: '[appDragDrop]'
})
export class DragDropDirective {

  @Output() onFileDropped = new EventEmitter<any>();

  @HostBinding('style.background-color') private _background = '#f5fcff';
  @HostBinding('style.opacity') private _opacity = '1';

  @HostListener('dragover', ['$event']) onDragOver(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this._setBackgroundAndOpacity('#9ecbec', '.8');
  }

  @HostListener('dragleave', ['$event']) onDragLeave(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this._setBackgroundAndOpacity('#f5fcff', '1');
  }

  @HostListener('drop', ['$event']) ondrop(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this._setBackgroundAndOpacity('#f5fcff', '1');
    const files = evt.dataTransfer.files;
    if (files.length > 0) {
      this.onFileDropped.emit(files);
    }
  }

  private _setBackgroundAndOpacity(background: string, opacity: string): void {
    this._background = background;
    this._opacity = opacity;
  }

}
