import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipleCameraUploadComponent } from './multiple-camera-upload.component';

describe('MultipleCameraUploadComponent', () => {
  let component: MultipleCameraUploadComponent;
  let fixture: ComponentFixture<MultipleCameraUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleCameraUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleCameraUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
