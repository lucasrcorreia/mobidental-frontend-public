import { Component, OnInit, Output } from '@angular/core';
import { NgbDateParserFormatter, NgbDatepickerI18n, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';
import { CustomDatepickerI18n, I18n } from '../../../../shared/ngb-datepicker/datepicker-i18n';
import { NgbDateFRParserFormatter } from '../../../../shared/ngb-datepicker/ngb-date-fr-parser-formatter';
import { equals } from 'ramda';

export enum FormatoPeriodo {
  HOJE = 'HOJE',
  ESSA_SEMANA = 'ESSA_SEMANA',
  ESSE_MES = 'ESSE_MES',
  MES_PASSADO = 'MES_PASSADO',
  ESPECIFICO = 'ESPECIFICO',
}

const descricoesPeriodo: { [k in FormatoPeriodo]: string } = {
  ESPECIFICO: 'Específico',
  ESSA_SEMANA: 'Essa semana',
  ESSE_MES: 'Esse mês',
  HOJE: 'Hoje',
  MES_PASSADO: 'Mês passado',
};

export const ngbToDate = (ngb: NgbDateStruct): Date =>
  moment({ ...ngb, month: ngb.month - 1 }).toDate();

export const dateToNgb = (date: Date): NgbDateStruct => ({
  year: date.getFullYear(),
  month: date.getMonth() + 1,
  day: date.getDate(),
});

const resolveStartAndEndDate = (period: FormatoPeriodo): [Date, Date] | undefined => {
  const endOfToday = moment().endOf('day').toDate();

  switch (period) {
    case FormatoPeriodo.ESSE_MES: {
      return [
        moment().startOf('month').toDate(),
        moment().endOf('month').toDate(),
      ];
    }

    case FormatoPeriodo.ESSA_SEMANA: {
      const lastWeek = moment().subtract(1, 'week').startOf('day');
      return [lastWeek.toDate(), endOfToday];
    }

    case FormatoPeriodo.MES_PASSADO: {
      const lastMonth = moment().startOf('month').subtract(1, 'month');
      return [
        lastMonth.toDate(),
        lastMonth.endOf('month').toDate(),
      ];
    }

    case FormatoPeriodo.HOJE: {
      return [moment().startOf('day').toDate(), endOfToday];
    }

    case FormatoPeriodo.ESPECIFICO:
      return undefined;
  }
};

@Component({
  selector: 'app-fluxo-caixa-filtros',
  templateUrl: './fluxo-caixa-filtros.component.html',
  styleUrls: ['./fluxo-caixa-filtros.component.scss'],
  providers: [
    I18n,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
    { provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
  ],
})
export class FluxoCaixaFiltrosComponent implements OnInit {

  readonly periodosOptions = Object.values(FormatoPeriodo).map((periodo: FormatoPeriodo) => ({
    value: periodo,
    label: descricoesPeriodo[periodo],
  }));

  readonly FormatoPeriodo = FormatoPeriodo;

  readonly periodoSubject = new BehaviorSubject(FormatoPeriodo.HOJE);
  readonly startEndDateSubject = new BehaviorSubject<[Date, Date] | undefined>(resolveStartAndEndDate(this.periodoSubject.value));

  ngStartDate?: NgbDateStruct | string = dateToNgb(this.startEndDateSubject.value[0]);
  ngEndDate?: NgbDateStruct | string = dateToNgb(this.startEndDateSubject.value[1]);

  @Output() readonly dates: Observable<[Date, Date] | undefined> =
    combineLatest([
      this.periodoSubject,
      this.startEndDateSubject,
    ]).pipe(
      map(([per, startEnd]) =>
        resolveStartAndEndDate(per) || startEnd,
      ),
      filter((it: [Date | undefined, Date | undefined] | undefined) => !!(it && it[0] && it[1])),
      distinctUntilChanged<[Date, Date] | undefined>(equals),
    );

  ngOnInit() {
  }

  get periodo() {
    return this.periodoSubject.value;
  }

  set periodo(p: FormatoPeriodo) {
    this.periodoSubject.next(p);

    const dates = resolveStartAndEndDate(p).map(dateToNgb);

    if (dates) {
      [this.ngStartDate, this.ngEndDate] = dates;
    }
  }

  selectDate() {
    if (typeof this.ngStartDate === 'string' || typeof this.ngEndDate === 'string') {
      return;
    }

    if (this.ngEndDate.year < 1000 || this.ngStartDate.year < 1000) {
      return;
    }

    this.startEndDateSubject.next([
      this.ngStartDate && ngbToDate(this.ngStartDate),
      this.ngEndDate && ngbToDate(this.ngEndDate),
    ]);
  }

  maybeSelectDate() {
    if (this.ngEndDate && this.ngStartDate) {
      this.selectDate();
    }
  }

}
