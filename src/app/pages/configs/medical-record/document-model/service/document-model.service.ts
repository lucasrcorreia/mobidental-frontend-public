import { Inject, Injectable } from '@angular/core';
import { API_CONFIGURATION, ApiConfiguration } from '../../../../../api/api-configuration';
import { HttpClient } from '@angular/common/http';
import { DocumentModel } from '../models/DocumentModel';
import { RespostaPaginada } from '../../../../../api/model/resposta-paginada';
import { Tag } from '../models/Tag';
import { IssuableDocument } from '../models/IssuableDocument';

const BASE_PATH = 'modeloDocumento'
export const BASE_PATH_ISSUABLE = 'documento'

@Injectable({
	providedIn: 'root',
})
export class DocumentModelService {

	constructor(
			@Inject(API_CONFIGURATION) private readonly _config: ApiConfiguration,
			private readonly _http: HttpClient,
	) {
	}

	listTags(): Promise<Tag[]> {
		return this._http
				.get<Tag[]>(`${ this._config.apiBasePath }/tagDocumento/listarTags`)
				.toPromise();
	}

	listAll(): Promise<RespostaPaginada<DocumentModel>> {
		const query = {
			query: '',
			size: 200,
			page: 0,
			sorting: { undefined: 'asc' },
		};
		return this._http
				.post<RespostaPaginada<DocumentModel>>(`${ this._config.apiBasePath }/${ BASE_PATH }/all`, query)
				.toPromise();
	}

	listAllIssuableDocumentByPatient(patientId: number): Promise<IssuableDocument[]> {
		const query = {
			pacienteId: patientId,
		};
		return this._http
				.post<IssuableDocument[]>(`${ this._config.apiBasePath }/${ BASE_PATH_ISSUABLE }/findAllDocumentos`, query)
				.toPromise();
	}

	findOne(id: number): Promise<DocumentModel> {
		return this._http
				.get<DocumentModel>(`${ this._config.apiBasePath }/${ BASE_PATH }/findOne/${ id }`)
				.toPromise();
	}

	listDocumentModelsAsDropdown(): Promise<DocumentModel[]> {
		return this._http
				.get<DocumentModel[]>(`${ this._config.apiBasePath }/${ BASE_PATH }/dropdown`)
				.toPromise();
	}

	save(documentModel: DocumentModel): Promise<DocumentModel> {
		return this._http
				.post<DocumentModel>(`${ this._config.apiBasePath }/${ BASE_PATH }/save`, documentModel)
				.toPromise();
	}

	emit(issuableDocument: IssuableDocument, file: File): Promise<IssuableDocument> {
		const formData = this._buildFormData(issuableDocument, file);

		return this._http
				.post<IssuableDocument>(`${ this._config.apiBasePath }/${ BASE_PATH_ISSUABLE }/salvarDocumentoComAnexo`, formData)
				.toPromise();
	}

	getPdfFromDocument(id: number): Promise<ArrayBuffer> {
		return this._http
				.get<ArrayBuffer>(`${ this._config.apiBasePath }/${ BASE_PATH_ISSUABLE }/download/` + id, { responseType: 'blob' as 'json' })
				.toPromise();
	}

	delete(id: number, basePath = BASE_PATH): Promise<void> {
		return this._http
				.delete<void>(`${ this._config.apiBasePath }/${ basePath }/${ id }`)
				.toPromise();
	}

	getTagValuesByDocument(issuableDocument: IssuableDocument): Promise<any> {
		const params = {
			data: issuableDocument.data,
			profissionalId: issuableDocument.profissionalId,
			pacienteId: issuableDocument.pacienteId,
		};
		return this._http
				.post<any>(`${ this._config.apiBasePath }/${ BASE_PATH_ISSUABLE }/obterValoresTagsByDocumento`, params)
				.toPromise();
	}

	private _buildFormData(issuableDocument: IssuableDocument, file: File): FormData {
		const formData = new FormData();
		formData.append('file', file);
		formData.append('dados', JSON.stringify(issuableDocument));

		return formData;
	}
}
