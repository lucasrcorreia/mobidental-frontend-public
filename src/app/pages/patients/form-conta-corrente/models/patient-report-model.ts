import { PatientReportRowModel } from "./patient-report-row-model";

export interface PatientReportModel {
	valorTotalRecebido?: number;
	quantidadeParcelasAPagar?: number;
	quantidadeParcelasPagas?: number;
	valorTotal?: number;
	valorTotalARecer?: number;
	content?: PatientReportRowModel[];
}
