import { TipoPerguntaAnamnese } from './enums/TipoPerguntaAnamnese';
import { TipoAlertaPerguntaAnamnese } from './enums/TipoAlertaPerguntaAnamnese';
import { AtivoInativo } from './enums/AtivoInativo';

export interface AnamnesePerguntaModel {
  id?: number;
  pergunta?: string;
  tipoPergunta?: TipoPerguntaAnamnese;
  tipoAlerta?: TipoAlertaPerguntaAnamnese;
  status?: AtivoInativo;
  posicao?: number;
  modeloId?: number;
}
