import { TestBed } from '@angular/core/testing';

import { PrintConfigurationsService } from './print-configurations.service';

describe('PrintService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrintConfigurationsService = TestBed.get(PrintConfigurationsService);
    expect(service).toBeTruthy();
  });
});
