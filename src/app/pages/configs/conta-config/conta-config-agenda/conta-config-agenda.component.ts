import { Component, Input, OnInit } from '@angular/core';
import { ContaModel } from '../../../../api/conta-api.service';

@Component({
  selector: 'app-conta-config-agenda',
  templateUrl: './conta-config-agenda.component.html',
  styleUrls: ['./conta-config-agenda.component.scss'],
})
export class ContaConfigAgendaComponent {

  @Input() conta: Partial<ContaModel> = {};

}
