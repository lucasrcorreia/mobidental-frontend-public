import { TestBed } from '@angular/core/testing';

import { DocumentModelService } from './document-model.service';

describe('DocumentModelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DocumentModelService = TestBed.get(DocumentModelService);
    expect(service).toBeTruthy();
  });
});
