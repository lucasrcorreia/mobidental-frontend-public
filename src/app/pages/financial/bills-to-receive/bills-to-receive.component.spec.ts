import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillsToReceiveComponent } from './bills-to-receive.component';

describe('BillsToReceiveComponent', () => {
  let component: BillsToReceiveComponent;
  let fixture: ComponentFixture<BillsToReceiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillsToReceiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillsToReceiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
