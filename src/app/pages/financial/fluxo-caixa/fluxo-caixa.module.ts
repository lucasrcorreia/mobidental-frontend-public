import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FluxoCaixaFiltrosComponent } from './fluxo-caixa-filtros/fluxo-caixa-filtros.component';
import { FluxoCaixaListagemComponent } from './fluxo-caixa-listagem/fluxo-caixa-listagem.component';
import { FluxoCaixaResumoComponent } from './fluxo-caixa-resumo/fluxo-caixa-resumo.component';
import { FluxoCaixaComponent } from './fluxo-caixa.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { NgbDatepickerModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { InputMaskModule } from 'racoon-mask-raw';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AppLibModule } from '../../../lib/app-lib.module';
import {CashFlowReportModule} from './cash-flow-report/cash-flow-report.module';
import { ReportsModule } from '../../reports/reports.module';

@NgModule({
  declarations: [
    FluxoCaixaFiltrosComponent,
    FluxoCaixaListagemComponent,
    FluxoCaixaResumoComponent,
    FluxoCaixaComponent,

  ],
              imports: [
                  CommonModule,
                  FormsModule,
                  NgSelectModule,
                  NgbDatepickerModule,
                  InputMaskModule,
                  NgxDatatableModule,
                  AppLibModule,
                  NgbTabsetModule,
                  CashFlowReportModule,
                  ReportsModule,
              ],
})
export class FluxoCaixaModule {}
