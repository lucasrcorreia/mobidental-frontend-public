import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { DailyCashFlow } from '../models/daily-cash-flow';
import { CashFlowStoreService } from '../service/cash-flow-store.service';
import { Subscription } from 'rxjs';
import { ReportPrintType } from '../../../../reports/report-header-actions/report-header-actions.component';
import { NotificationsService } from 'angular2-notifications';
import { GlobalSpinnerService } from '../../../../../lib/global-spinner.service';
import {
    CommonsReportBuildService,
    GREEN_COLOR,
    RED_COLOR,
    REPORT_STYLES
} from '../../../../../services/report/commons-report-build.service';
import { PdfGeneratorService, PDFMake } from '../../../../../services/pdf-generator/pdf-generator.service';
import { SheetGeneratorService } from '../../../../../services/sheets-generator/sheet-generator.service';
import { CASH_FLOW_MONTHS, CashFlowFilter, numberToBRLCurrency } from '../cash-flow-report.component';
import { isEmpty } from 'lodash-es';

const REPORT_FILE_NAME = 'relatório-fluxo-caixa-diário';
const DARK_BLUE = '#395A7B';

@Component({
  selector: 'app-daily-cash-flow',
  templateUrl: './daily-cash-flow.component.html',
  styleUrls: ['./daily-cash-flow.component.scss']
})
export class DailyCashFlowComponent implements OnInit, OnDestroy {

    @Input() filter: CashFlowFilter;
    cashFlow: DailyCashFlow;
    chart: any;

    numberToBRLCurrency = numberToBRLCurrency;

    loading;
    readonly portugueseMessages = {
        emptyMessage: 'Nenhum dado cadastrado...',
        totalMessage: 'registro(s)',
        selectedMessage: 'selecionado'
    };

    private _subscriptions: Subscription = new Subscription();

    constructor(
        private readonly _cashFlowStoreService: CashFlowStoreService,
        private readonly _notificationsService: NotificationsService,
        private readonly _globalSpinnerService: GlobalSpinnerService,
        private readonly _commonsReportService: CommonsReportBuildService,
        private readonly _pdfGeneratorService: PdfGeneratorService,
        private readonly _sheetGeneratorService: SheetGeneratorService,
    ) {
    }

    ngOnInit() {
        this._dailyCashFlowStoreSubscribe();
    }

    ngOnDestroy() {
        this._subscriptions.unsubscribe();
    }

    private _dailyCashFlowStoreSubscribe() {
        const sub = this._cashFlowStoreService.$dailyCashFlowObservable.subscribe((cashFlow: DailyCashFlow) => {
            this.cashFlow = cashFlow;
            if (cashFlow && !isEmpty(cashFlow.items)) {
                cashFlow.badgeClassName = this._getBalanceColor(cashFlow.saldo);
                cashFlow.items.forEach(item => item.badgeClassName = this._getBalanceColor(item.saldo));
                this._buildChart();
            }
        });
        const printSub = this._cashFlowStoreService.$dailyCashFlowPrintEmitter.subscribe((type: ReportPrintType) => {
            if (type !== undefined) {
                this.print(type);
            }
        });
        const loadingSub = this._cashFlowStoreService.$loadingEmitter.subscribe((loading: boolean) => {
            this.loading = loading;
        });
        this._subscriptions.add(sub);
        this._subscriptions.add(printSub);
        this._subscriptions.add(loadingSub);
    }

    private _buildChart() {
        this.chart = new Chart('daily-canvas', {
            type: 'bar',
            data: {
                datasets: [
                    {
                        label: 'Recebimentos',
                        data: this.cashFlow.items.map(item => item.totalEntrada),
                        order: 2,
                        backgroundColor: '#28a745'
                    },
                    {
                        label: 'Pagamentos',
                        data: this.cashFlow.items.map(item => -item.totalSaida),
                        order: 3,
                        backgroundColor: '#dc3545'
                    },
                    {
                        label: 'Saldo',
                        data: this.cashFlow.items.map(item => item.saldo),
                        type: 'line',
                        order: 1,
                        backgroundColor: 'transparent',
                        borderColor: DARK_BLUE,
                        pointBackgroundColor: DARK_BLUE
                    }
                ],
                labels: this.cashFlow.items.map(item => item.dia)
            },
            options: {
                maintainAspectRatio: false,
                legend: {
                    display: true
                },
                scales: {
                    xAxes: [{
                        display: true
                    }],
                    yAxes: [{
                        display: true
                    }],
                },
                tooltips: {
                    titleFontSize: 15,
                    titleAlign: 'center',
                    backgroundColor: DARK_BLUE,
                    callbacks: {
                        title: (tooltipItem) => {
                            return this.cashFlow.items[tooltipItem[0].index].data;
                        },
                        label: function (tooltipItem, data) {
                            return data.datasets[tooltipItem.datasetIndex].label + ': ' + numberToBRLCurrency(
                                tooltipItem.yLabel);
                        }
                    }
                },
            }
        });
    }

    private _getBalanceColor(saldo: number): string {
        if (saldo === 0) {
            return 'badge-light';
        }

        return saldo > 0 ? 'badge-success' : 'badge-danger';
    }

    print(type: ReportPrintType) {
        if (!this.cashFlow || !this.cashFlow.items || !(this.cashFlow.items.length > 0)) {
            this._notificationsService.info('Não há dados a serem exportados.');
            return;
        }
        const loading = this._globalSpinnerService.loadingManager.start();
        try {
            switch (type) {
                case ReportPrintType.SHEETS:
                    this._generateSheetsReport();
                    break;
                default:
                    this._generatePdfReport();
                    break;
            }
        } catch (e) {
            this._notificationsService.error('Ocorreu um erro ao emitir o relatório.');
        } finally {
            loading.finished();
        }
    }

    private _generateSheetsReport() {
        this._sheetGeneratorService.exportJsonAsExcelFile(this._buildRowsToSheet(), REPORT_FILE_NAME);
    }

    private _buildRowsToSheet(): unknown[] {
        return this.cashFlow.items.map(item => {
            return {
                'Data': item.data,
                'Recebimentos': numberToBRLCurrency(item.totalEntrada),
                'Pagamentos': numberToBRLCurrency(item.totalSaida),
                'Saldo Final': numberToBRLCurrency(item.saldo),
            };
        });
    }

    private async _generatePdfReport() {
        const pdfMake = await this._buildReport();
        const pdfMakeDefinition = await this._pdfGeneratorService.exportPDFMakeAsPDF(
            pdfMake,
            '',
            REPORT_FILE_NAME + '.pdf'
        );
        pdfMakeDefinition.open();
    }

    private async _buildReport(): Promise<PDFMake> {
        const pdfMake: PDFMake = { afterContent: [], styles: REPORT_STYLES, pageMargins: [20, 20, 20, 20] };
        pdfMake.afterContent.push(await this._buildReportHeader());
        pdfMake.afterContent.push(this._buildReportBody());
        pdfMake.afterContent.push(this._buildReportResume());

        return pdfMake;
    }

    private async _buildReportHeader() {
        return [
            { ...await this._commonsReportService.buildFinancialReportHeader('Relatório de Fluxo de Caixa Diário') },
            {
                text: [
                    { text: 'Mês: ', bold: true },
                    CASH_FLOW_MONTHS.find(month => month.value === this.filter.mes).label,
                ],
                fontSize: 9,
                margin: [0, 10, 0, 0],
            },
            {
                text: [
                    { text: 'Ano: ', bold: true },
                    this.filter.ano,
                ],
                fontSize: 9,
                margin: [0, 0, 0, 10],
            },
        ];
    }

    private _buildReportBody() {
        return {
            table: {
                widths: [100, 135, 135, 150],
                body: [
                    [
                        { text: 'Data', style: 'tableHeader' },
                        { text: 'Recebimentos', style: 'tableHeader' },
                        { text: 'Pagamentos', style: 'tableHeader' },
                        { text: 'Saldo Total', style: 'tableHeader', alignment: 'right' },
                    ],
                    ...this._buildReportRows(),
                ],
            },
        };
    }

    private _buildReportRows() {
        return this.cashFlow.items.map(item => {
                                           return [
                                               { text: item.data || ' ', style: 'tableRow', alignment: 'center' },
                                               { text: numberToBRLCurrency(item.totalEntrada) || ' ', style: 'tableRow', alignment: 'right' },
                                               { text: numberToBRLCurrency(item.totalSaida) || ' ', style: 'tableRow', alignment: 'right' },
                                               { text: numberToBRLCurrency(item.saldo) || ' ', style: 'tableRow', alignment: 'right' },
                                           ];
                                       },
        );
    }

    private _buildReportResume() {
        return {
            columns: [
                {
                    width: 460,
                    alignment: 'right',
                    stack: [
                        { text: 'Recibimentos:', style: 'resumeHeader', color: GREEN_COLOR },
                        { text: 'Pagamentos:', style: 'resumeHeader', color: RED_COLOR },
                        { text: 'Saldo Total:', style: 'resumeHeader' },
                    ],
                },
                {
                    alignment: 'right',
                    stack: [
                        {
                            text: numberToBRLCurrency(this.cashFlow.totalEntrada) || ' - ',
                            style: 'resumeRow',
                            color: GREEN_COLOR
                        },
                        {
                            text: numberToBRLCurrency(this.cashFlow.totalSaida) || ' - ',
                            style: 'resumeRow',
                            color: RED_COLOR
                        },
                        { text: numberToBRLCurrency(this.cashFlow.saldo) || ' - ', style: 'resumeRow' },
                    ],
                },
            ],
            margin: [0, 10, 0, 0],
        };
    }

}
