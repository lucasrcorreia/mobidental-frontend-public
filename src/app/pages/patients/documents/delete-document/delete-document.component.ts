import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DocumentModel } from '../../../../api/model/image';

@Component({
  selector: 'app-modal-content',
  templateUrl: './delete-document.component.html',
  styleUrls: ['./delete-document.component.scss']
})
export class DeleteDocumentComponent implements OnInit {

  @Input() document: DocumentModel;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
