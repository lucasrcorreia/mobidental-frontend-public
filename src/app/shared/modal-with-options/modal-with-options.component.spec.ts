import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalWithOptionsComponent } from './modal-with-options.component';

describe('ModalWithOptionsComponent', () => {
  let component: ModalWithOptionsComponent;
  let fixture: ComponentFixture<ModalWithOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalWithOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalWithOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
