import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { D3CollectionBillInterface } from '../../core/models/table/columns.model';
import { UtilsService } from '../../services/utils/utils.service';

export interface TotalizersInfo {
  total: number;
  received: number;
  percent: number;
  vanquished?: number;
}

@Component({
  selector: 'app-totalizers',
  templateUrl: './totalizers.component.html',
  styleUrls: ['./totalizers.component.scss'],
})
export class TotalizersComponent implements OnInit {

  @Input() item: D3CollectionBillInterface;
  @Input() max = true;
  @Input() progress = {};

  @Input() noBottomPadding = false;

  @Input() labelTotal?: string;
  @Input() labelExecutado?: string;
  @Input() labelRestante?: string;

  locale = this.service.translate.instant('COMMON.LOCALE');

  @HostBinding('class.row') readonly rowClass = true;

  constructor(
    private service: UtilsService,
  ) { }

  ngOnInit() {
  }

}
