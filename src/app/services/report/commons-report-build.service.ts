import * as moment from "moment";
import {
	MOBI_DENTAL_PATH,
	PrintConfigurationsService,
} from "../../pages/configs/medical-record/print/service/print-configurations.service";
import { convertBlobToBase64 } from "../utils/file-utils.service";
import { descricoesLancamentos, TipoLancamento } from "../../api/model/tipo-lancamento";
import { descricoesPagamentos, FormaPagamento } from "../../api/model/forma-pagamento";
import { isEmpty } from "lodash-es";
import { descricoesRecebimentos, TipoRecebimento } from "../../api/model/tipo-recebimento";
import { NotificationsService } from "angular2-notifications";
import { Injectable } from "@angular/core";
import { BirthDate } from "../../pages/reports/reports.service";
import { padNumber } from "app/shared/ngb-datepicker/ngb-date-fr-parser-formatter";

export const REPORT_IMAGE_HEADER_SIZE = 80;
export const BLUE_COLOR = "blue";
export const RED_COLOR = "red";
export const GREEN_COLOR = "green";
export const GREY_COLOR = "grey";
export const REPORT_STYLES = {
	title: {
		fontSize: 22,
		bold: true,
		alignment: "center",
		margin: [10, 40, 0, 0],
	},
	subTitle: {
		fontSize: 9,
		alignment: "right",
	},
	tableHeader: {
		bold: true,
		fontSize: 9,
		alignment: "center",
	},
	tableRow: {
		fontSize: 8,
	},
	resumeHeader: {
		bold: true,
		fontSize: 10,
	},
	resumeRow: {
		fontSize: 9,
	},
};

@Injectable({
	providedIn: "root",
})
export class CommonsReportBuildService {
	constructor(
		private readonly _printConfigurationsService: PrintConfigurationsService,
		private readonly _notificationsService: NotificationsService
	) {}

	checkIfPrintIsAvaliable(bills: any[]): boolean {
		if (isEmpty(bills)) {
			this._notificationsService.info(
				"",
				"A consulta que você selecionou não retorna nenhuma conta. " +
					"Selecione uma consulta que contenha pelo menos um conta."
			);
			return false;
		}
		return true;
	}

	async buildFinancialReportHeader(title: string) {
		const imageForHeader = await this.getImageForHeader();

		return {
			columns: [
				{
					width: REPORT_IMAGE_HEADER_SIZE,
					image: imageForHeader,
					fit: [REPORT_IMAGE_HEADER_SIZE, REPORT_IMAGE_HEADER_SIZE],
				},
				{
					width: "*",
					text: title,
					style: "title",
				},
				{
					width: 120,
					text: [{ text: "Data Emissão: ", bold: true }, moment().format("L")],
					style: "subTitle",
				},
			],
		};
	}

	async getImageForHeader() {
		let base64Url: any = MOBI_DENTAL_PATH;
		try {
			const blob = await this._printConfigurationsService.getLogoBlob();
			base64Url = await convertBlobToBase64(blob);
		} catch (e) {}

		return base64Url;
	}

	static buildPeriodo(dates: [Date, Date], label = "Período: ") {
		return {
			...this.buildGenericDates(dates, label),
			margin: [0, 20, 0, 0],
		};
	}

	static buildGenericDates(dates: [Date, Date], label: string) {
		return {
			text: [
				{ text: label, bold: true },
				moment(dates[0]).format("L"),
				{ text: " até ", bold: true },
				moment(dates[1]).format("L"),
			],
			fontSize: 9,
		};
	}

	static buildBirthDates(dates: [BirthDate, BirthDate]) {
		if (!dates[0] || !dates[1]) {
			return this.buildGeneric("Data de Nascimento: ", null);
		}
		return {
			text: [
				{ text: "Data de Nascimento: ", bold: true },
				padNumber(dates[0].dia) + "/" + padNumber(dates[0].mes),
				{ text: " até ", bold: true },
				padNumber(dates[1].dia) + "/" + padNumber(dates[1].mes),
			],
			fontSize: 9,
		};
	}

	static buildProfessional(professionalName: string, label = "Dentista: ") {
		return this.buildGeneric(label, professionalName);
	}

	static buildPatient(patientName: string) {
		return this.buildGeneric("Paciente: ", patientName);
	}

	static buildSituation(situationName: string[]) {
		return this.buildGeneric("Situação: ", situationName);
	}

	static buildStatus(statusName: string[]) {
		return this.buildGeneric("Status: ", statusName);
	}

	static buildCovenant(covenantName: string) {
		return this.buildGeneric("Convênio: ", covenantName);
	}

	static buildGeneric(name: string, value: any) {
		return {
			text: [{ text: name, bold: true }, isEmpty(value) ? "Todos" : value.toString()],
			fontSize: 9,
		};
	}

	static buildContasTipoLancamento(tipoLancamentos: TipoLancamento[]) {
		const list = (!isEmpty(tipoLancamentos) ? tipoLancamentos : Object.values(TipoLancamento)).map(
			(type) => descricoesLancamentos[type]
		);
		return this.buildMultipleItems(list, "Contas: ");
	}

	static buildContasTipoRecebimento(tipoRecebimentos: TipoRecebimento[]) {
		const list = (!isEmpty(tipoRecebimentos)
			? tipoRecebimentos
			: Object.values(TipoRecebimento)
		).map((type) => descricoesRecebimentos[type]);
		return this.buildMultipleItems(list, "Contas: ");
	}

	static buildFormaPagamento(formaPagamentos: FormaPagamento[]) {
		const list = (!isEmpty(formaPagamentos) ? formaPagamentos : Object.values(FormaPagamento)).map(
			(forma) => descricoesPagamentos[forma]
		);
		return this.buildMultipleItems(list, "Forma Pagamento: ");
	}

	private static buildMultipleItems(list: any[], label: string) {
		return {
			text: [{ text: label, bold: true }, this.stringWithBar(list)],
			fontSize: 9,
		};
	}

	private static stringWithBar(list: any[]): string {
		return list.map((item, index) => (index + 1 === list.length ? item : item + " | ")).join("");
	}
}
