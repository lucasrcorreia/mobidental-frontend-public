import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfessionalsComponent } from './professionals.component';
import { FormProfessionalComponent } from "./form-professional/form-professional.component";

const routes: Routes = [
	{
		path: '',
		component: ProfessionalsComponent
	},
	{
		path: ':id',
		component: FormProfessionalComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ProfessionalsRoutingModule {}
