export enum TipoParcela {
  PARCELA_TRATAMENTO = 'PARCELA_TRATAMENTO',
  PARCELA_AVULSAS = 'PARCELA_AVULSAS',
  PARCELA_DE_PLANOS = 'PARCELA_DE_PLANOS',
  PARCELA_DE_MANUTENCOES = 'PARCELA_DE_MANUTENCOES',
}

export interface TipoParcelaInfo {
  descricao: string;
}

const tipoParcelaInfos: { [k in TipoParcela]: TipoParcelaInfo } = {
  [TipoParcela.PARCELA_TRATAMENTO]: { descricao: 'Parcela de tratamento' },
  [TipoParcela.PARCELA_AVULSAS]: { descricao: 'Parcela Avulsas' },
  [TipoParcela.PARCELA_DE_PLANOS]: { descricao: 'Parcela de Planos' },
  [TipoParcela.PARCELA_DE_MANUTENCOES]: { descricao: 'Parcela de Manutenções' },
};

export const getTipoParcelaInfo = (tp: TipoParcela): TipoParcelaInfo => tipoParcelaInfos[tp];
