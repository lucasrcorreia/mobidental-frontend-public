import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../../services/utils/utils.service';
import { CONSTANTS } from '../../../core/constants/constants';
import { Router } from "@angular/router";

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss']
})
export class ForgotComponent implements OnInit {

  email = '';
  readonly loginUrl = '/auth/login/basic';

  constructor(
    public service: UtilsService,
    private readonly _router: Router,
  ) { }

  ngOnInit() {
  }

  recovery() {
    this.service.loading(true);
    this.service
      .httpGET(CONSTANTS.ENDPOINTS.forgot + this.email)
      .subscribe(
        data => {
          this.service.notification.success(
            this.service.translate.instant('FORGOT.SUCCESS.TITLE'),
            this.service.translate.instant('FORGOT.SUCCESS.SUBTITLE')
          );
          this.service.loading(false);
          this.goToLoginPage();
        },
        err => {

          this.service.loading(false);
          this.service.notification.error(
            this.service.translate.instant('COMMON.ERROR.SEARCH'),
            err.error.error
          );
        }
      );
  }

  goToLoginPage(): void {
      this._router.navigate([this.loginUrl]);
  }

}
