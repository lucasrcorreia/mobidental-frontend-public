import { SharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinancialRoutingModule } from './financial-routing.module';
import { FinancialComponent } from './financial.component';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NgxTranslateModule } from '../../../core/translate/translate.module';
import { TreeviewModule } from 'ngx-treeview';
import { ModalDetailsComponent } from './modal-details/modal-details.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [FinancialComponent, ModalDetailsComponent],
  imports: [
    CommonModule,
    SimpleNotificationsModule,
    SharedModule,
    NgxTranslateModule,
    TreeviewModule.forRoot(),
    FormsModule,
    FinancialRoutingModule
  ]
})
export class FinancialModule {}
