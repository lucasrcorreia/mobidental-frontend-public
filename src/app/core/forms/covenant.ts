import { FormsModel } from '../models/forms/forms.model';
import { Validators } from '@angular/forms';

export const FormCovenant: FormsModel[] = [
  {
    field: 'nome',
    validators: [Validators.required]
  }
];
