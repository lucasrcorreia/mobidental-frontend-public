import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BirthdayReportRoutingModule } from './birthday-report-routing.module';
import { BirthdayReportComponent } from './birthday-report.component';
import { NgxTranslateModule } from '../../../core/translate/translate.module';
import { ReportsModule } from '../reports.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
	declarations: [
		BirthdayReportComponent,
	],
	imports: [
		CommonModule,
		BirthdayReportRoutingModule,
		NgxTranslateModule,
		ReportsModule,
		NgxChartsModule,
	],
})
export class BirthdayReportModule {}
