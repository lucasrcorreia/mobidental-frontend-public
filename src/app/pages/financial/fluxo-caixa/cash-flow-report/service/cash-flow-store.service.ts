import { EventEmitter, Injectable } from '@angular/core';
import { DailyCashFlow } from "../models/daily-cash-flow";
import { BehaviorSubject } from "rxjs";
import { ReportPrintType } from "../../../../reports/report-header-actions/report-header-actions.component";
import { MonthlyCashFlow } from '../models/monthly-cash-flow';

@Injectable({
  providedIn: 'root'
})
export class CashFlowStoreService {

  private _dailyCashFlow = new BehaviorSubject<DailyCashFlow>({});
  $dailyCashFlowObservable = this._dailyCashFlow.asObservable();

  private _monthlyCashFlow = new BehaviorSubject<MonthlyCashFlow>({});
  $monthlyCashFlowObservable = this._monthlyCashFlow.asObservable();

  private _dailyCashFlowPrintEmitter: EventEmitter<ReportPrintType> = new EventEmitter<ReportPrintType>();
  $dailyCashFlowPrintEmitter = this._dailyCashFlowPrintEmitter.asObservable();

  private _monthlyCashFlowPrintEmitter: EventEmitter<ReportPrintType> = new EventEmitter<ReportPrintType>();
  $monthlyCashFlowPrintEmitter = this._monthlyCashFlowPrintEmitter.asObservable();

  private _loadingEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();
  $loadingEmitter = this._loadingEmitter.asObservable();

  constructor() {
  }

  fireLoadingEvent(payload: boolean) {
    this._loadingEmitter.next(payload);
  }

  fireDailyCashFlowEvent(payload: DailyCashFlow) {
    this._dailyCashFlow.next(payload);
  }

  fireMonthlyCashFlowEvent(payload: MonthlyCashFlow) {
    this._monthlyCashFlow.next(payload);
  }

  fireDailyCashFlowPrintEvent(type: ReportPrintType) {
    this._dailyCashFlowPrintEmitter.emit(type);
  }

  fireMonthlyCashFlowPrintEvent(type: ReportPrintType) {
    this._monthlyCashFlowPrintEmitter.emit(type);
  }

}
