export interface PaymentClientModel {
  agencia: string;
  banco: string;
  chequeConta: string;
  cpfCnpj: string;
  dataBomPara: string;
  dataRecebimento: any;
  formaDeRecebimento: string;
  id: number;
  numCheque: string;
  observacao: string;
  saldoAPagar: number;
  titular: string;
  convenioNome: string;
  descricao: string;
  pacientePessoaNome: string;
  valorRecebido: number;
}
