export interface DestinoDropdown {
  id: number;
  descricao: string;
  contaId: number;
}
