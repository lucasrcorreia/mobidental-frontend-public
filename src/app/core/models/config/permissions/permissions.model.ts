export class PermissionsModel {
  moduloTelaId: number = null;
  moduloTelaDescricao: string = null;
  moduloTelaAcao: Array<ModuloTelaAcaoModel> = [];
}

export class ModuloTelaAcaoModel {
  moduloTelaAcaoId: number = null;
  moduloTelaAcaoAcao: string = null;
  possuiPermissao = false;
}

