export enum PrescriptionType {
  RECEITUARIO_SIMPLES = 'RECEITUARIO_SIMPLES',
  RECEITUARIO_ESPECIAL = 'RECEITUARIO_ESPECIAL',
}

export const PRESCRIPTION_TYPE_TRANSLATE: { [key in PrescriptionType]: string } = {
  RECEITUARIO_SIMPLES: 'Receituário Simples',
  RECEITUARIO_ESPECIAL: 'Receituário Especial',
};
