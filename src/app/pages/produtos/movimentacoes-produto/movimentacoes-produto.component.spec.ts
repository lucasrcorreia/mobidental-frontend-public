import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovimentacoesProdutoComponent } from './movimentacoes-produto.component';

describe('MovimentacoesProdutoComponent', () => {
  let component: MovimentacoesProdutoComponent;
  let fixture: ComponentFixture<MovimentacoesProdutoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovimentacoesProdutoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovimentacoesProdutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
