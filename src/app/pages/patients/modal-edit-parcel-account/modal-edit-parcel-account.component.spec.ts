import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEditParcelAccountComponent } from './modal-edit-parcel-account.component';

describe('ModalEditParcelAccountComponent', () => {
  let component: ModalEditParcelAccountComponent;
  let fixture: ComponentFixture<ModalEditParcelAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEditParcelAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEditParcelAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
