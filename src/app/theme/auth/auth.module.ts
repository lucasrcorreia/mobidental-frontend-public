import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { ContaPickerComponent } from './conta-picker/conta-picker.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [ContaPickerComponent],
})
export class AuthModule {}
