import { TestBed } from '@angular/core/testing';

import { Proteticos.ApiService } from './proteticos.api.service';

describe('Proteticos.ApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Proteticos.ApiService = TestBed.get(Proteticos.ApiService);
    expect(service).toBeTruthy();
  });
});
