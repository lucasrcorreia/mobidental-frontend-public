import { ProstheticServiceObjectModel } from './../../../../core/models/tools/prosthetic/prostheticService.model';
import { UtilsService } from './../../../../services/utils/utils.service';
import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ConfirmServiceProsthetic } from '../../../../core/forms/prosthetic';
import { classToPlain } from 'class-transformer';
import { CONSTANTS } from '../../../../core/constants/constants';
import * as moment from 'moment';
import { dateToNgb } from '../../../financial/bills-to-receive/bills-to-receive-filters/bills-to-receive-filters.component';

const RECEBIDO = 'RECEBIDO';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: [
    './confirm.component.scss',
    './../../../../../assets/icon/icofont/css/icofont.scss',
  ],
})
export class ConfirmComponent implements OnChanges {

  @Input() modalId: string;
  @Input() prosthetic: ProstheticServiceObjectModel;

  @Output() resultModal = new EventEmitter<object>();

  formProsthetic: FormGroup;
  paid = false;

  constructor(
    public service: UtilsService,
  ) {
  }

  ngOnChanges() {
    if (this.prosthetic.dataPrevistaEntrega) {
      this.prosthetic.dataPrevistaEntrega =
        this.service.parseDatePicker(this.prosthetic.dataPrevistaEntrega);
    }

    if (this.prosthetic.dataEntrega) {
      this.prosthetic.dataEntrega =
        this.service.parseDatePicker(this.prosthetic.dataEntrega);
    }

    if (this.prosthetic.dataPagamento) {
      this.paid = true;
      this.prosthetic.dataPagamento =
        this.service.parseDatePicker(this.prosthetic.dataPagamento);
    } else {
      this.paid = false;
    }

    const tooths = this.prosthetic.dentes;
    this.prosthetic.dentes = null;
    this.formProsthetic = this.service.createForm(ConfirmServiceProsthetic, this.prosthetic);
    this.formProsthetic.get('dentes').setValue(tooths);
    this.formProsthetic.patchValue({
      dentes: tooths,
      dataEntrega: dateToNgb(new Date()),
      dataPagamento: dateToNgb(new Date()),
    });
    this.formProsthetic.get('dataPrevistaEntrega').disable();
  }

  closeModal() {
    this.resultModal.emit(null);
    this.service.closeModal(this.modalId, null);
  }

  save() {
    this.service.loading(true);
    this.prosthetic = classToPlain(this.formProsthetic.getRawValue()) as ProstheticServiceObjectModel;

    if (!this.paid) {
      this.prosthetic.dataPagamento = undefined;
    } else if (this.prosthetic.dataPagamento) {
      this.prosthetic.dataPagamento =
        this.service.parseDateFromDatePicker(this.prosthetic.dataPagamento);
    }

    this.prosthetic.dataPrevistaEntrega =
      this.service.parseDateFromDatePicker(this.prosthetic.dataPrevistaEntrega);

    this.prosthetic.dataEntrega =
      this.service.parseDateFromDatePicker(this.prosthetic.dataEntrega);

    this.prosthetic.statusServicoProtetico = RECEBIDO;

    this.service.httpPOST(CONSTANTS.ENDPOINTS.tools.prosthetic.save, this.prosthetic)
      .subscribe(
        data => {
          this.service.loading(false);
          this.resultModal.emit(this.prosthetic);
          this.service.closeModal(this.modalId, null);
        },
        err => {

          this.service.loading(false);
          this.service.notification.error(
            this.service.translate.instant('COMMON.ERROR.SEARCH'),
            err.error.error,
          );
        },
      );
  }


}
