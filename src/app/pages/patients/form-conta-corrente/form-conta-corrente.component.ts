import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { ShortListItemDesc } from "../../../core/models/forms/common/common.model";
import { UtilsService } from "../../../services/utils/utils.service";
import { CONSTANTS } from "../../../core/constants/constants";
import {
	ContentBillTreatmentModel,
	PatientModel,
} from "../../../core/models/patients/patients.model";
import * as moment from "moment";
import { takeWhile } from "rxjs/operators";
import { PaymentClientModel } from "../../../core/models/patients/payment/payment.model";
import {
	ColumnsTableModel,
	D3CollectionBillInterface,
} from "../../../core/models/table/columns.model";
import Swal from "sweetalert2";
import { DatatableRowDetailDirective } from "@swimlane/ngx-datatable";
import { GlobalSpinnerService } from "../../../lib/global-spinner.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { PaymentReceiptComponent } from "../../../shared/payment-receipt/payment-receipt.component";
import { PaymentReceipt } from "../../../shared/payment-receipt/model/paymentReceipt";
import { DetachedParcelComponent } from "./detached-parcel/detached-parcel.component";
import { TipoParcela } from "../../../api/model/tipo-parcela";
import {
	ModalWithOptionsComponent,
	Option,
} from "../../../shared/modal-with-options/modal-with-options.component";
import {
	PdfGeneratorService,
	PDFMake,
} from "../../../services/pdf-generator/pdf-generator.service";
import {
	BLUE_COLOR,
	CommonsReportBuildService,
	GREEN_COLOR,
	GREY_COLOR,
} from "../../../services/report/commons-report-build.service";
import {
	ParcelDocumentDialogComponent,
	ParcelDocumentModel,
	ParcelDocumentType,
} from "./parcel-document-dialog/parcel-document-dialog.component";
import { PatientReportRowModel } from "./models/patient-report-row-model";
import { PatientReportModel } from "./models/patient-report-model";
import { DetachedParcelDialogService } from "./detached-parcel/detached-parcel-dialog.service";

const promissory_left_image_base_64 =
	"iVBORw0KGgoAAAANSUhEUgAAAEwAAAEwCAYAAAAdAJclAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAE+rSURBVHhe7X0HYBRl8/5z/ZJceiM9hBJ6713EQpEiUgVBBbE3ROyKftbPrjQFRUWlqAgiKqCA0jsCIUBCeu/Xcv3+M5vbGCLCBvPzO8P/0WVvN3t7u7Pzzjwz77zvytwENAIcDgeUSqVnq+lC7ln/Lfz8888ICQmBxWLx7Gm6aBSBlZeXw2AwoLq62rOn6aJRBCbC6XR6PjVdNIrA5PKa07hcLmHdlCHZ6Ofn5wsaxEJhA8+L3W4X9m3evBmPPvooCgoK0KxZM883mihYYJfCmjVrWKiXXKqqqjzfaLqQpGFGoxE7duwQPnPzU6lUUCgUAo3gz7xmL5mUlCQc05QhuUlejGdxs8zJyUFiYqJnT9OFJIGxdt1www1YvXq1oFFsx0QDz1/fv38/nnvuOUET/fz8hP1NFiywS2HVqlW1dupiS0lJiecbTReSaAV7voiICJw6dQrp6ek4d+4cMjIykJmZKSxff/01/P394ePj4/lG00WjxZJsx9gRNHVIJq4HDhzA1VdfjbFjxwrbxcXFuO+++5CcnIwnn3zyimD5AoSGeQlQrOimJulu166de+PGjcK+mTNnCnZr4sSJwnru3LnC/qYOSQJjIbFQ0tLShO3U1FRh+6WXXhK2X3zxRWGbmL+w3ZQhqUmyYWe0aNFCWH/xxRfCmrRMWHfr1k1Yi8c1ZUgSWPv27YU1aRZKS0vx1ltvCbwsKipK2M/2jREdHS2smzQ8mnZRVFdXu3v27Okm6uCm8EdofgcPHnRTsO2+/fbbhe0JEyZ4jm7akCQwBgvnscceE4z9L7/8IuzjNTsDFpperxf2NXU0Cg/bvXs3Tpw4gdmzZ0Mmk3n2NlEIYrsMVFRUuBctWiRQDT4Na9qV4CUbLDAKtN133HGHICRehgwZIuTLzGaz54imDUkCs1qt7uXLl7uJPghCYuM/cuRI4fPx48c9R10ZkCSwjz/+uFajrrnmGoH579q1S9g+fPiw56grA5IExvbqhRdecMfGxtZq2JgxY4TPe/fu9Rx1ZaBBNowCbPe2bdvcU6dOrdW4Tp06ud944w13VlaW56imjcumFWVlZVi/fj0+//xzEB8T9nFHrlarFT43VTRYYJyaFvshRXAX3JEjR0COwLOnCYMFJgUpKSlC8+OvMLO32WxC1mL+/Pm14dKVQC0kC4wpBRv7hx56SBAOe0te8zJjxgz3t99+6zmyaUOSwM6cOSMIZunSpcL29OnThe3+/ftfEZ23dSEpvcMdHozBgwcL60GDBglr0jYEBAQIn68USBIYaZGwFj1gq1athHV8fLywvpIgSWBiBwd34tZdi4K8kiCJVmzduhVk5IW+SbVaDfKQQq+R2BfJ/CsuLg7Hjh1r8mWbkgRGdAHvv/++QFY538U8jAXD/ZD8mfdxZ++sWbM832i6uGymz+D6MNa2Jl9PUQeSbJgIzqpyKMSlAmvXrkVoaCh0Oh3uvvtuQXiNAa6V/eyzz7Bq1SrPHi8Da5gU1E3xiAuT1wcffFD4/PTTT3uOvDxw1kPsUBEXk8nk+av3QLLAOBXNKR0Oh8TkoZgLmzZtmpCibigqKyvdH3zwQW3IJS68/c0333iO8i5IEhjHkXwjYpkAh0G8ffbsWWFb1D7Om0kBNTs3NeNaAdVdOATzZkiyYZyNYHTs2FFYiz1D9H1hHRQUJKzZm0rB9u3bsWjRIs9WTc85PQzhs7dXAEkSmJjOOXr0qFBtyGsG14txF9vvv/8ubIuE9lIYOnQo5s2bJ/A4BjVtjBo1SvhstVprH4RXQtCzS4CzrHzopZaGpne4R517nK677rrzztO6dWvBtnkjJPEwpgxc5yoye/Er3DTF5slsv1evXsLnywEXsnz66adYuHChEEUwOPTytuD+bxHXv4PTp08LZer1mzFXa3MJKD8k8r6evd6D/4nAfv31VyFVxBVA/fv3F9LefBliZTZ5XYEce2UfAQvsn4bUkSVFRUWeb3gP/icatmvXLgwYMACTJ08W1oy69nDZsmWC5ySmD19fX2Gft6BBAmP7wjfL9ocD7rZt29ZWHzYUzO04w1G/B4rBfI4HSXA6yevAApMCDonE2oq6C4dMe/bs8RwlHeQJ3ffee6+b7JjQRBkcObz66qvu4cOHC/0G3HHsbZAkMNKs2gpEcv1CaMNh0Ntvv10rOK61aAi4A6Wu4Flwdbf5t/61AuNKQ74JcveePX+A+JPQLzlw4EDPnktD7IXigP35558/T1AcY65fv15yXPpPQ5LAmHXzzXDZ04Xw/vvvC3+Xmo755JNPhOM5NcTnZG3i7c8++8xzhPdCUiwpBtdcQX0hiBlXqcG32Kki9hGIvVB9+vQR1t4MSQITPeETTzwhrOuDNEYIm0TBXgr0oIQ1h0AcuLP3Zezduxf79u0T1mKGxOtQo2iXxp133ik0G7ZVnNzjorqTJ0/Wlj4tWLDAc+SlwdWM/J1LLd5YMyuZh7EWvPjii8JA0vqYO3cuXnnlFcldbFlZWcLALs7fMw8Te5/qLp07d8YLL7zg+Yb3oMFMnzMIPPKDmwyP8+7Ro0ejzCTA9o/tmbf3a0qyYSLY5ixZsgTkNYVeHa4Jy83N9fy14eAZVebPn4/IyEjBcXDmgocXciThtWANkwIyxgJv4q8wu6/L+l9++eUGk8zCwsLaujJeRGohLt5abCxJYBTXCTfEN3jixAnP3ppiYa4N4xvctGmTZ680cPjD32PBs/NgsCMRx2FyZOGNkCQwZvh8Ezt27PDsOR+cUmbBXQwcL/JxP//8c23JOj8E1rS6qEtkvTG9I8mG8bA/BlEKYV0fN998s8DFLtb7zfaPQiJhGPSGDRuEfVOmTBHsV13UJbJ6vV5YexMkCYx7cpiVi/mq+iDNEdackvkrcDmBSEnEHBcTVJG0ikhJSRFyYQxvHH8piVYwx3r88cfx5ZdfCsUnvHA6mSkAezbueuPqHtai8PBwz7cuDFFA/fr1E+gJd7lRTImWLVsK37/tttuE9PT48ePx1VdfCcd6FYSGeQmwQedDL7aw3WlINxuPURK9bv2FSwU4C+KNkExc2Z5w0KzRaASt4ubJ2iIuvL+hE33wFIArV67Etm3bBK0KDAwUOkUefvhhry2hajDTb0xwiMRNu3nz5sI2E1Ymwpzn5wfglWCB/S/AwTr/PDdlDrL/+9//1jZJ3sdJRm/E/0RgYjUQLzySpO62OMK3T58+nqO9C40iMI4E6kYAl4JYHsWdIAwWGm/zKBNqorXDDL0xTd2g4Ls+yNMJgxt4/ooOHTpILtvkoJvB5VPM8binm8ETh7Az4e47xl9leP+n8AhOMpg6fPHFF+f1+nDHRUOC5S1btgjf43OI48c5tuQ+gbqVQuR9Pd/wHkgWGPdLPvroo7Vxnsihjh496jlCOtjIc+ZWFAwvnA2pu497k7wRkgT23Xff1d4IMXPh5v7umO+ysjKht4nt16lTp4R93Gs0btw4oZfKG7WLIUlgfEM83YIoNL4pkRaw8C4XzOaJsHq2arR469atbiK0nj3ehwbZML45FlTdkIZiP2EuC/ZuDUGT5mF8Q2LFNIO3uaJaHDfJC/MnHqUrBU2eh4kTrnEWlDlU3YmJ2LNxcvC5556TbHf+zTxMsoaxQeanzjfCC4/aYMPf0KbI4Gkb+Bw8wpftldjEjx07JvxdHB5dV6u9BQ2yYQy+qXnz5tXSC047v/XWWw2aw/WK4GH1wZrBE+KKguO1VO/W5HlYfXBTefbZZ2ubEvcmNbTypknzMAb34CxZsuS8kIi9JDehhvZJsiZyfUVubq5nz/ngerS1a9d6trwLkhKI4hBmBnlK3HrrrUJBb3BwsLCvoeBxRVxyznX63EPESUTO5vKawWORGJzlpaYufPYaCGK7BHj+w4ULFzaa15Jadl6/z9IbICm9w+kYfo0F2azaCmeuvOGFtYA1kOd45VSNFHTp0kXomlu8eLEw0IsH1/PCtWK8cJccazKP+PU6eAR3UUjViNLSUs83mi4k2bB169bhxhtvxIcffih0TogduuKateK1114TNFGKXWPNpNixtuOXL4FtGK/JOwr7OLnImuZ1YIFdCpyCvtiQYo4hG5Lm2blz55+080KLN2YtJGnYt99+C+JHjToc77HHHsOrr74qfGZ7tmDBAqGugqsP+ZLYg3IVoteBBXYpiDassW3UypUra7WJ821/xcu8CQ3qBOHeaS7VJBILih2FNb/8ifexXWoouOqH6yvY+zL34mptqaXr/ys0SGA84jYmJqb2HSG85gob3sdNVTTYDQEXBHNTFCF13Pj/CpJsGM+CMnHiRFDTFEqVKHgWWDl7SbY5vGbOxHURDQHPsiKOuuXv8u+IU857KxokMCnlTFLAmsjvcuN5+RmxsbHCWO+6o2/Z6HMJlNeBBXYpiL1GFzL6eXl5QlEwxZaSMwwcWPP5LrVITXn/k5CkYRzycNGcWLLJzZFLlJYuXSpoH4NpB3+WMlEHTxHIAxsYXNbEZJibNkMMwDt16oRHHnlE+OxVEMQmEdQkhfQy57/4q7zwSwn+DXSgsSBJYEQbzush4smKeDYn/izm4S8HX331lTDJB2drueODX5PB6WtvhiSBcSZUFBZ3UHAYxMOWeZvfDXI54Ff8iOesv/BAME5jeyMkCYzsilCjX3d+LzE9Lb4fpCHg0ijxPCw4jhm5WXPaW9xPwbznaO9Cg2wYg+JJIeded+gMN9ELDW/+K4gjfHlEbn3UrePwxrx+gwVWF1wLwW/N4q42vkGefEgKxH5JHmRfH9w/IGqvN0453yCBcaqabVb9J8831pCxRl9++aUgEPa29bkdUQ7hb7zU7WH3FkgWmDgrHS/s0bgXibv86zZNqXX6bLPEcgD+/ocffijUmfH5uByB93NZlTdCksDYY3Ez4ZtZt26dQAPEDlxOy/CNMvVoCJiOiEKrv/BveesbICQJjGkE34iYdeVSAd5mr/l3wMUmixcvFiajZE1jysLekR2Lt0KSwMShM+np6cK2aIMaOkayPjgOrW8T2XEwx/s7hPj/EpLyYZyaZojjsTm7wOAx35cLjkM5j8ZjxjluJC0Gaaww/KZv375CepqzGt4GSQITBcXZ1ezsbBQWFgrbJ0+eFIa78HhKzrpKBRl9YdJIBiciechf9+7d8dFHHwn7WIAvv/yydw6Y92jaRSG1l0dqOCMW6DHh5TJQ8fu87e2BvKT0Dh/C049WVlbWZlj56fNnTueIGVfxxZ2XwooVK4T6DO4peuaZZ4QXE/NoXW6WXbt29RzlpWCBScHFyifZaDeEVlDTEzSK5wpjiNxLLHvyZkiyYVxtwz3aPIkjIy0tTXhVtQjO9XNniJj8uxTod4X18uXLcdNNN9WOKWetGzFihLDMnz9f2Od1qJHbxSHSCFHLxKJekdkvW7ZM2JaaUub5wfj4iy1MjL0xxSNJw8QaCjpeWIuaJKaVGwquDSP+JUym+9tvvwnzKhL3EgbNcyqc/8Za7I1eUtId18+31xeYuC0K9lLg4/gdIlwmNWPGDGFaGu5e6927t1DmxBSD6YY3okEC4wobRn1BifulzlTOtnD48OHC7E3cm/7ee+8J1UHc3+n1qGmZF8eGDRsEu8JzhfFQGbFen/P8HE+K21Ih8jAOstkziqM/vD2fz5CkYTxonbvRzp49K7By1iQSkuDduDaM57G48847PUdfGocOHRLWXP/Vpk2b2t5vni3K6+ER3D+KN998U9AozrwyeGAEb3OHsLdDEtMXwbZq3759Qj0qVw9ycQoHzw2dY4LnqqDmLNSFcaUhaxbbMt5m1s+2kWtmuZTgcj3x/xkEsUkAD0QQp7Cqu7Ad+vXXXz1HSYM4OPVSizcOzpKkYfzE2d1zrMez0/EsclyWxNyJ5825nDiQbSG/iYt/nmkGL6xNvPBnTv2IaSSvAgvsUhBnCr7Q8BiDwSCwch4PfiVAkoEQvdqECROEdV3wm7OYfHIGQuRlTRmSBMblmMy8/2o+HGboXDt2OWWb/zZIEhjbKxZIbm4u0tPTBf7FNoizrezdxBR2YzF1PidzPq+Ep2leFDwlPB96qeXv9Pbw4FK2kXVnL5Daz/lPQnJBHRfQMQ/jZll//jDWLM64MvtvKCg0El5pwaNM6jZp9r5iSadXQRCbBDRmxpWcg1D7X39ULi9cPsAlCd4KSQITK2rEJsLDAJ944gnhM4Pn4uG/cyGJFPz000/nCYk7cblDlz9zutqbIcnoi4OoxOF9O3fuxEsvvVSbshYHI4hpnkuBO0tISJ6tmnez8WAthtRz/K8gSWBsrxgkYGEt8q3LjfOYxTO348zqvffeK4y6Zc/IYOFxFoPjVW+EpDuunzCsLzBxWxTspcAayVlWrsPn5CF3DHNALlZp88BTHoQqarY3oUECE5tLfUGJ+6VmXHl+Vp4lmHuNGDy6hMcd8SvIOC7lvkpevHKmzRpTdnE0dsZ10aJFwvE8YdG/DZJ4GFEKoVCEmT5rES+sVaJm8ZpzY9yUpIDn5L/rrruE4TGcDxNDLvFSWHM581G379NrwAKTgsYsn+T5L/inL7X8a/sluTuMh/dR+CJs8zhJNtKXC9E5cNaVKQpHEb/88oswZpL7KtmWcZXQv7Z6h+fY4UP5BQEMsef7csdkc00rf58Mu2fPvweSNEz0hqIXrO81GwoumOMasKuuusqz598DSQKrz6/+rsCY6TMx5RT19ddfLzR37gnnMZnc/L0aHk27KDhQ5kPFAFyMHflN8JeLJj3WSBwQWrfcvP7CJeRS81f/5rFGkngYe0UeYsxNk9k3F+7yZ/4qN0u2cfxeNn7bghRw7uuOO+64YM5LnPmJwbk2qdHDPwYW2D+Nf/NYI0lGv7Ehvhzl3XffFQx/XTD/4v4DxuXOT/Z/Co/g/lE0+bFG/xf4t441alAxyoXAAxo+/fRTgVdxuNQQI81l7JwX+/HHH5GTkyN0pPDUgXPmzGm0SZEaHYLYGgg2zEQwhUHtfApeeHZM7gxp6miQwBprGobDhw8LzU7kdeLC+8SFc2/eCEkC4y60xpyGQcpQHBaaN2qsJIH9X0zDwMLgun5eOEIQp4vnhWteeTy5N0KSwIjJN+o0DHXBU4/yZB/ieXnGcy4b8FY02Og3xjQMIupSC34A7Ei8HQ0WWF1ws7mcaRgYYseKKHB2KJyh4HPwJJXe6nEbJDDWiLqvnuDPbN+4/rUhOX8pRp898b82p89Yv369MKyY4z8RTDg5L5+YmCgQT6ngXiKiEZ6t88H7qXkKL4MSE5VeBY/gLgqejIMP5fiOm0tdcGEKa8PlFJGwBv3byK4kgYnVO3/1/jWxU+RKmFpZks6TIIT1X8V34ryI/7/G1QPu4WGQJgnr+hCrrL3xJcCNDo+mXRRsZ7hakOO9+u/6Ft8Bzi80vxIgmVZkZ2fXJveYmfM7iUjzhG3eL3byNnU0KB9GXk2Y0YRfRsCTfPBAKu5L5FIlb5/ht7EgWWDULP+y1oF7jpiHMR9r6pAkMC4Q4a6v1atXC5rE3Wpi+QB/nQe281viuWLQW1833WhggV0KYjHKpZaGvD3r3wpJtEKc3ZwHIfDQGS7g5dl+MzMzhYU8pRDSNPTF6P9G/O1OEAYX9XJzfeCBBzx7mi4aJbplG8fd/twL1NTRKAKrX1XdlNEoAhPrxy63XuzfhEbVsCtBYJKMPg/25MQhk1cWCjN+/swTfPDnrKwsYcgLG//IyEjPt5omJAmMZ1+6//77hc9inT4TWHHhl6Aw9Vi0aJF3Vj43IhqFVlxJuGyB8dA/DsSZSnBen2cyuSLAApMCLhfgF6VwPyJ36IqpHnHZu3ev58imDckC4+IQTiBOmzatVkj8qjDOk3EnCJcQXAmQJDDuKWIB8SxMDHEkB/cYMcQScqvVKmw3ZUjiYTxyljFs2DBhzYE4g6kFQ3xhE496a+qQJDCRkIq9RqKgxOyEWHV4JYRGkrwkjzLjcUFDhw4VhMSpHZ4ZZciQIQIPy8vLE7ZZw7hevylDksB4ilCmDlwGzsONRU3ikEgMi3hCbn5xp1d27zci/j9xbSAkCYzjRe6s5VdUM2Flm8ZNk0eg8aSPTT0cOg8ssEtBnFr5QgtzMy63pGbqObpp46IaxtkIDqzZK3I+n7WKbRjnv3gKLDb+JEx89tlnePbZZ4WeoyYPQWx/AS7OnThx4iUL27henzXtSsBFXRrn6desWSMU010MPEMdV+7wi9GbOi5p9HmOVS4J4KZ4IfBMAxwB8PQwPHymqUOSl2RCyq/fZ6Hxe2zZpvFrqzkTyy8G5uF6PDB05MiRnm80YbDALgWeVJsPrb+w3WIbxwMdrhRIJq7sMbl2gjkZe0mejvSvmmlThuQ4hkMenjNn/PjxmDt37hXRaXtBCHomAZxA5MP5PWq85hmdrkRIEhinp1lIYgJRfGmKmEC8kiCpSXKfI4Mn7Wb07NlTWNcf4H4lQJLAROMuJhLF+b7ESSSvJCgo/rtkAMh1+jw5B4/J5tLMo0ePCnWunB/jeJJfr8MJRJ63sKlDEq3guad57M/FwHl+Fl5TpxqSeRiDuRg3Q2b6TDPEMoErKR8myYYxYeVpqlg4XJrJTTEwMFDQJhYWG//vvvvOc3QTB2vYpSBOh2UwGIRtHu/IPd+kacK2+Da/S6WBmgIkaZjoHcXuNQ66Dxw4IDRRRt0S9KYOSQITKwxFgYgC8rqpqv4BSBKY2HUmapoosPqlmk29i40h6Q7r92yLXlFsoqLArgSNk0QrmKRed911tTUV3NXGKWneZs/JHSK8fSXYMEkCY03i1A6/tpqbHS+sbXUXHoTKo9qaOhpEXP8/JNowBje7Rx55BKNHj66dVpmzr99//70w3xcXq4g0o0mDNUwKOHfPh4svz+Te7rrTwPCcOWT8PUc3XUgSGL8FRhQSQ3z3EAtq+/btV4SgREhqklwmwBDnVx0zZoywfuWVVzB48OArgn+JkHSnYoeHOE1ohw4dhPWlUj5NEZIEJr6Gh9M5DLFU80rMuEqiFewJR40ahalTpwr9kVyiyfu4ZJNrxJjpM4l98803a8OlpgpJAmPCeuuttwrlAQwOgcRF1DqeFYWnJ23q4dH/J64NxJXj3hoJTVZg3Gw+PnhOWNdsNQ6apMBYPOx6zEQze364FzZhq3HQpG0YZ+vCVqTTvzIcmBiLVr5qYf/fgXdp2F88u8t9opzmfKalE3qZCm3WFGDN6cKaP/wN/OMC+23jYySXmhcM/AESidCO/mg6JTk/IvvkIuEz7+U/SxXcrEXfotOrG/Dk+gPo1TEJPjKKmWVyTN5rx33rj132A2D8I02Sf6KG0JpxcvN4OOShMNl90W/4BzUH1AEfe/S3BYjSZcCtCEVk+ychV4Z6/iod+0rMuGpjASzyPzfD6y3p+H7OkMuybP+Ihonsv+T4bVAEXIMw7Wm0i89AjycnwESWxgInGWbuLzAi/eRXiFT+AKshFaqQLji14xbhuw1F73BfFN2ShChXTVhXg5rb/VHbAr2X7BAeTkPRaAJzOavhsGbQp3oX4dk8s+9ZpGcp0Kb3KIQGO2F3hKDMokfIvP6Ifmc0IhaORsIro6AOVMOujEZYsBFB/kB4TH+c3vcyrn1qBp2K/6sLN6pKeP5FNu81HTR14a+QIfPWZLR3cFkWf/OPYw76JOG6T/Z5tqSj0QRWXnQU7tQByDj+JkrztsJmyaa91Lz2voKykjSEaE+g25B7YCrfiXnfFuHp9aex57nFcGiUcBQYoDxSgooAf/T7dCG25MRCrrAgKzsLAbGd4TAdRJamFB1emwiL246q0lNI3f8CijI24FTKfIx58R76rQvfCgdux27vggHKP9eybZXH4NmfU4XPUnWtkWyYGyl7HkWs+iuYAudDY1gKo7M9wts8Ckv2eFhtDpysegBvHj6K/aUZMMplcEZroXVR3JlWBXebYDjcDsizjHBXWKDoEIFAmw3jfKLxbdlpHJz9IuYsnobvLXGCY+hqV2HLrQ4UVnXG7d+swunmQ1B0z7cksr+2SnyT4z4/hA2Omp4vESzmX0eEoV+4tKm8GklgLhQd6A913GPwLX4IRZb+iGp/N9L3PYXWLfKxfWwmYleT3fgyBNV2uimtHPIQLRTFFsh8VXD5q8mLuWDPrIRKp4FcrYSlVI+o4GDY6OosOhm6GH1wQF8CZ3EV5L3jEJdpxZHJxQhcoURIWARCXL5IUIXgx8eWea7pz+AbHfnZQfzo4tlbeKtGwGqnDVW3tUBNmeDF0ShNMi/9awQmvwp95lJY7D7QxY5G5sGnERVKzexEOVqVxyNr3DmkTjchIFgLtb8PXJUWuOmp2gMUsOgNcJHmyOMCIQ/1gyOnHMrmoSg26bGkbwxkpHn7ysnjJfjCRTbOWaJHTrwG35Z2hjM5HPZQLYqU1djlm0vu46+fP3c3b5zeA8kOzrrUCIsYB2wKNdp8kvKHheNT/MVpJArszwa1LkIjO6Hs5Fy4XEq4Y1+BuuQ5+PgFIMtxK45/U42TWiMi7WE4NvwkTowvgCOrGE7SMLvcCUduBVQkQAs1SbucjLrLDVV8mND0YHLg8z1uxAU44UMC1LjksPsp4KiyQEXHzVmbCnl6BVEUE6zFlbDSuYa/NMtzVXVAN/9DVhUiPzqLgcv2kk3rhkiP93R7WnG2LAC3rvudPtG98j7P/vqQKDA+jIXGXoqfE4ufPROvq1Fx6hbY1T3hEzsdB46uxfO/KtHrh3K8sPkNKA8r8EtCMFrt9Ue0OwqHrklBxjwXgox045llCNQGQZaph/w4sfD0Uljzq6DWUBMlLbImB2NDWSqC3SbYS0wIcmmgtMuhSgqHtswJcyDZQIsVrkIjfLQ+UBda8VtwPqxEUoRrJQfB64+PZWPUtipUKLQ4Shzwpi8O4yx5T0UtP+TDXFhboUMlPRTPDs/6fEi2YfvX90arxGCkkgdTaTRIijyHgKQXYMm8Bwb5VDz90y6skZfDUFKFoKAIVOkcWN+hGMnTKnDnxOYYMKgAz/euwJn+QcjEGbTd3gHxC3VQxgbCQe1CRQ/FfqYAvslxkNmdUCoUqMrOQ3BcLIw/n4b9mkQo6WacJ3Lg7ky0w+kD68kCGDsGw+dAMT0wF5mCSBiqzdg7sgOCUUlia4NXDwfiU914uv0awbQq/hLt/Q5j8nUfwJd2jf6tCjqXCVuvmY/9Bw1YXPYOTtzWQVDwC6mZZBsW2/kd+KmKER9ZBJXjFCy+48kDzsaC453QbeuPWF5wFio9nc7igkXpgJ9NQZ7HDI1Mid9j1Fh6Wg6XogStd+kRjxZIu+4sMmabEZ9SjjCzCjqHAiGxMQgudgpZ26r8IsRHJUKVbYJvkA/a7c+BXzX5iw5x1DTJ7ikcqO4YAtmxAtJIDfzaxMFldZETkRFViSCvq4TZegQrCxOgya2hDrHVv2PrnQcwpk0R5vycgSEJ/uhpy8OuSV9h366T+PZkBJJcC/FdNjVXsa3Wg2SBZR+diwLXLQjxzSfDHQ5b0YfwSXgP8c5fUZRbBIVNBlkIueZWYXDaXXAey0AA/aZJpYSJopNKnxi45L4U7hSizU4LoqzRyB57Dj8+ZCQ35YYxqwx2hRslTgOcTgdiAqMQXOTGFzcMwLz+SdBEK2HMpRiUbJc7pQSOIo4RSG/ig6BQqlCpscNsMQHBOjy4aTNkjjxEEncLsxURrbHAz16NdwduRqXBhI17Lfh10gp0WfE7ts/ug6s/H4QyoxyLbzfhgS67ccdWMg8Xlpe0JskH/LTuFcT6/QSfyGugc/+M/RVX41DmRiwq0cGglsFWqUd4ULhQ2WNNLYR/Nz/s8jkLw7sR6HNPPDU7YGWPEkxNOlJzUlsszgxUodivFAlrE9BuSxSCTlixaGo3NPOVw08bgDgSWp/Fi5EZGAa1nwYygwMapRpmhRP+pJEVFZVkwqoRFB8JvcoOBd2lUqaAM6sULc2F2DR7MiyGIsz+Pgq73d3h3yIJLyW8Chl5xVLFAOxOyUCnlrMQonQjInQF4pR5KMpLgSJhBCqr78EdHWumva8LSRrGwvaTHYB/cAzuXPsR4ra6MW7XJrxcqIaFfIAzizydQoXy7AJo/f3gExYCv9wc5H1vR5bSJQiLz/HI5jpsW52LVjtciDCFIntCJk5dXY69s7qib4gBzbWV2HSiGC0+XYtUOXnT3ErI6SQKnRoG8rCGslJUFJZC1yyQtD0IDqcdfhXE4woq4cyogFuuwllnOOZtPQyluxzLxwVh8Q0dYUzPwpLTfREeqYO+bC/mdf8Sa84cQbtIH8zbPw3NFEcxoJ0TTpsJz++r6fCpD8lNstx9NcZ9tR+H5Alwkvt2cTGd0wWrUQ93cggUVcTUyZa4duXAGaXF2C46+Kf54nBYTa8Sa2lVUBLdDFEPj7rLtFlouY3iRWMosiakQuezhVSePJ8rGsPiorC2d1/sHz4WLYMcMJkMsLqcCIoKRcsqOywtdXDK5KgyVAFFZoTtL4K2GVEPoigKhxsKfxXWO2V4aHMpTK5AWNPXYeed/ZGuvh6rTrZHD9cPKHD3whjtQtz0Qx4W9GuOubvH4OaVfeCylGFU1AbsJGJdH5dskvzHzcd+wS1b3oTGXI6CiADSGBcCbBQk55ZDnhBC9qRKMMTqQF9UF1XA1iIQP3fLRswEE+4am4DNbf8IO/aPNKKH327PFl0ACc9paIH0oQ4U+5ah6xp/pBfdjvAwHTSqKiIzFpiMRphtZooAwiCzVSEssiU0UZ3w+je/oXVAEXqG2BEQMwSjXn0QEe2D0FXlwKCuUegZEYhAvR4WzRkM+6Qtdr54CL+XGDFgYx56Wb7EzDa7sf63IvwedB/yiwIQ2CIWm0e9hsHfL0C5PATtXWU4fuv5o1v+WsPcVdSe05Gzbyqu6xSMs8PKsHbS7QikOC7QQFGbkpg50YwwA3k3I4U30X6w5RugCPKFosSMrmEuKEibjsefH6ONX54qPARSL9jsvtBTyFPlyEbEWi01a7KVN5cgPmI7fc6E3aZESTkd54wBKKTSyjSoNlXgwK6FyNg+CbPabMVkoi4J2mNQH3sBH4e4cP+6Egx5y4jYyUaYBpUibUwFyhWJOEwcLvH+AYh0lqCHvAzJUVa0SIxF35YmjG+VAkdUaxScyUfnH5YQj5OjY/kK+Ni20ONi/KFT52kYf6jI+ZGEUY3itI8R7OdAeOcFqDp+O8r9F6DTh68jjDxkcX4BgltEwVRYCWO8FrpK4kDVZISjyOBSWKOOUSK/B9mwmzXo/nACTJ4mqOFKReMhrL5Oj6Ck7sgwhOBIhhtZpVzVCLRu5kaHRBtaaCqRdjQfpQVR6NLOB6FEFWz6UlgoUD+5vQilBxQIKtchxtUMOuKmVoULJeQoinxkyNK4cCqYvGZkNe75zQ31DWbMCPND+rlgODuF4NNWCXj0p6Mo6/I51g1ZjLbBGXj76zx8bnkZlXQ/1S17YXXfpUR9DqDKbwB+OjkBb1zTpuYGCH9qkrlnVyLE8DjZowgoW74PfeojWHl6EJ4oO47q7FKyIZEwKmykAcSFiEk7zxRTbKiDotwKdWIAzL5KqE+fxa9BBsjWhqL3fTUZhs6Vu7H1nuZYvCMESwueRZaqHXxdVeTxWANJYKWZcIXF02eiJ8Shmht+wqsDv8TYBCN2DNqNlsaWMCkVKCfhFZMjSfF14fdYHdI6RCAlTE6aUFOgLCMD2ctwAJuvDULpjTJEbNOi2ddRcBhtsIf5wueMAX6B5UgKvAdXtzmNcZ0yMXn9DKT79oc74zD8QyPQwucYuuU/iqfn3oSpH12F3fdPEc7N+FOTjG01BenmOSixXw97ygS6/BDcPnYOBhWVYs2ESUgdFoC0YVMw26rAc6pEBLckl+9Dxj5ah5G6OPgQG2/jNsKww4wCurFQWxlyp5ZiYEwXRHz1PZ4vXoZsdQuE20uQcXMSRgRaMSdJBtPc/hgbRDFiWQ6eSCxFtv9QTDz2IULXfYjSj65F9a5o9HswHAPvjsWNs5vjqZubY83gcBwOddODI1dNgvJxOrG6Zx723KbH7xPJ3nYqxYSvq+FTJYc2LLDGXlrNxAVjsGRkEl4umo9hW/6DdurtSJv4AiITY2AoyEaoKxPDesZTS3NAL8/0NMsaXMCGKRAV3Qwhqo04WTERVmUnGI/fi/ExM1C8eS2KXGG4eunb+NxtwazhXWHPqIIsSAvfAA0eH9IPNtK6CVcFIzg3EGl+WTg+W43uq17B264vKGC2omJmS8wPK4MPEckgjRInThzH9ydykVlhxp7sMrQk27lgcHvcEVYJbUkW4szE3VI+xKRvRiHjlnJEmwvoxs9rFMJNRFjKkTc1DTc1PwlLeSSiKdjv/k4s9tLDMXGwzx62inhah2jIdBpqykfR0a3HV9d9DUOxFWk51fA1EA1xmLFDOwezC9egxernkOJ/Mxbuy6y1YvUEVrO7pOg0zpVfhVadBsMHW7Ht5CC4l32KNw6W4qfDNpQnRONGsw4phzdB7ucHdUE1NnZNxqYt25CcU4zpzcnTxxXg2m/6IWHVCtgMKoRU5JJWVQg3O7BXEoq0oWixbDecoSrYrKkY/MlXkGn0ZDf8cPWHv+KDsiA8lKzBkXsHQWcljfGfhFarPsTxWSrEGrOE66ToGnK3HJ0rDiN35ikKzjnLK8f+G6uQH12EuVuryaMTraHQykZ2zlilh1nmQAXFo3f+cA7Lx7TELRv6o51sE1GJKiwbexDu8CRoqwoEU0GBFp1Phk+PFQqfGH9BKxwoyV4NOQWiNmcifFouILLoQOG5Fbjh+O+4T2dFv+43Q5/7He7PsGL70GQ6oRpyVwjc5D1l1kWwxSWi0/fLYaVwaPtQLTrHRSDs4ywSXBoilIfxyeiNiAx3YfcJILUwEEq5G53iqtAjWYbjGX6YvXUiqhXtMaWllmxeEPrZzuGcgzwqcb20ibei9cdaGJR+WNIpG9PanSMx1aSg7KZI5A32R9QmJeKW+aE6UAe3lWxtbACRWAreiRI508qRqAlHyosbEfJxCpY2G4rDlcNx3/V56PTtUlSdOQxZm17C+ThoD3DZUHVrkrD9Z4HxFomz9MS1WPtDMAYMnY1Q7Xp8vecwXsoHysL9cGbwIPy471dM6mZGtvMmhMhO0oNVQ+bTDqaS/YjqfAwJqz8iYQXBvzIXSqcVCU49cv3c2DPlLXyxLwQv5T1GcWYz0oDz85xqtxlB1Wfxbre3ERMG3LjtWXRQqbDllt4Y+8mv2ChrjhjzYRyY/h+KF/WIcJ3xfJMhw/brNfC1+GHb80F4MS8ZahMJSO6CiZyFPMcIWYASqlwLjv/nGyQGR6P34t8wKH4LPs3sgWrf5nC5ZGT8U2Bu3Y+Ew+ek2FUuQ8nUWIQR7fmzDSNh5Wf/jJyilrjrtnfRNrwtbv3yKDTaZJhCA9GqUg0txWKr06qx62QvBMvTKWxJo9NSQF6xAirdRvT/9E6Y5cHImZ6AN/sEozisFZT+R7Fj6nvo8s3LmF+yGEZ5JOmxmp66k37Sk2ujK7TJfIjAdsKUU8sx/YebkTJpHgpNx7Bgfxa2uWMplCpCiU8XPLgyBmf21hUWtQt7CBLLYtBuYQB66EZBnlECa5UJSrUKumIbfGP94VdlhY/GDW01f9eO9yZ0x7ult6KnzwGk3HgvhslXwmJ3wq8230cthgL+r4/n8k/UCKxGxWr+Pbl9FlTGXeg0cA4WrVyCVevn4N1rQrGuoBjRGU4sHPwIqp3J+Om5TejbegqqHS2RX2pGhPtL7L/7CM7JOyLFbwTULjte3XQUy3adQTf9Crwxch/ar/8ApRoycPRbfR25eDO6ANnTEqBxixndmmvgtZtcWqb/1Wj91SL8MPkLfHFkFzo46Pi7e6C7NRNfad9Du4E9yUfpPN8Bdk0xoFJpQYmagvG4OyALvRvuGH+Ys0rgDPfFYEUxMu4rQMo8M3Tu+2DM74XWmIpwSwre6v0xVNVGPDiSGmF4AsWCBZ7Lqbmm/MpqYS0IjA0a9yue2X0nErpMhdOnLQqOv4HpQ79Fx9bl8G19BwxpDtyS2AmxBSVYvfoLrPlsGYwBB5EUvBSmF46h4EYXrlsSh5t3PoEQaxUW9wvCWxVhSA/wx7qZv2DwLwthkesENeff86VjHri2F8KUEJqssL82B+VZ074KOXm7b1/H/mnE7ik6OFhmQLNm4dTMgKveHYg9B0zCoQ53IBKy4hD6jBVFjm6YtMcXhuirEZw3AHKiN7IsPXaeM+N4fhjif1yLiC1r0XnNnThEjsqqIQEFN4fBTiTZchLaoEjYzQYo6aI0cKI7yvHUkFbC7wgCM5TsoAP2oWXPOTh9ZDNkVeuIXafiTI4ayX0fxdhPP8TPD7+DET064pThHdz70ETcOGQhyv/7FLKvtSC6MBp43oDCiDCU+SZjgDMfk9tGINqYj603vIqeaxaQsLT0SzWaJM9Pw4u3DCOBuKC32LDqhlhPQF7zNGtAO2r+R5kiElcvGYDHkv6LQd+VYX9aEcKslTgR/RDa92pPt6TG3tssRGkc0LT1w02Hx5BxV0BF1GfftP04Orkvhke1gywyDi1ilRRlaWGR+WFY+HEMHr8ZseZCKFRByHV2xOjVQ6EhT1r8xLWomhEP84xEHJjRWejfZAgCK8r4Aci6B84TvdA2cDmC1ClIOatAavFUnC6IxONtp5CnseF0air69y6Dc+OzyBlhRehWCpMmFaDV/iJYkktx/5YbSCRybNGr0PadHXDbTmDHKTcKVS1qMxQMZ3RL9Fxfjnf2ZSPpiyyM+kGcqE24HA9qhCeK8EjYfbi+QwliyV6mzL0K6bM7kv1z4KPfgqE3qRB9MhqGWypQZkwgasIRgxzR1VlkXkoQoLbiofYhKHjwI5zJUAh2k/9+V7dsuE4OQTvjUpS626FDdDE235kKH/MZBNIRHIPUXrbnQjxe0o2UHbOE59+2Yz+4dR1hIcbtVA+Eb6UBRtsT2LxjEyb064rDU35HM0MkjvVMx8glwcSa+d2TMuSUhKLzzh/oWetwk18+VhuisHbgo5j062Nkt/gG/gwlaZhDVldIIvgy3fBxULhF+hNReAKmkDgy+CvRJqIaUckvIy8nB2tMYWhj2IKVVfMQ/GEwwjbJUYHpaLd3ksCj/J0mbOr5A+Zt9UOGf3fs6vwSTp/OwaTqpTCrQmC4ZjQyM/vi+3QDbLGx8HcdQstoOcWaA3H0oaeJwv8ZnquVoc2A19Ci2+0wk0s+9/t6pB5Zi0Pb52LdDz0Q5JuHyA+MKBhVjNxAE2L3WjDiQ6dHWHRrpP5hMQGCjbpJV4J3JrNaG9AxvgqV6hjhmBrwY/pDQDXC4v5qcuUCuxGfZ83jtJI3zrgtGalP3IhkwzmcCn0Eb92Yi6WnzNh3Ng8BZHfP+Q1D+2ktkNa/ABm5/lBYiJDG7sNbyVYQG8DAw5OwN+R6FKkiMGfvNQgN0SARWQiUmeB2aJDQcRpuu3k5NqXH4pujnfHjTj3i6R6NwhV41KoOaq9erghF/tnNsJd+jHCfM/T096Jv1zOIJracPaSYBKlF5FYnxmx0wK1lRl0H5NGKqohNy3yxuliD+IX74XRUYftx4kCyunNasEBEjyjChftjjJgfmI8bNUWefTVQ0AWzS2c8Mfk6aGx6WB0GcioFSHt8JH6b0ZqcPwXewRYMWpCEgvJ+KDR2wVWJZsxMPI7N/SoQXZpKSzqiy9JgLg9B0AMV+OTVu7HnhXEoGaLHT/1mQaYg+2opwrxWb2JU2zRk6uOQI4zZqG2QtTivPbTo9hyyynsS4SyGD3mfnGsK4T6lRshKN67fZYEyPM1z5Plw2F3IrggU7NeMdn7wp7hOTje1Oz2Mbrmu5vwZGqIf49uE4+WxvdErUjStNVDZTWi7cDfWnszDhE05MGgDkWfwp6igHN3e3ox+r2+Hi0ipS6nBieIZuHbSu+g28AGEt7oDAZGj0aN1b+Q8fBUtQ5Dz0BDsnD+aYlgHiiLKkLTViPCV/nSdckEIv+um4N3se7FibxLCbLuQka8XtJ5f1Jebm4vs7BolOW9KkyMHv0b7xGX4ZmoxBuW2wm55Bb6bp4MptRLuVKLdfwGFlShHXw1U+hI8f3VXTGoeginfb0CFxYdYMjBMXYrfbMHwN5XArvSBXlv3XbsyTF99CAceuAYLj5ZBGcz9lDXP0awmDkXLjIN22LVBgujNRIdsCgWOB7enFsOkl5u2EkERV+FkSorn0Vz4IYWQtla2A675XEMxaD6OfFtMwqqZ2j6qfA2aW5bCRQ/foutTO46dJ2bibr+YmBrTUkdgbnTtMR4n95zDwOUH4Z/+OxLmuvDoW8F4vo8vvu0dAhvdh2BvhAv6A34OE+7R2OAICMfMdYexk/XZ7Y9IPwNkFqB7hC++H5ZAzTweuk/Ob85WuRJZYW3R/P19ZNj5vSJ/vlErB9AeIfhTeKV2KXC98zR2E3vXx7an89pQkPEp+g97Wzj+fPD3ar4ro5jSkeKm8MmOnouj0So6CHvpnoQ4w6mBpdkN5MJtCDAVw89HLYyxYsHxEG2eVygpKaluk5Th1J4nERvwK/yqC1AeHI+E9W2gnlaKBbuK8NuiPFx32lzTve45nsEncCkViA8x02cX9he5ESCzA9ooDG1TAa3LgVl94okT1Vx0Ytn54Yx4HlMwP0FRWPWFVvOA+Jfj/PSwO/2xYfZQfHd7XzqvDEqbA31bbEXanidwdNtLyDzxAQy5n2PDiVMIX5ZKy2mELTuF/u//glI/DbT6aKRNDUfJk9Wo9q85e5J/OZID8hEuy0TzZhYkR/oJ78rs1q2boF0sLEatwGyWDERFt0K+vg0yCgKh1oZAZo+HrW9bRP8UDFtSGpZ8W4hvlxWgV76TvlhzE/x0OIUSEETq6zbioS4hSLl3IAXRPujaSoYE/Rms2VNj+06UVGJkqxBi0CwQUSjEwmtOVQckmtp9pNGeQ3UkfBsikK+JQfNFBzFiUzE9QDtU5Gi2zMlAReGHaBaRjuv39UD4L0Mx7lAgLAoZytUBqKCld9RZuN4JwMP33YM+j32N+C1ajNzwFIU9FHI5EhEUHAWLOww/GO5BhND2/nRhfwjs2M734KsyonWPm9Ch/70orIyCoaIILaLogvRGKG+NQdw2X6htKfj60xwsXZGDdlU133UQrTieXgmNoxzPpgATPt2FIm00tp+OgUXrxFP5gfj8jB4DN5bjjQruxucL4YbNP/+HQOrij31/CK9X1dOYvSoeWyZH4o0x3SjA16Jl1RqsX56JDmdj0KUjOR5FLLIVARRWOzFQnYl90Xdhlvs7tLRl4emuW2Gke8lXdqW/asmehkNO8emila/jjVGF2HouCnkVFuiM+4j/Mf58YYLAzu6eie5h78JV+CQcp6ZBf/px+CgL0XzAM6gIfAWHc3qgY8c+OHIiELEfdEPUJn90LMvC5kXpeHZ1LmKq5Xh5hwzDNOtgUCiRapJhbIAeD++ZguWDXmeRYOZeA/3tfC9Yk6Wo+SQXAvAaydR8Ph9y+tOKWzJxwj0evbRKDI+Rkxbb8WTf7/FwRCg5EhV2v1KCM0c/JoZeM1g/XFEFs12PN/rvwzsx6zBwBfnxRA61oumBuHGwOBHysv/gYIUTkeExyK8KQb6dhOk3SPg9z3M6D4LAmnd7Cc6251CoeRtZld2h0JLBDjqNnINf4ueC4xh/zoRpS1Pw9N4M3PG9L7KddyFyZQR83tBhUn45dr6TjlbfufDOiN+oiThwFdGDlTd2QKiyGdQaf7Qy7yA+dqGfF+FGpCEfizvJ8Ul3JUKM9QcgyDG8+mn858dElGsT0GbxYcSTTbLJ1LiW1LxEG4vFLRVI+DkM3TrK0MtaU+y71dwcgVH+GLi6EGOyTiAnPwvtmingULL+uPH2XgrtfN5CVewstFWsxLrZO/HciLPUjINRScrFufz6Vy0ITEnNR6mJRoDOHy36PoHsqp5wOEKg0PyKJOcZWLKqsLF5BPar22BvtRFnLeytesHWcQxiNvih+iYTHkpzw7GvBDGmQ1hREYJnN59Aijoak7fOw87pixBMzfViUDrtuKNLDKa3awaZ6vy+zFjzQSyflo315vlkrOMwMEKOKlUAhhQ/gtu+KIJdIcdHI6JhVWuQudGM1zv/gAAXz9RiQY+ffXHKaIAiy4DglkqczXOSLaxCoKuc+H5bZKZSKyLWf/+SAuzLicWkQ2+glHxW5ItbEEge3XdFJjp/dJiaeA1qbRj5GoQk3ITc078hptUgIOJePP6dP4b+dAjuWD9o8kyoMFfAbXbige3riXG7oFapUWa+EZqJ1yL+Jw0OLi7CT2PehE2pxLu5Wqzp6cI1skr0/2QOjt94H4LcF56VU0XkdU5bHxipJZYQp3uyg5auh+2HDNHmIzgy/RV0W/UUfJ1WhGkUkNH5OYD+6u5UbHHU9Bma6U4+iadm+qEvwn0z8evVVjhyn4STmq1PiB/kYX7omaAgm1yFk9dPQ86107Bv3CJE9+iPaop/3WH9oXJk4KvRq1FdkA0leUgH/b6V7PPviggs2Fbj3S+Y0zdR3FaV+xm0gT3RYekrqCqqhqx9OKqzS+DXLAwOkwGnxrWDVtMcLsMBskQ+kFOsVlhwCr5B+3DPkXuwU3M7Mqcl4pOfduGp0ngk6bdg9+3L0euDGcgOvl6oi/gDbPjd6GZMR75DhaLAeBKIHQMsy/Hl1F/R64un8N/h/XHbxnTIicJYqJkvCL0T3546gP0B3fnrQtsJtBMzfzsL/q+YMCujFXY4aLfBAUeQEioTPQ2tDKpgP5jJxn3a615M6j4Kc7/ah48L9RQeUSCmCoJFSQ6DzqttPxAmrvPwNMpFnWS4s2vsHxpWF37+SfBz7UVV8X78OONBIFQFe1oJFEotrPkVkAUT+/4uDxbDITxNrrtS3ZVoSQ6aJY1BUNBDWDdxNYLtmYhfmYn/nFOjU2UquinC0eHz17D0mq+xvc90tDT8Aj+iIWzzuPeHTdwJ3ziYAwLRo/orHLt+Jsa1PoPWXy2GvzwY4xN06EQezUjhUZvKNZjZ7iAqXDpBVgLoBFUqGb6JUaDgKQdmJOeToLTwi9Lg/qBYqAOVcKlIKPn08MuteHDNu/QlBdYYI/DrjZ/gXtddODX6VkSY06BUk4Y7KvFei6fQ3bqBT47xnWOFn7mgwIiVwWjvhGj1OqiLd+PwzNvhR95PVVgJXXw47FllOBwM9N5lxfrqIgz/7kdy0wqoFVa4iDE7Skw4NXk+fN3kpQKi8MhVbbH4ntEoVcdj/o7HMOXnh/Bol804d+OtOHvDdOzoPQk7+0xC+tiZODlmNkYmpOCq717AmrMPYkFbNbLU4Uh+axtSEUL27Cj2zlyEyBD6jTvy8JD/MSiJiYt4Y3ws/B0BGBHpwN0VOdgxdCquS8iCgZi7y0TRiM0GNzVRrdYHBjq+mq7xBPmYzyz3odjZHmWqNrA4iCA7DiNRfgDXWP6DOBsRWo+kLtgkD+2Yj2bRbaF05EJj/wHVvneg5Oxi7CcT9PBRFYUXRdCNagPDiQIoE0OhNFixckA0+nW6FvqSNKitX+D4jFz02tsdLda8DRvRDJlGizhrIX6//xos+z0fcw9ZEGrOI7brJB7Hz1AOh1wNH0M1ro3SYkV1NO4LKMRL43oj/KOzREm0aG3ajkNTn4OWbKGcblREBtmxrmvCoFf6Chr3HhnrruZC7HkkCN2j22HCj3qY43TQFFhgraqCmh66SqHGrORnsersL7gx+FtsLh+PBwcexO3rJsA/Jg4jtO/DZazCLvkUhGkTcWxOb+G3LqhhcmsafIxL4BvcEVlZLgTpH6LgdxC6hpKtStRB2TIE5pwqyAI1FDLKYa3U49WdmdBXlsBms6KMyF+4Ixz7xh9G5oSpSFZsh8knEOkBLZFvqkahVYHkytPIuGcgVGUm+j0NTs8aBKXJDR3Fcksm9MHclk4sKdIh7p1dMJKwRlQ9jgMTn4GPu+Q8YTGaK1NRMOMsWuhreqhfGh+HCGM47unjg0mHzTDJOJpwwdFMC41OB9OpAsgq9Hi/LBhbZ5+CtewUZndPw/P7b4Cfykk8LBQvjs6BonwPhiZUYnqXP15jewGBudC271xk5sqhyrsFJlcM2ai5OFG2B4EDXkT06QI4WgTB7SuHM55syEHSwsQIVPr6wmkqwsvUPJ/5LQT65hXQnVOR/ynGhmEvY2vf2QhyVaDF2hIsOGVDWmASUirJ81LIEqxWCm672mpBdmAiRn62D6+cpeDXJwD+mmKcHT0D/4n4nsyCAi5O/Amh1fnwtRWR0M9htpyiDJ0Me4LlODgrGzf6ZQpOwelD31W5cOxWC1TdI5EY1o0ikwJct2oyNvguRix50TLfvjA5FJjXegOCNZXwsZdjf5YD9/YmtuuBR2A1rZL/PbnnacjSriVO5gtt5w3o0DwLGv8WWLDPgLbPPISsNtHwJXWuphBDo6fmFOELl05FBFWHCO1OJCb2wRa9CcHDdNTu/eF2WBEVUYkumm3IGDMe+4ffhQ55RD1UOnT7thwVoYk4oW6G6KWpqAhvSc0qELtc4ehfNB/pZIQP934OJQOOo9m8SOR3VSKzRwJS+ofjt34y/DpZjtJvWkFmJU9JgofCQdppwpEx2XhtXBSicqPw+mi6Dw3ZuJPlSDx3CjFJr+A2sw9OB05Fp6qPcXP4CuyY9ilOpB+HMWUnAqLjcDzPAZOiBYV1wbBrelMQ9Qf+ZMOqTSlCt31lKhHYfBdadBoLZcX7UCQtRcunZ6AyLhY+56rhiveH3EcFM6s3Nc2BagfG9bsWof5huHn9MuhvNiN3nBsJBwrJHXhGXdDz0VNs8vhPVjw/sjlP/Io8awD0ZpVQcxGss6KZ1gRNVRGO7CuHxdYdgf56tE1SIlAjg91QBluGESnbC1GwT46gMh1inM3ga1cQeXXCoDbCpCV7pTPBv18r2L6yQNa3Es90DsRJTWukzrgPeb8vR5UuBmOOT8eziqEYPaInMgyRuOmXx1GenQNX8244M+Y/ZPj1eOhjAzp2ewOvD0sWrp9xQaNfo2tu6KsKkbrrKbToMAga0zL4NH8ZA56ehiMRyXCW6eEuNSMovhla7MvGmxkyTBkbjDwyrv42BTJGZcJwjQ0Rv1igCcivOS3hkxPhuDWlK7USGTQuYt3WSqjc1RiYbsGTW0woiClFu9e7w9e3FwnMDaOqNeZt2ISzbgOccl+UFaQhLg6IMunRo4MfxjaPQhcStNvohCmvAvn7y5G71wpZjhLR9mhyEEo02+KC0f9NZB1cg9b93sTgZb/glquqULHrXkycMgsGqxn93x9DV0QxZctmeFR3Nwb074zpX1+Fow/cfnEN+zP4zy6cOfIefB0boQ0bi7LyE9hy7hyqfYbA+f03GPmbgxgx6OSReNggJ/Lnwg+d8hB/ixHBr/sg9OqagQWM5kuSkRWaUHNaJl9uGfpnkpeleC+T3H/r/wbCqnoEdhJkUKAW7+xPwcq8AhSVWKDQaWCjZucgwfgkREBpk6E6WoMBe0twS3oZjl7th+15ZUhoB/QKVaNHu0AMCI+A1nUcpoA18AsZjFaPLYc+uSc2Xv0y3l9bjGfvSEb/VbdBnkNkvWUffNHvE8TLjyLV2AqvH7wJR2/rUXPhHvwFD6sLNrAK5Be6YHO1RVn2T1DajRiT3Atz4n9EV7JtB4mVr+/oQv/hjyLI4hK0b+VOeoKBZpxbTdTBY6MtijBkh5ABFR4R7SRh9c6x4fPVBchsngP5S7HQ+85CgDILfn4y8rYVsJLnVNhNcKidsCodUAX5QdchngRaDUszcioUE+UmRuFd2h/yuRH55In3Wrrj9XMtcdvmCOQVtURa+SKoQgYh4qkvUd6iD3687i1EqKvQuWsMDuUGwnD2DBbddg38HGVIUh7DthNmfHNSi/dH1Uz7VRcSBFYDue0Y5HITFGTwVX7tEOxej0z3OIwZHYRHb03A223C0G7jqzCS1ijotNurNahua4X1GDP5GokdKg4gWXmkR5+6F9ix6os8ZCRlQ/ZqLK7dGIkuG3ajy0c7cZo85tyvD+LNokxklFjJy5HNovi9+jRFGEXl9NCUcORUwF5igC1AhdRWvohr4YtoemDbh5qQDF/sGkb2jZxBjv4YIp5eB71LhQ7OrchhbVUa8Uz287hn7yxUvTYVt28vR7/SZyieDcPIDgbsNk3GgIi6jbEGkgWm0F2L6qozkAf0RRjWoKC6F+JkG/HD6MlCN3oweb3AbAfsVWYKdoOETov40SqEych7CTEZMOcL4k/cDGnpXOTA15/k41zzc4h/rSUyy26AS0mH6q0oJIoyessx9Os6BHfrtAhq3YwemAtVyTr4RoZByT09zfzgE01RR6wv7HozQsMjEfD4Y9hxVxBCAgdj29gUBMXegENFboz4uRssFLvqWrfH2slrUeJKxrTlHTDT5w38fGMC3tt2guzdYfSU/4SgABuu/uVNvND7wjMZSBaY0rKaGPAkxMhXopDat9qVgoBWCxBo+Ax+FNiW+VihT1DBGq6GXc11XiGI6RsKlYtoh1xBNs4fZyLI/ZPJ7FQqx3cf5+FMwlkoXmlFIVYLzD1O5wsOgm9oIGQx/lD4KPFK1UHk7CrCByuqMOWsEQnFLihDdFC0CgVxXbjKjZBXu+AqrUZRUQE+/foB+MTPg9z0I8rcIyEveRFTdvYRAn3/0DCsH/wCTp00Ymj0QXww24JDxuloQzHmstPHsera5ciuqsmE6ODA7AsMm2FIFljLHm8iwrkcpdVxMFSVQdNsNqzn7kJSl7dh17mgc2nge7gU9mLSoqwKVIb4oLxaDqVTDld1CPLc0UII1F4vw3cfZuJEVBq6Ptcar99fDWNqHszlBhi0Dmg0agSVumHIK0L778/h9kwNcs0luOpgFmRaC0yFFVBlGBBu9YE1UgONlvRbzpkIJSo1Q+HIuh9+zZ+H25KCs4bhcLA5oOjDGhABu8MCS3UGVLowjPviBuy9vRcGf7AHC3p/h8MbtuL28f3w4kYfvDmso+eu/wzJAjtz4GGYiPNkFMfAoWwHpZEC84QVkGU/hl1jpxJJzIerI6mxjwbK2BBY5XZsy6SbIldtPhGCGYuz0M6owqaFGTgSfAaDXo3H7jsVCLW7YQ+UQ0a8zpVNRp74VElVKfwSmuFU2xCEKH1gIiL8e3Q88oPpmGojbHF+qFTZoCPhB1WSPFoECEkBBXlDTes1sGS9DHX4zaQpBjyebISiTQ9Y5D64ftc72KMfhOHLBmLjzBHYmF6FY+pQ3HX0Ybzifg/Bbbtgv3U2bkj099z1nyGBVojgjAAbbD6c11yhx9bLhpJjV6Pa1R/wb4mXf1yDz/MsUDYLQMeyA3jhByO0bQMwp31nfL0kEylhZxH8YRvc92UMntyYhwMBVrxzNWmfkfhaBQVIPaOoOctRmUsBPoVcM5em43qnCfdM60C8DCgJd8N5rhRyfx+EBwSjIqcEKh5Un+CHVV0exE29r/Nco5xav5MIsQKHy23ovz6XHmJN/+YjzYFnBsUj7t31+HRcGnqHbEW3FTNR5NcHRTMSECL6pQugAQL7AzXfIH/niensllyUHR+BMmtPDDicCaPJAQXZFmVVOn50kWfdGoYwmxJZcWkIeK0F7vu1GzVRE6IyykmLAiAvrxaKie1B5IFdCigqrRSW0CMpNWIUhUp+FFRvbUYBsN6BivwqaMLIqZDWOUm4ylxamy3QJkeidM4GT28Pgbvc69x4QbUN/ZbswsND2+LOzs0Q9/FpTNaPhNbHH3dPjMX3xwOwr2weVoy9+JtzLktg9VFWfAw6XwOKTzyDPXkuPJiuRhXxKHlWPnJHVEI/0wcZyRmIeioEZu0tGLw/FZacYmiCA2Amlg2jnYytCho72bwIP4rfaE30wOGvwNFuNnowLRDfZhhyM1PRYc8eVKcVQuZyISQxnPRbBYfKjZnK3lg461nPFdWD2CgI3E56LvsVvysT8E3PeVi56hgmTBqCxw7eh7MzO9SV8QUh2YZdDCERnVB68nEEtZqP0a0ysfuajiiY/jw+79WMmpUOWZ3z0fr1XnDZwvHg0hVQGiqgSQiFI0AhOAUeEUcGCLJYfxj8XdDkkhA1CiTnpECtjEZC+zHIPLkDEz9bDb+sauiSIjDWV4eTI2MoJLIhloT//qynPVdzAdQR1rgVv2H5iHcw3f8TTNm3ALr2N+Ah4mInJQiL0Sgaxtiz6T60CttGJHIaAlzEdYw9EdbqVtgKZqG8pAThrT6EwpqKyoKVCPen4D3xPZyrsqPf2vegNNthDtVCXWaDgrTNHuuHYH0p9ozsjIiYgSgtzKCYMA191lTi8f5T0dt/PWIDsqGNnY+ez92JTyY+iC795nuu5MLgmxy3cj/m9HgfX+7xQXFeOR6e5MSIXW9j58hw9JE4M0qjaBijU//7oVOXwewIg0k7F3HdXoB/cC9kVt6PlgPPoST9E/jFjyQC2Q9uRwnSju0gAUdia+82ODFWjRCHBnYDhTtyBzoW2nF8tB1+wd1BuyAzbYeu1UKcef473HrdFLTrt4Q09H3s2bUX+24LRVQUhzCsPxcASYr/MmjlEWyia3vs+1a4ZWAx4tp1wxM/dMFT8SbJwmI0moYJXtNZTE0r2rN9Po4f+AgOSw66DBgFy9EbcSInGR2HPousU5vQotMMjFj2Fm5OisGAOCW0+u/gdloR0HouCs5+CzP6ouuABz1nqg+e9pnf1lVr7s8DJya7LD+CVCXXqtUgVr8ZAXS9Uf4dsGUmefcGoBEF9tfgH2D7kH1gFmLa3gZb1gMwu5rDopuLmOad6C/iE7Zh7/YVaBP+I8zVPlD4JsJtS0ezLqs8f/8LcJEv0Yf60DtdaPPpGRQo/OhGPTuFRuVCLyKwe+4cJMlu1UWjCUwUysXhwO6N9yNQsZv0Yjj6D3/Zs78G4jmyUj6Hgnvcy3Yjoe/XUGnEAjxuXH9lRc7/2+5iE4Z9X0hNnLnXH1fGxJ9nqNt0xxDa+IMaScU/omECPNKwlO+DOoCCaUXCRSXsdlfg3LGVFHrdd+kHcd7TcuO2hetwgKKAMe1jcd2gLrh+QwGqPRmTu0PL8d6ozpc+51/gnxPYP4U6wnOT1r21Mw3z0qnJ022u6qfBhNZ/9ABdDpqewOqADX74inRqqDLsnxiHlr7cPP8eGo1WeBNEDfjgQBZa2UtQODMJLRpBWIwmKTBukSw0P4UbB2b3EQjH5dqs8wH8P/9eqUNt3FmQAAAAAElFTkSuQmCC";
const REPORT_STYLES = {
	tableHeader: {
		bold: true,
		fontSize: 10,
		alignment: "center",
	},
	title: {
		fontSize: 22,
		bold: true,
		alignment: "center",
		margin: [10, 40, 0, 0],
	},
	subTitle: {
		fontSize: 9,
		alignment: "right",
	},
	resumeHeader: {
		bold: true,
		fontSize: 10,
	},
	resumeRow: {
		fontSize: 9,
	},
};

const extenso = require("numero-por-extenso");
const getExtenso = (value: number): string => {
	return extenso.porExtenso(value, extenso.estilo.monetario);
};

const SOMENTE_ESTE_LANCAMENTO = "SOMENTE_ESTE_LANCAMENTO";
const DELETE_PARCEL_OPTIONS: Option[] = [
	{ label: "Excluir somente este lançamento", value: SOMENTE_ESTE_LANCAMENTO },
	{ label: "Excluir este e os próximos lançamentos", value: "ESTE_E_PROXIMOS_LANCAMENTOS" },
	{ label: "Excluir todos os lançamentos", value: "TODOS" },
];

const lastOfPage = (index: number): boolean => {
	return (index + 1) % 4 === 0;
};

@Component({
	selector: "app-form-conta-corrente",
	templateUrl: "./form-conta-corrente.component.html",
	styleUrls: ["./form-conta-corrente.component.scss"],
})
export class FormContaCorrenteComponent implements OnInit {
	@ViewChild(DatatableRowDetailDirective)
	_datatableRowDetailDirective?: DatatableRowDetailDirective;

	@Input() patientModel: PatientModel;

	readonly parcelTypeCannotDelete = TipoParcela.PARCELA_TRATAMENTO;
	readonly PARCEL_DOCUMENT_TYPE = ParcelDocumentType;

	recordsDropdown: ShortListItemDesc[] = [];
	treatmentId = 0;
	billsTable: ContentBillTreatmentModel[] = [];
	bills: D3CollectionBillInterface[] = [];

	itemTotal = {
		key: "all",
	};

	progressTotal = {
		all: {
			total: 0,
			received: 0,
			percent: 0,
		},
	};

	showModalPayParcel = false;
	showModalEditParcel = false;
	max = true;
	selectedParcel: ContentBillTreatmentModel;
	totalParcels = 1;
	parcelStatus = { text: "", color: "" };
	activeDetail?: unknown;
	payment: PaymentClientModel;
	locale = this.service.translate.instant("COMMON.LOCALE");

	tableColumnsDetails: ColumnsTableModel[] = [
		{
			action: true,
			name: this.service.translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_QUOTA.COL1"),
			prop: " id",
			sortable: false,
			width: 50,
		},
		{
			action: false,
			name: this.service.translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_DETAILS.COL2"),
			prop: "dataRecebimento",
			sortable: true,
			width: 100,
		},
		{
			action: false,
			name: this.service.translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_DETAILS.COL3"),
			prop: "valorRecebido",
			sortable: true,
			currency: true,
			width: 65,
		},
		{
			action: false,
			name: this.service.translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_DETAILS.COL4"),
			prop: "formaDeRecebimentoDescricao",
			sortable: true,
			width: 150,
		},
	];

	tableBills: ColumnsTableModel[] = [
		{
			action: true,
			name: this.service.translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_QUOTA.COL1"),
			prop: " id",
			sortable: true,
			width: 145,
		},
		{
			action: false,
			name: this.service.translate.instant(
				"PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_QUOTA.DESCRIPTION"
			),
			prop: "descricao",
			sortable: true,
			width: 120,
		},
		{
			action: false,
			name: this.service.translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_QUOTA.COL2"),
			prop: "numeroParcela",
			sortable: false,
			width: 70,
		},
		{
			action: false,
			name: this.service.translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_QUOTA.COL3"),
			prop: "dataVencimento",
			sortable: false,
			width: 100,
		},
		{
			action: false,
			name: this.service.translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_QUOTA.COL4"),
			prop: "valor",
			sortable: false,
			currency: true,
			width: 100,
		},
		{
			action: false,
			name: this.service.translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_QUOTA.COL5"),
			prop: "valorRecebido",
			sortable: false,
			currency: true,
			width: 130,
		},
		{
			action: false,
			name: this.service.translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_QUOTA.COL6"),
			prop: "formaDeRecebimentoDescricao",
			sortable: false,
			width: 200,
		},
		{
			action: false,
			name: this.service.translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.TABLE_QUOTA.COL7"),
			prop: "status",
			sortable: false,
			width: 100,
		},
	];

	constructor(
		private readonly _globalSpinnerService: GlobalSpinnerService,
		private readonly _modalService: NgbModal,
		private readonly _detachedParcelDialogService: DetachedParcelDialogService,
		/** @deprecated */ public service: UtilsService,
		private readonly _pdfGeneratorService: PdfGeneratorService,
		private readonly _commonsReportService: CommonsReportBuildService
	) {}

	ngOnInit() {
		this.getRecordsDropdown();
	}

	getRecordsDropdown() {
		this.service.loading(true);
		const ref = CONSTANTS.ENDPOINTS.patients.records.dropdown.replace(
			"#id",
			this.patientModel.id.toString()
		);
		this.service.httpGET(ref).subscribe(
			(data) => {
				const all: ShortListItemDesc = {
					id: 0,
					descricao: this.service.translate.instant("COMMON.ALL"),
				};

				this.recordsDropdown = [all, ...(data.body as ShortListItemDesc[])];
				this.service.loading(false);
				this.getRecordsData();
			},
			(err) => {
				this.service.loading(false);
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			}
		);
	}

	getRecordsData() {
		const loadingToken = this._globalSpinnerService.loadingManager.start();

		const ref =
			CONSTANTS.ENDPOINTS.patients.financial.listQuota.replace(
				"#treatmentId",
				this.treatmentId.toString()
			) + this.patientModel.id;

		this.service.httpGET(ref).subscribe(
			(data) => {
				this.billsTable = (data.body as any).content as ContentBillTreatmentModel[];
				this.processRows();
			},
			(err) => {
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			},
			() => loadingToken.finished()
		);
	}

	calculateProgress() {
		let total = 0;
		let received = 0;

		(this.billsTable || []).forEach((parcel) => {
			total += parcel.valor;
			received += parcel.valorRecebido;
		});

		this.progressTotal.all = {
			total,
			received,
			percent: total === 0 ? 100 : Math.floor((received / total) * 100),
		};
	}

	processRows() {
		this.billsTable.sort(
			(a, b) =>
				moment(a.dataEmissao, "DD/MM/YYYY").toDate().getTime() -
				moment(b.dataEmissao, "DD/MM/YYYY").toDate().getTime()
		);

		this.billsTable.forEach((bill) => {
			(bill.children || []).sort(
				(a, b) =>
					moment(a.dataRecebimento, "DD/MM/YYYY").toDate().getTime() -
					moment(b.dataRecebimento, "DD/MM/YYYY").toDate().getTime()
			);
		});

		this.calculateProgress();
	}

	toggleExpandRow(row) {
		const rowRef = this._datatableRowDetailDirective;

		if (!rowRef) {
			return;
		}

		if (this.activeDetail !== row) {
			const details = this.service.httpGET(
				CONSTANTS.ENDPOINTS.patients.financial.getItemsPaid + row.id
			);
			if (!row.children) {
				this.service.loading(true);
				details.subscribe(
					(res) => {
						this.service.loading(false);
						row.children = res.body;
					},
					(err) => {
						this.service.loading(false);
						this.service.notification.error(
							this.service.translate.instant("COMMON.ERROR.SEARCH"),
							err.error.error
						);
					}
				);
			}
			rowRef.collapseAllRows();
		}

		rowRef.toggleExpandRow(row);

		this.activeDetail = row;
	}

	getStatusDesc(row: ContentBillTreatmentModel) {
		const expiration = moment(row.dataVencimento, "DD/MM/YYYY");
		const now = moment();
		const amount = row.valor;
		const received = row.valorRecebido;

		if (received) {
			if (received < amount) {
				return this.service.translate.instant(
					"PATIENTS.FORM_CHECKING_ACCOUNT.STATUS_LIST.RECEIVED_PARTIAL"
				);
			} else {
				return this.service.translate.instant(
					"PATIENTS.FORM_CHECKING_ACCOUNT.STATUS_LIST.RECEIVED_ALL"
				);
			}
		} else {
			if (now.isBefore(expiration)) {
				return this.service.translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.STATUS_LIST.ON_DATE");
			} else if (now.isSame(expiration, "day")) {
				return this.service.translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.STATUS_LIST.TODAY");
			} else {
				return this.service.translate.instant(
					"PATIENTS.FORM_CHECKING_ACCOUNT.STATUS_LIST.LATE_PAYMENT"
				);
			}
		}
	}

	getStatusColor(row: ContentBillTreatmentModel) {
		const STATUS_LIST_COLOR = CONSTANTS.STATUS_LIST_COLOR;

		const expiration = moment(row.dataVencimento, "DD/MM/YYYY");
		const now = moment();
		const amount = row.valor;
		const received = row.valorRecebido;

		if (received) {
			if (received < amount) {
				return STATUS_LIST_COLOR.RECEIVED_PARTIAL;
			} else {
				return STATUS_LIST_COLOR.RECEIVED_ALL;
			}
		} else {
			if (now.isBefore(expiration)) {
				return STATUS_LIST_COLOR.ON_DATE;
			} else if (now.isSame(expiration, "day")) {
				return STATUS_LIST_COLOR.TODAY;
			} else {
				return STATUS_LIST_COLOR.LATE_PAYMENT;
			}
		}
	}

	payParcel(row: ContentBillTreatmentModel, total: number) {
		this.totalParcels = total;
		this.selectedParcel = { ...row };
		this.parcelStatus = {
			text: this.getStatusDesc(this.selectedParcel),
			color: this.getStatusColor(this.selectedParcel),
		};
		this.service.modalResult.next(false);

		this.service.loading(true);
		this.service
			.httpGET(CONSTANTS.ENDPOINTS.patients.financial.getParcelModal + this.selectedParcel.id)
			.subscribe(
				(data) => {
					this.payment = data.body as PaymentClientModel;
					this.showModalPayParcel = true;

					setTimeout(() => {
						this.service.loading(false);
						document.querySelector("#modal-parcel").classList.add("md-show");
					}, 500);
				},
				(err) => {
					this.service.loading(false);
					this.service.notification.error(
						this.service.translate.instant("COMMON.ERROR.SEARCH"),
						err.error.error
					);
				}
			);
	}

	saveParcel(row: ContentBillTreatmentModel, dataModal: any) {
		if (dataModal) {
			this.getRecordsData();
		}
	}

	editParcel(row: ContentBillTreatmentModel, total: number) {
		this.showModalEditParcel = true;
		this.totalParcels = total;
		this.selectedParcel = row;
		this.parcelStatus = {
			text: this.getStatusDesc(row),
			color: this.getStatusColor(row),
		};
		this.service.modalResult.next(false);
		setTimeout(() => {
			this.service.loading(false);

			this.service.modalResult.pipe(takeWhile((val) => val !== null)).subscribe((dataModal) => {
				if (dataModal) {
					const index = this.billsTable.findIndex((item) => item.id === row.id);
					this.billsTable[index] = dataModal;
					this.processRows();
					this.showModalEditParcel = false;
					this.service.modalResult.next(null);
				}
			});

			document.querySelector("#modal-edit-parcel-account").classList.add("md-show");
		}, 500);
	}

	reversePayment(rowDetail) {
		const translate = this.service.translate;
		const refSWAL = "PATIENTS.FORM_CHECKING_ACCOUNT.SWAL";

		Swal.fire({
			title: translate.instant(`${refSWAL}.TITLE`),
			text: translate.instant(`${refSWAL}.TEXT`),
			type: "question",
			showCancelButton: true,
			confirmButtonText: translate.instant(`${refSWAL}.CONFIRM_BUTTON`),
			cancelButtonText: translate.instant(`${refSWAL}.CANCEL_BUTTON`),
		}).then((result) => {
			if (result.value) {
				this.service.loading(true);
				const ref = CONSTANTS.ENDPOINTS.patients.financial.reverseParcel.replace(
					":id",
					rowDetail.id
				);
				this.service.httpPUT(ref, {}).subscribe(
					(data) => {
						this.service.loading(false);
						this.service.notification.info(
							this.service.translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.REVERSE_TITLE"),
							this.service.translate.instant("PATIENTS.FORM_CHECKING_ACCOUNT.REVERSE_MSG")
						);
						this.getRecordsData();
					},
					(err) => {
						this.service.loading(false);
						this.service.notification.error(
							this.service.translate.instant("COMMON.ERROR.SEARCH"),
							err.error.error
						);
					}
				);
			}
		});
	}

	async emitPaymentReceipt(row) {
		const { id, pessoaNome, pessoaCpfCnpj } = this.patientModel;
		const modalRef = this._modalService.open(PaymentReceiptComponent, { size: "lg" });
		modalRef.componentInstance.paymentReceipt = {
			id: row.reciboId,
			itemRecebidoId: row.id,
			valor: row.valorRecebido,
			data: row.dataRecebimento,
			pacienteId: id,
			nomePaciente: pessoaNome,
			cpfCnpjPaciente: pessoaCpfCnpj,
			referente: "Tratamento odontológico",
			status: "EMITIDO",
		} as PaymentReceipt;

		const result = await modalRef.result;

		if (result) {
			this.getRecordsDropdown();
		}
	}

	paid(row: ContentBillTreatmentModel) {
		return row.valor === row.valorRecebido;
	}

	async detachedParcel() {
		const result = await this._detachedParcelDialogService.open(this.patientModel);

		if (result) {
			this.getRecordsDropdown();
		}
	}

	async generateParcelDocument(parcelDocumentType: ParcelDocumentType) {
		if (!this.haveTreatmentSelected) {
			this.service.notification.info("Selecione um tratamento.");
			return;
		}

		const modalRef = this._modalService.open(ParcelDocumentDialogComponent, {
			size: "lg",
			centered: true,
		});
		modalRef.componentInstance.parcelDocumentType = parcelDocumentType;
		modalRef.componentInstance.patientModel = this.patientModel;

		const parcelDocumentModel = await modalRef.result;

		if (!parcelDocumentModel) {
			return;
		}

		const loading = this._globalSpinnerService.loadingManager.start();
		try {
			let pdfMake;
			let pdfName;
			switch (parcelDocumentType) {
				case ParcelDocumentType.BOOKLET:
					pdfMake = await this._buildBookletDocument(parcelDocumentModel);
					pdfName = this.patientModel.pessoaNome + " - Carnês.pdf";
					break;
				case ParcelDocumentType.PROMISSORY:
					pdfMake = await this._buildPromissoryDocument(parcelDocumentModel);
					pdfName = this.patientModel.pessoaNome + " - Promissórias.pdf";
					break;
				default:
					return;
			}
			const pdfMakeDefinition = await this._pdfGeneratorService.exportPDFMakeAsPDF(
				pdfMake,
				"",
				pdfName
			);
			pdfMakeDefinition.open();
		} catch (e) {
			this.service.notification.error(
				"Ocorreu um erro durante emissão. Por favor, tente novamente."
			);
		} finally {
			loading.finished();
		}
	}

	private async _buildPromissoryDocument(
		parcelDocumentModel: ParcelDocumentModel
	): Promise<PDFMake> {
		const pdfMake: PDFMake = {};
		pdfMake.pageMargins = [20, 20, 20, 20];
		pdfMake.defaultStyle = {
			fontSize: 9,
		};
		const imageForHeader = await this._commonsReportService.getImageForHeader();
		pdfMake.beforeContent = this.billsTable.map((bill, index) => {
			return [
				{
					table: {
						widths: [50, 240, 115, 115],
						body: [
							[
								{
									rowSpan: 3,
									image: "data:image/jpeg;base64," + promissory_left_image_base_64,
									width: 45,
									height: 170,
								},
								{
									width: 35,
									image: imageForHeader,
									fit: [35, 35],
									border: [true, true, false, true],
								},
								{ text: "", border: [false, true, false, true] },
								{
									text: "Nota Promissória",
									alignment: "right",
									bold: true,
									border: [false, true, true, true],
								},
							],
							[
								"",
								{
									text: [
										{
											text: "Número da Parcela\n",
										},
										{
											text: bill.numeroParcela + "/" + bill.numeroTotalParcelas,
											bold: true,
										},
									],
								},
								{
									text: [
										{
											text: "Vencimento\n",
											alignment: "center",
										},
										{
											text: bill.dataVencimento || " ",
											bold: true,
											alignment: "center",
										},
									],
								},
								{
									text: [
										{
											text: "Valor R$\n",
											alignment: "right",
										},
										{
											text: "R$ " + bill.valor.toFixed(2),
											bold: true,
											alignment: "right",
										},
									],
								},
							],
							[
								"",
								{
									colSpan: 3,
									stack: [
										{
											text: [
												"Aos " +
													moment().format("D [de] MMMM [de] YYYY") +
													" pagarei por esta única via de NOTA PROMISSÓRIA a " +
													parcelDocumentModel.clinicName +
													" CPF/CNPJ " +
													parcelDocumentModel.clinicCpfCnpj +
													" ou a sua ",
												"ordem a quantia de " +
													getExtenso(bill.valor) +
													" em moeda corrente deste pais.\n\n",
												{ text: "Nome do Emitente: ", bold: true },
												parcelDocumentModel.name + "\n",
												{ text: "Endereço: ", bold: true },
												parcelDocumentModel.address +
													", " +
													parcelDocumentModel.neighborhood +
													", " +
													parcelDocumentModel.zipCode +
													"\n\n\n\n",
											],
										},
										{
											canvas: [{ type: "line", x1: 0, y1: -1, x2: 180, y2: -1, lineWidth: 0.5 }],
											alignment: "center",
										},
										{ text: "Assinatura", alignment: "center", bold: true },
										{
											text:
												parcelDocumentModel.clinicCity +
												moment().format("[,] dddd[,] D [de] MMMM [de] YYYY"),
											alignment: "center",
											bold: true,
										},
									],
								},
								"",
								"",
							],
						],
					},
					pageBreak: lastOfPage(index) ? "after" : "",
				},
				FormContaCorrenteComponent._strokedLine(index),
			];
		});

		return pdfMake;
	}

	private async _buildBookletDocument(parcelDocumentModel: ParcelDocumentModel): Promise<PDFMake> {
		const pdfMake: PDFMake = {};
		pdfMake.pageMargins = [20, 20, 20, 20];
		pdfMake.styles = {
			tableHeader: {
				bold: true,
				color: "black",
			},
			tableRightValue: {
				bold: true,
				alignment: "right",
			},
			tableRightTitle: {
				color: "#444",
				alignment: "right",
			},
		};
		pdfMake.beforeContent = this.billsTable.map((bill, index) => {
			return [
				{
					fontSize: 9,
					table: {
						widths: [135, 280, 115],
						body: [
							[
								{
									text: "Recibo do Sacado",
									style: "tableHeader",
								},
								{
									text: "Ficha de Compensação Interna",
									style: "tableHeader",
									border: [true, true, false, true],
								},
								{
									border: [false, true, true, true],
									text: [
										{
											text: "Data Emissão: ",
											fontSize: 9,
											color: "#444",
										},
										{
											text: moment().format("L"),
											bold: true,
											fontSize: 9,
										},
									],
									style: "tableHeader",
								},
							],
							[
								{
									text: [
										{
											text: "Vencimento \n",
											color: "#444",
										},
										{
											text: bill.dataVencimento || " ",
											style: "tableRightValue",
										},
									],
								},
								{
									text: [
										{
											text: "Local de Pagamento \n",
											color: "#444",
										},
										{
											text: "Na recepção do consultório/clínica",
											bold: true,
										},
									],
								},
								{
									text: [
										{
											text: "Vencimento \n",
											style: "tableRightTitle",
										},
										{
											text: bill.dataVencimento || " ",
											style: "tableRightValue",
										},
									],
								},
							],
							[
								{
									text: [
										{
											text: "Número da Parcela \n",
											color: "#444",
										},
										{
											text: bill.numeroParcela + "/" + bill.numeroTotalParcelas,
											style: "tableRightValue",
										},
									],
								},
								{
									rowSpan: 3,
									text: [
										{
											text: "Cendente \n",
											color: "#444",
										},
										{
											text: parcelDocumentModel.clinicName + "\n",
											bold: true,
										},
										{
											text: parcelDocumentModel.clinicAddress + "\n\n",
										},
										{
											text: "Instruções \n",
											color: "#444",
										},
										{
											text: "Tolerância de 1 dia de atraso, Multa 2% após o vencimento\n",
											bold: true,
										},
									],
								},
								{
									text: [
										{
											text: "Número da Parcela \n",
											style: "tableRightTitle",
										},
										{
											text: bill.numeroParcela + "/" + bill.numeroTotalParcelas,
											style: "tableRightValue",
										},
									],
								},
							],
							[
								{
									text: [
										{
											text: "Valor da Parcela \n",
											color: "#444",
										},
										{
											text: "R$ " + bill.valor.toFixed(2),
											style: "tableRightValue",
										},
									],
								},
								" ",
								{
									text: [
										{
											text: "Valor da Parcela \n",
											style: "tableRightTitle",
										},
										{
											text: "R$ " + bill.valor.toFixed(2),
											style: "tableRightValue",
										},
									],
								},
							],
							[
								{
									text: [
										{
											text: "Data do Pagamento \n",
											color: "#444",
											margin: [0, 0, 0, 5],
										},
										{
											text: "___/___/_____",
											style: "tableRightValue",
										},
									],
								},
								" ",
								{
									text: [
										{
											text: "Juros/Multa R$ \n",
											style: "tableRightTitle",
										},
										{
											text: " ",
											style: "tableRightValue",
										},
									],
								},
							],
							[
								{
									text: [
										{
											text: "Assinatura \n",
											color: "#444",
										},
										{
											text: " ",
											style: "tableRightValue",
										},
									],
								},
								[
									{
										table: {
											widths: [180, 100],
											body: [
												[
													{
														text: "Sacado",
														color: "#444",
													},
													{
														text: "CPF",
														color: "#444",
													},
												],
												[
													{
														text: parcelDocumentModel.name || " ",
														fontSize: 9,
														bold: true,
													},
													{
														text: parcelDocumentModel.cpf || " ",
														fontSize: 9,
														bold: true,
													},
												],
												[
													{
														colSpan: 2,
														text:
															parcelDocumentModel.address +
															", " +
															parcelDocumentModel.neighborhood +
															", " +
															parcelDocumentModel.zipCode,
													},
													"",
												],
											],
										},
										layout: "noBorders",
									},
								],
								{
									text: [
										{
											text: "Valor Pago R$ \n",
											style: "tableRightTitle",
										},
										{
											text: " ",
											style: "tableRightValue",
										},
									],
								},
							],
						],
					},
					pageBreak: lastOfPage(index) ? "after" : "",
				},
				FormContaCorrenteComponent._strokedLine(index),
			];
		});

		return pdfMake;
	}

	private static _strokedLine(index: number) {
		if (lastOfPage(index)) {
			return;
		}
		return {
			canvas: [
				{
					type: "line",
					x1: -50,
					y1: 0,
					x2: 600,
					y2: 0,
					lineWidth: 1,
					dash: { length: 5, space: 5 },
				},
			],
			margin: [0, 10, 0, 10],
		};
	}

	get haveTreatmentSelected(): boolean {
		return this.treatmentId && this.treatmentId !== 0;
	}

	async generateReport() {
		if (!this.billsTable || this.billsTable.length === 0) {
			this.service.notification.info("Não há dados a serem exportados.");
			return;
		}
		const loading = this._globalSpinnerService.loadingManager.start();
		try {
			const patientReportModel: PatientReportModel = await this.prepareReport();
			await this._generatePdfReport(patientReportModel);
		} catch (e) {
			this.service.notification.error("Ocorreu um erro ao emitir o relatório.");
		} finally {
			loading.finished();
		}
	}

	private async prepareReport(): Promise<PatientReportModel> {
		const body = {
			tratamentoId: this.treatmentId,
			pacienteId: this.patientModel.id,
			dataInicio: null,
			dataFim: null,
			formaDeRecebimento: [],
			tipoLancamento: [],
		};
		const patientReportModel = await this.service
			.httpPOST(CONSTANTS.ENDPOINTS.patients.financial.patientReport, body)
			.toPromise();

		return patientReportModel.body as PatientReportModel;
	}

	private async _generatePdfReport(patientReportModel: PatientReportModel) {
		const pdfMake = await this._buildReport(patientReportModel);
		const pdfMakeDefinition = await this._pdfGeneratorService.exportPDFMakeAsPDF(
			pdfMake,
			"",
			"Relatório Financeiro do Paciente.pdf"
		);
		pdfMakeDefinition.open();
	}

	private async _buildReport(patientReportModel: PatientReportModel): Promise<PDFMake> {
		const pdfMake: PDFMake = { afterContent: [], pageOrientation: "landscape" };
		pdfMake.styles = REPORT_STYLES;
		pdfMake.afterContent.push(await this.buildReportHeader());
		pdfMake.afterContent.push(this._buildReportBody(patientReportModel));
		pdfMake.afterContent.push(FormContaCorrenteComponent._buildReportResume(patientReportModel));

		return pdfMake;
	}

	private async buildReportHeader() {
		return [
			{
				...(await this._commonsReportService.buildFinancialReportHeader(
					"Relatório Financeiro do Paciente"
				)),
			},
			{
				text: [
					{ text: "Plano de Tratamento: ", bold: true, color: GREY_COLOR },
					this.reportFilterLabel,
				],
				fontSize: 10,
				margin: [0, 10, 0, 0],
			},
			{
				text: [{ text: "Paciente: ", bold: true, color: GREY_COLOR }, this.patientModel.pessoaNome],
				fontSize: 10,
				margin: [0, 0, 0, 10],
			},
		];
	}

	get reportFilterLabel(): string {
		return this.recordsDropdown.find((item) => item.id === this.treatmentId).descricao;
	}

	private _buildReportBody(patientReportModel: PatientReportModel) {
		return {
			table: {
				widths: [70, 70, 150, 200, 120, 60, 70],
				body: [
					[
						{ text: "Vencimento", style: "tableHeader" },
						{ text: "Recebimento", style: "tableHeader" },
						{ text: "Lançamento", style: "tableHeader" },
						{ text: "Descrição", style: "tableHeader" },
						/*{ text: 'Parcela', style: 'tableHeader' },*/
						{ text: "Forma Pagamento", style: "tableHeader" },
						{ text: "Cheque", style: "tableHeader" },
						{ text: "Valor", style: "tableHeader" },
					],
					...this._buildReportRows(patientReportModel),
				],
			},
		};
	}

	private _buildReportRows(patientReportModel: PatientReportModel) {
		return patientReportModel.content.map((bill: PatientReportRowModel, index: number) => {
			/*const parcelCount = index + 1 + '/' + patientReportModel.content.length;*/
			return [
				{ text: (bill.dataVencimento || " ").split(" "), alignment: "center", fontSize: 9 },
				{ text: (bill.dataRecebimento || " ").split(" "), alignment: "center", fontSize: 9 },
				{ text: bill.tipoLancamento || " ", fontSize: 9 },
				{ text: bill.descricao || " ", fontSize: 9 },
				/*{ text: parcelCount, alignment: 'center', fontSize: 9 },*/
				{ text: bill.formaRecebimento || " ", fontSize: 9 },
				{ text: bill.numeroCheque || " ", alignment: "center", fontSize: 9 },
				{ text: bill.valor ? bill.valor.toFixed(2) : " ", alignment: "right", fontSize: 9 },
			];
		});
	}

	private static _buildReportResume(patientReportModel: PatientReportModel) {
		return {
			columns: [
				{
					width: 695,
					alignment: "right",
					stack: [
						{ text: "Pago (R$):", style: "resumeHeader", color: GREEN_COLOR },
						{ text: "A Pagar (R$):", style: "resumeHeader", color: BLUE_COLOR },
						{ text: "Total Geral (R$):", style: "resumeHeader" },
					],
				},
				{
					alignment: "right",
					stack: [
						{
							text: patientReportModel.valorTotalRecebido.toFixed(2),
							style: "resumeRow",
							color: GREEN_COLOR,
						},
						{
							text: patientReportModel.valorTotalARecer.toFixed(2),
							style: "resumeRow",
							color: BLUE_COLOR,
						},
						{ text: patientReportModel.valorTotal.toFixed(2), style: "resumeRow" },
					],
				},
			],
			margin: [0, 30, 0, 0],
		};
	}

	async deleteParcel(parcel) {
		const choosed = parcel.recorrenciaId
			? await this._buildDialogWithOptions(
					"Remover Parcela",
					"Como você prefere remover esta parcela?",
					"Atenção! Serão excluídas apenas parcelas que não foram pagas.",
					"Excluir",
					DELETE_PARCEL_OPTIONS
			  )
			: SOMENTE_ESTE_LANCAMENTO;

		if (!choosed) {
			return false;
		}

		const { value } = await Swal.fire({
			title: "Confirmação de exclusão",
			text: "Tem certeza que deseja apagar?",
			type: "question",
			showCancelButton: true,
			confirmButtonText: "Sim. Apague",
			cancelButtonText: "Cancelar",
		});

		if (!value) {
			return false;
		}

		const loading = this._globalSpinnerService.loadingManager.start();
		const content = {
			id: parcel.id,
			recorrenciaId: parcel.recorrenciaId,
			tipoExclusao: choosed,
		};
		try {
			await this.service
				.httpPUT(CONSTANTS.ENDPOINTS.patients.financial.removeParcel, content)
				.toPromise();
			this.service.notification.success("Parcela removida.");
			this.getRecordsDropdown();
		} catch (e) {
			this.service.notification.error("Ocorreu um erro ao remover a Parcela.");
		} finally {
			loading.finished();
		}
	}

	private async _buildDialogWithOptions(
		title: string,
		message: string,
		alertMessage: string,
		confirmText: string,
		options: Option[]
	) {
		const modalRef = this._modalService.open(ModalWithOptionsComponent, {
			size: "lg",
			centered: true,
		});
		modalRef.componentInstance.title = title;
		modalRef.componentInstance.message = message;
		modalRef.componentInstance.alertMessage = alertMessage;
		modalRef.componentInstance.confirmText = confirmText;
		modalRef.componentInstance.options = options;

		return await modalRef.result;
	}
}
