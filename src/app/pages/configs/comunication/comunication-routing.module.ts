import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComunicationComponent } from './comunication.component';

const routes: Routes = [
  {
    path: '',
    component: ComunicationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComunicationRoutingModule {}
