import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UtilsService } from '../../../../services/utils/utils.service';
import { FormLogin } from '../../../../core/forms/login';
import { CONSTANTS } from '../../../../core/constants/constants';
import { ContaUserModel, UserModel } from '../../../../core/models/user/user.model';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ContaApiService } from '../../../../api/conta-api.service';
import { LoginService } from '../../../../auth/login.service';
import { GlobalSpinnerService } from '../../../../lib/global-spinner.service';
import { NotificationsService } from 'angular2-notifications';
import { errorToString } from '../../../../lib/helpers/error-to-string';
import { Router } from '@angular/router';

@Component({
               selector: 'app-basic-login',
               templateUrl: './basic-login.component.html',
               styleUrls: ['./basic-login.component.scss'],
               animations: [
                   trigger('flipState', [
                       state(
                           'active',
                           style({
                                     transform: 'rotateY(179deg)',
                                 }),
                       ),
                       state(
                           'inactive',
                           style({
                                     transform: 'rotateY(0)',
                                 }),
                       ),
                       transition('active => inactive', animate('500ms ease-out')),
                       transition('inactive => active', animate('500ms ease-in')),
                   ]),
               ],
           })
export class BasicLoginComponent implements OnInit {
    formulario: FormGroup;
    accounts: ContaUserModel[] = [];
    flip = 'inactive';

    constructor(
        public service: UtilsService,
        private readonly _contaApiService: ContaApiService,
        private readonly _loginService: LoginService,
        private readonly _globalSpinnerService: GlobalSpinnerService,
        private readonly _notificationsService: NotificationsService,
        private readonly _router: Router,
    ) {
    }

    ngOnInit() {

        this.formulario = this.service.createForm(FormLogin, {
            login: '',
            senha: '',
        });
    }

    login() {
        this.service.loading(true, this.service.translate.instant('LOGIN.LOGIN'));
        this.service
            .httpGET(
                `${ CONSTANTS.ENDPOINTS.login }?login=${
                    this.formulario.get('login').value
                }&senha=${ this.formulario.get('senha').value }`,
            )
            .subscribe(
                data => {
                    this.service.user = data.body as UserModel;
                    this.service.setStorage('authToken', this.service.user.token);
                    if (this.service.user.usuarioPerfilContaModuloSelecionado) {
                        this.service.setStorage('user', this.service.user);
                        this.service.loading(false);
                        this.service.navigate(CONSTANTS.ROUTERS.DASHBOARD, null);
                    } else {
                        this.service.httpGET(CONSTANTS.ENDPOINTS.account).subscribe(
                            dataContas => {
                                this.service.loading(false);
                                this.accounts = dataContas.body as ContaUserModel[];
                                this.toggleFlip();
                            },
                            errContas => {
                                this.service.loading(false);

                                this.service.notification.error(
                                    this.service.translate.instant('LOGIN.ERROR.TITLE'),
                                    this.service.translate.instant('LOGIN.ERROR.INFO'),
                                );
                            },
                        );
                    }
                },
                err => {
                    this.service.loading(false);

                    this.service.notification.error(
                        this.service.translate.instant('LOGIN.ERROR.TITLE'),
                        this.service.translate.instant('LOGIN.ERROR.INFO'),
                    );
                },
            );
    }

    toggleFlip() {
        this.flip = this.flip === 'inactive' ? 'active' : 'inactive';
    }

    async selectAccount(usuarioPerfilContaModuloId: number) {
        const loadingToken = this._globalSpinnerService.loadingManager.start();

        try {
            await this._loginService.escolherConta(usuarioPerfilContaModuloId);
            await this._router.navigate(['/']);
        } catch (e) {
            this._notificationsService.error('Erro ao escolher conta', errorToString(e));
        } finally {
            loadingToken.finished();
        }
    }

}
