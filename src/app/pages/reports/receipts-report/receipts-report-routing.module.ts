import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReceiptsReportComponent } from './receipts-report.component';

const routes: Routes = [
	{
		path: '',
		component: ReceiptsReportComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ReceiptsReportRoutingModule {}
