import { Inject, Injectable } from '@angular/core';
import { API_CONFIGURATION, ApiConfiguration } from './api-configuration';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ProteticosApiService {

  constructor(
    @Inject(API_CONFIGURATION) private readonly _config: ApiConfiguration,
    private readonly _http: HttpClient,
  ) {
  }

  apagar(id: number): Promise<void> {
    return this._http
      .delete<void>(`${this._config.apiBasePath}/servicoProtetico/${id}`)
      .toPromise();
  }

}
