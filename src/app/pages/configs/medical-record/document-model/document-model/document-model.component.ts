import { Component, OnInit } from "@angular/core";
import { UtilsService } from "../../../../../services/utils/utils.service";
import { NotificationsService } from "angular2-notifications";
import { DocumentModel } from "../models/DocumentModel";
import { DocumentModelService } from "../service/document-model.service";
import { RespostaPaginada } from "../../../../../api/model/resposta-paginada";
import Swal from "sweetalert2";
import { AsyncTemplate } from "../../../../../lib/helpers/async-template";
import {
	ContractDataApiService,
	ModeloContratoListing,
} from "../../../../../api/contract-data-api.service";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbTabChangeEvent } from "@ng-bootstrap/ng-bootstrap";

@Component({
	selector: "app-document-model",
	templateUrl: "./document-model.component.html",
	styleUrls: ["./document-model.component.scss"],
})
export class DocumentModelComponent implements OnInit {
	pageSize = 200;
	paginationResponse: RespostaPaginada<DocumentModel>;
	contractsPaginationResponse: RespostaPaginada<ModeloContratoListing>;

	readonly startingId: string | undefined = this._route.snapshot.queryParams.tab + "-tab";

	constructor(
		private readonly _documentModelService: DocumentModelService,
		private readonly _asyncTemplate: AsyncTemplate,
		private readonly _contractDataApiService: ContractDataApiService,
		private readonly _router: Router,
		private readonly _route: ActivatedRoute,
		readonly _service: UtilsService,
		private readonly _notificationsService: NotificationsService
	) {}

	ngOnInit() {
		this._initDocumentModels();
		this._loadContracts();
	}

	onTabChange(e: NgbTabChangeEvent) {
		this._router.navigate([], {
			relativeTo: this._route,
			queryParams: { tab: e.nextId.replace(/-tab$/, "") },
			queryParamsHandling: "merge",
		});
	}

	private async _initDocumentModels() {
		try {
			this.paginationResponse = await this._documentModelService.listAll();
		} catch (e) {
			this._notificationsService.error("Ocorreu um erro ao listar os modelos");
		}
	}

	private async _loadContracts(page?: number) {
		this.contractsPaginationResponse = await this._asyncTemplate.wrapUserFeedback(
			"Erro ao consultar contratos",
			() =>
				this._contractDataApiService.listContractModels(undefined, { page, size: this.pageSize })
		);
	}

	async delete(id: number) {
		const { value } = await Swal.fire({
			title: "Confirmação de exclusão",
			text: "Tem certeza que quer apagar este Modelo?",
			type: "question",
			showCancelButton: true,
			confirmButtonText: "Sim. Apague",
			cancelButtonText: "Cancelar",
		});

		if (!value) {
			return;
		}

		try {
			this._service.loading(true);
			await this._documentModelService.delete(id);
			this._notificationsService.success("Modelo removido");
			this._initDocumentModels();
		} catch (e) {
			this._notificationsService.error("Ocorreu um erro ao tentar remover o modelo");
		} finally {
			this._service.loading(false);
		}
	}
}
