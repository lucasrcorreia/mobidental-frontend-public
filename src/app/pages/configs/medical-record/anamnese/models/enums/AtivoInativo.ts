export enum AtivoInativo {
  ATIVO = 'ATIVO',
  INATIVO = 'INATIVO',
}

export const ATIVO_INATIVO_TRANSLATE: { [key in AtivoInativo]: string } = {
  ATIVO: 'Ativo',
  INATIVO: 'Inativo',
};
