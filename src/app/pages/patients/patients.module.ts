import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PatientsRoutingModule } from "./patients-routing.module";
import { PatientsComponent } from "./patients.component";
import { FormPatientsComponent } from "./form-patients/form-patients.component";
import { NgxTranslateModule } from "../../core/translate/translate.module";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { SimpleNotificationsModule } from "angular2-notifications";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "../../shared/shared.module";
import { NgxSelectModule } from "ngx-select-ex";
import { NgxMaskModule } from "ngx-mask";
import { FormTreatmentComponent } from "./form-treatment/form-treatment.component";
import { PipesModule } from "../../core/pipes/pipes.module";
import { ProcedureDialogComponent } from "./procedure-dialog/procedure-dialog.component";
import { NgSelectModule } from "@ng-select/ng-select";
import { DirectivesModule } from "../../core/directive/directives.module";
import { NgxCurrencyModule } from "ngx-currency";
import { CURRENCY_MASK_CONFIG, CurrencyMaskConfig } from "ngx-currency/src/currency-mask.config";
import { CONSTANTS } from "../../core/constants/constants";
import { FormRecordsComponent } from "./form-records/form-records.component";
import { ModalEditParcelaComponent } from "./modal-edit-parcela/modal-edit-parcela.component";
import { FormContaCorrenteComponent } from "./form-conta-corrente/form-conta-corrente.component";
import { ModalEditRecordComponent } from "./modal-edit-record/modal-edit-record.component";
import { ModalPaymentComponent } from "./modal-payment/modal-payment.component";
import { ModalEditParcelAccountComponent } from "./modal-edit-parcel-account/modal-edit-parcel-account.component";
import { ImageCropperModule } from "ngx-image-cropper";
import { WebcamModule } from "ngx-webcam";
import { ComponentsModule } from "../../components/components.module";
import { AppLibModule } from "../../lib/app-lib.module";
import { ListDocumentsComponent } from "./documents/list-documents.component";
import { EditDocumentComponent } from "./documents/edit-document/edit-document.component";
import { VisualizeDocumentComponent } from "./documents/visualize-document/visualize-document.component";
import { DeleteDocumentComponent } from "./documents/delete-document/delete-document.component";
import { UploadDocumentComponent } from "./documents/upload-document/upload-document.component";
import { DragDropDirective } from "./documents/upload-document/drag-drop.directive";
import { CameraUploadComponent } from "./documents/upload-document/camera-upload/camera-upload.component";
import { DocumentsCommonService } from "./documents/documents-common.service";
import { AnamneseComponent } from "./anamnese/anamnese.component";
import { EmitListComponent } from "./emit-documents/emit-list/emit-list.component";
import { EmitDocumentComponent } from "./emit-documents/emit-document/emit-document.component";
import { EmitPrescriptionComponent } from "./emit-documents/emit-prescription/emit-prescription.component";
import { EmitDocumentsComponent } from "./emit-documents/emit-documents.component";
import { InputMaskModule } from "racoon-mask-raw";
import { CKEditorModule } from "ckeditor4-angular";
import { VisualizeIssuableDocumentComponent } from "./emit-documents/visualize-issuable-document/visualize-issuable-document.component";
import { CommonPrintService } from "./emit-documents/service/common-print.service";
import { TreatmentPlanPrintConfigComponent } from "./form-patients/treatment-plan-print-config/treatment-plan-print-config.component";
import { UiSwitchModule } from "ngx-ui-switch";
import { DetachedTreatmentComponent } from "./form-records/detached-treatment/detached-treatment.component";
import { MultipleCameraUploadComponent } from "./documents/upload-document/multiple-camera-upload/multiple-camera-upload.component";
import { DetachedParcelComponent } from "./form-conta-corrente/detached-parcel/detached-parcel.component";
import { ParcelDocumentDialogComponent } from "./form-conta-corrente/parcel-document-dialog/parcel-document-dialog.component";
import { AnnotationsComponent } from "./annotations/annotations.component";
import { AnnotationComponent } from "./annotations/annotation/annotation.component";
import { EditProcedureComponent } from "./form-treatment/edit-procedure/edit-procedure.component";
import { ProcedureDialogService } from "./procedure-dialog/procedure-dialog.service";
import { DetachedParcelDialogService } from "./form-conta-corrente/detached-parcel/detached-parcel-dialog.service";
import { GalleryComponent } from "./documents/gallery/gallery.component";
import { GalleryItemDirective } from "./documents/gallery/gallery-item.directive";

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
	align: "right",
	allowNegative: true,
	allowZero: true,
	decimal: CONSTANTS.COMMONS_VALUES.DECIMAL,
	precision: 2,
	prefix: CONSTANTS.COMMONS_VALUES.CURRENCY_SYMBOL + " ",
	suffix: "",
	thousands: CONSTANTS.COMMONS_VALUES.THOUSANDS,
	nullable: false,
};

@NgModule({
	declarations: [
		PatientsComponent,
		FormPatientsComponent,
		FormTreatmentComponent,
		ProcedureDialogComponent,
		FormRecordsComponent,
		ModalEditParcelaComponent,
		FormContaCorrenteComponent,
		ModalEditRecordComponent,
		ModalPaymentComponent,
		ModalEditParcelAccountComponent,
		ListDocumentsComponent,
		EditDocumentComponent,
		VisualizeDocumentComponent,
		DeleteDocumentComponent,
		UploadDocumentComponent,
		DragDropDirective,
		CameraUploadComponent,
		AnamneseComponent,
		EmitDocumentsComponent,
		EmitListComponent,
		EmitDocumentComponent,
		EmitPrescriptionComponent,
		VisualizeIssuableDocumentComponent,
		TreatmentPlanPrintConfigComponent,
		DetachedTreatmentComponent,
		MultipleCameraUploadComponent,
		DetachedParcelComponent,
		ParcelDocumentDialogComponent,
		AnnotationsComponent,
		AnnotationComponent,
		EditProcedureComponent,
		GalleryComponent,
		GalleryItemDirective,
	],
	imports: [
		CommonModule,
		NgxTranslateModule,
		SimpleNotificationsModule,
		FormsModule,
		ReactiveFormsModule,
		NgxDatatableModule,
		SharedModule,
		NgxSelectModule,
		NgxMaskModule.forRoot(),
		PatientsRoutingModule,
		PipesModule,
		DirectivesModule,
		NgSelectModule,
		NgxCurrencyModule,
		ImageCropperModule,
		WebcamModule,
		ComponentsModule,
		AppLibModule,
		InputMaskModule,
		CKEditorModule,
		UiSwitchModule,
	],
	exports: [
		FormTreatmentComponent,
		FormRecordsComponent,
		FormContaCorrenteComponent,
		ModalPaymentComponent,
		ModalEditParcelAccountComponent,
	],
	providers: [
		{ provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig },
		DocumentsCommonService,
		CommonPrintService,
		ProcedureDialogService,
		DetachedParcelDialogService,
	],
	entryComponents: [
		DeleteDocumentComponent,
		EditDocumentComponent,
		UploadDocumentComponent,
		CameraUploadComponent,
		MultipleCameraUploadComponent,
		VisualizeDocumentComponent,
		VisualizeIssuableDocumentComponent,
		TreatmentPlanPrintConfigComponent,
		DetachedTreatmentComponent,
		DetachedParcelComponent,
		ParcelDocumentDialogComponent,
		AnnotationComponent,
		EditProcedureComponent,
		ProcedureDialogComponent,
	],
})
export class PatientsModule {}
