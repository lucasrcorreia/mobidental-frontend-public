import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../../../../services/utils/utils.service';
import { TranslateService } from '@ngx-translate/core';
import { AnamneseService } from '../service/anamnese.service';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { AnamneseModel } from '../models/AnamneseModel';
import { NotificationsService } from 'angular2-notifications';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-anamnese',
  templateUrl: './anamnese.component.html',
  styleUrls: ['./anamnese.component.scss'],
})
export class AnamneseComponent implements OnInit {

  anamneses: AnamneseModel[];

  ColumnMode = ColumnMode;
  readonly portugueseMessages = {
    // Message to show when array is presented
    // but contains no values
    emptyMessage: 'Nenhum modelo cadastrado...',

    // Footer total message
    totalMessage: 'registro(s)',

    // Footer selected message
    selectedMessage: 'selecionado'
  };

  constructor(private readonly _service: UtilsService,
              private readonly _translate: TranslateService,
              private readonly _anamneseService: AnamneseService,
              private readonly _notificationsService: NotificationsService,
              private readonly _router: Router,
  ) {
  }

  ngOnInit() {
    this._initAnamneses();
  }

  async delete(id: number) {
    const { value } = await Swal.fire({
      title: 'Confirmação de exclusão',
      text: 'Tem certeza que quer apagar este Modelo?',
      type: 'question',
      showCancelButton: true,
      confirmButtonText: 'Sim. Apague',
      cancelButtonText: 'Cancelar',
    });

    if (!value) {
      return;
    }

    try {
      this._service.loading(true);
      await this._anamneseService.delete(id);
      this._notificationsService.success('Modelo removido.');
      this._initAnamneses();
    } catch (e) {
      this._notificationsService.error('Ocorreu um erro ao remover o modelo.');
    } finally {
      this._service.loading(false);
    }
  }

  private async _initAnamneses() {
    try {
      this._service.loading(true);
      this.anamneses = await this._anamneseService.listAll();
    } catch (e) {
      this._notificationsService.error('Ocorreu um erro ao listar os modelos.');
    } finally {
      this._service.loading(false);
    }
  }

}
