import { Inject, Injectable } from '@angular/core';
import { API_CONFIGURATION, ApiConfiguration } from './api-configuration';
import { HttpClient } from '@angular/common/http';
import { UserModel } from '../core/models/user/user.model';

export interface InviteInfo {
  id: number;
  chaveConvite: string;
  usuarioEmail: string;
  isUsuarioNovo: boolean;
  conviteExiste: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class InvitesApiService {

  constructor(
    @Inject(API_CONFIGURATION) private readonly _config: ApiConfiguration,
    private readonly _http: HttpClient,
  ) {
  }

  fetchInviteInfo(chaveConvite: string): Promise<InviteInfo> {
    return this._http
      .get<InviteInfo>(
        `${this._config.openBasePath}/usuarioPerfilContaModulo/convite/recuperar`,
        { params: { chaveConvite } },
      )
      .toPromise();
  }

  acceptInvite(email: string, password: string, inviteKey: string): Promise<UserModel> {
    return this._http
      .get<UserModel>(
        `${this._config.openBasePath}/usuarioPerfilContaModulo/convite/aceitar`,
        { params: { login: email, senha: password, chaveConvite: inviteKey } },
      )
      .toPromise();
  }
}
