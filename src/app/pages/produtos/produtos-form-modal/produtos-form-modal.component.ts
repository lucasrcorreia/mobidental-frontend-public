import { Component, OnInit, Input } from '@angular/core';
import { Produto } from '../../../api/model/produto';
import { ModalAnimationComponent } from '../../../shared/modal-animation/modal-animation.component';
import { clone } from 'ramda';
import {
  getInfoUnidadeMedida,
  UnidadeMedidaInfo,
  UnidadeMedida
} from '../../../api/model/unidade-medida';
import { AsyncTemplate } from '../../../lib/helpers/async-template';
import { ProdutosApiService } from '../../../api/produtos-api.service';

@Component({
  selector: 'app-produtos-form-modal',
  templateUrl: './produtos-form-modal.component.html',
  styleUrls: ['./produtos-form-modal.component.scss']
})
export class ProdutosFormModalComponent implements OnInit {
  @Input() data: Partial<Produto> = {};
  @Input() title: string = 'Novo Produto';
  @Input() modal?: ModalAnimationComponent;

  readonly unidades: ReadonlyArray<UnidadeMedida> = Object.values(
    UnidadeMedida
  );

  constructor(
    private readonly _asyncTemplate: AsyncTemplate,
    private readonly _produtosApiService: ProdutosApiService
  ) {}

  ngOnInit() {}

  cancelar() {
    if (this.modal) this.modal.close();
  }

  async finalizar() {
    this._asyncTemplate.wrapUserFeedback('Erro ao salvar edição', async () => {
      const produto = this._validarProduto(clone(this.data));

      await this._produtosApiService.salvarProduto(produto);

      if (this.modal) this.modal.close(produto);
    });
  }

  infoUnidade(un: UnidadeMedida): UnidadeMedidaInfo {
    return getInfoUnidadeMedida(un);
  }

  private _validarProduto(talvezProduto: Partial<Produto>): Produto {
    if (talvezProduto.nome == null)
      throw Error('Nome do produto é obrigatório');

    if (talvezProduto.unidadeMedida == null)
      throw Error('Unidade de medida é obrigatória');

    if (talvezProduto.produtoEstoqueQuantidade == null)
      throw Error('Quantidade em estoque é obrigatório');

    if (talvezProduto.estoqueMinimo == null)
      throw Error('Estoque mínimo é obrigatório');

    return talvezProduto as Produto;
  }
}
