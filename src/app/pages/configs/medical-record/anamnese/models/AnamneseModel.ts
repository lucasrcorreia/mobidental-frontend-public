import { AnamnesePerguntaModel } from './AnamnesePerguntaModel';

export interface AnamneseModel {
  id?: number;
  nome?: string;
  perguntas?: AnamnesePerguntaModel[];
}
