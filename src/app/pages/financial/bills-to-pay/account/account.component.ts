import { Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { AccountBillsToPayModel } from '../../../../core/models/financial/billsToPay/billsToPay.model';
import { FormGroup } from '@angular/forms';
import { UtilsService } from '../../../../services/utils/utils.service';
import { FormBillsToPay } from '../../../../core/forms/billsToPay';
import { FINANCIAL_CONST } from '../../constants';
import { LISTAS } from '../../lists';
import * as moment from 'moment';
import { CONSTANTS } from '../../../../core/constants/constants';
import { TreeviewService } from '../../../../services/treeview/treeview.service';
import { CategoryInterface, CenterCostInterface } from '../../../../core/models/config/financial/financial.model';
import { classToPlain } from 'class-transformer';
import { CustomDatepickerI18n, I18n } from '../../../../shared/ngb-datepicker/datepicker-i18n';
import { NgbDateParserFormatter, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateFRParserFormatter } from '../../../../shared/ngb-datepicker/ngb-date-fr-parser-formatter';
import { BehaviorSubject, combineLatest, Observable, Subscription } from 'rxjs';
import { debounceTime, flatMap, map, shareReplay, startWith, take } from 'rxjs/operators';
import { contains } from 'ramda';
import { CategoriaConta, ContasAPagarApiService } from '../../../../api/contas-a-pagar.api.service';
import { AsyncTemplate } from '../../../../lib/helpers/async-template';
import { NotificationsService } from 'angular2-notifications';
import Swal from 'sweetalert2';

export interface Categoria {
  id: number;
  descricao: string;
}

@Component({
  selector: 'app-account-pay',
  templateUrl: './account.component.html',
  styleUrls: [
    './account.component.scss',
    './../../../../../assets/icon/icofont/css/icofont.scss',
  ],
  providers: [
    I18n,
    { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
    { provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
  ],
})
export class AccountComponent implements OnInit, OnChanges {

  private readonly _subs = new Subscription();

  @ViewChild('cardMain') cardMain: ElementRef;

  @Input() account: AccountBillsToPayModel;
  @Input() centerCost: CenterCostInterface[];
  @Input() category: CategoryInterface[];
  @Input() paid = false;
  @Output() resultModal = new EventEmitter<any>();

  locale = this.service.translate.instant('COMMON.LOCALE');
  formAccount: FormGroup;
  financialConsts = FINANCIAL_CONST;
  localLists = new LISTAS(this.service.translate);
  haveScroll = false;

  private readonly _forceReloadCategoriasSubject = new BehaviorSubject(undefined);

  private readonly _categoriasSubject = new BehaviorSubject<CategoriaConta[]>([]);

  readonly categoria$ = this._forceReloadCategoriasSubject.pipe(
    flatMap(() => this._asyncTemplate.wrapUserFeedback(
      'Erro ao consultar categorias',
      () => this._contasAPagarApiService.consultarTodasCategorias(),
    )),
    shareReplay(1),
  );

  private readonly _seachSubject = new BehaviorSubject<string | undefined>(undefined);

  readonly categoriasFiltrada$: Observable<CategoriaConta[]> = combineLatest([
    this._seachSubject.pipe(debounceTime(300), startWith(undefined)),
    this.categoria$,
  ]).pipe(
    map(([query, cs]) => [query && query.trim() || '', cs]),
    map(([q, cs]) => q ? cs.filter(c => contains(q.toLowerCase(), c.descricao.toLowerCase())) : cs),
    shareReplay(1),
  );

  categories: Categoria[] = [];
  todasCategorias: Categoria[] = [];
  cCost = [];

  errors = {
    parcel: false,
    ratio: true,
  };

  constructor(
    public service: UtilsService,
    private treeviewService: TreeviewService,
    private readonly _contasAPagarApiService: ContasAPagarApiService,
    private readonly _asyncTemplate: AsyncTemplate,
    private readonly _notificationsService: NotificationsService,
  ) {
    this._subs.add(this.categoria$.subscribe(this._categoriasSubject));
  }

  ngOnInit() {
  }

  ngOnChanges() {
    setTimeout(() => {
      this.treeviewService.findNoChildren('categoriaChilds', this.category, this.todasCategorias);
      this.categories = this.todasCategorias;
      this.treeviewService.findNoChildren('centroCustoChilds', this.centerCost, this.cCost);

      this.formAccount = this.service.createForm(FormBillsToPay, this.account);

      if (this.paid) {
        for (const field in this.formAccount.controls) {
          const control = this.formAccount.get(field);
          control.disable();
        }
      }

      if (this.account.id) {
        this.formAccount.get('numeroTotalParcelas').disable();
        this.formAccount.get('dataCompetencia').setValue(
          this.service.parseDatePicker(this.account.dataCompetencia),
        );

        this.formAccount.get('dataVencimento').setValue(
          this.service.parseDatePicker(this.account.dataVencimento),
        );

        if (this.account.dataPagamento) {
          this.formAccount.get('dataPagamento').setValue(
            this.service.parseDatePicker(this.account.dataPagamento),
          );
        }
      }

      this.detectScrollPos();
    });
  }

  procurarCategoria(str: string | null | undefined) {
    this._seachSubject.next(str);
  }

  async apagarCategoria(id: number) {
    const confirmation = await Swal.fire({
      title: 'Confirmação de exclusão',
      text: 'Tem certeza que quer apagar essa categoria?',
      type: 'question',
      showCancelButton: true,
      confirmButtonText: 'Sim. Apague',
      cancelButtonText: 'Cancelar',
    });

    if (!confirmation.value) {
      return;
    }

    await this._asyncTemplate.wrapUserFeedback(
      'Erro ao apagar destino',
      () => this._contasAPagarApiService.apagarCategoria(id),
    );

    this._forceReloadCategoriasSubject.next(undefined);
    await this._espereProximoReloadCategorias();
    this.formAccount.patchValue({ categoriaId: null });
  }

  async adicionarNovaCategoria(str: string = this._seachSubject.value) {
    const strTratada = str && str.trim() || '';

    if (!strTratada) {
      this._notificationsService.alert('A categoria não pode ser vazia!');
      return;
    }

    const categoria = this._categoriasSubject.value.find(cat => cat.descricao.toLowerCase() === strTratada.toLowerCase())
      || await this._persistaCategoria(strTratada);

    this.formAccount.patchValue({ categoriaId: categoria.id });
    this._seachSubject.next(undefined);
  }

  clearSearch() {
    this._seachSubject.next(undefined);
  }

  changedValue(id: number | null) {
    if (id == null) {
      this.adicionarNovaCategoria();
    }
  }

  async editeCategoria(id: number = this.formAccount.getRawValue().categoriaId) {
    const categoria: CategoriaConta = this._categoriasSubject.value.find(it => it.id === id);

    const input = await Swal.fire({
      title: 'Modifique o nome da categoria',
      input: 'text',
      inputValue: categoria.descricao,
      showCancelButton: true,
      confirmButtonText: 'Salvar',
      cancelButtonText: 'Cancelar',
    });

    const value: string = String(input.value || '').trim();

    if (!value) {
      return;
    }

    const destinoSalvo = await this._asyncTemplate.wrapUserFeedback(
      'Erro ao salvar destino',
      () => this._contasAPagarApiService.salvarCategoria({
        id: categoria.id,
        descricao: value,
      }),
    );

    this._forceReloadCategoriasSubject.next(undefined);
    await this._espereProximoReloadCategorias();
    this.formAccount.patchValue({ categoriaId: destinoSalvo.id });
  }

  private _persistaCategoria(str: string) {
    return this._asyncTemplate.wrapUserFeedback('Erro ao salvar categoria', async () => {
      const cat = await this._contasAPagarApiService.salvarCategoria({ descricao: str });

      this._notificationsService.success('Categoria salva com sucesso');

      this._forceReloadCategoriasSubject.next(undefined);
      await this._espereProximoReloadCategorias();

      return cat;
    });
  }

  private _espereProximoReloadCategorias(): Promise<void> {
    return this.categoria$.pipe(take(2)).toPromise().then(() => undefined);
  }

  validaTotalParcelas(): boolean {
    const parcelas = this.formAccount.get('numeroTotalParcelas').value;
    if (!parcelas) {
      return false;
    } else {
      if (parseFloat(parcelas) < 1) {
        return false;
      }

      return true;
    }
  }

  detectScroll() {
    this.haveScroll = false;
    const { scrollHeight, clientHeight } = this.cardMain.nativeElement;
    if (scrollHeight > clientHeight) {
      this.haveScroll = true;
    }
  }

  detectScrollPos() {
    setTimeout(() => {
      this.detectScroll();

      const {
        scrollTop,
        scrollHeight,
        clientHeight,
      } = this.cardMain.nativeElement;
      if (scrollTop + clientHeight >= scrollHeight - 50) {
        this.haveScroll = false;
      }
    });
  }

  closeModal(id: string) {
    this.service.closeModal(id, null);
    this.resultModal.emit(false);
  }

  save() {
    this.service.loading(true);

    this.account = this.formAccount.getRawValue();
    this.account.dataCompetencia = this.service.parseDateFromDatePicker(
      this.account.dataCompetencia,
    );

    this.account.dataVencimento = this.service.parseDateFromDatePicker(
      this.account.dataVencimento,
    );

    if (this.account.dataPagamento) {
      this.account.dataPagamento = this.service.parseDateFromDatePicker(
        this.account.dataPagamento,
      );
    }

    if (this.account.servicoProteticoId) {
      this.account.juros = 0;
      this.account.desconto = 0;
      this.account.valorTotal = this.account.valor;
    } else {
      const { valor, juros, desconto } = this.formAccount.getRawValue();

      this.account.valorTotal = Number(valor || 0)
        + Number(juros || 0)
        - Number(desconto || 0);
    }

    let accounts: AccountBillsToPayModel[] = [];
    if (this.account.id) {
      accounts = [this.account];
    } else {
      for (
        let i = 0;
        i < parseFloat(this.formAccount.get('numeroTotalParcelas').value);
        i++
      ) {
        const item = classToPlain(this.account) as AccountBillsToPayModel;
        item.numeroParcela = i + 1;
        item.dataVencimento = moment(item.dataVencimento, 'DD/MM/YYYY')
          .add(i, 'months')
          .format('DD/MM/YYYY');
        accounts = [...accounts, item];
      }
    }

    this.service
      .httpPOST(
        CONSTANTS.ENDPOINTS.financial.billsToPay.save,
        accounts,
      )
      .subscribe(
        data => {
          this.resultModal.emit(true);
          this.service.loading(false);
        },
        err => {

          this.service.loading(false);
          this.service.notification.error(
            this.service.translate.instant('COMMON.ERROR.SEARCH'),
            err.error.error,
          );
        },
      );
  }

  get tipoCusto() {
    return this.formAccount.get('tipoCusto').value;
  }
}
