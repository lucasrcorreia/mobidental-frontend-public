import { Pipe, PipeTransform } from '@angular/core';

import 'intl';
import 'intl/locale-data/jsonp/pt-BR.js';
import 'intl/locale-data/jsonp/en-US.js';
import 'intl/locale-data/jsonp/es-ES.js';

@Pipe({
  name: 'decimalWithLocale',
})
export class DecimalWithLocalePipe implements PipeTransform {

  transform(value: number, fraction = 2, locale = 'pt-BR') {
    if (isNaN(value)) {
      return value;
    }

    return new Intl.NumberFormat(
      locale,
      { minimumFractionDigits: fraction, maximumFractionDigits: fraction },
    ).format(value);
  }
}
