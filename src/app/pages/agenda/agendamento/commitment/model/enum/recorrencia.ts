export enum Recorrencia {
	TODOS_OS_DIAS = 'TODOS_OS_DIAS',
	SEMANALMENTE = 'SEMANALMENTE',
	QUINZENALMENTE = 'QUINZENALMENTE',
	MENSALMENTE = 'MENSALMENTE',
	ANUALMENTE = 'ANUALMENTE',
}

export const RECORRENCIA_TRANSLATE: { [key in Recorrencia]: string } = {
	TODOS_OS_DIAS: 'Todos os dias',
	SEMANALMENTE: 'Semanalmente',
	QUINZENALMENTE: 'Quinzenalmente',
	MENSALMENTE: 'Mensalmente',
	ANUALMENTE: 'Anualmente',
};
