export interface UserModel {
  login: string;
  token: string;
  usuarioPerfilContaModuloSelecionado: UsuarioPerfilContaModuloSelecionado | null;
}

export interface UsuarioPerfilContaModuloSelecionado {
  id: number;
  usuarioId: number;
  usuarioNome: string;
  perfilContaModuloId: number;
  perfilContaModuloDescricao: string;
  perfilContaModuloContaUrlImagem: string;
  perfilContaModuloContaId: number;
  perfilContaModuloContaRazaoSocial: string;
  listaModuloTelaAcaoDTOSelecionado: ListaModuloTelaAcaoDTOSelecionado[];
}

export interface ListaModuloTelaAcaoDTOSelecionado {
  acao: string;
  modulo: string;
  moduloTelaAcaoDescricao: string;
  moduloTelaAcaoId: number;
  moduloTelaDescricao: string;
  moduloTelaId: number;
  perfil: string;
  tela: string;
}

export interface ContaUserModel {
  contaId: number;
  perfilContaModuloContaUrlImagem: string;
  perfilContaModuloDescricao: string;
  razaoSocial: string;
  usuarioPerfilContaModuloId: number;
}
