import { TestBed } from '@angular/core/testing';

import { ContasReceberApiService } from './contas-receber-api.service';

describe('ContasReceberApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContasReceberApiService = TestBed.get(ContasReceberApiService);
    expect(service).toBeTruthy();
  });
});
