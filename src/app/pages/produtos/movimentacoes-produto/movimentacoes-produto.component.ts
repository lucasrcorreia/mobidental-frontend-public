import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { ProdutosApiService, RespostaMovimentacoes } from '../../../api/produtos-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { flatMap, map } from 'rxjs/operators';
import { AsyncTemplate } from '../../../lib/helpers/async-template';
import { NgxSorting } from '../produtos-list/produtos-list-table/produtos-list-table.component';
import { getTipoMovimentoInfo, MovimentoEstoque, TipoMovimento } from '../../../api/model/movimento-estoque';
import { RespostaPaginada } from '../../../api/model/resposta-paginada';

@Component({
  selector: 'app-movimentacoes-produto',
  templateUrl: './movimentacoes-produto.component.html',
  styleUrls: ['./movimentacoes-produto.component.scss'],
})
export class MovimentacoesProdutoComponent implements OnInit {
  private _pageSubject = new BehaviorSubject(0);

  readonly produtoId$ = this._activatedRoute.params.pipe(
    map(({ idProduto }) => idProduto && +idProduto),
  );

  readonly consulta$: Observable<RespostaMovimentacoes> = combineLatest([
    this.produtoId$,
    this._pageSubject,
  ]).pipe(
    flatMap(([idProduto, page]) =>
      this._asyncTemplate.wrapUserFeedback(
        'Erro ao consultar movimentações',
        () =>
          this._produtosApiService.listarMovimentacoes(idProduto, {
            page,
            size: 10,
          }),
      ),
    ),
  );

  readonly resultado$: Observable<RespostaPaginada<MovimentoEstoque>> = this.consulta$.pipe(
    map(res => res.movimentacoes),
  );

  readonly nomeProduto$: Observable<string> = this.consulta$.pipe(
    map(res => res.produtoNome),
  );

  readonly pageNumber$ = this._pageSubject.asObservable();

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _asyncTemplate: AsyncTemplate,
    private readonly _produtosApiService: ProdutosApiService,
  ) {}

  ngOnInit() {}

  updateSortings(sorts: NgxSorting[]) {
    // not supported yet
  }

  updatePage(page: number) {
    this._pageSubject.next(page);
  }

  movimentacaoInfo(tipo: TipoMovimento) {
    return getTipoMovimentoInfo(tipo);
  }
}
