import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ContasAReceberTableComponent } from "./contas-a-receber-table/contas-a-receber-table.component";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { NgbTooltipModule, NgbDropdownModule } from "@ng-bootstrap/ng-bootstrap";
import { TranslateModule } from "@ngx-translate/core";
import { PipesModule } from "../core/pipes/pipes.module";
import { AppLibModule } from "../lib/app-lib.module";

@NgModule({
	declarations: [ContasAReceberTableComponent],
	imports: [
		CommonModule,
		NgxDatatableModule,
		NgbTooltipModule,
		NgbDropdownModule,
		AppLibModule,
		TranslateModule,
		PipesModule,
	],
	exports: [ContasAReceberTableComponent],
})
export class ContasModule {}
