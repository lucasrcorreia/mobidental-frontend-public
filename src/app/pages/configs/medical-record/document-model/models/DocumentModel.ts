export interface DocumentModel {
	id?: number;
	nome?: string;
	texto?: string;
	dataCriacao?: string;
}
