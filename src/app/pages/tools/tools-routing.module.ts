import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'prosthetic',
    loadChildren: './prosthetic/prosthetic-tools.module#ProstheticToolsModule',
    data: {
      id: 14,
      children: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ToolsRoutingModule { }
