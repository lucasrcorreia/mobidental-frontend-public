import { Component, OnInit } from "@angular/core";
import { ProfessionalModel } from "../../../../core/models/config/professional/professional.model";
import { FormGroup } from "@angular/forms";
import { UtilsService } from "../../../../services/utils/utils.service";
import { FormProfessional } from "../../../../core/forms/professional";
import { ShortListItemDesc } from "../../../../core/models/forms/common/common.model";
import { animate, style, transition, trigger } from "@angular/animations";
import { CustomDatepickerI18n, I18n } from "../../../../shared/ngb-datepicker/datepicker-i18n";
import Swal from "sweetalert2";
import {
	NgbDateParserFormatter,
	NgbDatepickerI18n,
	NgbTabChangeEvent,
	NgbModal,
} from "@ng-bootstrap/ng-bootstrap";
import { NgbDateFRParserFormatter } from "../../../../shared/ngb-datepicker/ngb-date-fr-parser-formatter";
import { CONSTANTS } from "../../../../core/constants/constants";
import { ActivatedRoute, Router } from "@angular/router";
import {
	AGUARDANDO_ATIVACAO,
	ATIVO,
	INATIVO,
	PROFESSIONAL_STATUS_LIST,
} from "../professionals.component";
import { ComissionProcedureModalComponent } from "./comission-procedure/comission-procedure-modal.component";
import {
	ComissionProcedure,
	ProfessionalsApiService,
	SavedComissionProcedure,
} from "../../../../api/professionals.api.service";
import { AsyncTemplate } from "../../../../../app/lib/helpers/async-template";
import clone from "ramda/es/clone";
import { NotificationsService } from "angular2-notifications";

export interface ListProfessional extends ShortListItemDesc {
	isDentista: boolean;
}

const IMPOSSIBLE_ID = "novo";

export enum TipoComissao {
	VALOR_FIXO_PROCEDIMENTO = "VALOR_FIXO_PROCEDIMENTO",
	POR_PROCEDIMENTO = "POR_PROCEDIMENTO",
	PORCENTAGEM_FIXA = "PORCENTAGEM_FIXA",
}

export enum Tab {
	DETAILS = "details-tab",
	COMMISSION = "commission-tab",
}

@Component({
	selector: "app-form-professional",
	templateUrl: "./form-professional.component.html",
	styleUrls: [
		"./form-professional.component.scss",
		"./../../../../../assets/icon/icofont/css/icofont.scss",
	],
	animations: [
		trigger("fadeInOutTranslate", [
			transition(":enter", [
				style({ opacity: 0 }),
				animate("400ms ease-in-out", style({ opacity: 1 })),
			]),
			transition(":leave", [
				style({ transform: "translate(0)" }),
				animate("400ms ease-in-out", style({ opacity: 0 })),
			]),
		]),
	],
	providers: [
		I18n,
		{ provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n },
		{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
	],
})
export class FormProfessionalComponent implements OnInit {
	readonly Tab = Tab;
	readonly TipoComissao = TipoComissao;

	professional: ProfessionalModel;
	professionalStatusList = PROFESSIONAL_STATUS_LIST(this.service);
	aguardandoAtivacao = AGUARDANDO_ATIVACAO;
	ativo = ATIVO;
	inativo = INATIVO;

	formProfessional: FormGroup;
	listTypeProfessionals: Array<ListProfessional>;
	isDentist = false;
	fixedLunch = false;

	private _currentTab: Tab = Tab.DETAILS;
	professionalId: number | "novo";

	procedimentosComissao: SavedComissionProcedure[] = [];

	_comissao?: number;
	readonly percentageMask = {
		mask: Number,
		scale: 2,
		thousandsSeparator: ".",
		radix: ",",
		max: 100,
		padFractionalZeros: true,
		normalizeZeros: true,
	};

	weekAgenda = [
		{
			model: "profissionalSegunda",
			label: "Seg",
			checked: false,
		},
		{
			model: "profissionalTerca",
			label: "Ter",
			checked: false,
		},
		{
			model: "profissionalQuarta",
			label: "Qua",
			checked: false,
		},
		{
			model: "profissionalQuinta",
			label: "Qui",
			checked: false,
		},
		{
			model: "profissionalSexta",
			label: "Sex",
			checked: false,
		},
		{
			model: "profissionalSabado",
			label: "Sab",
			checked: false,
		},
		{
			model: "profissionalDomingo",
			label: "Dom",
			checked: false,
		},
	];

	listCommission = [
		{
			label: this.service.translate.instant("CONFIG.PROFESSIONAL.FORM.PORCENTAGEM_FIXA"),
			value: TipoComissao.PORCENTAGEM_FIXA,
		},
		{
			label: this.service.translate.instant("CONFIG.PROFESSIONAL.FORM.POR_PROCEDIMENTO"),
			value: TipoComissao.POR_PROCEDIMENTO,
		},
		{
			label: this.service.translate.instant("CONFIG.PROFESSIONAL.FORM.VALOR_FIXO_PROCEDIMENTO"),
			value: TipoComissao.VALOR_FIXO_PROCEDIMENTO,
		},
	];

	constructor(
		public service: UtilsService,
		private readonly _route: ActivatedRoute,
		private readonly _router: Router,
		private readonly _asyncTemplate: AsyncTemplate,
		private readonly _professionalsApiService: ProfessionalsApiService,
		private readonly _notificationsService: NotificationsService,
		private readonly _ngbModal: NgbModal
	) {}

	ngOnInit() {
		this._initProfessional();
		this._fetchComissionProcedures();
	}

	private async _initForm() {
		this.service.loading(true);

		this.formProfessional = this.service.createForm(FormProfessional, this.professional);
		(window as any).foo = this.formProfessional;

		this._comissao = Number(this.professional.profissionalPorcentagemComissao) || 0;

		this.formProfessional.patchValue({
			profissionalDescontaPorcentagemCartaoCreditoVista: this.professional
				? this.professional.profissionalDescontaPorcentagemCartaoCreditoVista
				: false,

			profissionalDescontaPorcentagemCartaoDebito: this.professional
				? this.professional.profissionalDescontaPorcentagemCartaoDebito
				: false,

			profissionalDescontaPorcentagemCartaoParcelado: this.professional
				? this.professional.profissionalDescontaPorcentagemCartaoParcelado
				: false,
		});

		this.weekAgenda.map((item) => {
			const { initial, final } = this.getDayOfWeekFormLabelFromItem(item);
			if (this.professional[initial]) {
				item.checked = true;
			} else {
				this._toggleDayOfWeekFormLabelFormControls(initial, final, false);
			}
		});

		if (this.professional.profissionalIntervaloAlmocoFinal) {
			this.fixedLunch = true;
		}

		if (this.professional.profissionalDataNascimento) {
			this.formProfessional
				.get("profissionalDataNascimento")
				.setValue(this.service.parseDatePicker(this.professional.profissionalDataNascimento));
		}

		if (this.professional.profissionalDescontaPorcentagemCartaoDebito === null) {
			this.formProfessional.get("profissionalDescontaPorcentagemCartaoDebito").setValue(false);
		}

		if (this.professional.profissionalDescontaPorcentagemCartaoCreditoVista === null) {
			this.formProfessional
				.get("profissionalDescontaPorcentagemCartaoCreditoVista")
				.setValue(false);
		}

		if (this.professional.profissionalDescontaPorcentagemCartaoParcelado === null) {
			this.formProfessional.get("profissionalDescontaPorcentagemCartaoParcelado").setValue(false);
		}

		this.service.httpGET(CONSTANTS.ENDPOINTS.config.professional.dropdownProfessionals).subscribe(
			(data) => {
				this.listTypeProfessionals = data.body as Array<ListProfessional>;
				this.service.loading(false);

				if (this.professional.perfilContaModuloId) {
					this.changeTypeProfessional();
				}
			},
			(err) => {
				this.service.loading(false);
				this.service.notification.error(
					this.service.translate.instant("COMMON.ERROR.SEARCH"),
					err.error.error
				);
			}
		);
	}

	private async _fetchComissionProcedures() {
		const { professionalId: id } = this;

		if (typeof id === "number") {
			const {
				content,
			} = await this._asyncTemplate.wrapUserFeedback(
				"Erro ao pesquisar comissão para esse professional",
				() => this._professionalsApiService.listProfessionalCommission(id)
			);

			this.procedimentosComissao = content;
		}
	}

	private async _initProfessional() {
		const professionalId = this._route.snapshot.params.id;
		if (professionalId === IMPOSSIBLE_ID) {
			this.service.httpGET(CONSTANTS.ENDPOINTS.config.professional.new).subscribe(
				(data) => {
					this.professional = data.body as ProfessionalModel;
					this._initForm();
					this._setEmStatusUsuarioPerfilContaModulo(this.ativo);
				},
				(err) => {
					this.service.notification.error(
						this.service.translate.instant("COMMON.ERROR.SEARCH"),
						err.error.error
					);
				}
			);
		} else {
			this.service
				.httpGET(CONSTANTS.ENDPOINTS.config.professional.findOne + professionalId)
				.subscribe(
					(data) => {
						this.professional = data.body as ProfessionalModel;
						this._initForm();
					},
					(err) => {
						this.service.notification.error(
							this.service.translate.instant("COMMON.ERROR.SEARCH"),
							err.error.error
						);
					}
				);
		}

		this.professionalId = Number(professionalId) || undefined;
	}

	changeTypeProfessional() {
		const id = this.formProfessional.get("perfilContaModuloId").value;
		this.isDentist = this.listTypeProfessionals.find((item) => item.id === id).isDentista;
	}

	async openModalForNewProcedure() {
		const profId = this.professionalId;

		if (typeof profId === "string") return;

		const modalRef = this._ngbModal.open(ComissionProcedureModalComponent, {
			size: "lg",
			centered: true,
		});

		const comp: ComissionProcedureModalComponent = modalRef.componentInstance;
		comp.proc.profissionalId = profId;

		const result: ComissionProcedure | undefined = await modalRef.result;
		if (!result) return;

		await this._asyncTemplate.wrapUserFeedback(
			"Erro ao salvar nova comissão de procedimentos",
			() => this._professionalsApiService.saveNewProfessionalComission(result)
		);

		try {
			await this._fetchComissionProcedures();
		} catch {}

		this._notificationsService.success("Comissão criada com sucesso!");
	}

	async editComissionProcedure(savedProc: SavedComissionProcedure) {
		const modalRef = this._ngbModal.open(ComissionProcedureModalComponent, {
			size: "lg",
			centered: true,
		});

		const comp: ComissionProcedureModalComponent = modalRef.componentInstance;
		comp.proc = clone(savedProc);

		const result: ComissionProcedure | undefined = await modalRef.result;
		if (!result || !result.id) return;

		await this._asyncTemplate.wrapUserFeedback("Erro ao alterar comissão de procedimentos", () =>
			this._professionalsApiService.saveNewProfessionalComission(result)
		);

		try {
			await this._fetchComissionProcedures();
		} catch {}

		this._notificationsService.success("Comissão editada com sucesso!");
	}

	async deleteComissionProcedure(savedProc: SavedComissionProcedure) {
		const { value } = await Swal.fire({
			title: "Apagar procedimento com comissão",
			text: "Deseja mesmo apagar essa comissão?",
			type: "question",
			showCancelButton: true,
			confirmButtonText: "Sim, apagar",
			cancelButtonText: "Não, cancelar",
		});

		if (!value) return;

		await this._asyncTemplate.wrapUserFeedback("Erro ao apagar comissão", () =>
			this._professionalsApiService.deleteProcedureComission(savedProc.id)
		);

		try {
			await this._fetchComissionProcedures();
		} catch {}

		this._notificationsService.success("Comissão apagada com sucesso!");
	}

	save() {
		this.service.loading(true);
		this.professional = this.formProfessional.value;

		if (!this.fixedLunch) {
			this.professional.profissionalIntervaloAlmocoFinal = null;
			this.professional.profissionalIntervaloAlmocoInicial = null;
		}

		if (
			this.professional.profissionalDescontaPorcentagemCartaoCreditoVista === null ||
			this.professional.profissionalDescontaPorcentagemCartaoCreditoVista === undefined
		) {
			this.professional.profissionalDescontaPorcentagemCartaoCreditoVista = false;
		}

		if (
			this.professional.profissionalDescontaPorcentagemCartaoDebito === null ||
			this.professional.profissionalDescontaPorcentagemCartaoDebito === undefined
		) {
			this.professional.profissionalDescontaPorcentagemCartaoDebito = false;
		}

		if (
			this.professional.profissionalDescontaPorcentagemCartaoParcelado === null ||
			this.professional.profissionalDescontaPorcentagemCartaoParcelado === undefined
		) {
			this.professional.profissionalDescontaPorcentagemCartaoParcelado = false;
		}

		if (this.formProfessional.get("profissionalDataNascimento").value) {
			this.professional.profissionalDataNascimento = this.service.parseDateFromDatePicker(
				this.professional.profissionalDataNascimento
			);
		}

		this.service
			.httpPOST(CONSTANTS.ENDPOINTS.config.professional.save, this.professional)
			.subscribe(
				(data) => {
					this.goBackToProfessionalsPage();
					this.service.loading(false);
					const professionalId = this._route.snapshot.params.id;
					const successMessage =
						professionalId === IMPOSSIBLE_ID ? "Profissional criado." : "Profissional editado.";
					this.service.notification.success(successMessage);
				},
				(err) => {
					this.service.loading(false);
					this.service.notification.error(
						this.service.translate.instant("COMMON.ERROR.SEARCH"),
						err.error.error
					);
				}
			);
	}

	goBackToProfessionalsPage() {
		this._router.navigate([".."], { relativeTo: this._route });
	}

	patchComissao(val: unknown) {
		const { formProfessional } = this;

		if (formProfessional) {
			formProfessional.patchValue({ profissionalPorcentagemComissao: val });
		}
	}

	wasUnchecked(item) {
		const { initial, final } = this.getDayOfWeekFormLabelFromItem(item);
		if (!item.checked) {
			this.formProfessional.patchValue({ [initial]: null, [final]: null });
		}
		this._toggleDayOfWeekFormLabelFormControls(initial, final, item.checked);
	}

	getDayOfWeekFormLabelFromItem(item) {
		const formLabel = item.model;
		const initial = formLabel + "Inicial";
		const final = formLabel + "Final";

		return { initial, final };
	}

	private _toggleDayOfWeekFormLabelFormControls(initial, final, checked = true) {
		if (!checked) {
			this.formProfessional.get(initial).disable();
			this.formProfessional.get(final).disable();
		} else {
			this.formProfessional.get(initial).enable();
			this.formProfessional.get(final).enable();
		}
	}

	get emStatusUsuarioPerfilContaModulo() {
		return this.formProfessional.get("emStatusUsuarioPerfilContaModulo").value;
	}

	private _setEmStatusUsuarioPerfilContaModulo(status: unknown) {
		this.formProfessional.patchValue({ emStatusUsuarioPerfilContaModulo: status });
	}

	onStatusChange($event: unknown) {
		if (!$event) {
			this._setEmStatusUsuarioPerfilContaModulo(this.inativo);
		} else {
			this._setEmStatusUsuarioPerfilContaModulo(this.ativo);
		}
	}

	get translatedStatus(): string {
		if (this.professionalStatusList) {
			const status = this.professionalStatusList.find(
				(status) => status.value == this.emStatusUsuarioPerfilContaModulo
			);

			return status ? status.label : "";
		}

		return "";
	}

	tabChanged(evt: NgbTabChangeEvent) {
		this._currentTab = evt.nextId as Tab;
	}

	get currentTab() {
		return this._currentTab;
	}
}
