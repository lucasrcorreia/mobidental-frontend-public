import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ShortListItem } from '../../../core/models/forms/common/common.model';
import { ProcedureDialogComponent } from './procedure-dialog.component';
import { ProceduresModel } from '../../../core/models/patients/patients.model';
import { Observable } from 'rxjs';

@Injectable({
                providedIn: 'root'
            })
export class ProcedureDialogService {

    constructor(private readonly _modalService: NgbModal) {
    }

    open(
        convenioId: number,
        professionalId: number,
        tratamentoId: number,
        profissionais: ShortListItem[]
    ): Observable<ProceduresModel[] | undefined> {
        const modalRef = this._modalService.open(ProcedureDialogComponent, { size: 'xl' as 'lg' });
        modalRef.componentInstance.convenioId = convenioId;
        modalRef.componentInstance.professionalId = professionalId;
        modalRef.componentInstance.tratamentoId = tratamentoId;
        modalRef.componentInstance.profissionais = profissionais;

        return modalRef.componentInstance.emitSave;
    }

}
