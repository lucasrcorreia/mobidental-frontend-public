import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";

export enum ReportPrintType {
	PDF,
	SHEETS,
}

@Component({
	selector: "app-report-header-actions",
	templateUrl: "./report-header-actions.component.html",
	styleUrls: ["./report-header-actions.component.scss"],
})
export class ReportHeaderActionsComponent implements OnInit {
	@Input() printEnable = true;
	@Input() searchEnable = true;
	@Output() searchTrigger: EventEmitter<undefined> = new EventEmitter<undefined>();
	@Output() printTrigger: EventEmitter<ReportPrintType> = new EventEmitter<ReportPrintType>();

	readonly reportPrintType = ReportPrintType;

	constructor() {}

	ngOnInit() {}
}
