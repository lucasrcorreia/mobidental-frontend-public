import { ContentPatientServicesTableModel } from "../patients/procedures/procedures.model";
import { ContentBillTreatmentModel } from "../patients/patients.model";

export interface ColumnsTableModel {
	name: string;
	prop: string;
	sortable: boolean;
	width: number;
	action: boolean;
	currency?: boolean;
	cellphone?: boolean;
	fixed?: boolean;
	grow?: number;
	comparator?: (a: any, b: any) => number;
}

export interface D3CollectionServicesInterface {
	key: string;
	values: ContentPatientServicesTableModel[];
}

export interface D3CollectionBillInterface {
	key: string;
	values?: ContentBillTreatmentModel[];
}
