import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProdutosRoutingModule } from './produtos-routing.module';
import { ProdutosListComponent } from './produtos-list/produtos-list.component';
import { ProdutosListTableComponent } from './produtos-list/produtos-list-table/produtos-list-table.component';
import { FormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ProdutosFormModalComponent } from './produtos-form-modal/produtos-form-modal.component';
import { SharedModule } from '../../shared/shared.module';
import { AppLibModule } from '../../lib/app-lib.module';
import { MovimentacoesProdutoComponent } from './movimentacoes-produto/movimentacoes-produto.component';
import { RouterModule } from '@angular/router';
import { ModalMovimentacaoComponent } from './modal-movimentacao/modal-movimentacao.component';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    ProdutosRoutingModule,
    RouterModule,
    CommonModule,
    FormsModule,
    SharedModule,
    AppLibModule,
    NgxDatatableModule,
    NgSelectModule,
  ],
  declarations: [
    ProdutosListComponent,
    ProdutosListTableComponent,
    ProdutosFormModalComponent,
    MovimentacoesProdutoComponent,
    ModalMovimentacaoComponent,
  ],
})
export class ProdutosModule {}
