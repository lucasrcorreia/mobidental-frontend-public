export class ComissionFinancial {
  data: string = null;
  id: number = null;
  profissionalId: number = null;
  profissionalNome: string = null;
  statusComissao: string = null;
  tratamentoId: number = null;
  tratamentoServicoPacientePessoaNome: string = null;
  tratamentoServicoConvenioEspecialidadeProcedimentoConvenioEspecialidadeConvenioNome: string = null;
  tratamentoServicoConvenioEspecialidadeProcedimentoProcedimentoNome: string = null;
  tratamentoServicoId: number = null;
  tratamentoServicoPorcentagemComissao: number = null;
  tratamentoServicoValorComDesconto: number = null;
  valor: number = null;
  valorDescontoCartao: number = null;
  valorPagar: number = null;
  checked = false;
}
