export interface CenterCostInterface {
  id: number;
  descricao: string;
  status: boolean;
  excluido: boolean;
  centroCustoPaiId: number;
  centroCustoChilds: Array<CenterCostInterface>;
}

export interface CategoryInterface {
  id: number;
  descricao: string;
  status: boolean;
  excluido: boolean;
  categoriaPaiId: number;
  categoriaChilds: Array<CategoryInterface>;
}
