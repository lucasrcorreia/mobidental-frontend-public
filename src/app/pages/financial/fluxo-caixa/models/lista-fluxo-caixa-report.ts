import { FormaPagamento } from '../../../../api/model/forma-pagamento';

export interface ListaFluxoCaixaReport {
	cliente?: string;
	data?: string;
	descricao?: string;
	formaDeRecebimento?: FormaPagamento;
	usuario?: string;
	valor?: number;
	tipoConta?: 'CONTA_RECEBER' | 'CONTA_PAGAR';
}
