import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FluxoCaixaFiltrosComponent } from './fluxo-caixa-filtros.component';

describe('FluxoCaixaFiltrosComponent', () => {
  let component: FluxoCaixaFiltrosComponent;
  let fixture: ComponentFixture<FluxoCaixaFiltrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FluxoCaixaFiltrosComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FluxoCaixaFiltrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
