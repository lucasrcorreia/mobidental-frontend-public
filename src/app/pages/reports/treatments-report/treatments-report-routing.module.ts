import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TreatmentsReportComponent } from './treatments-report.component';

const routes: Routes = [
	{
		path: '',
		component: TreatmentsReportComponent,
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class TreatmentsReportRoutingModule {}
